bl_info = {
    "name": "RenderMan RIB format",
    "author": "Jan Walter",
    "blender": (2, 5, 8),
    "api": 35622,
    "location": "File > Import-Export",
    "description": "Export a scene in RenderMan RIB file format",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Import-Export"}

if "bpy" in locals():
    import imp
    if "export_renderman_rib" in locals():
        imp.reload(export_renderman_rib)

import bpy
from bpy.props import BoolProperty, StringProperty, EnumProperty
from bpy_extras.io_utils import ExportHelper

class ExportRenderManRib(bpy.types.Operator, ExportHelper):
    bl_idname = "export_scene.rib"
    bl_label = "Export RenderMan RIB"
    bl_options = {'PRESET'}
    filename_ext = ".rib"
    filter_glob = StringProperty(default = "*.rib", options = {'HIDDEN'})

    # operator properties
    opt_selection = BoolProperty(name = "Selection Only", 
                                 description = "Export selected objects only", 
                                 default = False)
    opt_global_surface_shader = EnumProperty(items = [('none', 
                                                       'none', 
                                                       'none'),
                                                      ('show_ndoteye', 
                                                       'show_ndoteye', 
                                                       'show_ndoteye'),
                                                      ('show_st', 
                                                       'show_st', 
                                                       'show_st'),
                                                      ('show_xyz', 
                                                       'show_xyz',
                                                       'show_xyz'),
                                                      ('show_N', 
                                                       'show_N',
                                                       'show_N'),
                                                      ('show_grid', 
                                                       'show_grid',
                                                       'show_grid')],
                                             name = "Global surface",
                                             description = ("Replace surface "
                                                            + "shaders " +
                                                            "globally"),
                                             default = 'none')
    opt_render = BoolProperty(name = "Start renderer", 
                              description = "Start renderer after export",
                              default = True)

    def execute(self, context):
        keywords = self.as_keywords(ignore = (
                "check_existing", 
                "filter_glob"))
        from . import export_renderman_rib
        return export_renderman_rib.save(self, context, **keywords)

def menu_func(self, context):
    self.layout.operator(ExportRenderManRib.bl_idname, 
                         text = "RenderMan RIB (.rib)")

def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_export.append(menu_func)

def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_export.remove(menu_func)

if __name__ == "__main__":
    register()
