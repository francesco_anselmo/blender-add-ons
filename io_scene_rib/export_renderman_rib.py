"""
This script is an exporter to the RIB file format for RenderMan
compatible renderers.
"""
# for cut&paste:
# bpy.ops.export_scene.rib(filepath = "/usr/people/jan/tmp/rib/untitled.rib")

import os
import math
import time
import subprocess
# my own modules
import matrix
import pntvec
# Blender
import bpy
import mathutils

VERSION = 100 # will be divided by 100, e.g. 100 means 1.00

# TODO: Update this revision number before distributing a new version
# of this add-on !!!
REVISION = 252

class Options:
    def __init__(self,
                 selected = False,
                 global_surface_shader = 'none',
                 render = True):
        self.selected = selected
        self.global_surface_shader = global_surface_shader
        self.render = render

def time_stamp():
    return time.strftime('%a %b %d %H:%M:%S %Y', time.localtime())

def fix_name(name):
    new_name = name
    # " " -> "_"
    new_name = "_".join(new_name.split(" "))
    # "." -> "_"
    new_name = "_".join(new_name.split("."))
    return new_name

class RibExporter:
    def __init__(self, options):
        # options
        self.options = options
        # RIB file
        self.filename = None
        self.ribFile = None
        # camera related
        self.camera = None
        self.camera_obj = None
        # motion blur related
        self.use_motion_blur = False
        self.shutter_start = 0.0
        self.shutter_end = 0.0
        self.motion_blur_samples = 0
        self.animatedObjects = []
        self.animatedUniqueNames = []
        self.motionMatrices = {}
        # use BMesh?
        self.useBmesh = False
        version = bpy.app.version
        if version[0] >= 2 and version[1] >= 63:
            self.useBmesh = True

    def openRibFile(self, filename):
        self.filename = filename
        time_str = time_stamp()
        self.ribFile = open(filename, "w")
        self.ribFile.write("# RibExport Version %.2f (rev %s) - %s\n" %
                           (VERSION / 100.0, REVISION, time_str))
        self.ribFile.write("# exporter written by Jan Walter in 2011 - ")
        self.ribFile.write("http://www.janwalter.com\n")
        self.ribFile.write("# exported from Blender v%s\n" %
                           bpy.app.version_string)
        self.ribFile.write("# %s\n" % os.path.basename(bpy.data.filepath))
        self.ribFile.write("# %s\n\n" % os.path.basename(filename))

    def closeRibFile(self):
        self.ribFile.close()

    def isAnimated(self, obj):
        if obj.animation_data != None:
            return True
        elif obj.parent != None:
            return self.isAnimated(obj.parent)
        return False

    def isPrimitive(self, obj):
        isPrimitive = False
        primType = None
        if (self.nameStartsWith(obj.data.name, "Cone")):
            isPrimitive = True
            primType = "Cone"
        elif (self.nameStartsWith(obj.data.name, "Cylinder")):
            isPrimitive = True
            primType = "Cylinder"
        elif (self.nameStartsWith(obj.data.name, "Disk")):
            isPrimitive = True
            primType = "Disk"
        elif (self.nameStartsWith(obj.data.name, "Ring")):
            isPrimitive = True
            primType = "Ring"
        elif (self.nameStartsWith(obj.data.name, "Sphere")):
            isPrimitive = True
            primType = "Sphere"
#         elif (self.nameStartsWith(obj.data.name, "genbox")):
#             isPrimitive = True
#             primType = "genbox"
        return isPrimitive, primType

    def isUsingDuplication(self, obj):
        if obj.dupli_type != 'NONE':
            return True
        else:
            return False

    def isVisible(self, obj, scene):
        visible = False
        for i in range(20):
            if obj.layers[i]:
                if scene.layers[i]:
                    visible = True
        return visible

    def exportMaterial(self, obj):
        # assume a material is in the first slot
        if len(obj.material_slots) >= 1:
            mats = obj.material_slots.items()
            matName = mats[0][0]
            mat = bpy.data.materials[matName]
            if mat == None:
                print("WARNING: %s has no material" % obj.name)
            else:
                if mat.emit != 0.0:
                    # just use the diffuse color
                    diffuse_color = [1.0, 1.0, 1.0]
                    diffuse_color = mat.diffuse_color
                    self.ribFile.write("    Color [%s %s %s]\n" % 
                                       (diffuse_color[0],
                                        diffuse_color[1],
                                        diffuse_color[2]))
                    self.ribFile.write("    Surface \"constant\" ")
                elif (not mat.use_transparency):
                    architectural = self.getArchitecturalMaterial(obj)
                    name = architectural[0]
                    diffuse_scale = architectural[1]
                    diffuse_color = architectural[2]
                    diffuse_roughness = architectural[3]
                    reflectivity = architectural[4]
                    reflection_color = architectural[5]
                    environment_map = architectural[6]
                    if reflectivity != 0.0 or diffuse_roughness != 0.0:
                        # actually use the architectural shader
                        self.ribFile.write("    # %s\n" % name)
                        self.ribFile.write("    Declare \"diffuse_scale\" "
                                           "\"float\"\n")
                        self.ribFile.write("    Declare \"i_diffuse_color\" "
                                           "\"color\"\n")
                        self.ribFile.write("    Declare " +
                                           "\"i_diffuse_roughness\" "
                                           "\"float\"\n")
                        self.ribFile.write("    Declare \"reflectivity\" "
                                           "\"float\"\n")
                        self.ribFile.write("    Declare \"reflection_color\" "
                                           "\"color\"\n")
                        self.ribFile.write("    Declare \"environment_map\" "
                                           "\"string\"\n")
                        self.ribFile.write("    Color [%s %s %s]\n" %
                                           (diffuse_color[0],
                                            diffuse_color[1],
                                            diffuse_color[2]))
                        self.ribFile.write("    Surface \"architectural\" ")
                        self.ribFile.write("\"diffuse_scale\" [%s] " %
                                           diffuse_scale)
                        self.ribFile.write("\"i_diffuse_color\" [%s %s %s] " %
                                           (diffuse_color[0],
                                            diffuse_color[1],
                                            diffuse_color[2]))
                        self.ribFile.write("\"i_diffuse_roughness\" [%s] " %
                                           diffuse_roughness)
                        self.ribFile.write("\"reflectivity\" [%s] " %
                                           reflectivity)
                        self.ribFile.write("\"reflection_color\" [%s %s %s] " %
                                           (reflection_color[0],
                                            reflection_color[1],
                                            reflection_color[2]))
                        self.ribFile.write("\"environment_map\" [\"%s\"] " %
                                           environment_map)
                        self.ribFile.write("\n")
                    else:
                        # use plastic shader
                        plastic = self.getPlasticMaterial(obj)
                        name = plastic[0]
                        color = plastic[1]
                        Ks = plastic[2]
                        Kd = plastic[3]
                        Ka = plastic[4]
                        roughness = plastic[5]
                        specularcolor = plastic[6]
                        self.ribFile.write("    # %s\n" % name)
                        self.ribFile.write("    Declare \"Ks\" \"float\"\n")
                        self.ribFile.write("    Declare \"Kd\" \"float\"\n")
                        self.ribFile.write("    Declare \"Ka\" \"float\"\n")
                        self.ribFile.write("    Declare \"roughness\" " +
                                           "\"float\"\n")
                        self.ribFile.write("    Declare " +
                                           "\"specularcolor\" " +
                                           "\"color\"\n")
                        self.ribFile.write("    Color [%s %s %s]\n" %
                                           (color[0], color[1], color[2]))
                        self.ribFile.write("    Surface \"plastic\" ")
                        self.ribFile.write("\"Ks\" [%s] " % Ks)
                        self.ribFile.write("\"Kd\" [%s] " % Kd)
                        self.ribFile.write("\"Ka\" [%s] " % Ka)
                        self.ribFile.write("\"roughness\" [%s] " %
                                           roughness)
                        self.ribFile.write("\"specularcolor\" " +
                                           "[%s %s %s] " %
                                           (specularcolor[0],
                                            specularcolor[1],
                                            specularcolor[2]))
                        self.ribFile.write("\n")
                elif mat.use_transparency and mat.raytrace_mirror.use:
                    dielectric = self.getDielectricMaterial(obj)
                    name = dielectric[0]
                    refraction_color = dielectric[1]
                    refraction_ior = dielectric[2]
                    self.ribFile.write("    # %s\n" % name)
                    self.ribFile.write("    Declare \"diffuse_scale\" "
                                       "\"float\"\n")
                    self.ribFile.write("    Declare \"reflectivity\" "
                                       "\"float\"\n")
                    self.ribFile.write("    Declare \"reflection_color\" "
                                       "\"color\"\n")
                    self.ribFile.write("    Declare \"environment_map\" "
                                       "\"string\"\n")
                    self.ribFile.write("    Declare \"transparency\" "
                                       "\"float\"\n")
                    self.ribFile.write("    Declare \"refraction_color\" "
                                       "\"color\"\n")
                    self.ribFile.write("    Declare \"refraction_ior\" "
                                       "\"float\"\n")
                    self.ribFile.write("    Color [%s %s %s]\n" %
                                       (refraction_color[0],
                                        refraction_color[1],
                                        refraction_color[2]))
                    self.ribFile.write("    Surface \"architectural\" ")
                    self.ribFile.write("\"diffuse_scale\" [%s] " % 0.0)
                    self.ribFile.write("\"reflectivity\" [%s] " % 0.08)
                    self.ribFile.write("\"reflection_color\" [%s %s %s] " %
                                       (1.0, 1.0, 1.0))
                    self.ribFile.write("\"environment_map\" [\"%s\"] " %
                                       "raytrace")
                    self.ribFile.write("\"transparency\" [%s] " % 0.92)
                    self.ribFile.write("\"refraction_color\" [%s %s %s] " %
                                       (1.0, 1.0, 1.0))
                    self.ribFile.write("\"refraction_ior\" [%s] " %
                                       refraction_ior)
                    self.ribFile.write("\n")
                else:
                    # use plastic shader
                    plastic = self.getPlasticMaterial(obj)
                    name = plastic[0]
                    color = plastic[1]
                    Ks = plastic[2]
                    Kd = plastic[3]
                    Ka = plastic[4]
                    roughness = plastic[5]
                    specularcolor = plastic[6]
                    self.ribFile.write("    # %s\n" % name)
                    self.ribFile.write("    Declare \"Ks\" \"float\"\n")
                    self.ribFile.write("    Declare \"Kd\" \"float\"\n")
                    self.ribFile.write("    Declare \"Ka\" \"float\"\n")
                    self.ribFile.write("    Declare \"roughness\" " +
                                       "\"float\"\n")
                    self.ribFile.write("    Declare " +
                                       "\"specularcolor\" " +
                                       "\"color\"\n")
                    self.ribFile.write("    Color [%s %s %s]\n" %
                                       (color[0], color[1], color[2]))
                    self.ribFile.write("    Surface \"plastic\" ")
                    self.ribFile.write("\"Ks\" [%s] " % Ks)
                    self.ribFile.write("\"Kd\" [%s] " % Kd)
                    self.ribFile.write("\"Ka\" [%s] " % Ka)
                    self.ribFile.write("\"roughness\" [%s] " %
                                       roughness)
                    self.ribFile.write("\"specularcolor\" " +
                                       "[%s %s %s] " %
                                       (specularcolor[0],
                                        specularcolor[1],
                                        specularcolor[2]))
                    self.ribFile.write("\n")

    def exportMeshWithFaceNormals(self, data):
        if self.useBmesh:
            # delegate to new BMesh function
            return self.exportBMeshWithFaceNormals(data)
        self.ribFile.write("    # export mesh with face normals\n")
        self.ribFile.write("    # %s faces %s vertices\n" %
                           (len(data.faces), len(data.vertices)))
        self.ribFile.write("    PointsPolygons\n")
        # number of vertices per face
        self.ribFile.write("      [ ")
        for face in data.faces:
            self.ribFile.write("%s " % len(face.vertices))
        self.ribFile.write("]\n")
        # indices per triangle/quad
        self.ribFile.write("      [ ")
        for face in data.faces:
            for i in list(range(len(face.vertices))):
                self.ribFile.write("%s " % face.vertices[i])
        self.ribFile.write("]\n")
        # coordinates
        self.ribFile.write("      \"P\" [\n")
        for v in data.vertices:
            self.ribFile.write("      %s %s %s\n" %
                               (v.co[0], v.co[1], v.co[2]))
        self.ribFile.write("      ]\n")


    def exportBMeshWithFaceNormals(self, data):
        self.ribFile.write("    # export mesh with face normals\n")
        self.ribFile.write("    # %s polygons %s vertices\n" %
                           (len(data.polygons), len(data.vertices)))
        self.ribFile.write("    PointsPolygons\n")
        # number of vertices per polygon
        self.ribFile.write("      [ ")
        for polygon in data.polygons:
            self.ribFile.write("%s " % len(polygon.vertices))
        self.ribFile.write("]\n")
        # indices per triangle/quad
        self.ribFile.write("      [ ")
        for polygon in data.polygons:
            for i in list(range(len(polygon.vertices))):
                self.ribFile.write("%s " % polygon.vertices[i])
        self.ribFile.write("]\n")
        # coordinates
        self.ribFile.write("      \"P\" [\n")
        for v in data.vertices:
            self.ribFile.write("      %s %s %s\n" %
                               (v.co[0], v.co[1], v.co[2]))
        self.ribFile.write("      ]\n")

    def exportMeshWithFaceNormalsAndUvs(self, data, uv_layer):
        if self.useBmesh:
            # delegate to new BMesh function
            return self.exportBMeshWithFaceNormalsAndUvs(data, uv_layer)
        self.ribFile.write("    PointsPolygons\n")
        # number of vertices per face
        self.ribFile.write("      [ ")
        for fi in list(range(len(data.faces))): # face index
            face = data.faces[fi]
            self.ribFile.write("%s " % len(face.vertices))
        self.ribFile.write("]\n")
        # indices per triangle/quad
        counter = 0
        self.ribFile.write("      [ ")
        for fi in list(range(len(data.faces))): # face index
            face = data.faces[fi]
            for vi in list(range(len(face.vertices))):
                self.ribFile.write("%s " % counter)
                counter +=1
        self.ribFile.write("]\n")
        # coordinates
        self.ribFile.write("      \"P\" [\n")
        for fi in list(range(len(data.faces))): # face index
            face = data.faces[fi]
            for vi in face.vertices: # vertex index
                v = data.vertices[vi]
                self.ribFile.write("      %s %s %s\n" %
                                   (v.co[0], v.co[1], v.co[2]))
        self.ribFile.write("      ]\n")
        # face normal
        self.ribFile.write("      \"N\" [\n")
        for fi in list(range(len(data.faces))): # face index
            face = data.faces[fi]
            for vi in face.vertices: # vertex index
                v = data.vertices[vi]
                self.ribFile.write("      %s %s %s\n" % (face.normal[0],
                                                       face.normal[1],
                                                       face.normal[2]))
        self.ribFile.write("      ]\n")
        # uv-coordinates
        self.ribFile.write("      \"st\" [\n")
        for fi in list(range(len(data.faces))): # face index
            face = data.faces[fi]
            for vi in list(range(len(face.vertices))): # vertex index
                uv = uv_layer[fi].uv[vi]
                self.ribFile.write('      %s %s\n' % (uv[0], 1.0 - uv[1]))
        self.ribFile.write("      ]\n")

    def exportBMeshWithFaceNormalsAndUvs(self, data, uv_layer):
        self.ribFile.write("    PointsPolygons\n")
        # number of vertices per face
        self.ribFile.write("      [ ")
        for fi in list(range(len(data.polygons))): # face index
            face = data.polygons[fi]
            self.ribFile.write("%s " % len(face.vertices))
        self.ribFile.write("]\n")
        # indices per triangle/quad
        counter = 0
        self.ribFile.write("      [ ")
        for fi in list(range(len(data.polygons))): # face index
            face = data.polygons[fi]
            for vi in list(range(len(face.vertices))):
                self.ribFile.write("%s " % counter)
                counter +=1
        self.ribFile.write("]\n")
        # coordinates
        self.ribFile.write("      \"P\" [\n")
        for fi in list(range(len(data.polygons))): # face index
            face = data.polygons[fi]
            for vi in face.vertices: # vertex index
                v = data.vertices[vi]
                self.ribFile.write("      %s %s %s\n" %
                                   (v.co[0], v.co[1], v.co[2]))
        self.ribFile.write("      ]\n")
        # face normal
        self.ribFile.write("      \"N\" [\n")
        for fi in list(range(len(data.polygons))): # face index
            face = data.polygons[fi]
            for vi in face.vertices: # vertex index
                v = data.vertices[vi]
                self.ribFile.write("      %s %s %s\n" % (face.normal[0],
                                                         face.normal[1],
                                                         face.normal[2]))
        self.ribFile.write("      ]\n")
        # uv-coordinates
        self.ribFile.write("      \"st\" [\n")
        counter = 0
        for fi in list(range(len(data.polygons))): # face index
            face = data.polygons[fi]
            uv_layers = data.uv_layer_clone.data.values()
            for vi in list(range(len(face.vertices))): # vertex index
                uv = uv_layers[counter].uv
                self.ribFile.write('      %s %s\n' % (uv[0], 1.0 - uv[1]))
                counter += 1
        self.ribFile.write("      ]\n")

    def exportMeshWithVertexNormals(self, data):
        if self.useBmesh:
            # delegate to new BMesh function
            return self.exportBMeshWithVertexNormals(data)
        self.ribFile.write("    # export mesh with vertex normals\n")
        self.ribFile.write("    # %s faces %s vertices\n" %
                           (len(data.faces), len(data.vertices)))
        self.ribFile.write("    PointsPolygons\n")
        # number of vertices per face
        self.ribFile.write("      [ ")
        for face in data.faces:
            self.ribFile.write("%s " % len(face.vertices))
        self.ribFile.write("]\n")
        # indices per triangle/quad
        self.ribFile.write("      [ ")
        for face in data.faces:
            for i in list(range(len(face.vertices))):
                self.ribFile.write("%s " % face.vertices[i])
        self.ribFile.write("]\n")
        # coordinates
        self.ribFile.write("      \"P\" [\n")
        for v in data.vertices:
            self.ribFile.write("      %s %s %s\n" %
                               (v.co[0], v.co[1], v.co[2]))
        self.ribFile.write("      ]\n")
        # vertex normals
        self.ribFile.write("      \"N\" [\n")
        for v in data.vertices:
            self.ribFile.write("      %s %s %s\n" %
                               (v.normal[0], v.normal[1], v.normal[2]))
        self.ribFile.write("      ]\n")

    def exportBMeshWithVertexNormals(self, data):
        self.ribFile.write("    # export mesh with vertex normals\n")
        self.ribFile.write("    # %s faces %s vertices\n" %
                           (len(data.polygons), len(data.vertices)))
        self.ribFile.write("    PointsPolygons\n")
        # number of vertices per face
        self.ribFile.write("      [ ")
        for face in data.polygons:
            self.ribFile.write("%s " % len(face.vertices))
        self.ribFile.write("]\n")
        # indices per triangle/quad
        self.ribFile.write("      [ ")
        for face in data.polygons:
            for i in list(range(len(face.vertices))):
                self.ribFile.write("%s " % face.vertices[i])
        self.ribFile.write("]\n")
        # coordinates
        self.ribFile.write("      \"P\" [\n")
        for v in data.vertices:
            self.ribFile.write("      %s %s %s\n" %
                               (v.co[0], v.co[1], v.co[2]))
        self.ribFile.write("      ]\n")
        # vertex normals
        self.ribFile.write("      \"N\" [\n")
        for v in data.vertices:
            self.ribFile.write("      %s %s %s\n" %
                               (v.normal[0], v.normal[1], v.normal[2]))
        self.ribFile.write("      ]\n")

    def exportMeshWithFaceAndVertexNormals(self, data):
        if self.useBmesh:
            # delegate to new BMesh function
            return self.exportBMeshWithFaceAndVertexNormals(data)
        flat_vertices = {}
        smooth_vertices = {}
        flat_faces = []
        smooth_faces = []
        # initialize the dictionaries with an vertex index as key
        for vi in list(range(len(data.vertices))):
            flat_vertices[vi] = []
            smooth_vertices[vi] = []
        # split all faces into 'flat' or 'smooth'
        for face in data.faces:
            if face.use_smooth:
                smooth_faces.append(face.index)
                dictionary = smooth_vertices
            else:
                flat_faces.append(face.index)
                dictionary = flat_vertices
            for vi in face.vertices:
                dictionary[vi].append(face.index)
        # re-index the vertices
        flat_count = 0
        smooth_count = 0
        for vi in list(range(len(data.vertices))):
            if flat_vertices[vi] == []:
                flat_vertices[vi] = -1
            else:
                flat_vertices[vi] = flat_count
                flat_count += 1
            if smooth_vertices[vi] == []:
                smooth_vertices[vi] = -1
            else:
                smooth_vertices[vi] = smooth_count
                smooth_count += 1
        # flat part
        self.ribFile.write("    # export mesh with face normals\n")
        self.ribFile.write("    # %s faces %s vertices\n" %
                           (len(flat_faces), flat_count))
        self.ribFile.write("    PointsPolygons\n")
        self.ribFile.write("      [ ")
        for fi in flat_faces:
            face = data.faces[fi]
            self.ribFile.write("%s " % len(face.vertices))
        self.ribFile.write("]\n")
        self.ribFile.write("      [ ")
        for fi in flat_faces:
            face = data.faces[fi]
            for i in list(range(len(face.vertices))):
                self.ribFile.write("%s " % flat_vertices[face.vertices[i]])
        self.ribFile.write("]\n")
        self.ribFile.write("      \"P\" [\n")
        for vi in list(range(len(data.vertices))):
            if flat_vertices[vi] != -1:
                v = data.vertices[vi]
                self.ribFile.write("      %s %s %s\n" %
                                   (v.co[0], v.co[1], v.co[2]))
        self.ribFile.write("      ]\n")
        # smooth part
        self.ribFile.write("    # export mesh with vertex normals\n")
        self.ribFile.write("    # %s faces %s vertices\n" %
                           (len(smooth_faces), smooth_count))
        self.ribFile.write("    PointsPolygons\n")
        self.ribFile.write("      [ ")
        for fi in smooth_faces:
            face = data.faces[fi]
            self.ribFile.write("%s " % len(face.vertices))
        self.ribFile.write("]\n")
        self.ribFile.write("      [ ")
        for fi in smooth_faces:
            face = data.faces[fi]
            for i in list(range(len(face.vertices))):
                self.ribFile.write("%s " % smooth_vertices[face.vertices[i]])
        self.ribFile.write("]\n")
        self.ribFile.write("      \"P\" [\n")
        for vi in list(range(len(data.vertices))):
            if smooth_vertices[vi] != -1:
                v = data.vertices[vi]
                self.ribFile.write("      %s %s %s\n" %
                                   (v.co[0], v.co[1], v.co[2]))
        self.ribFile.write("      ]\n")
        self.ribFile.write("      \"N\" [\n")
        for vi in list(range(len(data.vertices))):
            if smooth_vertices[vi] != -1:
                v = data.vertices[vi]
                self.ribFile.write("      %s %s %s\n" %
                                   (v.normal[0], v.normal[1], v.normal[2]))
        self.ribFile.write("      ]\n")

    def exportBMeshWithFaceAndVertexNormals(self, data):
        flat_vertices = {}
        smooth_vertices = {}
        flat_faces = []
        smooth_faces = []
        # initialize the dictionaries with an vertex index as key
        for vi in list(range(len(data.vertices))):
            flat_vertices[vi] = []
            smooth_vertices[vi] = []
        # split all faces into 'flat' or 'smooth'
        for face in data.polygons:
            if face.use_smooth:
                smooth_faces.append(face.index)
                dictionary = smooth_vertices
            else:
                flat_faces.append(face.index)
                dictionary = flat_vertices
            for vi in face.vertices:
                dictionary[vi].append(face.index)
        # re-index the vertices
        flat_count = 0
        smooth_count = 0
        for vi in list(range(len(data.vertices))):
            if flat_vertices[vi] == []:
                flat_vertices[vi] = -1
            else:
                flat_vertices[vi] = flat_count
                flat_count += 1
            if smooth_vertices[vi] == []:
                smooth_vertices[vi] = -1
            else:
                smooth_vertices[vi] = smooth_count
                smooth_count += 1
        # flat part
        self.ribFile.write("    # export mesh with face normals\n")
        self.ribFile.write("    # %s faces %s vertices\n" %
                           (len(flat_faces), flat_count))
        self.ribFile.write("    PointsPolygons\n")
        self.ribFile.write("      [ ")
        for fi in flat_faces:
            face = data.polygons[fi]
            self.ribFile.write("%s " % len(face.vertices))
        self.ribFile.write("]\n")
        self.ribFile.write("      [ ")
        for fi in flat_faces:
            face = data.polygons[fi]
            for i in list(range(len(face.vertices))):
                self.ribFile.write("%s " % flat_vertices[face.vertices[i]])
        self.ribFile.write("]\n")
        self.ribFile.write("      \"P\" [\n")
        for vi in list(range(len(data.vertices))):
            if flat_vertices[vi] != -1:
                v = data.vertices[vi]
                self.ribFile.write("      %s %s %s\n" %
                                   (v.co[0], v.co[1], v.co[2]))
        self.ribFile.write("      ]\n")
        # smooth part
        self.ribFile.write("    # export mesh with vertex normals\n")
        self.ribFile.write("    # %s faces %s vertices\n" %
                           (len(smooth_faces), smooth_count))
        self.ribFile.write("    PointsPolygons\n")
        self.ribFile.write("      [ ")
        for fi in smooth_faces:
            face = data.polygons[fi]
            self.ribFile.write("%s " % len(face.vertices))
        self.ribFile.write("]\n")
        self.ribFile.write("      [ ")
        for fi in smooth_faces:
            face = data.polygons[fi]
            for i in list(range(len(face.vertices))):
                self.ribFile.write("%s " % smooth_vertices[face.vertices[i]])
        self.ribFile.write("]\n")
        self.ribFile.write("      \"P\" [\n")
        for vi in list(range(len(data.vertices))):
            if smooth_vertices[vi] != -1:
                v = data.vertices[vi]
                self.ribFile.write("      %s %s %s\n" %
                                   (v.co[0], v.co[1], v.co[2]))
        self.ribFile.write("      ]\n")
        self.ribFile.write("      \"N\" [\n")
        for vi in list(range(len(data.vertices))):
            if smooth_vertices[vi] != -1:
                v = data.vertices[vi]
                self.ribFile.write("      %s %s %s\n" %
                                   (v.normal[0], v.normal[1], v.normal[2]))
        self.ribFile.write("      ]\n")

    def exportNurbsSurface(self, data):
        worked = False
        for spline in data.splines:
            if (spline.order_u == 4 and spline.order_v == 4):
                self.ribFile.write('    Patch "bicubic" "Pw" [\n')
                for v in spline.points:
                    self.ribFile.write("      %s %s %s %s\n" %
                                       (v.co[0], v.co[1], v.co[2], v.co[3]))
                self.ribFile.write("    ]\n")
                worked = True
            else:
                return False
        return worked

    def exportObject(self, obj, scene, light_counter, use_raytrace,
                     group_matrix = None):
        if (self.isVisible(obj, scene) or group_matrix != None):
            if obj.type == 'EMPTY':
                m = obj.matrix_world.copy()
                if group_matrix != None:
                    m = group_matrix * m
                if obj.dupli_type == 'GROUP':
                    for child in obj.dupli_group.objects:
                        self.exportObject(child, scene, light_counter,
                                          use_raytrace, m)
            elif obj.type == 'SURFACE':
                # motion blur
                if self.use_motion_blur:
                    self.ribFile.write("  # %s\n" % obj.name)
                    self.ribFile.write("  Attribute " +
                                       "\"%s\" \"%s\" \"%s\"\n" %
                                       ("identifier", "name", obj.name))
                    self.ribFile.write("  AttributeBegin\n")
                    # many matrices
                    matrices = self.motionMatrices[obj.name]
                    self.ribFile.write("    MotionBegin [ ")
                    frame_step = 1.0 / float(self.motion_blur_samples - 1)
                    for i in range(self.motion_blur_samples):
                        subframe = self.shutter_end * frame_step * i
                        if i == 0:
                            self.ribFile.write("%s\n" % subframe)
                        elif i == (self.motion_blur_samples - 1):
                            self.ribFile.write("                  %s ]\n" % subframe)
                        else:
                            self.ribFile.write("                  %s\n" % subframe)
                    for midx in range(len(matrices)):
                        m = matrices[midx]
                        self.ribFile.write("      ConcatTransform [\n" +
                                           "        %s %s %s %s\n" %
                                           (m[0][0], m[1][0], m[2][0], m[3][0]) +
                                           "        %s %s %s %s\n" %
                                           (m[0][1], m[1][1], m[2][1], m[3][1]) +
                                           "        %s %s %s %s\n" %
                                           (m[0][2], m[1][2], m[2][2], m[3][2]) +
                                           "        %s %s %s %s\n      ]\n" %
                                           (m[0][3], m[1][3], m[2][3], m[3][3]))
                    self.ribFile.write("    MotionEnd\n")
                else:
                    self.ribFile.write("  # %s\n" % obj.name)
                    self.ribFile.write("  Attribute " +
                                       "\"%s\" \"%s\" \"%s\"\n" %
                                       ("identifier", "name", obj.name))
                    self.ribFile.write("  AttributeBegin\n")
                    # one matrix
                    m = obj.matrix_world.copy()
                    if group_matrix != None:
                        m = group_matrix * m
                    self.ribFile.write("    ConcatTransform [\n" +
                                       "      %s %s %s %s\n" %
                                       (m[0][0], m[1][0], m[2][0], m[3][0]) +
                                       "      %s %s %s %s\n" %
                                       (m[0][1], m[1][1], m[2][1], m[3][1]) +
                                       "      %s %s %s %s\n" %
                                       (m[0][2], m[1][2], m[2][2], m[3][2]) +
                                       "      %s %s %s %s\n    ]\n" %
                                       (m[0][3], m[1][3], m[2][3], m[3][3]))
                if self.options.global_surface_shader == 'show_xyz':
                    # bounding box
                    minX = maxX = obj.bound_box[0][0]
                    minY = maxY = obj.bound_box[0][1]
                    minZ = maxZ = obj.bound_box[0][2]
                    bb = obj.bound_box
                    for i in list(range(8)):
                        corner = bb[i]
                        if corner[0] < minX:
                            minX = corner[0]
                        if corner[1] < minY:
                            minY = corner[1]
                        if corner[2] < minZ:
                            minZ = corner[2]
                        if corner[0] > maxX:
                            maxX = corner[0]
                        if corner[1] > maxY:
                            maxY = corner[1]
                        if corner[2] > maxZ:
                            maxZ = corner[2]
                    # make sure the bound box isn't zero in one dimension
                    epsilon = 0.0001
                    if minX == maxX:
                        minX -= epsilon
                        maxX += epsilon
                    if minY == maxY:
                        minY -= epsilon
                        maxY += epsilon
                    if minZ == maxZ:
                        minZ -= epsilon
                        maxZ += epsilon
                    # declare parameter types and define surface shader
                    self.ribFile.write("    Declare \"xmin\" \"float\"\n")
                    self.ribFile.write("    Declare \"ymin\" \"float\"\n")
                    self.ribFile.write("    Declare \"zmin\" \"float\"\n")
                    self.ribFile.write("    Declare \"xmax\" \"float\"\n")
                    self.ribFile.write("    Declare \"ymax\" \"float\"\n")
                    self.ribFile.write("    Declare \"zmax\" \"float\"\n")
                    self.ribFile.write("    Surface \"show_xyz\"\n"
                                       "    \"xmin\" [%s]\n" % minX +
                                       "    \"ymin\" [%s]\n" % minY +
                                       "    \"zmin\" [%s]\n" % minZ +
                                       "    \"xmax\" [%s]\n" % maxX +
                                       "    \"ymax\" [%s]\n" % maxY +
                                       "    \"zmax\" [%s]\n" % maxZ)
                elif self.options.global_surface_shader == 'show_st':
                    self.ribFile.write("    Surface \"show_st\"\n")
                elif self.options.global_surface_shader == 'show_N':
                    self.ribFile.write("    Surface \"show_N\"\n")
                elif self.options.global_surface_shader == 'show_grid':
                    self.ribFile.write("    Surface \"show_grid\"\n")
                elif self.options.global_surface_shader == 'show_ndoteye':
                    # do nothing (will use the default surface shader)
                    pass
                else:
                    if light_counter > 0:
                        self.exportMaterial(obj)
                    else:
                        # assume a material is in the first slot
                        if len(obj.material_slots) >= 1:
                            mats = obj.material_slots.items()
                            matName = mats[0][0]
                            mat = bpy.data.materials[matName]
                            if mat != None:
                                # just use the diffuse color
                                diffuse_color = [1.0, 1.0, 1.0]
                                diffuse_color = mat.diffuse_color
                                if mat.emit != 0.0:
                                    self.ribFile.write("    Color " +
                                                       "[%s %s %s]\n" % 
                                                       (diffuse_color[0],
                                                        diffuse_color[1],
                                                        diffuse_color[2]))
                                    self.ribFile.write("    Surface " +
                                                       "\"constant\" ")
                                else:
                                    if mat.use_transparency:
                                        self.ribFile.write("    Opacity  " +
                                                           "[%s %s %s]\n" %
                                                           (mat.alpha,
                                                            mat.alpha,
                                                            mat.alpha))
                                    self.ribFile.write("    Color  " +
                                                       "[%s %s %s]\n" %
                                                       (diffuse_color[0],
                                                        diffuse_color[1],
                                                        diffuse_color[2]))
                                    self.ribFile.write("    Surface " +
                                                       "\"defaultsurface\"\n")
                isPrimitive, primType = self.isPrimitive(obj)
                if isPrimitive:
                    # primitives
                    if primType == "Sphere":
                        # unit sphere
                        self.ribFile.write("    Sphere 1 -1 1 360\n")
                    elif primType == "Cylinder":
                        self.ribFile.write("    Cylinder 1 0 1 360\n")
                    elif primType == "Cone":
                        p1 = obj.data.splines[0].points[0].co
                        p2 = obj.data.splines[0].points[8].co
                        self.ribFile.write("    Hyperboloid " +
                                           "%s %s %s %s %s %s 360\n" %
                                           (math.fabs(p1.y), 0.0, p1.z,
                                            math.fabs(p2.y), 0.0, p2.z))
                    elif primType == "Disk":
                        # unit disk
                        self.ribFile.write("    Disk 0 1 360\n")
                    elif primType == "Ring":
                        p1 = obj.data.splines[0].points[0].co
                        p2 = obj.data.splines[0].points[8].co
                        self.ribFile.write("    Hyperboloid " +
                                           "%s %s %s %s %s %s 360\n" %
                                           (math.fabs(p1.y), 0.0, p1.z,
                                            math.fabs(p2.y), 0.0, p2.z))
                    else:
                        print('TODO: primType = "%s"' % primType)
                        self.ribFile.write("    Sphere 1 -1 1 360\n")
                else:
                    data = obj.data
                    worked = self.exportNurbsSurface(data)
                    if not worked:
                        print("WARNING: exportNurbsSurface() failed")
                        # unit sphere
                        self.ribFile.write("    Sphere 1 -1 1 360\n")
                self.ribFile.write("  AttributeEnd\n")
            elif obj.type == 'MESH':
                if self.isUsingDuplication(obj):
                    if obj.dupli_type == 'VERTS':
                        # for now we expect exactly one child
                        if len(obj.children) != 1:
                            print("TODO: dupli_type('%s') with %s " %
                                  (obj.dupli_type, len(obj.children)) +
                                  "children")
                        else:
                            child = obj.children[0]
                            # for each vertex ...
                            for vidx in range(len(obj.data.vertices)):
                                v = obj.data.vertices[vidx]
                                pm = obj.matrix_world.copy()
                                pmi = obj.matrix_world.copy()
                                pmi.invert()
                                m = pm * pm.Translation(v.co) * pmi
                                self.exportObject(child, scene, light_counter,
                                                  use_raytrace, m)
                    else:
                        print("TODO: dupli_type = '%s'" % obj.dupli_type)
                    return
                m = obj.matrix_world.copy()
                if group_matrix != None:
                    m = group_matrix * m
                self.ribFile.write("  # %s\n" % obj.name)
                self.ribFile.write("  Attribute " +
                                   "\"%s\" \"%s\" \"%s\"\n" %
                                   ("identifier", "name", obj.name))
                self.ribFile.write("  AttributeBegin\n")
                self.ribFile.write("    ConcatTransform [\n" +
                                   "      %s %s %s %s\n" %
                                   (m[0][0], m[1][0], m[2][0], m[3][0]) +
                                   "      %s %s %s %s\n" %
                                   (m[0][1], m[1][1], m[2][1], m[3][1]) +
                                   "      %s %s %s %s\n" %
                                   (m[0][2], m[1][2], m[2][2], m[3][2]) +
                                   "      %s %s %s %s\n    ]\n" %
                                   (m[0][3], m[1][3], m[2][3], m[3][3]))
                data = obj.data

                if self.options.global_surface_shader == 'show_xyz':
                    # bounding box
                    minX = maxX = obj.bound_box[0][0]
                    minY = maxY = obj.bound_box[0][1]
                    minZ = maxZ = obj.bound_box[0][2]
                    bb = obj.bound_box
                    for i in list(range(8)):
                        corner = bb[i]
                        if corner[0] < minX:
                            minX = corner[0]
                        if corner[1] < minY:
                            minY = corner[1]
                        if corner[2] < minZ:
                            minZ = corner[2]
                        if corner[0] > maxX:
                            maxX = corner[0]
                        if corner[1] > maxY:
                            maxY = corner[1]
                        if corner[2] > maxZ:
                            maxZ = corner[2]
                    # make sure the bound box isn't zero in one dimension
                    epsilon = 0.0001
                    if minX == maxX:
                        minX -= epsilon
                        maxX += epsilon
                    if minY == maxY:
                        minY -= epsilon
                        maxY += epsilon
                    if minZ == maxZ:
                        minZ -= epsilon
                        maxZ += epsilon
                    # declare parameter types and define surface shader
                    self.ribFile.write("    Declare \"xmin\" \"float\"\n")
                    self.ribFile.write("    Declare \"ymin\" \"float\"\n")
                    self.ribFile.write("    Declare \"zmin\" \"float\"\n")
                    self.ribFile.write("    Declare \"xmax\" \"float\"\n")
                    self.ribFile.write("    Declare \"ymax\" \"float\"\n")
                    self.ribFile.write("    Declare \"zmax\" \"float\"\n")
                    self.ribFile.write("    Surface \"show_xyz\"\n"
                                       "    \"xmin\" [%s]\n" % minX +
                                       "    \"ymin\" [%s]\n" % minY +
                                       "    \"zmin\" [%s]\n" % minZ +
                                       "    \"xmax\" [%s]\n" % maxX +
                                       "    \"ymax\" [%s]\n" % maxY +
                                       "    \"zmax\" [%s]\n" % maxZ)
                elif self.options.global_surface_shader == 'show_st':
                    self.ribFile.write("    Surface \"show_st\"\n")
                elif self.options.global_surface_shader == 'show_N':
                    self.ribFile.write("    Surface \"show_N\"\n")
                elif self.options.global_surface_shader == 'show_grid':
                    self.ribFile.write("    Surface \"show_grid\"\n")
                elif self.options.global_surface_shader == 'show_ndoteye':
                    # do nothing (will use the default surface shader)
                    pass
                else:
                    if light_counter > 0:
                        self.exportMaterial(obj)
                    else:
                        # assume a material is in the first slot
                        if len(obj.material_slots) >= 1:
                            mats = obj.material_slots.items()
                            matName = mats[0][0]
                            mat = bpy.data.materials[matName]
                            if mat != None:
                                # just use the diffuse color
                                diffuse_color = [1.0, 1.0, 1.0]
                                diffuse_color = mat.diffuse_color
                                if mat.emit != 0.0:
                                    self.ribFile.write("    Color " +
                                                       "[%s %s %s]\n" % 
                                                       (diffuse_color[0],
                                                        diffuse_color[1],
                                                        diffuse_color[2]))
                                    self.ribFile.write("    Surface " +
                                                       "\"constant\" ")
                                else:
                                    if mat.use_transparency:
                                        self.ribFile.write("    Opacity  " +
                                                           "[%s %s %s]\n" %
                                                           (mat.alpha,
                                                            mat.alpha,
                                                            mat.alpha))
                                    self.ribFile.write("    Color  " +
                                                       "[%s %s %s]\n" %
                                                       (diffuse_color[0],
                                                        diffuse_color[1],
                                                        diffuse_color[2]))
                                    self.ribFile.write("    Surface " +
                                                       "\"defaultsurface\"\n")
                isPrimitive, primType = self.isPrimitive(obj)
                if isPrimitive:
                    # primitives
                    if primType == "Sphere":
                        # unit sphere
                        self.ribFile.write("    Sphere 1 -1 1 360\n")
                    elif primType == "Cylinder":
                        self.ribFile.write("    Cylinder 1 0 1 360\n")
                    elif primType == "Disk":
                        # unit disk
                        self.ribFile.write("    Disk 0 1 360\n")
                    else:
                        print('TODO: primType = "%s"' % primType)
                        self.ribFile.write("    Sphere 1 -1 1 360\n")
                else:
                    # first check how many faces are smooth
                    smooth_faces = 0
                    if self.useBmesh:
                        for polygon in data.polygons:
                            if polygon.use_smooth:
                                smooth_faces += 1
                    else:
                        for face in data.faces:
                            if face.use_smooth:
                                smooth_faces += 1
                    # then check for uv-coordinates
                    if (data.uv_textures.active and
                        data.uv_textures.active.data):
                        uv_layer = data.uv_textures.active.data
                    else:
                        uv_layer = None
                    if smooth_faces == 0:
                        # flat
                        if uv_layer:
                            self.exportMeshWithFaceNormalsAndUvs(data,
                                                                 uv_layer)
                        else:
                            self.exportMeshWithFaceNormals(data)
                    elif ((self.useBmesh and 
                           smooth_faces == len(data.polygons)) or
                          ((not self.useBmesh) and 
                           smooth_faces == len(data.faces))):
                        # smooth
                        self.exportMeshWithVertexNormals(data)
                    else:
                        # both
                        self.exportMeshWithFaceAndVertexNormals(data)
                self.ribFile.write("  AttributeEnd\n")
            else:
                print("INFO: ignore %s(%s)" % (obj.name, obj.type)) # TMP

    def exportAll(self):
        scene = bpy.context.scene
        # get access to active camera
        self.camera_obj = scene.camera
        try:
            self.camera = self.camera_obj.data
        except AttributeError:
            print("ERROR: %s" % "no camera found")
            return
        # scene specific data
        percent = scene.render.resolution_percentage / 100.0
        xresolution = int(scene.render.resolution_x * percent)
        yresolution = int(scene.render.resolution_y * percent)
        aspect = xresolution / float(yresolution)
        use_raytrace = scene.render.use_raytrace
        self.use_motion_blur = scene.render.use_motion_blur
        if self.use_motion_blur:
            # store for later
            self.shutter_start = 0.0
            self.shutter_end = scene.render.motion_blur_shutter
            self.motion_blur_samples = scene.render.motion_blur_samples
            # loop through all objects
            for obj in scene.objects:
                if self.isVisible(obj, scene) and self.isAnimated(obj):
                    uniqueName = obj.name
                    self.animatedObjects.append(obj)
                    self.animatedUniqueNames.append(uniqueName)
        # Exposure
        gamma = scene.view_settings.gamma
        exposure = scene.view_settings.exposure
        self.ribFile.write("Exposure %s %s\n" % (math.pow(2, exposure), gamma))
        # Display
        basename, ext = os.path.basename(self.filename).split(".")
        tiff_file = basename + ".tiff"
        self.ribFile.write("Display \"%s\" \"file\" \"rgba\"\n" % tiff_file)
        # Format
        self.ribFile.write("Format %s %s 1.0\n" % (xresolution, yresolution))
        # PixelSamples
        if scene.render.use_antialiasing:
            if scene.render.antialiasing_samples == '5':
                AA_samples = 5
            elif scene.render.antialiasing_samples == '8':
                AA_samples = 8
            elif scene.render.antialiasing_samples == '11':
                AA_samples = 11
            elif scene.render.antialiasing_samples == '16':
                AA_samples = 16
        else:
            AA_samples = 1
        self.ribFile.write("PixelSamples %s %s\n" % (AA_samples, AA_samples))
        # Shutter (motion blur)
        if self.use_motion_blur:
            self.ribFile.write("Shutter %s %s\n" % (self.shutter_start, self.shutter_end))
        # Projection
        if self.camera.type == "PERSP":
            fov = 2 * math.degrees(math.atan(16.0 /
                                             (aspect * self.camera.lens)))
            self.ribFile.write("Projection \"%s\" \"%s\" [ %s ] # lens %s\n" %
                               ("perspective", "fov", fov, self.camera.lens))
        else:
            self.ribFile.write("Projection \"%s\"\n" % "orthographic")
        useSun = False
        useSky = False
        try:
            sun_data = bpy.data.lamps['sun']
        except KeyError:
            try:
                sun_data = bpy.data.lamps['Sun']
            except KeyError:
                useSun = False
                useSky = False
            else:
                useSun = True
                useSky = sun_data.sky.use_sky
        else:
            useSun = True
            useSky = sun_data.sky.use_sky
        hasCompass = False
        try:
            compass = bpy.data.objects['compass']
        except KeyError:
            hasCompass = False
        else:
            hasCompass = True
        if useSun:
            try:
                sun = bpy.data.objects[sun_data.name]
            except KeyError:
                print("WARNING: No object called \"%s\" found" % sun_data.name)
            else:
                # has compass?
                if hasCompass:
                    if sun.parent == compass:
                        m = sun.matrix_local.copy()
                    else:
                        m = sun.matrix_world.copy()
                else:
                    m = sun.matrix_world.copy()
                if useSky:
                    # see export_arnold_ass.py for sundir calculation
                    sunPos, rot, scale = m.decompose()
                    obj = bpy.data.objects[sun_data.name]
                    constraint = obj.constraints[0]
                    if constraint.type == 'TRACK_TO':
                        compass = constraint.target
                        m = compass.matrix_world.copy()
                        compassPos, rot, scale = m.decompose()
                        sundir = [sunPos[0] - compassPos[0],
                                  sunPos[1] - compassPos[1],
                                  sunPos[2] - compassPos[2]]
                        sundirLen = math.sqrt((sundir[0] * sundir[0] +
                                               sundir[1] * sundir[1] +
                                               sundir[2] * sundir[2]))
                        sundir[0] = sundir[0] / sundirLen
                        sundir[1] = sundir[1] / sundirLen
                        sundir[2] = sundir[2] / sundirLen
                        self.ribFile.write("# physicalsky\n")
                        self.ribFile.write("Declare \"i_sun_direction\" ")
                        self.ribFile.write("\"vector\"\n")
                        self.ribFile.write("Declare \"i_y_is_up\" ")
                        self.ribFile.write("\"float\"\n")
                        self.ribFile.write("Imager \"physicalsky\" " +
                                           "\"i_sun_direction\" [%s %s %s] " %
                                           (sundir[0], sundir[1], sundir[2]) +
                                           "\"i_y_is_up\" [0]\n")
                        self.ribFile.write("# physicalsky\n")
        # camera
        self.ribFile.write("Rotate 180 0 1 0 # right handed\n")
        self.ribFile.write("Scale -1 1 1 # right handed\n")
        m = self.camera_obj.matrix_world.copy()
        m.invert()
        self.ribFile.write("ConcatTransform [\n" +
                           "  %s %s %s %s\n" %
                           (m[0][0], m[1][0], m[2][0], m[3][0]) +
                           "  %s %s %s %s\n" %
                           (m[0][1], m[1][1], m[2][1], m[3][1]) +
                           "  %s %s %s %s\n" %
                           (m[0][2], m[1][2], m[2][2], m[3][2]) +
                           "  %s %s %s %s\n  ]\n" %
                           (m[0][3], m[1][3], m[2][3], m[3][3]))
        # WorldBegin
        self.ribFile.write("WorldBegin\n")
        if use_raytrace:
            # turn on raytracing for reflections/refractions
            self.ribFile.write('  Attribute "visibility" "int specular" [1]\n')
        if scene.render.use_shadows:
            self.ribFile.write('  Attribute "visibility" "int transmission" ' +
                               '[1]\n')
            self.ribFile.write('  Attribute "light" "shadows" "on"\n')
        # add lights
        light_counter = 0
        if not (self.options.global_surface_shader in 
                ['show_st', 'show_xyz', 'show_N', 'show_grid']):
            for obj in scene.objects:
                if (self.isVisible(obj, scene)):
                    data = obj.data
                    if obj.type == 'LAMP':
                        if data.type == 'POINT':
                            light_counter += 1
                            m = obj.matrix_world.copy()
                            loc, rot, scale = m.decompose()
                            self.ribFile.write("  # %s\n" % obj.name)
                            self.ribFile.write("  Attribute " +
                                               "\"%s\" \"%s\" \"%s\"\n" %
                                               ("identifier", "name", obj.name))
                            self.ribFile.write("  LightSource \"pointlight\" " +
                                               "%s\n" % light_counter +
                                               "  \"point from\" [%s %s %s]\n" %
                                               (loc[0], loc[1], loc[2]) +
                                               "  \"float intensity\" %s\n" %
                                               data.energy +
                                               "  \"color lightcolor\" " +
                                               "[%s %s %s]\n" % (data.color[0],
                                                                 data.color[1],
                                                                 data.color[2]))
                            if data.falloff_type == 'INVERSE_SQUARE':
                                self.ribFile.write("  \"float decay\" 2\n")
                            elif data.falloff_type == 'INVERSE_LINEAR':
                                self.ribFile.write("  \"float decay\" 1\n")
                            else: # 'CONSTANT'
                                self.ribFile.write("  \"float decay\" 0\n")
                        elif data.type == 'SUN':
                            light_counter += 1
                            # has compass?
                            sun = obj
                            if hasCompass:
                                if sun.parent == compass:
                                    m = sun.matrix_local.copy()
                                else:
                                    m = sun.matrix_world.copy()
                            else:
                                m = sun.matrix_world.copy()
                            # calculate the 'to' location
                            mx = matrix.Matrix(4, 4)
                            for i in range(4):
                                for j in range(4):
                                    mx[i][j] = m[j][i]
                            t = matrix.Matrix(1, 4)
                            t.init(0.0)
                            t[0][2] = -1.0
                            t[0][3] = 1.0
                            lookAt = t * mx
                            lookAt[0][0] = lookAt[0][0] / lookAt[0][3]
                            lookAt[0][1] = lookAt[0][1] / lookAt[0][3]
                            lookAt[0][2] = lookAt[0][2] / lookAt[0][3]
                            loc, rot, scale = m.decompose()
                            self.ribFile.write("  # %s\n" % obj.name)
                            self.ribFile.write("  Attribute " +
                                               "\"%s\" \"%s\" \"%s\"\n" %
                                               ("identifier", "name", obj.name))
                            self.ribFile.write("  LightSource " +
                                               "\"distantlight\" " +
                                               "%s\n" % light_counter +
                                               "  \"point from\" [%s %s %s]\n" %
                                               (loc[0], loc[1], loc[2]) +
                                               "  \"point to\" [%s %s %s]\n" %
                                               (lookAt[0][0],
                                                lookAt[0][1],
                                                lookAt[0][2]) +
                                               "  \"float intensity\" %s\n" %
                                               data.energy +
                                               "  \"color lightcolor\" " +
                                               "[%s %s %s]\n" % (data.color[0],
                                                                 data.color[1],
                                                                 data.color[2]))
                        elif data.type == 'SPOT':
                            light_counter += 1
                            m = obj.matrix_world.copy()
                            # calculate the 'to' location
                            mx = matrix.Matrix(4, 4)
                            for i in range(4):
                                for j in range(4):
                                    mx[i][j] = m[j][i]
                            t = matrix.Matrix(1, 4)
                            t.init(0.0)
                            t[0][2] = -1.0
                            t[0][3] = 1.0
                            lookAt = t * mx
                            lookAt[0][0] = lookAt[0][0] / lookAt[0][3]
                            lookAt[0][1] = lookAt[0][1] / lookAt[0][3]
                            lookAt[0][2] = lookAt[0][2] / lookAt[0][3]
                            loc, rot, scale = m.decompose()
                            self.ribFile.write("  # %s\n" % obj.name)
                            self.ribFile.write("  Attribute " +
                                               "\"%s\" \"%s\" \"%s\"\n" %
                                               ("identifier", "name", obj.name))
                            self.ribFile.write("  LightSource " +
                                               "\"spotlight\" " +
                                               "%s\n" % light_counter +
                                               "  \"point from\" [%s %s %s]\n" %
                                               (loc[0], loc[1], loc[2]) +
                                               "  \"point to\" [%s %s %s]\n" %
                                               (lookAt[0][0],
                                                lookAt[0][1],
                                                lookAt[0][2]) +
                                               "  \"float coneangle\" %s\n" %
                                               (data.spot_size / 2.0) +
                                               "  \"float conedeltaangle\" " +
                                               "%s\n" % 0.0 +
                                               "  \"float intensity\" %s\n" %
                                               data.energy +
                                               "  \"color lightcolor\" " +
                                               "[%s %s %s]\n" % (data.color[0],
                                                                 data.color[1],
                                                                 data.color[2]))
                            if data.falloff_type == 'INVERSE_SQUARE':
                                self.ribFile.write("  \"float decay\" 2\n")
                            elif data.falloff_type == 'INVERSE_LINEAR':
                                self.ribFile.write("  \"float decay\" 1\n")
                            else: # 'CONSTANT'
                                self.ribFile.write("  \"float decay\" 0\n")
        if light_counter and not (self.options.global_surface_shader in 
                                  ['show_st',
                                   'show_xyz',
                                   'show_N',
                                   'show_grid']):
            # default surface shader
            self.ribFile.write("  Surface \"plastic\"\n")
        # motion blur
        if self.use_motion_blur:
            # collect matrices for all animated objects
            frame_current = scene.frame_current
            frame_step = 1.0 / float(self.motion_blur_samples - 1)
            for i in range(self.motion_blur_samples):
                subframe = self.shutter_end * frame_step * i
                scene.frame_set(frame_current, subframe)
                for objIdx in range(len(self.animatedObjects)):
                    obj = self.animatedObjects[objIdx]
                    # collect matrices (per object)
                    if i == 0:
                        self.motionMatrices[obj.name] = []
                    m = obj.matrix_world.copy()
                    self.motionMatrices[obj.name].append(m)
            # reset frame
            scene.frame_set(frame_current, 0.0)
        # loop through all objects
        for obj in scene.objects:
            self.exportObject(obj, scene, light_counter, use_raytrace)
        # WorldEnd
        self.ribFile.write("WorldEnd\n")

    def exportSelected(self):
        print("TODO: RibExporter.exportSelected(self)")

    def getArchitecturalMaterial(self, obj):
        # default values
        name = "DefaultArchitectural"
        diffuse_scale = 0.8
        diffuse_color = [1.0, 1.0, 1.0]
        diffuse_roughness = 0.5
        reflectivity = 0.5
        reflection_color = [1.0, 1.0, 1.0]
        environment_map = "raytrace"
        # assume a material is in the first slot
        if len(obj.material_slots) >= 1:
            mats = obj.material_slots.items()
            matName = mats[0][0]
            mat = bpy.data.materials[matName]
            if mat == None:
                print("WARNING: %s has no material" % obj.name)
            else:
                name = mat.name
                diffuse_scale = mat.diffuse_intensity
                diffuse_color = mat.diffuse_color
                if mat.diffuse_shader == 'OREN_NAYAR':
                    diffuse_roughness = mat.roughness
                else:
                    diffuse_roughness = 0.0
                if mat.raytrace_mirror.use:
                    # use mirror settings
                    reflectivity = mat.raytrace_mirror.reflect_factor
                    reflection_color = mat.mirror_color
                    environment_map = "raytrace"
                else:
                    # use specular settings
                    reflectivity = mat.specular_intensity
                    reflection_color = mat.specular_color
                    environment_map = ""
        # return values in a list
        architectural = [name,
                         diffuse_scale, diffuse_color, diffuse_roughness,
                         reflectivity, reflection_color, environment_map]
        return architectural

    def getDielectricMaterial(self, obj):
        # default values
        name = "DefaultDielectric"
        refraction_color = [1.0, 1.0, 1.0]
        refraction_ior = 1.52
        # assume a material is in the first slot
        if len(obj.material_slots) >= 1:
            mats = obj.material_slots.items()
            matName = mats[0][0]
            mat = bpy.data.materials[matName]
            if mat == None:
                print("WARNING: %s has no material" % obj.name)
            else:
                name = mat.name
                refraction_color = mat.diffuse_color # convention !!!
                refraction_ior = mat.raytrace_transparency.ior
        # return values in a list
        dielectric = [name, refraction_color, refraction_ior]
        return dielectric

    def getPlasticMaterial(self, obj):
        # default values
        name = "DefaultPlastic"
        color = [1.0, 1.0, 1.0]
        Ks = 0.5
        Kd = 0.5
        Ka = 1.0
        roughness = 0.1
        specularcolor = [1.0, 1.0, 1.0]
        # assume a material is in the first slot
        if len(obj.material_slots) >= 1:
            mats = obj.material_slots.items()
            matName = mats[0][0]
            mat = bpy.data.materials[matName]
            if mat == None:
                print("WARNING: %s has no material" % obj.name)
            else:
                name = mat.name
                Ka = mat.ambient
                color = mat.diffuse_color
                Kd = mat.diffuse_intensity
                specularcolor = mat.specular_color
                Ks = mat.specular_intensity
                # roughness? ... * Ks*specular(Nf,V,roughness)
                roughness = 0.125 # TODO: find a relationship !!!
        # return values in a list
        plastic = [name, color, Ks, Kd, Ka, roughness, specularcolor]
        return plastic

    def getMirrorMaterial(self, obj):
        # default values
        name = "DefaultMirror"
        reflection_color = [1.0, 1.0, 1.0]
        reflectivity = 1.0
        # assume a material is in the first slot
        if len(obj.material_slots) >= 1:
            mats = obj.material_slots.items()
            matName = mats[0][0]
            mat = bpy.data.materials[matName]
            if mat == None:
                print("WARNING: %s has no material" % obj.name)
            else:
                name = mat.name
                reflection_color = mat.mirror_color
                reflectivity = mat.raytrace_mirror.reflect_factor
        # return values in a list
        mirror = [name, reflection_color, reflectivity]
        return mirror

    def nameStartsWith(self, name, pattern):
        retBool = False
        if len(name) >= len(pattern):
            words = name.split(pattern)
            if words[0] == "":
                retBool = True
        return retBool

    def writer(self, filename):
        # call methods similar to the Maya version of the exporter
        self.openRibFile(filename)
        if self.options.selected:
            self.exportSelected()
        else:
            self.exportAll()
        self.closeRibFile()
        if self.options.render:
            renderer_args = ["renderdl", '-d', filename]
            print("INFO: starting 3Delight: %s" % renderer_args)
            renderer_proc = subprocess.Popen(renderer_args)
            renderer_pid = renderer_proc.pid


def save(operator, context, filepath = "",
         # user interface
         opt_selection = False,
         opt_global_surface_shader = 'none',
         opt_render = True):
    if bpy.ops.object.mode_set.poll():
        bpy.ops.object.mode_set(mode = 'OBJECT')
    # print a usage message
    print("=" * 79)
    print("bpy.ops.export_scene.rib(filepath = \"%s\", " %
          "\\\\".join(filepath.split('\\')) +
          "opt_selection = %s, opt_global_surface_shader = '%s', " %
          (opt_selection, opt_global_surface_shader) +
          "opt_render = %s)" % opt_render)
    # delegate work to classes
    exporter = RibExporter(Options(selected = opt_selection,
                                   global_surface_shader = 
                                   opt_global_surface_shader,
                                   render = opt_render))
    exporter.writer(filepath)
    return {'FINISHED'}
