bl_info = {
    "name": "Radiance RAD format",
    "author": "Jan Walter",
    "blender": (2, 5, 8),
    "api": 35622,
    "location": "File > Import-Export",
    "description": "Import a scene in Radiance's RAD file format",
    "warning": "",
    "wiki_url": "",
    "tracker_url": "",
    "category": "Import-Export"}

if "bpy" in locals():
    import imp
    if "import_radiance_rad" in locals():
        imp.reload(import_radiance_rad)

import bpy
from bpy.props import BoolProperty, StringProperty, EnumProperty
from bpy_extras.io_utils import ImportHelper

class ImportRadianceRad(bpy.types.Operator, ImportHelper):
    bl_idname = "import_scene.rad"
    bl_label = "Import Radiance RAD"
    bl_options = {'PRESET'}
    filename_ext = ".rad"
    filter_glob = StringProperty(default = "*.rad", options = {'HIDDEN'})

    def execute(self, context):
        keywords = self.as_keywords(ignore = (
                "check_existing", 
                "filter_glob"))
        from . import import_radiance_rad
        return import_radiance_rad.load(self, context, **keywords)

def menu_func(self, context):
    self.layout.operator(ImportRadianceRad.bl_idname, 
                         text = "Radiance RAD (.rad)")

def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_import.append(menu_func)

def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_import.remove(menu_func)

if __name__ == "__main__":
    register()
