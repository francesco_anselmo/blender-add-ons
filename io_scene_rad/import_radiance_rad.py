"""
This script imports a Radiance RAD file into Blender.
"""
# for cut&paste:
# bpy.ops.import_scene.rad(filepath = "/usr/people/jan/git/blender-add-ons/test_scenes/radiance/scene0/room.rad")

import os
# Blender
import bpy
# my own modules
import rad2py
import py2bpy

def load(operator, context, filepath = ""):
    print("#" * 79)
    # state
    state = rad2py.State()
    iFilename = filepath
    dir, base = os.path.split(iFilename)
    state.directory = dir
    state.filename, state.ext = os.path.splitext(base)
    iFile = open(iFilename, "r")
    rad2py.test(iFile, state)
    iFile.close()
    # make sure there are no double surfaces
    doubleCheck = []
    surfaces = []
    for surface in state.surfaces:
        reprStr = "%s" % surface
        if reprStr in doubleCheck:
            print("WARNING: removing '%s' from state.surfaces" % surface)
        else:
            surfaces.append(surface)
        doubleCheck.append(reprStr)
    # replace state.surfaces by new list !!!
    state.surfaces = surfaces
    # continue as usual
    py2bpy.doIt(state.surfaces,
                state.materials,
                state.commands,
                state.includes,
                state.camera)
    return {'FINISHED'}
