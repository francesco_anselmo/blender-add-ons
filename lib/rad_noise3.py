# see noise3.c of Radiance source code

def hpoly1(t):
    return (2.0*t-3.0)*t*t+1.0

def hpoly2(t):
    return (-2.0*t+3.0)*t*t

def hpoly3(t):
    return ((t-2.0)*t+1.0)*t

def hpoly4(t):
    return (t-1.0)*t*t

def hermite(p0, p1, r0, r1, t):
    return (float(p0)*hpoly1(float(t)) +
            float(p1)*hpoly2(float(t)) +
            float(r0)*hpoly3(float(t)) +
            float(r1)*hpoly4(float(t)))
