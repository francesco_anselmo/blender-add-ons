# (c) Jan Walter 2015

# Blender
import bpy
import mathutils

def idx_to_co(idx, width):
    r = int(idx / width)
    c = idx % width
    return c, r

def px_list_to_dict(px_list, width):
    px_dict = {}
    for idx, px in enumerate(px_list):
        px_dict[idx_to_co(idx, width)] = px
    return px_dict

name = "renderman"
img_rgba = bpy.data.images["%s.exr" % name]
img_xyza = bpy.data.images["%s_aov_P.exr" % name]
if (img_rgba.size[0] == img_xyza.size[0] and
    img_rgba.size[1] == img_xyza.size[1]):
    img_width  = img_rgba.size[0]
    img_height = img_rgba.size[1]
    pxs_rgba = tuple(img_rgba.pixels)
    pxs_xyza = tuple(img_xyza.pixels)
    r, g, b, a1 = pxs_rgba[::4], pxs_rgba[1::4], pxs_rgba[2::4], pxs_rgba[3::4]
    x, y, z, a2 = pxs_xyza[::4], pxs_xyza[1::4], pxs_xyza[2::4], pxs_xyza[3::4]
    px_list_rgba = zip(r, g, b, a1)
    px_list_xyza = zip(x, y, z, a2)
    px_dict_rgba = px_list_to_dict(px_list_rgba, img_width)
    px_dict_xyza = px_list_to_dict(px_list_xyza, img_width)
    verts = []
    edges = []
    faces = []
    for ix in range(img_width):
        for iy in range(img_height):
            r, g, b, a1 = px_dict_rgba[(ix, iy)]
            x, y, z, a2 = px_dict_xyza[(ix, iy)]
            if a1 == 1.0:
                verts.append(mathutils.Vector((x, y, z)))
    mesh_data = bpy.data.meshes.new(name)
    mesh_data.from_pydata(verts, edges, faces)
    mesh = bpy.data.objects.new(name, object_data=mesh_data)
    mesh_data.update()
    mesh.update_tag()
    bpy.context.scene.objects.link(mesh)
    bpy.context.scene.update_tag()
    bpy.data.objects[name].matrix_world = bpy.data.objects['Camera'].matrix_world
