#!/usr/bin/env python

# (c) Jan Walter 1997

# CVS
# $Author: jan $
# $Date: 2007-10-21 23:39:32 -0700 (Sun, 21 Oct 2007) $
# $Revision: 39 $

import math

class Point:
    def __init__(self, x, *rest):
        self.coords = [x]
        for coord in rest:
            self.coords.append(coord)

    def __cmp__(self, point):
        if len(self.coords) == len(point.coords):
            compare = cmp(self.coords[0], point.coords[0])
            for i in range(1, len(self.coords)):
                cmpTmp = cmp(self.coords[i], point.coords[i])
                if compare != cmpTmp:
                    if compare:
                        return compare
                    else:
                        return cmpTmp
            return compare
        else:
            print("%s and %s have not same length!!!" % (self, point))
            return None
    
    def __getitem__(self, index):
        return self.coords[index]

    def __setitem__(self, index, value):
        self.coords[index] = value

    def __add__(self, point):
        if len(self.coords) == len(point.coords):
            newPoint = self.copy()
            for i in range(len(self.coords)):
                newPoint[i] = newPoint[i] + point[i]
            return newPoint
        else:
            print("%s and %s have not same length!!!" % (self, point))
            return None

    def __sub__(self, point):
        if len(self.coords) == len(point.coords):
            newPoint = self.copy()
            for i in range(len(self.coords)):
                newPoint[i] = newPoint[i] - point[i]
            return newPoint
        else:
            print("%s and %s have not same length!!!" % (self, point))
            return None

    def __mul__(self, factor):
        newPoint = self.copy()
        for i in range(len(self.coords)):
            newPoint[i] = newPoint[i] * factor
        return newPoint

    __rmul__ = __mul__

    def __div__(self, fract):
        return self.__mul__(1.0 / fract)

    def __len__(self):
        return len(self.coords)

    def __repr__(self):
        string = "Point("
        length = len(self.coords)
        for index in range(length):
            if index == length - 1:
                string = string + "%s)" % self.coords[index]
            else:

                string = string + "%s, " % self.coords[index]
        return string

    def copy(self):
        newPoint = Point(self[0], self[1])
        for i in range(2, len(self.coords)):
            newPoint.coords.append(self[i])
        return newPoint

class NullPoint(Point):
    def __init__(self, dimension):
        args = [self]
        for i in range(dimension):
            args.append(0)
        Point.__init__(*tuple(args))

class Vector(Point):
    def __init__(self, arg, *rest):
        if arg.__class__ == Point:
            self.coords = arg.coords[:]
        else:
            args = [self, arg]
            for i in range(len(rest)):
                args.append(rest[i])
            Point.__init__(*tuple(args))

    def __neg__(self):
        newVector = self.copy()
        length = len(self.coords)
        for index in range(length):
            newVector[index] = -self.coords[index]
        return newVector

    def __add__(self, vector):
        if self.__class__ != vector.__class__:
            raise TypeError("%s + %s" % (self, vector))
        else:
            return Point.__add__(self, vector)

    def __sub__(self, vector):
        if self.__class__ != vector.__class__:
            raise TypeError("%s - %s" % (self, vector))
        else:
            return Point.__sub__(self, vector)

    def __mul__(self, factor):
        if self.__class__ == factor.__class__:
            if len(self.coords) == len(factor.coords):
                result = self[0] * factor[0]
                for i in range(1, len(self.coords)):
                    result = result + self[i] * factor[i]
                return result
            else:
                print("%s and %s have not same length!!!" % (self, factor))
                return None
        else:
            return Point.__mul__(self, factor)

    def __repr__(self):
        string = "Vector("
        length = len(self.coords)
        for index in range(length):
            if index == length - 1:
                string = string + "%s)" % self.coords[index]
            else:

                string = string + "%s, " % self.coords[index]
        return string

    def angleBetween(self, vector):
        vector1 = self
        vector1.normalize()
        vector2 = vector
        vector2.normalize()
        scalar = vector1 * vector2
        cross  = vector1.cross(vector2)
        sin = cross.length()
        cos = scalar
        if sin != 0.0:
            axis = cross * (1.0 / sin)
        else:
            if cos == -1:
                if vector1[0] != vector1[1] != 0:
                    return (180.0, Vector(vector1[1], vector1[0], vector1[2]))
                elif vector1[0] != vector1[2] != 0:
                    return (180.0, Vector(vector1[2], vector1[1], vector1[0]))
                else:
                    return (180.0, Vector(vector1[0], vector1[2], vector1[1]))
            else:
                return (0.0, Vector(1.0, 0.0, 0.0))
        if cos >= 0:
            if sin > 1:
                sin = 1.0
            return (math.asin(sin) / math.pi * 180, axis)
        if cos <  0:
            return (math.acos(cos) / math.pi * 180, axis)

    def copy(self):
        newVector = Vector(self[0], self[1])
        for i in range(2, len(self.coords)):
            newVector.coords.append(self[i])
        return newVector

    def cross(self, vector):
        if len(self) == len(vector) == 3:
            x, y, z = 0, 1, 2
            a, b = self, vector
            return Vector(a[y]*b[z] - a[z]*b[y],
                          a[z]*b[x] - a[x]*b[z],
                          a[x]*b[y] - a[y]*b[x])
        else:
            print("%s and %s should have a length of 3!!!" % (self, vector))

    def length(self):
        return math.sqrt(self * self)

    def normalize(self):
        myLength = self.length()
        for i in range(len(self.coords)):
            self.coords[i] /= myLength

class NullVector(Vector, NullPoint):
    __init__ = NullPoint.__init__

def testPntvec():
    point1 = Point(1, 2, 3, 4)
    point2 = Point(4, 3, 2, 1)
    point3 = Point(1, 1.1, 1.2, 1.3)
    vector1 = Vector(1, 2, 3, 4)
    vector2 = Vector(4, 3, 2, 1)
    vector3 = Vector(1, 1.1, 1.2, 1.3)
    print("point1 =", point1)
    print("point2 =", point2)
    print("point3 =", point3)
    print("vector1 =", vector1)
    print("vector2 =", vector2)
    print("vector3 =", vector3)
    # NullPoint(dimension) = Point(0,..., 0)
    nullPoint = NullPoint(4)
    print("nullPoint  =", nullPoint)
    nullVector = NullVector(4)
    print("nullVector =", nullVector)
    print("%s + %s = %s" % (point1, point2, point1 + point2))
    print("%s + %s = %s" % (point1, point3, point1 + point3))
    print("%s + %s = %s" % (vector1, vector2, vector1 + vector2))
    print("%s + %s = %s" % (vector1, vector3, vector1 + vector3))
    print("%s + %s = %s" % (point1, vector3, point1 + vector3))
    print("%s + %s = %s" % (vector1, point3, vector1 + point3))
    print("%s - %s = %s" % (point1, point2, point1 - point2))
    print("%s - %s = %s" % (point1, point3, point1 - point3))
    print("%s - %s = %s" % (point1, point2, Vector(point1 - point2)))
    print("%s - %s = %s" % (point1, point3, Vector(point1 - point3)))
    print("%s - %s = %s" % (vector1, vector2, vector1 - vector2))
    print("%s - %s = %s" % (vector1, vector3, vector1 - vector3))
    print("%s - %s = %s" % (point1, vector3, point1 - vector3))
    print("%s - %s = %s" % (vector1, point3, vector1 - point3))
    print("%s * %s = %s" % (point3, 5, point3 * 5))
    print("%s * %s = %s" % (5, point3, 5 * point3))
    print("%s * %s = %s" % (point1, point1, point1 * point1))
    print("%s * %s = %s" % (point1, point3, point1 * point3))
    print("%s * %s = %s" % (vector3, 5, vector3 * 5))
    print("%s * %s = %s" % (5, vector3, 5 * vector3))
    print("%s * %s = %s" % (vector1, vector1, vector1 * vector1))
    print("%s * %s = %s" % (vector1, vector3, vector1 * vector3))
    print("%s / %s = %s" % (point1, 2, point1 / 2))
    print("%s / %s = %s" % (vector1, 2, vector1 / 2))
    print("len(%s) = %s" % (point1, len(point1)))
    print("len(%s) = %s" % (vector1, len(vector1)))
    print("length(%s) = %s" % (vector1, vector1.length()))

if __name__ == "__main__":
    testPntvec()
