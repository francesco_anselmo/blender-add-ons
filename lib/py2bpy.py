# (c) Jan Walter 2011

import os
import re
import math
# my own modules
import rad
import pntvec
# Blender
import bpy
import mathutils

class Xform:
    def __init__(self, type, value, axis = None):
        self.type = type
        self.value = value
        self.axis = axis

    def apply(self):
        bpy.ops.object.duplicate_move_linked()
        if self.type == "rotate":
            bpy.ops.transform.rotate(value = self.value, axis = self.axis)
        elif self.type == "translate":
            bpy.ops.transform.translate(value = self.value)
        elif self.type == "resize":
            bpy.ops.transform.resize(value = self.value)

class XformState:
    def __init__(self):
        self.reset()
        self.array = []
        self.offset = 0
        self.arrayUsed = False
        self.arrayClosed = False
        self.matrices = []

    def reset(self):
        self.expectName = False
        self.expectA = False
        self.expectI = False
        self.expectRx = False
        self.expectRy = False
        self.expectRz = False
        self.expectTx = False
        self.expectTy = False
        self.expectTz = False
        self.expectS = False

def genbox(mat = "Material", name = "Genbox",
           xsiz = 1.0, ysiz = 1.0, zsiz = 1.0, rad = 0.0, bev = 0.0):
    geoList = []
    if rad > 0.0:
        # create geometry for a sphere
        bpy.ops.surface.primitive_nurbs_surface_sphere_add()
        # sphere1 (origin)
        sphere1 = bpy.context.selected_objects[0]
        sphere1.name = 'Sphere1'
        geoList.append(sphere1)
        # one single sphere for all corners
        sphere1.data.name = 'Sphere'
        bpy.ops.transform.resize(value=(rad, rad, rad)) # scale to radius
        # translate
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(0.0+rad, 0.0+rad, 0.0+rad))
        # sphere2 (x)
        bpy.ops.object.duplicate_move_linked() # duplicate data
        sphere2 = bpy.context.selected_objects[0]
        sphere2.name = 'Sphere2'
        geoList.append(sphere2)
        # translate
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(xsiz-rad, 0.0+rad, 0.0+rad))
        # sphere3 (xy)
        bpy.ops.object.duplicate_move_linked() # duplicate data
        sphere3 = bpy.context.selected_objects[0]
        sphere3.name = 'Sphere3'
        geoList.append(sphere3)
        # translate
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(xsiz-rad, ysiz-rad, 0.0+rad))
        # sphere4 (y)
        bpy.ops.object.duplicate_move_linked() # duplicate data
        sphere4 = bpy.context.selected_objects[0]
        sphere4.name = 'Sphere4'
        geoList.append(sphere4)
        # translate
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(0.0+rad, ysiz-rad, 0.0+rad))
        # sphere5 (z)
        bpy.ops.object.duplicate_move_linked() # duplicate data
        sphere5 = bpy.context.selected_objects[0]
        sphere5.name = 'Sphere5'
        geoList.append(sphere5)
        # translate
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(0.0+rad, 0.0+rad, zsiz-rad))
        # sphere6 (xz)
        bpy.ops.object.duplicate_move_linked() # duplicate data
        sphere6 = bpy.context.selected_objects[0]
        sphere6.name = 'Sphere6'
        geoList.append(sphere6)
        # translate
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(xsiz-rad, 0.0+rad, zsiz-rad))
        # sphere7 (xyz)
        bpy.ops.object.duplicate_move_linked() # duplicate data
        sphere7 = bpy.context.selected_objects[0]
        sphere7.name = 'Sphere7'
        geoList.append(sphere7)
        # translate
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(xsiz-rad, ysiz-rad, zsiz-rad))
        # sphere8 (yz)
        bpy.ops.object.duplicate_move_linked() # duplicate data
        sphere8 = bpy.context.selected_objects[0]
        sphere8.name = 'Sphere8'
        geoList.append(sphere8)
        # translate
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(0.0+rad, ysiz-rad, zsiz-rad))
        # create geometry for a unit cylinder (height = radius = 1.0)
        bpy.ops.surface.primitive_nurbs_surface_cylinder_add()
        bpy.ops.object.editmode_toggle()
        bpy.ops.curve.select_all(action="SELECT")
        bpy.ops.transform.resize(value=(1.0, 1.0, 0.5))
        bpy.ops.transform.translate(value=(0.0, 0.0, 0.5))
        bpy.ops.object.editmode_toggle()
        # cylinders along z-axis
        cylinder_z1 = bpy.context.selected_objects[0]
        cylinder_z1.name = 'Cylinder_z1'
        cylinder_z1.data.name = 'Cylinder'
        geoList.append(cylinder_z1)
        bpy.ops.object.location_clear()
        bpy.ops.transform.resize(value=(rad, rad, zsiz-2*rad))
        bpy.ops.transform.translate(value=(0.0+rad, 0.0+rad, 0.0+rad))
        bpy.ops.object.duplicate_move_linked() # duplicate data
        cylinder_z2 = bpy.context.selected_objects[0]
        cylinder_z2.name = 'Cylinder_z2'
        geoList.append(cylinder_z2)
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(xsiz-rad, 0.0+rad, 0.0+rad))
        bpy.ops.object.duplicate_move_linked() # duplicate data
        cylinder_z3 = bpy.context.selected_objects[0]
        cylinder_z3.name = 'Cylinder_z3'
        geoList.append(cylinder_z3)
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(xsiz-rad, ysiz-rad, 0.0+rad))
        bpy.ops.object.duplicate_move_linked() # duplicate data
        cylinder_z4 = bpy.context.selected_objects[0]
        cylinder_z4.name = 'Cylinder_z4'
        geoList.append(cylinder_z4)
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(0.0+rad, ysiz-rad, 0.0+rad))
        # cylinders along y-axis
        bpy.ops.object.duplicate_move_linked() # duplicate data
        cylinder_y1 = bpy.context.selected_objects[0]
        cylinder_y1.name = 'Cylinder_y1'
        geoList.append(cylinder_y1)
        bpy.ops.object.location_clear()
        bpy.ops.object.scale_clear()
        bpy.ops.transform.resize(value=(rad, rad, ysiz-2*rad))
        bpy.ops.transform.rotate(value=-math.pi/2.0, axis=(1, 0, 0))
        bpy.ops.transform.translate(value=(0.0+rad, 0.0+rad, 0.0+rad))
        bpy.ops.object.duplicate_move_linked() # duplicate data
        cylinder_y2 = bpy.context.selected_objects[0]
        cylinder_y2.name = 'Cylinder_y2'
        geoList.append(cylinder_y2)
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(xsiz-rad, 0.0+rad, 0.0+rad))
        bpy.ops.object.duplicate_move_linked() # duplicate data
        cylinder_y3 = bpy.context.selected_objects[0]
        cylinder_y3.name = 'Cylinder_y3'
        geoList.append(cylinder_y3)
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(xsiz-rad, 0.0+rad, zsiz-rad))
        bpy.ops.object.duplicate_move_linked() # duplicate data
        cylinder_y4 = bpy.context.selected_objects[0]
        cylinder_y4.name = 'Cylinder_y4'
        geoList.append(cylinder_y4)
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(0.0+rad, 0.0+rad, zsiz-rad))
        # cylinders along x-axis
        bpy.ops.object.duplicate_move_linked() # duplicate data
        cylinder_x1 = bpy.context.selected_objects[0]
        cylinder_x1.name = 'Cylinder_x1'
        geoList.append(cylinder_x1)
        bpy.ops.object.location_clear()
        bpy.ops.object.scale_clear()
        bpy.ops.object.rotation_clear()
        bpy.ops.transform.resize(value=(rad, rad, xsiz-2*rad))
        bpy.ops.transform.rotate(value=math.pi/2.0, axis=(0, 1, 0))
        bpy.ops.transform.translate(value=(0.0+rad, 0.0+rad, 0.0+rad))
        bpy.ops.object.duplicate_move_linked() # duplicate data
        cylinder_x2 = bpy.context.selected_objects[0]
        cylinder_x2.name = 'Cylinder_x2'
        geoList.append(cylinder_x2)
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(0.0+rad, ysiz-rad, 0.0+rad))
        bpy.ops.object.duplicate_move_linked() # duplicate data
        cylinder_x3 = bpy.context.selected_objects[0]
        cylinder_x3.name = 'Cylinder_x3'
        geoList.append(cylinder_x3)
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(0.0+rad, ysiz-rad, zsiz-rad))
        bpy.ops.object.duplicate_move_linked() # duplicate data
        cylinder_x4 = bpy.context.selected_objects[0]
        cylinder_x4.name = 'Cylinder_x4'
        geoList.append(cylinder_x4)
        bpy.ops.object.location_clear()
        bpy.ops.transform.translate(value=(0.0+rad, 0.0+rad, zsiz-rad))
        # remaining faces as one mesh
        verts = []
        edges = []
        faces = []
        face = [0, 1, 2, 3]
        verts.append(mathutils.Vector((rad, rad, 0.0))) # bottom
        verts.append(mathutils.Vector((rad, ysiz-rad, 0.0)))
        verts.append(mathutils.Vector((xsiz-rad, ysiz-rad, 0.0)))
        verts.append(mathutils.Vector((xsiz-rad, rad, 0.0)))
        faces.append(face)
        face = [4, 5, 6, 7]
        verts.append(mathutils.Vector((rad, rad, zsiz))) # top
        verts.append(mathutils.Vector((xsiz-rad, rad, zsiz)))
        verts.append(mathutils.Vector((xsiz-rad, ysiz-rad, zsiz)))
        verts.append(mathutils.Vector((rad, ysiz-rad, zsiz)))
        faces.append(face)
        face = [8, 9, 10, 11]
        verts.append(mathutils.Vector((0.0, rad, rad))) # left
        verts.append(mathutils.Vector((0.0, rad, zsiz-rad)))
        verts.append(mathutils.Vector((0.0, ysiz-rad, zsiz-rad)))
        verts.append(mathutils.Vector((0.0, ysiz-rad, rad)))
        faces.append(face)
        face = [12, 13, 14, 15]
        verts.append(mathutils.Vector((xsiz, rad, rad))) # right
        verts.append(mathutils.Vector((xsiz, ysiz-rad, rad)))
        verts.append(mathutils.Vector((xsiz, ysiz-rad, zsiz-rad)))
        verts.append(mathutils.Vector((xsiz, rad, zsiz-rad)))
        faces.append(face)
        face = [16, 17, 18, 19]
        verts.append(mathutils.Vector((rad, 0.0, rad))) # front
        verts.append(mathutils.Vector((xsiz-rad, 0.0, rad)))
        verts.append(mathutils.Vector((xsiz-rad, 0.0, zsiz-rad)))
        verts.append(mathutils.Vector((rad, 0.0, zsiz-rad)))
        faces.append(face)
        face = [20, 21, 22, 23]
        verts.append(mathutils.Vector((rad, ysiz, rad))) # back
        verts.append(mathutils.Vector((rad, ysiz, zsiz-rad)))
        verts.append(mathutils.Vector((xsiz-rad, ysiz, zsiz-rad)))
        verts.append(mathutils.Vector((xsiz-rad, ysiz, rad)))
        faces.append(face)
        mesh_data = bpy.data.meshes.new('QuadFaces')
        mesh_data.from_pydata(verts, edges, faces)
        mesh = bpy.data.objects.new('QuadFaces', object_data=mesh_data)
        mesh_data.update()
        mesh.update_tag()
        geoList.append(mesh)
        bpy.context.scene.objects.link(mesh)
        bpy.context.scene.update_tag()
        # add empty
        bpy.ops.object.add(type='EMPTY')
        empty = bpy.context.selected_objects[0]
        empty.name = name
        # select other generated objects (spheres, cylinders, and mesh)
        sphere1.select = True
        sphere2.select = True
        sphere3.select = True
        sphere4.select = True
        sphere5.select = True
        sphere6.select = True
        sphere7.select = True
        sphere8.select = True
        cylinder_x1.select = True
        cylinder_x2.select = True
        cylinder_x3.select = True
        cylinder_x4.select = True
        cylinder_y1.select = True
        cylinder_y2.select = True
        cylinder_y3.select = True
        cylinder_y4.select = True
        cylinder_z1.select = True
        cylinder_z2.select = True
        cylinder_z3.select = True
        cylinder_z4.select = True
        mesh.select = True
        # make the empty the parent of all others
        bpy.ops.object.parent_set(type='OBJECT')
        # select empty only
        bpy.ops.object.select_all(action='DESELECT')
        selected = bpy.data.objects[name]
        selected.select = True
        bpy.context.scene.objects.active = selected
    elif bev > 0.0:
        # single mesh
        verts = []
        edges = []
        faces = []
        face = [0, 1, 2, 3]
        verts.append(mathutils.Vector((bev, bev, 0.0))) # bottom
        verts.append(mathutils.Vector((bev, ysiz-bev, 0.0)))
        verts.append(mathutils.Vector((xsiz-bev, ysiz-bev, 0.0)))
        verts.append(mathutils.Vector((xsiz-bev, bev, 0.0)))
        faces.append(face)
        face = [4, 5, 6, 7]
        verts.append(mathutils.Vector((bev, bev, zsiz))) # top
        verts.append(mathutils.Vector((xsiz-bev, bev, zsiz)))
        verts.append(mathutils.Vector((xsiz-bev, ysiz-bev, zsiz)))
        verts.append(mathutils.Vector((bev, ysiz-bev, zsiz)))
        faces.append(face)
        face = [8, 9, 10, 11]
        verts.append(mathutils.Vector((0.0, bev, bev))) # left
        verts.append(mathutils.Vector((0.0, bev, zsiz-bev)))
        verts.append(mathutils.Vector((0.0, ysiz-bev, zsiz-bev)))
        verts.append(mathutils.Vector((0.0, ysiz-bev, bev)))
        faces.append(face)
        face = [12, 13, 14, 15]
        verts.append(mathutils.Vector((xsiz, bev, bev))) # right
        verts.append(mathutils.Vector((xsiz, ysiz-bev, bev)))
        verts.append(mathutils.Vector((xsiz, ysiz-bev, zsiz-bev)))
        verts.append(mathutils.Vector((xsiz, bev, zsiz-bev)))
        faces.append(face)
        face = [16, 17, 18, 19]
        verts.append(mathutils.Vector((bev, 0.0, bev))) # front
        verts.append(mathutils.Vector((xsiz-bev, 0.0, bev)))
        verts.append(mathutils.Vector((xsiz-bev, 0.0, zsiz-bev)))
        verts.append(mathutils.Vector((bev, 0.0, zsiz-bev)))
        faces.append(face)
        face = [20, 21, 22, 23]
        verts.append(mathutils.Vector((bev, ysiz, bev))) # back
        verts.append(mathutils.Vector((bev, ysiz, zsiz-bev)))
        verts.append(mathutils.Vector((xsiz-bev, ysiz, zsiz-bev)))
        verts.append(mathutils.Vector((xsiz-bev, ysiz, bev)))
        faces.append(face)
        face = [16, 0, 3, 17] # front bottom
        faces.append(face)
        face = [19, 18, 5, 4] # front top
        faces.append(face)
        face = [1, 20, 23, 2] # back bottom
        faces.append(face)
        face = [6, 22, 21, 7] # back top
        faces.append(face)
        face = [8, 11, 1, 0] # left bottom
        faces.append(face)
        face = [9, 4, 7, 10] # left top
        faces.append(face)
        face = [2, 13, 12, 3] # right bottom
        faces.append(face)
        face = [5, 15, 14, 6] # right top
        faces.append(face)
        face = [8, 16, 19, 9] # left front
        faces.append(face)
        face = [10, 21, 20, 11] # left back
        faces.append(face)
        face = [12, 15, 18, 17] # right front
        faces.append(face)
        face = [13, 23, 22, 14] # right back
        faces.append(face)
        face = [0, 16, 8] # bottom corners
        faces.append(face)
        face = [3, 12, 17]
        faces.append(face)
        face = [2, 23, 13]
        faces.append(face)
        face = [1, 11, 20]
        faces.append(face)
        face = [4, 9, 19] # top corners
        faces.append(face)
        face = [5, 18, 15]
        faces.append(face)
        face = [6, 14, 22]
        faces.append(face)
        face = [7, 21, 10]
        faces.append(face)
        mesh_data = bpy.data.meshes.new(name)
        mesh_data.from_pydata(verts, edges, faces)
        mesh = bpy.data.objects.new(name, object_data=mesh_data)
        mesh_data.update()
        mesh.update_tag()
        bpy.context.scene.objects.link(mesh)
        bpy.context.scene.update_tag()
        geoList.append(mesh)
    else:
        # create a mesh cube
        bpy.ops.mesh.primitive_cube_add(location = (0.0, 0.0, 0.0))
        # rename the cube
        cube = bpy.context.selected_objects[0]
        cube.name = '%s' % name
        cube.data.name = 'genbox_%s' % name
        # create a unit cube with one corner located at the origin
        bpy.ops.object.editmode_toggle()
        bpy.ops.mesh.select_all(action="SELECT")
        bpy.ops.transform.resize(value=(0.5, 0.5, 0.5))
        bpy.ops.transform.translate(value=(0.5, 0.5, 0.5))
        bpy.ops.object.editmode_toggle()
        # scale cube
        bpy.ops.transform.resize(value=(xsiz, ysiz, zsiz))
        geoList.append(cube)
    return geoList

def genprism(mat = "Material", name = "Genprism",
             xyPairs = [[0.0, 0.0], [1.0, 0.0], [0.0, 1.0]],
             l = [0.0, 0.0, 1.0], r = 0.0, c = False, e = False):
    geoList = []
    # create a mesh
    verts = []
    edges = []
    faces = []
    for xyPair in xyPairs: # base
        x = xyPair[0]
        y = xyPair[1]
        z = 0.0
        verts.append(mathutils.Vector((x, y, z)))
    if not (len(xyPairs) in [3, 4]):
        # let's create edges
        for i in range(len(xyPairs)-1):
            edge = [i, i+1]
            edges.append(edge)
        # closing edge
        edge = [i+1, 0]
        edges.append(edge)
    else:
        if len(xyPairs) == 3:
            # bottom
            face = [0, 1, 2]
            faces.append(face)
        else : # len(xyPairs) == 4
            # bottom
            face = [0, 1, 2, 3]
            faces.append(face)
    # we created only the bottom face so far
    mesh_data = bpy.data.meshes.new(name )
    mesh_data.from_pydata(verts, edges, faces)
    mesh = bpy.data.objects.new(name, object_data=mesh_data)
    mesh_data.update()
    mesh.update_tag()
    bpy.context.scene.objects.link(mesh)
    bpy.context.scene.update_tag()
    # let's extrude it now
    bpy.ops.object.select_all(action='DESELECT')
    selected = bpy.data.objects[name]
    selected.select = True
    bpy.context.scene.objects.active = selected
    bpy.ops.object.editmode_toggle()
    bpy.ops.mesh.select_all(action="SELECT")
    if not (len(xyPairs) in [3, 4]):
        bpy.ops.mesh.fill()
        bpy.ops.mesh.beautify_fill()
    bpy.ops.mesh.extrude_region_move()
    bpy.ops.transform.translate(value=(l[0], l[1], l[2]))
    bpy.ops.object.editmode_toggle()
    # make sure the polygon is rendered "flat" (for now)
    bpy.ops.object.shade_flat()
    geoList.append(mesh)
    return geoList

def genrev(mat = "Material", name = "Genrev",
            zt = "", rt = "", nseg = 1,
            expressions = {}, constants = {}):
    geoList = []
    # z_coord
    try:
        z_coord = str(constants[zt]) # we need a string here !!!
    except KeyError:
        try:
            z_coord = expressions[zt]
        except KeyError:
            z_coord = zt
    # radius
    try:
        radius = str(constants[rt]) # we need a string here !!!
    except KeyError:
        try:
            radius = expressions[rt]
        except KeyError:
            radius = rt
    # regular expressions
    sinRex = re.compile("sin\(")
    cosRex = re.compile("cos\(")
    hermiteRex = re.compile("hermite\(")
    piRex = re.compile("PI")
    # replace some function calls (e.g. sin(...) or cos(...))
    if sinRex.search(z_coord):
        z_coord = z_coord.replace("sin(", "math.sin(")
    if cosRex.search(z_coord):
        z_coord = z_coord.replace("cos(", "math.cos(")
    if hermiteRex.search(z_coord):
        z_coord = z_coord.replace("hermite(", "rad_noise3.hermite(")
    if piRex.search(z_coord):
        z_coord = z_coord.replace("PI", "math.pi")
    if sinRex.search(radius):
        radius = radius.replace("sin(", "math.sin(")
    if cosRex.search(radius):
        radius = radius.replace("cos(", "math.cos(")
    if hermiteRex.search(radius):
        radius = radius.replace("hermite(", "rad_noise3.hermite(")
    if piRex.search(radius):
        radius = radius.replace("PI", "math.pi")
    # prepare for exec(...)
    my_globals = {}
    my_locals = constants
    exec("import math", my_globals, my_locals) # make sure we got math ...
    exec("import rad_noise3", my_globals, my_locals) # .. and rad_noise3
    # create a mesh
    verts = []
    edges = []
    faces = []
    face = []
    mesh_data = bpy.data.meshes.new(name)
    # loop
    step = 1.0 / nseg
    steps = 32
    for i in range(nseg + 1):
        # create points
        exec("t = %s * %s" % (i, step), my_globals, my_locals)
        exec("z_coord_t = float(%s)" % z_coord, my_globals, my_locals)
        exec("radius_t = float(%s)" % radius, my_globals, my_locals)
        r = my_locals["radius_t"]
        z = my_locals["z_coord_t"]
        for j in range(steps):
            angle = math.radians(j * 360.0 / float(steps))
            x = r * math.cos(angle)
            y = r * math.sin(angle)
            verts.append(mathutils.Vector((x, y, z)))
    for i in range(nseg):
        for j in range(steps):
            idx1 = i*steps+j%steps
            idx2 = i*steps+((j+1)%steps)
            idx3 = (i+1)*steps+((j+1)%steps)
            idx4 = (i+1)*steps+j%steps
            face = [idx1, idx2, idx3, idx4]
            faces.append(face)
    mesh_data.from_pydata(verts, edges, faces)
    mesh = bpy.data.objects.new(name, object_data=mesh_data)
    mesh.update_tag()
    mesh_data.update()
    mesh.update_tag()
    bpy.context.scene.objects.link(mesh)
    bpy.context.scene.update_tag()
    geoList.append(mesh)
    # select mesh only
    bpy.ops.object.select_all(action='DESELECT')
    selected = bpy.data.objects[name]
    selected.select = True
    bpy.context.scene.objects.active = selected
    # make sure the polygon is rendered "smooth"
    bpy.ops.object.shade_smooth()
    return geoList

def genworm(mat = "Material", name = "Genworm",
            xt = "", yt = "", zt = "", rt = "",
            nseg = 1,
            expressions = {}, constants = {}):
    geoList = []
    # x_coord
    try:
        x_coord = str(constants[xt]) # we need a string here !!!
    except KeyError:
        try:
            x_coord = expressions[xt]
        except KeyError:
            x_coord = xt
    # y_coord
    try:
        y_coord = str(constants[yt]) # we need a string here !!!
    except KeyError:
        try:
            y_coord = expressions[yt]
        except KeyError:
            y_coord = yt
    # z_coord
    try:
        z_coord = str(constants[zt]) # we need a string here !!!
    except KeyError:
        try:
            z_coord = expressions[zt]
        except KeyError:
            z_coord = zt
    # radius
    try:
        radius = str(constants[rt]) # we need a string here !!!
    except KeyError:
        try:
            radius = expressions[rt]
        except KeyError:
            radius = rt
    # regular expressions
    sinRex = re.compile("sin\(")
    cosRex = re.compile("cos\(")
    hermiteRex = re.compile("hermite\(")
    # replace some function calls (e.g. sin(...) or cos(...))
    if sinRex.search(x_coord):
        x_coord = x_coord.replace("sin(", "math.sin(")
    if cosRex.search(x_coord):
        x_coord = x_coord.replace("cos(", "math.cos(")
    if hermiteRex.search(x_coord):
        x_coord = x_coord.replace("hermite(", "rad_noise3.hermite(")
    if sinRex.search(y_coord):
        y_coord = y_coord.replace("sin(", "math.sin(")
    if cosRex.search(y_coord):
        y_coord = y_coord.replace("cos(", "math.cos(")
    if hermiteRex.search(y_coord):
        y_coord = y_coord.replace("hermite(", "rad_noise3.hermite(")
    if sinRex.search(z_coord):
        z_coord = z_coord.replace("sin(", "math.sin(")
    if cosRex.search(z_coord):
        z_coord = z_coord.replace("cos(", "math.cos(")
    if hermiteRex.search(z_coord):
        z_coord = z_coord.replace("hermite(", "rad_noise3.hermite(")
    if sinRex.search(radius):
        radius = radius.replace("sin(", "math.sin(")
    if cosRex.search(radius):
        radius = radius.replace("cos(", "math.cos(")
    if hermiteRex.search(radius):
        radius = radius.replace("hermite(", "rad_noise3.hermite(")
    # prepare for exec(...)
    my_globals = {}
    my_locals = constants
    exec("import math", my_globals, my_locals) # make sure we got math ...
    exec("import rad_noise3", my_globals, my_locals) # .. and rad_noise3
    # loop
    step = 1.0 / nseg
    spheres = []
    for i in range(nseg + 1):
        # create spheres first
        exec("t = %s * %s" % (i, step), my_globals, my_locals)
        exec("x_coord_t = float(%s)" % x_coord, my_globals, my_locals)
        exec("y_coord_t = float(%s)" % y_coord, my_globals, my_locals)
        exec("z_coord_t = float(%s)" % z_coord, my_globals, my_locals)
        exec("radius_t = float(%s)" % radius, my_globals, my_locals)
        # create a NURBS sphere (radius 1)
        bpy.ops.surface.primitive_nurbs_surface_sphere_add()
        # assume that the created sphere is the only selected object
        sphere = bpy.context.selected_objects[0]
        # rename the sphere
        sphere.name = name + ".s%s" % i
        sphere.data.name = 'Sphere_' + ".s%s" % i
        # scale the sphere (to the radius size)
        bpy.ops.transform.resize(value=(my_locals["radius_t"],
                                        my_locals["radius_t"],
                                        my_locals["radius_t"]))
        # translate the sphere
        bpy.ops.transform.translate(value=(my_locals["x_coord_t"],
                                           my_locals["y_coord_t"],
                                           my_locals["z_coord_t"]))
        geoList.append(sphere)
        # remember values for later
        spheres.append([my_locals["x_coord_t"],
                        my_locals["y_coord_t"],
                        my_locals["z_coord_t"],
                        my_locals["radius_t"]])
    for i in range(nseg):
        # create connecting cones (general case: the radii are not the same)
        x0 = spheres[i    ][0]
        x1 = spheres[i + 1][0]
        y0 = spheres[i    ][1]
        y1 = spheres[i + 1][1]
        z0 = spheres[i    ][2]
        z1 = spheres[i + 1][2]
        r0 = spheres[i    ][3]
        r1 = spheres[i + 1][3]
        # calculate the length of the vector between p1 and p0
        vec = pntvec.Vector(x1 - x0, y1 - y0, z1 - z0)
        vecLen = vec.length()
        if r0 == r1:
            # create a NURBS cylinder (radius 1)
            bpy.ops.surface.primitive_nurbs_surface_cylinder_add()
            # assume that the created cylinder is the only selected object
            cylinder = bpy.context.selected_objects[0]
            # rename the cylinder
            cylinder.name = name + ".c%s" % i
            cylinder.data.name = 'Cylinder_' + ".c%s" % i
            # scale the cylinder (to the radius size in x and y)
            bpy.ops.transform.resize(value=(r0, r0, 1.0))
            # move the lower controlpoints up towards the origin (z = 0)
            bpy.ops.object.editmode_toggle()
            bpy.ops.curve.select_all(action="SELECT")
            bpy.ops.transform.resize(value=(1.0, 1.0, 0.5))
            bpy.ops.transform.translate(value=(0.0, 0.0, 0.5))
            bpy.ops.object.editmode_toggle()
            # scale the cylinder (to the length in z)
            x = x1 - x0
            y = y1 - y0
            z = z1 - z0
            length = math.sqrt(x*x + y*y + z*z)
            bpy.ops.transform.resize(value=(1.0, 1.0, length))
            # rotate the cylinder (based on the normal given)
            normal = pntvec.Vector(x1 - x0, y1 - y0, z1 - z0)
            default = pntvec.Vector(0.0, 0.0, 1.0)
            angle, axis = default.angleBetween(normal)
            angle = math.radians(angle)
            xa = axis[0]
            ya = axis[1]
            za = axis[2]
            bpy.ops.transform.rotate(value=angle, axis=(xa, ya, za))
            # translate to p0
            bpy.ops.transform.translate(value = (x0, y0, z0))
            geoList.append(cylinder)
        else:
            # see genworm.c
            p0 = pntvec.Vector(x0, y0, z0)
            p1 = pntvec.Vector(x1, y1, z1)
            vec = p1 - p0
            vecLen = vec.length()
            f = (r0 - r1) / (vec * vec)
            np0 = p0 + f * r0 * vec
            np1 = p1 + f * r1 * vec
            f = 1.0 - (r0 - r1) * f
            if f <= 0.0:
                f = 0.0
            else:
                f = math.sqrt(f)
            nr0 = f * r0
            nr1 = f * r1
            # use nvec below
            nvec = np1 - np0
            nvecLen = nvec.length()
            # create a NURBS circle (radius 1)
            bpy.ops.surface.primitive_nurbs_surface_circle_add()
            # assume that the created circle is the only selected object
            cone = bpy.context.selected_objects[0]
            # rename the cone
            cone.name = name + ".c%s" % i
            cone.data.name = 'Cone_' + ".c%s" % i
            # extrude the circle, scale both sides, and move one circle up
            bpy.ops.object.editmode_toggle()
            bpy.ops.curve.select_all(action="SELECT")
            bpy.ops.curve.extrude()
            bpy.ops.transform.resize(value=(nr1, nr1, nr1))
            bpy.ops.transform.translate(value=(0.0, 0.0, nvecLen)) # along z
            bpy.ops.curve.select_all(action = 'INVERT')
            bpy.ops.transform.resize(value=(nr0, nr0, nr0))
            bpy.ops.object.editmode_toggle()
            # rotate the object into place before moving it
            default = pntvec.Vector(0.0, 0.0, 1.0)
            angle, axis = default.angleBetween(nvec)
            angle = math.radians(angle)
            xa = axis[0]
            ya = axis[1]
            za = axis[2]
            bpy.ops.transform.rotate(value=angle, axis=(xa, ya, za))
            # move the object, not the CVs
            bpy.ops.transform.translate(value=(np0[0], np0[1], np0[2]))
            geoList.append(cone)
    # add empty
    bpy.ops.object.add(type='EMPTY')
    empty = bpy.context.selected_objects[0]
    empty.name = name
    for object in geoList:
        object.select = True
    # make the empty the parent of all others
    bpy.ops.object.parent_set(type='OBJECT')
    # select empty only
    bpy.ops.object.select_all(action='DESELECT')
    selected = bpy.data.objects[name]
    selected.select = True
    bpy.context.scene.objects.active = selected
    return geoList

def addMaterial(surface, geoList, materials):
    # return hide
    hide = False
    if type(surface) == type("test_string"):
        words = surface.split()
        matName = words[1]
    else:
        matName = surface.mod
    radMaterial = materials[matName]
    if isinstance(radMaterial, rad.RadDielectric):
        # create a new material (or re-use existing one)
        try:
            material = bpy.data.materials[matName]
        except KeyError:
            bpy.ops.material.new()
            material = bpy.data.materials.new(matName)
        # diffuse color
        material.diffuse_color = mathutils.Color((radMaterial.rtn,
                                                  radMaterial.gtn,
                                                  radMaterial.btn))
        # mirror color
        material.mirror_color = mathutils.Color((1.0, 1.0, 1.0))
        # set intensities/factors
        material.diffuse_intensity = 0.0
        material.specular_intensity = 0.0
        material.raytrace_mirror.reflect_factor = 0.08
        material.raytrace_mirror.use = True
        material.alpha = 0.08
        material.raytrace_mirror.reflect_factor = 0.08
        material.raytrace_transparency.ior = radMaterial.n
        material.use_transparency = True
        material.transparency_method = 'RAYTRACE'
        # TODO: radMaterial.hc
        for geo in geoList:
            # select object
            bpy.ops.object.select_all(action='DESELECT')
            selected = bpy.data.objects[geo.name]
            selected.select = True
            bpy.context.scene.objects.active = selected
            # create a slot
            bpy.ops.object.material_slot_add()
            # and add the material to the object (not the data)
            geo.material_slots[0].link = 'OBJECT'
            geo.material_slots[0].material = material
    elif isinstance(radMaterial, rad.RadGlass):
        # create a new material (or re-use existing one)
        try:
            material = bpy.data.materials[matName]
        except KeyError:
            bpy.ops.material.new()
            material = bpy.data.materials.new(matName)
        # diffuse color
        material.diffuse_color = mathutils.Color((radMaterial.rtn,
                                                  radMaterial.gtn,
                                                  radMaterial.btn))
        # mirror color
        material.mirror_color = mathutils.Color((1.0, 1.0, 1.0))
        # set intensities/factors
        material.diffuse_intensity = 0.0
        material.specular_intensity = 0.0
        material.raytrace_mirror.reflect_factor = 0.08
        material.raytrace_mirror.use = True
        material.alpha = 0.08
        material.raytrace_mirror.reflect_factor = 0.08
        material.raytrace_transparency.ior = 1.52
        material.use_transparency = True
        material.transparency_method = 'RAYTRACE'
        # TODO: radMaterial.hc
        for geo in geoList:
            # select object
            bpy.ops.object.select_all(action='DESELECT')
            selected = bpy.data.objects[geo.name]
            selected.select = True
            bpy.context.scene.objects.active = selected
            # create a slot
            bpy.ops.object.material_slot_add()
            # and add the material to the object (not the data)
            geo.material_slots[0].link = 'OBJECT'
            geo.material_slots[0].material = material
    elif (isinstance(radMaterial, rad.RadLight) or
          isinstance(radMaterial, rad.RadGlow) or
          isinstance(radMaterial, rad.RadIllum)):
        if isinstance(radMaterial, rad.RadIllum):
            hide = True
        # create a new material (or re-use existing one)
        try:
            material = bpy.data.materials[matName]
        except KeyError:
            bpy.ops.material.new()
            material = bpy.data.materials.new(matName)
        # diffuse color
        maxColor = radMaterial.red
        if radMaterial.green > maxColor:
            maxColor = radMaterial.green
        if radMaterial.blue > maxColor:
            maxColor = radMaterial.blue
        material.diffuse_color = mathutils.Color((radMaterial.red / maxColor,
                                                  radMaterial.green / maxColor,
                                                  radMaterial.blue / maxColor))
        # set diffuse and specular intensities to zero
        material.diffuse_intensity = 0.0
        material.specular_intensity = 0.0
        # emit
        material.emit = maxColor
        # TODO: create "emission" attribute for Arnold
        for geo in geoList:
            # select object
            bpy.ops.object.select_all(action='DESELECT')
            selected = bpy.data.objects[geo.name]
            selected.select = True
            bpy.context.scene.objects.active = selected
            # create a slot
            bpy.ops.object.material_slot_add()
            # and add the material to the object (not the data)
            geo.material_slots[0].link = 'OBJECT'
            geo.material_slots[0].material = material
    elif (isinstance(radMaterial, rad.RadMetal) or
          isinstance(radMaterial, rad.RadMetal2)):
        # create a new material (or re-use existing one)
        try:
            material = bpy.data.materials[matName]
        except KeyError:
            bpy.ops.material.new()
            material = bpy.data.materials.new(matName)
        # diffuse color
        material.diffuse_color = mathutils.Color((radMaterial.red,
                                                  radMaterial.green,
                                                  radMaterial.blue))
        # mirror color
        material.mirror_color = mathutils.Color((radMaterial.red,
                                                 radMaterial.green,
                                                 radMaterial.blue))
        # set intensities/factors
        material.diffuse_intensity = 1.0 - radMaterial.spec
        if radMaterial.spec < 1.0:
            material.diffuse_shader = 'OREN_NAYAR'
            try:
                material.roughness = radMaterial.rough
            except AttributeError:
                # metal2
                material.roughness = radMaterial.urough
                # TODO: metal2 vrough
        material.specular_intensity = 0.0
        material.raytrace_mirror.reflect_factor = radMaterial.spec
        material.raytrace_mirror.use = True
        for geo in geoList:
            # select object
            bpy.ops.object.select_all(action='DESELECT')
            selected = bpy.data.objects[geo.name]
            selected.select = True
            bpy.context.scene.objects.active = selected
            # create a slot
            bpy.ops.object.material_slot_add()
            # and add the material to the object (not the data)
            geo.material_slots[0].link = 'OBJECT'
            geo.material_slots[0].material = material
    elif isinstance(radMaterial, rad.RadMirror):
        # create a new material (or re-use existing one)
        try:
            material = bpy.data.materials[matName]
        except KeyError:
            bpy.ops.material.new()
            material = bpy.data.materials.new(matName)
        # diffuse color
        material.diffuse_color = mathutils.Color((radMaterial.red,
                                                  radMaterial.green,
                                                  radMaterial.blue))
        # mirror color
        material.mirror_color = mathutils.Color((radMaterial.red,
                                                 radMaterial.green,
                                                 radMaterial.blue))
        # set intensities/factors
        material.diffuse_intensity = 0.0
        material.specular_intensity = 0.0
        material.raytrace_mirror.reflect_factor = 1.0
        material.raytrace_mirror.use = True
        for geo in geoList:
            # select object
            bpy.ops.object.select_all(action='DESELECT')
            selected = bpy.data.objects[geo.name]
            selected.select = True
            bpy.context.scene.objects.active = selected
            # create a slot
            bpy.ops.object.material_slot_add()
            # and add the material to the object (not the data)
            geo.material_slots[0].link = 'OBJECT'
            geo.material_slots[0].material = material
    elif (isinstance(radMaterial, rad.RadPlastic) or
          isinstance(radMaterial, rad.RadPlastic2) or
          isinstance(radMaterial, rad.RadTrans)):
        # create a new material (or re-use existing one)
        try:
            material = bpy.data.materials[matName]
        except KeyError:
            bpy.ops.material.new()
            material = bpy.data.materials.new(matName)
        # diffuse color
        material.diffuse_color = mathutils.Color((radMaterial.red,
                                                  radMaterial.green,
                                                  radMaterial.blue))
        # diffuse intensity
        material.diffuse_intensity = 1.0 - radMaterial.spec
        # specular color
        material.specular_color = mathutils.Color((1.0, 1.0, 1.0)) # white
        # specular intensity
        material.specular_intensity = radMaterial.spec
        # roughness
        try:
            material.roughness = radMaterial.rough
        except AttributeError:
            material.diffuse_shader = 'OREN_NAYAR'
            # plastic2
            material.roughness = radMaterial.urough
            # TODO: plastic2 vrough
        else:
            material.diffuse_shader = 'OREN_NAYAR'
        for geo in geoList:
            # select object
            bpy.ops.object.select_all(action='DESELECT')
            selected = bpy.data.objects[geo.name]
            selected.select = True
            bpy.context.scene.objects.active = selected
            # create a slot
            bpy.ops.object.material_slot_add()
            # and add the material to the object (not the data)
            geo.material_slots[0].link = 'OBJECT'
            geo.material_slots[0].material = material
    elif isinstance(radMaterial, rad.RadSpotlight):
        # create a new material (or re-use existing one)
        try:
            material = bpy.data.materials[matName]
        except KeyError:
            bpy.ops.material.new()
            material = bpy.data.materials.new(matName)
        # diffuse color
        maxColor = radMaterial.red
        if radMaterial.green > maxColor:
            maxColor = radMaterial.green
        if radMaterial.blue > maxColor:
            maxColor = radMaterial.blue
        material.diffuse_color = mathutils.Color((radMaterial.red / maxColor,
                                                  radMaterial.green / maxColor,
                                                  radMaterial.blue / maxColor))
        # set diffuse and specular intensities to zero
        material.diffuse_intensity = 0.0
        material.specular_intensity = 0.0
        # emit
        material.emit = maxColor
        # TODO: create "emission" attribute for Arnold
        for geo in geoList:
            # select object
            bpy.ops.object.select_all(action='DESELECT')
            selected = bpy.data.objects[geo.name]
            selected.select = True
            bpy.context.scene.objects.active = selected
            # create a slot
            bpy.ops.object.material_slot_add()
            # and add the material to the object (not the data)
            geo.material_slots[0].link = 'OBJECT'
            geo.material_slots[0].material = material
    else:
        print("TODO: %s" % radMaterial)
    return hide

def applyTransformations(words, name, empty = None, 
                         groupName = "", emptyName = ""):
    isInstantiated = False
    state = XformState()
    matrixList = []
    for i in range(len(words)):
        word = words[i]
        if word == "xform":
            pass # ignore
        elif word == "-a":
            state.expectA = True
        elif state.expectA:
            state.arrayUsed = True
            previousA = None
            if len(state.array):
                previousA = state.array[-1]
                state.offset += previousA
            aInt = int(word)
            state.reset()
            state.array.append(aInt)
            state.matrices.append(matrixList)
            matrixList = []
        elif word == "-i":
            state.expectI = True
        elif state.expectI:
            state.reset()
            state.matrices.append(matrixList)
            matrixList = []
        elif word == "-n":
            state.expectName = True
        elif state.expectName:
            name = word
            state.reset()
        elif word == "-rx":
            state.expectRx = True
        elif state.expectRx:
            rx = float(word)
            state.reset()
            m = bpy.context.selected_objects[0].matrix_world.copy()
            matrixList.append(m.Rotation(math.radians(rx), 4, 'X'))
        elif word == "-ry":
            state.expectRy = True
        elif state.expectRy:
            ry = float(word)
            state.reset()
            m = bpy.context.selected_objects[0].matrix_world.copy()
            matrixList.append(m.Rotation(math.radians(ry), 4, 'Y'))
        elif word == "-rz":
            state.expectRz = True
        elif state.expectRz:
            rz = float(word)
            state.reset()
            m = bpy.context.selected_objects[0].matrix_world.copy()
            matrixList.append(m.Rotation(math.radians(rz), 4, 'Z'))
        elif word == "-t":
            state.expectTx = True
        elif state.expectTx:
            tx = float(word)
            state.reset()
            state.expectTy = True
        elif state.expectTy:
            ty = float(word)
            state.reset()
            state.expectTz = True
        elif state.expectTz:
            tz = float(word)
            state.reset()
            m = bpy.context.selected_objects[0].matrix_world.copy()
            matrixList.append(m.Translation((tx, ty, tz)))
        elif word == "-s":
            state.expectS = True
        elif state.expectS:
            s = float(word)
            state.reset()
            m = bpy.context.selected_objects[0].matrix_world.copy()
            matrixList.append(m.Scale(s, 4))
        elif word == "-mx":
            state.reset()
            m = bpy.context.selected_objects[0].matrix_world.copy()
            i = m.Identity(4)
            i[0][0] = -1.0
            matrixList.append(i)
        elif word == "-my":
            state.reset()
            m = bpy.context.selected_objects[0].matrix_world.copy()
            i = m.Identity(4)
            i[1][1] = -1.0
            matrixList.append(i)
        elif word == "-mz":
            state.reset()
            m = bpy.context.selected_objects[0].matrix_world.copy()
            i = m.Identity(4)
            i[2][2] = -1.0
            matrixList.append(i)
    state.matrices.append(matrixList)
    if len(state.array):
        empties = []
        if len(state.array) == 1:
            for i in range(state.array[0]):
                # create new empty
                bpy.ops.object.add(type='EMPTY')
                newEmpty = bpy.context.selected_objects[0]
                empties.append(newEmpty)
                newEmpty.name = emptyName + "_%s" % (i + 1)
                newEmpty.empty_draw_type = 'SINGLE_ARROW'
                newEmpty.dupli_type = 'GROUP'
                newEmpty.dupli_group = bpy.data.groups[groupName]
                # apply transformations before first '-a'
                m = bpy.context.selected_objects[0].matrix_world
                matrixList = state.matrices[0]
                for i1 in range(len(matrixList)):
                    m = matrixList[i1] * m
                # apply first array
                if i != 0:
                    matrixList = state.matrices[1]
                    for i2 in range(i):
                        for i1 in range(len(matrixList)):
                            m = matrixList[i1] * m
                bpy.context.selected_objects[0].matrix_world = m
        elif len(state.array) == 2:
            counter = 0
            for j in range(state.array[1]):
                for i in range(state.array[0]):
                    counter = counter + 1
                    # create new empty
                    bpy.ops.object.add(type='EMPTY')
                    newEmpty = bpy.context.selected_objects[0]
                    empties.append(newEmpty)
                    newEmpty.name = emptyName + "_%s" % counter
                    newEmpty.empty_draw_type = 'SINGLE_ARROW'
                    newEmpty.dupli_type = 'GROUP'
                    newEmpty.dupli_group = bpy.data.groups[groupName]
                    # apply transformations before first '-a'
                    m = bpy.context.selected_objects[0].matrix_world
                    matrixList = state.matrices[0]
                    for i1 in range(len(matrixList)):
                        m = matrixList[i1] * m
                    # apply first array
                    if i != 0:
                        matrixList = state.matrices[1]
                        for i2 in range(i):
                            for i1 in range(len(matrixList)):
                                m = matrixList[i1] * m
                    # apply second array
                    if j != 0:
                        matrixList = state.matrices[2]
                        for i2 in range(j):
                            for j1 in range(len(matrixList)):
                                m = matrixList[j1] * m
                    bpy.context.selected_objects[0].matrix_world = m
        else:
            print("TODO: len(state.array) = %s" % len(state.array))
        if empty != None:
            # select nothing
            bpy.ops.object.select_all(action='DESELECT')
            # select each child
            for child in empties:
                child.select = True
                bpy.context.scene.objects.active = child
            # select empty last
            selected = bpy.data.objects[emptyName]
            selected.select = True
            bpy.context.scene.objects.active = selected
            # make the empty the parent of all others
            bpy.ops.object.parent_set(type='OBJECT')
            # apply transformations before after '-i'
            m = selected.matrix_world
            # we expect a matrix list before the first '-a'
            # after that we expect as many matrix lists as we found '-a'
            # the last entry should be a matrix list after '-i'
            afterArrayMatrixListIndex = 1 + len(state.array)
            try:
                matrixList = state.matrices[afterArrayMatrixListIndex]
            except IndexError:
                pass
            else:
                for i1 in range(len(matrixList)):
                    m = matrixList[i1] * m
            selected.matrix_world = m
    elif len(state.matrices):
        # apply transformations before first '-a'
        m = bpy.context.selected_objects[0].matrix_world
        matrixList = state.matrices[0]
        for i1 in range(len(matrixList)):
            m = matrixList[i1] * m
        bpy.context.selected_objects[0].matrix_world = m

def doIt(surfaces, materials, commands, includes, camera):
    usedIncludes = []
    if camera != None:
        print(camera)
        cameraExists = False
        targetExists = False
        try:
            blendCam = bpy.data.objects[camera.id]
        except KeyError:
            # no camera found under that name, so create a new one
            bpy.ops.object.camera_add(location = (camera.vp[0],
                                                  camera.vp[1],
                                                  camera.vp[2]))
            # assume that the created camera is the only selected object
            blendCam = bpy.context.selected_objects[0]
            blendCam.name = camera.id
            blendCam.data.name = camera.id
            blendCam.data.lens_unit = 'FOV'
            if camera.vh > camera.vv:
                blendCam.data.angle = math.radians(camera.vh)
            else:
                blendCam.data.angle = math.radians(camera.vv)
        else:
            cameraExists = True
            blendCam.location[0] = camera.vp[0]
            blendCam.location[1] = camera.vp[1]
            blendCam.location[2] = camera.vp[2]
        blendCam.data.draw_size = 0.1
        targetName = camera.id + "_Target"
        try:
            target = bpy.data.objects[targetName]
        except KeyError:
            # no object with targetName found, so let's create an empty
            bpy.ops.object.add(type='EMPTY',
                               location = (camera.vp[0] + camera.vd[0],
                                           camera.vp[1] + camera.vd[1],
                                           camera.vp[2] + camera.vd[2]))
            empty = bpy.context.selected_objects[0]
            empty.name = targetName
            target = empty
        else:
            targetExists = True
            # check if empty
            if target.data != None:
                # object with targetName found, but it's not an empty
                bpy.ops.object.add(type='EMPTY',
                                   location = (camera.vp[0] + camera.vd[0],
                                               camera.vp[1] + camera.vd[1],
                                               camera.vp[2] + camera.vd[2]))
                empty = bpy.context.selected_objects[0]
                empty.name = targetName
                target = empty
            else:
                target.location[0] = camera.vp[0] + camera.vd[0]
                target.location[1] = camera.vp[1] + camera.vd[1]
                target.location[2] = camera.vp[2] + camera.vd[2]
        target.empty_draw_size = 0.1
        if not (cameraExists and targetExists):
            # select nothing
            bpy.ops.object.select_all(action='DESELECT')
            # make the camera look at the target
            blendCam.select = True
            target.select = True
            bpy.ops.object.track_set(type = 'TRACKTO')
    for surface in surfaces:
        geoList = []
        # get material
        material = materials[surface.mod]
        # cone
        if isinstance(surface, rad.RadCone):
            # calculate the length of the vector between p1 and p0
            vec = pntvec.Vector(surface.x1 - surface.x0,
                                surface.y1 - surface.y0,
                                surface.z1 - surface.z0)
            vecLen = vec.length()
            # create a NURBS circle (radius 1)
            bpy.ops.surface.primitive_nurbs_surface_circle_add()
            # assume that the created circle is the only selected object
            cone = bpy.context.selected_objects[0]
            # rename the cone
            cone.name = surface.id
            cone.data.name = 'Cone_' + surface.id
            # extrude the circle, scale both sides, and move one circle up
            bpy.ops.object.editmode_toggle()
            bpy.ops.curve.select_all(action="SELECT")
            bpy.ops.curve.extrude()
            bpy.ops.transform.resize(value=(surface.r1,
                                            surface.r1,
                                            surface.r1))
            bpy.ops.transform.translate(value=(0.0, 0.0, vecLen)) # along z
            bpy.ops.curve.select_all(action = 'INVERT')
            bpy.ops.transform.resize(value=(surface.r0,
                                            surface.r0,
                                            surface.r0))
            bpy.ops.object.editmode_toggle()
            # rotate the object into place before moving it
            default = pntvec.Vector(0.0, 0.0, 1.0)
            angle, axis = default.angleBetween(vec)
            angle = math.radians(angle)
            xa = axis[0]
            ya = axis[1]
            za = axis[2]
            bpy.ops.transform.rotate(value=angle, axis=(xa, ya, za))
            # move the object, not the CVs
            bpy.ops.transform.translate(value=(surface.x0,
                                               surface.y0,
                                               surface.z0))
            geoList.append(cone)
        # cup
        elif isinstance(surface, rad.RadCup):
            # calculate the length of the vector between p1 and p0
            vec = pntvec.Vector(surface.x1 - surface.x0,
                                surface.y1 - surface.y0,
                                surface.z1 - surface.z0)
            vecLen = vec.length()
            # create a NURBS circle (radius 1)
            bpy.ops.surface.primitive_nurbs_surface_circle_add()
            # assume that the created circle is the only selected object
            cup = bpy.context.selected_objects[0]
            # rename the cup
            cup.name = surface.id
            cup.data.name = 'Cone_' + surface.id
            # extrude the circle, scale both sides, and move one circle up
            bpy.ops.object.editmode_toggle()
            bpy.ops.curve.select_all(action="SELECT")
            bpy.ops.curve.extrude()
            bpy.ops.transform.resize(value=(surface.r1,
                                            surface.r1,
                                            surface.r1))
            bpy.ops.transform.translate(value=(0.0, 0.0, vecLen)) # along z
            bpy.ops.curve.select_all(action = 'INVERT')
            bpy.ops.transform.resize(value=(surface.r0,
                                            surface.r0,
                                            surface.r0))
            bpy.ops.object.editmode_toggle()
            # rotate the object into place before moving it
            default = pntvec.Vector(0.0, 0.0, 1.0)
            angle, axis = default.angleBetween(vec)
            angle = math.radians(angle)
            xa = axis[0]
            ya = axis[1]
            za = axis[2]
            bpy.ops.transform.rotate(value=angle, axis=(xa, ya, za))
            # move the object, not the CVs
            bpy.ops.transform.translate(value=(surface.x0,
                                               surface.y0,
                                               surface.z0))
            geoList.append(cup)
        # cylinder
        elif isinstance(surface, rad.RadCylinder):
            # create a NURBS cylinder (radius 1)
            bpy.ops.surface.primitive_nurbs_surface_cylinder_add()
            # assume that the created cylinder is the only selected object
            cylinder = bpy.context.selected_objects[0]
            # rename the cylinder
            cylinder.name = surface.id
            cylinder.data.name = 'Cylinder_' + surface.id
            # scale the cylinder (to the radius size in x and y)
            bpy.ops.transform.resize(value=(surface.rad, surface.rad, 1.0))
            # move the lower controlpoints up towards the origin (z = 0)
            bpy.ops.object.editmode_toggle()
            bpy.ops.curve.select_all(action="SELECT")
            bpy.ops.transform.resize(value=(1.0, 1.0, 0.5))
            bpy.ops.transform.translate(value=(0.0, 0.0, 0.5))
            bpy.ops.object.editmode_toggle()
            # scale the cylinder (to the length in z)
            x = surface.x1 - surface.x0
            y = surface.y1 - surface.y0
            z = surface.z1 - surface.z0
            length = math.sqrt(x*x + y*y + z*z)
            bpy.ops.transform.resize(value=(1.0, 1.0, length))
            # rotate the cylinder (based on the normal given)
            normal = pntvec.Vector(surface.x1 - surface.x0,
                                   surface.y1 - surface.y0,
                                   surface.z1 - surface.z0)
            default = pntvec.Vector(0.0, 0.0, 1.0)
            angle, axis = default.angleBetween(normal)
            angle = math.radians(angle)
            xa = axis[0]
            ya = axis[1]
            za = axis[2]
            bpy.ops.transform.rotate(value=angle, axis=(xa, ya, za))
            # translate to p0
            bpy.ops.transform.translate(value = (surface.x0,
                                                 surface.y0,
                                                 surface.z0))
            geoList.append(cylinder)
        # polygon
        elif isinstance(surface, rad.RadPolygon):
            # create a mesh
            verts = []
            edges = []
            faces = []
            face = []
            mesh_data = bpy.data.meshes.new(surface.id)
            if surface.n in [3, 4]:
                for i in range(surface.n):
                    vertex = surface.vertices[i]
                    verts.append(mathutils.Vector((vertex[0],
                                                   vertex[1],
                                                   vertex[2])))
                    face.append(i)
                faces.append(face)
                mesh_data.from_pydata(verts, edges, faces)
                mesh = bpy.data.objects.new(surface.id, object_data=mesh_data)
                mesh.update_tag()
                mesh_data.update()
                mesh.update_tag()
                bpy.context.scene.objects.link(mesh)
                bpy.context.scene.update_tag()
            else:
                for i in range(surface.n):
                    vertex = surface.vertices[i]
                    verts.append(mathutils.Vector((vertex[0],
                                                   vertex[1],
                                                   vertex[2])))
                    if i != 0:
                        edge = [i, i-1]
                        edges.append(edge)
                # closing edge
                edge = [0, i]
                edges.append(edge)
                # create mesh (with edges only)
                mesh_data.from_pydata(verts, edges, faces)
                mesh = bpy.data.objects.new(surface.id, object_data=mesh_data)
                mesh.update_tag()
                mesh_data.update()
                mesh.update_tag()
                bpy.context.scene.objects.link(mesh)
                bpy.context.scene.update_tag()
                # fill the mesh with triangles
                bpy.ops.object.select_all(action='DESELECT')
                selected = bpy.data.objects[surface.id]
                selected.select = True
                bpy.context.scene.objects.active = selected
                bpy.ops.object.editmode_toggle()
                bpy.ops.mesh.select_all(action="SELECT")
                bpy.ops.mesh.fill()
                bpy.ops.mesh.beautify_fill()
                bpy.ops.object.editmode_toggle()
                # make sure the polygon is rendered "flat" (for now)
                bpy.ops.object.shade_flat()
            geoList.append(mesh)
        # ring
        elif isinstance(surface, rad.RadRing):
            # create a NURBS circle (radius 1)
            bpy.ops.surface.primitive_nurbs_surface_circle_add()
            # assume that the created circle is the only selected object
            ring = bpy.context.selected_objects[0]
            # rename the ring
            ring.name = surface.id
            ring.data.name = 'Ring_' + surface.id
            # extrude the circle, scale both sides, and move one circle up
            bpy.ops.object.editmode_toggle()
            bpy.ops.curve.select_all(action="SELECT")
            bpy.ops.curve.extrude()
            bpy.ops.transform.resize(value=(surface.r1,
                                            surface.r1,
                                            surface.r1))
            bpy.ops.curve.select_all(action = 'INVERT')
            bpy.ops.transform.resize(value=(surface.r0,
                                            surface.r0,
                                            surface.r0))
            bpy.ops.object.editmode_toggle()
            # rotate the ring (based on the normal given)
            normal = pntvec.Vector(surface.xdir, surface.ydir, surface.zdir)
            default = pntvec.Vector(0.0, 0.0, 1.0)
            angle, axis = default.angleBetween(normal)
            angle = math.radians(angle)
            xa = axis[0]
            ya = axis[1]
            za = axis[2]
            bpy.ops.transform.rotate(value=angle, axis=(xa, ya, za))
            # translate the ring
            bpy.ops.transform.translate(value=(surface.xcent,
                                               surface.ycent,
                                               surface.zcent))
            geoList.append(ring)
        # sphere
        elif isinstance(surface, rad.RadSphere):
            # create a NURBS sphere (radius 1)
            bpy.ops.surface.primitive_nurbs_surface_sphere_add()
            # assume that the created sphere is the only selected object
            sphere = bpy.context.selected_objects[0]
            # rename the sphere
            sphere.name = surface.id
            sphere.data.name = 'Sphere_' + surface.id
            # scale the sphere (to the radius size)
            bpy.ops.transform.resize(value=(surface.radius,
                                            surface.radius,
                                            surface.radius))
            # translate the sphere
            bpy.ops.transform.translate(value=(surface.xcent,
                                               surface.ycent,
                                               surface.zcent))
            geoList.append(sphere)
        # tube
        elif isinstance(surface, rad.RadTube):
            # create a NURBS tube (radius 1)
            bpy.ops.surface.primitive_nurbs_surface_cylinder_add()
            # assume that the created tube is the only selected object
            tube = bpy.context.selected_objects[0]
            # rename the tube
            tube.name = surface.id
            tube.data.name = 'Cylinder_' + surface.id
            # scale the tube (to the radius size in x and y)
            bpy.ops.transform.resize(value=(surface.rad, surface.rad, 1.0))
            # move the lower controlpoints up towards the origin (z = 0)
            bpy.ops.object.editmode_toggle()
            bpy.ops.curve.select_all(action="SELECT")
            bpy.ops.transform.resize(value=(1.0, 1.0, 0.5))
            bpy.ops.transform.translate(value=(0.0, 0.0, 0.5))
            bpy.ops.object.editmode_toggle()
            # scale the tube (to the length in z)
            x = surface.x1 - surface.x0
            y = surface.y1 - surface.y0
            z = surface.z1 - surface.z0
            length = math.sqrt(x*x + y*y + z*z)
            bpy.ops.transform.resize(value=(1.0, 1.0, length))
            # translate to p0
            bpy.ops.transform.translate(value = (surface.x0,
                                                 surface.y0,
                                                 surface.z0))
            # rotate the tube (based on the normal given)
            normal = pntvec.Vector(surface.x1 - surface.x0,
                                   surface.y1 - surface.y0,
                                   surface.z1 - surface.z0)
            default = pntvec.Vector(0.0, 0.0, 1.0)
            angle, axis = default.angleBetween(normal)
            angle = math.radians(angle)
            xa = axis[0]
            ya = axis[1]
            za = axis[2]
            bpy.ops.transform.rotate(value=angle, axis=(xa, ya, za))
            geoList.append(tube)
        else:
            print("TODO: %s" % surface)
            ##pass
        if geoList:
            hide = addMaterial(surface, geoList, materials)
            if hide == True:
                # take last element in the geoList
                geo = geoList[-1]
                geo.hide = True
        else:
            print("TODO: %s" % material)
            ##pass
    for command in commands:
        words = command.commandLine.split(" ")
        geoList = []
        if words[0] == "cat":
            pass
        elif words[0] == "genbox":
            # get name
            name = words[2]
            # get material
            material = materials[words[1]]
            # create box (no radius, no bevel used yet)
            execString = ('genbox(mat = "%s", ' % words[1] +
                          'name = "%s", ' % name +
                          'xsiz = %s, ' % float(words[3]) +
                          'ysiz = %s, ' % float(words[4]) +
                          'zsiz = %s' % float(words[5]))
            # is there an "-i" option (inward normals)?
            index = 6
            if len(words) > index and words[index] == "-i":
                print("TODO: genbox -i option")
                index += 1
            # is there a radius?
            if len(words) > index and words[index] == "-r":
                index += 1
                radius = float(words[index]) # don't use "rad" (module name)
                execString += ', rad = %s' % radius
                index += 1
            # are there beveled edges?
            if len(words) > index and words[index] == "-b":
                index += 1
                bev = float(words[index])
                execString += ', bev = %s' % bev
                index += 1
            # terminate execString
            execString += ')'
            geoList = eval(execString)
            # are there any transformations?
            words = command.commandLine.split("|")
            try:
                words = words[1].split()
            except IndexError:
                pass
            else:
                # select nothing
                bpy.ops.object.select_all(action='DESELECT')
                if len(geoList) == 1:
                    geo = geoList[0]
                    selected = bpy.data.objects[geo.name]
                else:
                    # select empty
                    selected = bpy.data.objects[name]
                selected.select = True
                bpy.context.scene.objects.active = selected
                applyTransformations(words, name)
        elif words[0] == "genprism":
            # get name
            name = words[2]
            # get material
            material = materials[words[1]]
            # create extruded polygon
            N = int(words[3])
            xyPairs = []
            for i in range(N*2):
                if not (i % 2):
                    xyPair = [float(words[4+i])]
                else:
                    xyPair.append(float(words[4+i]))
                    xyPairs.append(xyPair)
            if words[5+i] == '-l':
                l = [float(words[6+i]), float(words[7+i]), float(words[8+i])]
            execString = ('genprism(mat = "%s", ' % words[1] +
                          'name = "%s", ' % name +
                          'xyPairs = %s, ' % xyPairs +
                          'l = %s)' % l) # TODO: r, c, e
            # TODO: use other parameters
            geoList = eval(execString)
            # are there any transformations?
            words = command.commandLine.split("|")
            try:
                words = words[1].split()
            except IndexError:
                pass
            else:
                # select nothing
                bpy.ops.object.select_all(action='DESELECT')
                for geo in geoList:
                    selected = bpy.data.objects[geo.name]
                    selected.select = True
                    bpy.context.scene.objects.active = selected
                applyTransformations(words, name)
        elif words[0] == "genrev":
            # genrev mat name 'z(t)' 'r(t)' nseg [ -e expr ] [ -f file ] [ -s ]

            # get name
            name = words[2]
            # get material
            mat = words[1]
            material = materials[mat]
            # get 'z(t)' 'r(t)' formulas
            zt = words[3]
            rt = words[4]
            # how many segments?
            nseg = int(words[5])
            # remaining words
            words = words[6:]
            if words and words[0] == "-s": # TODO: do NOT skip '-s'
                words = words[1:] # skip '-s'
            my_globals = {}
            my_locals = {}
            constants = {}
            expressions = {}
            exec("import math", my_globals, my_locals) # make sure we got math
            exprExpected = False
            for i in range(len(words)):
                word = words[i]
                if word == '|':
                    words = words[(i + 1):]
                    break
                else:
                    # we expect pairs (e.g. ['-e', 'expr'] or ['-f', 'file'])
                    if word == "-e":
                        exprExpected = True
                    elif exprExpected:
                        # check for constants (e.g. "const : sqrt(PI/2) ;")
                        words2 = word[1:-1].split(':') # get rid of "'"
                        if len(words2) == 1:
                            # no constants in this expression
                            words3 = word[1:-1].split('=') # get rid of "'"
                            if len(words3) == 2:
                                # user-defined functions
                                key = words3[0]
                                function = words3[1]
                                expressions[key] = function
                            else:
                                print("TODO: %s" % word)
                        else:
                            # constant(s) found
                            words3 = word[1:-1].split(';') # get rid of "'"
                            for cExpr in words3:
                                cExpr = cExpr.replace(':', '=')
                                try:
                                    exec(cExpr, my_globals, my_locals)
                                except NameError:
                                    # TODO: other things can go wrong ;)
                                    cExpr = cExpr.replace('PI', 'math.pi')
                                    # try again
                                    exec(cExpr, my_globals, my_locals)
                            keys = my_locals.keys()
                            for key in keys:
                                if key != "math":
                                    constants[key] = my_locals[key]
                        exprExpected = False
                    else:
                        print("TODO: %s in %s" % (word, words))
            # TODO: [ -f file ] [ -s ]
            if zt[0] == "'":
                zt = zt[1:-1]
            if rt[0] == "'":
                rt = rt[1:-1]
            execString = ('genrev(mat = "%s", ' % mat +
                          'name = "%s", ' % name +
                          'zt = "%s", rt = "%s", ' % (zt, rt) +
                          'nseg = %d, ' % nseg +
                          'expressions = %s, ' % expressions +
                          'constants = %s)' % constants)
            geoList = eval(execString)
            applyTransformations(words, name)
        elif words[0] == "genworm":
            # genworm mat name 'x(t)' 'y(t)' 'z(t)' 'r(t)'
            # nseg [ -e expr ] [ -f file ]

            # get name
            name = words[2]
            # get material
            mat = words[1]
            material = materials[mat]
            # get 'x(t)' 'y(t)' 'z(t)' 'r(t)' formulas
            xt = words[3]
            yt = words[4]
            zt = words[5]
            rt = words[6]
            # how many segments?
            nseg = int(words[7])
            # remaining words
            words = words[8:]
            my_globals = {}
            my_locals = {}
            constants = {}
            expressions = {}
            exec("import math", my_globals, my_locals) # make sure we got math
            exprExpected = False
            for i in range(len(words)):
                word = words[i]
                if word == '|':
                    words = words[(i + 1):]
                    break
                else:
                    # we expect pairs (e.g. ['-e', 'expr'] or ['-f', 'file'])
                    if word == "-e":
                        exprExpected = True
                    elif exprExpected:
                        # check for constants (e.g. "const : sqrt(PI/2) ;")
                        words2 = word[1:-1].split(':') # get rid of "'"
                        if len(words2) == 1:
                            # no constants in this expression
                            words3 = word[1:-1].split('=') # get rid of "'"
                            if len(words3) == 2:
                                # user-defined functions
                                key = words3[0]
                                function = words3[1]
                                expressions[key] = function
                            else:
                                print("TODO: %s" % word)
                        else:
                            # constant(s) found
                            words3 = word[1:-1].split(';') # get rid of "'"
                            for cExpr in words3:
                                cExpr = cExpr.replace(':', '=')
                                try:
                                    exec(cExpr, my_globals, my_locals)
                                except NameError:
                                    # TODO: other things can go wrong ;)
                                    cExpr = cExpr.replace('PI', 'math.pi')
                                    # try again
                                    exec(cExpr, my_globals, my_locals)
                            keys = my_locals.keys()
                            for key in keys:
                                if key != "math":
                                    constants[key] = my_locals[key]
                        exprExpected = False
                    else:
                        print("TODO: %s in %s" % (word, words))
            # TODO: [ -f file ]
            if xt[0] == "'":
                xt = xt[1:-1]
            if yt[0] == "'":
                yt = yt[1:-1]
            if zt[0] == "'":
                zt = zt[1:-1]
            if rt[0] == "'":
                rt = rt[1:-1]
            execString = ('genworm(mat = "%s", ' % mat +
                          'name = "%s", ' % name +
                          'xt = "%s", yt = "%s", zt = "%s", rt = "%s", ' %
                          (xt, yt, zt, rt) +
                          'nseg = %d, ' % nseg +
                          'expressions = %s, ' % expressions +
                          'constants = %s)' % constants)
            geoList = eval(execString)
            applyTransformations(words, name)
        elif words[0] == "xform":
            # get material
            material = None
            # get name
            words2 = command.commandLine.split("-n")
            if len(words2) > 1:
                words3 = words2[1].split()
                name = words3[0]
            else:
                basename = os.path.basename(words[-1])
                name, ext = os.path.splitext(basename)
            # check if included was used already
            groupName = words[-1]
            usedInclude = False
            if groupName in usedIncludes:
                usedInclude = True
            else:
                usedIncludes.append(groupName)
                # select nothing
                old_layers = [False] * 20
                for layer in range(20):
                    old_layers[layer] = bpy.context.scene.layers[layer]
                new_layers = [True] * 20
                bpy.context.scene.layers = new_layers
                bpy.ops.object.select_all(action='DESELECT')
                # layer 20
                layers = [False] * 20
                layers[19] = True
                # create a new group containing the included objects
                for objName in includes[groupName]:
                    # select object by name
                    try:
                        obj = bpy.data.objects[objName]
                    except KeyError:
                        obj = bpy.data.objects[objName + "_xf"]
                    obj.select = True
                    if obj.type == 'EMPTY':
                        for child in obj.children:
                            child.select = True
                            # move child to layer 20
                            child.layers = layers
                    # move object to layer 20
                    obj.layers = layers
                # now create the group
                bpy.ops.group.create(name = groupName)
                bpy.context.scene.layers = old_layers # restore
            # make sure nothing is selected
            bpy.ops.object.select_all(action='DESELECT')
            # create an empty with a new name
            emptyName = name + "_xf"
            bpy.ops.object.add(type='EMPTY')
            empty = bpy.context.selected_objects[0]
            empty.name = emptyName
            if (empty.name != emptyName):
                print("WARNING: %s != %s" %
                      (empty.name, emptyName))
                bpy.ops.object.select_all(action='DESELECT')
                selected = bpy.data.objects[empty.name]
                bpy.ops.object.delete(use_global=False)
            else:
                empty.empty_draw_type = 'SINGLE_ARROW'
                if not "-a" in words:
                    empty.dupli_type = 'GROUP'
                    empty.dupli_group = bpy.data.groups[groupName]
                # now apply the transformations
                applyTransformations(words, name, empty, groupName, emptyName)
        else:
            print("TODO: %s" % command)
        if geoList:
            addMaterial(command, geoList, materials)
    for matName in materials:
        radMaterial = materials[matName]
        if isinstance(radMaterial, rad.RadSource):
            # use modifier name to get access to the RadLight
            radLight = materials[radMaterial.mod]
            if isinstance(radLight, rad.RadLight):
                maxColor = radLight.red
                if radLight.green > maxColor:
                    maxColor = radLight.green
                if radLight.blue > maxColor:
                    maxColor = radLight.blue
                # create new lamp
                bpy.ops.object.lamp_add(type = 'SUN')
                lamp = bpy.context.selected_objects[0]
                lamp.name = matName
                lamp.data.name = matName
                lamp.data.color = mathutils.Color((radLight.red / maxColor,
                                                   radLight.green / maxColor,
                                                   radLight.blue / maxColor))
                lamp.data.energy = maxColor
                lamp.data.shadow_method = 'RAY_SHADOW'
                # move the lamp
                bpy.ops.transform.translate(value=(10.0 * radMaterial.xdir,
                                                   10.0 * radMaterial.ydir,
                                                   10.0 * radMaterial.zdir))

                # create an empty for the compass
                bpy.ops.object.add(type = 'EMPTY')
                compass = bpy.context.selected_objects[0]
                compass.name = "compass"
                # select nothing
                bpy.ops.object.select_all(action='DESELECT')
                # make the lamp look at the compass
                lamp.select = True
                compass.select = True
                bpy.ops.object.track_set(type = 'TRACKTO')
            else:
                print("WARNING: found %s (RadLight expected)" % radLight)

if __name__ == "__main__":
    # TODO: get the filename interactively
    filename = "/mill3d/users/jan/tmp/rad/room.py"
    filename = "/Users/wahn/Python/Rendering/Python/tmp.py"
    # get rid of the path and the extension
    basename = os.path.basename(filename)
    module, ext = os.path.splitext(basename)
    # get access to the surfaces and materials by importing the module
    exec("import %s" % module)
    exec("surfaces = %s.surfaces" % module)
    exec("materials = %s.materials" % module)
    exec("commands = %s.commands" % module)
    exec("includes = %s.includes" % module)
    exec("camera = %s.camera" % module)
    doIt(surfaces, materials, commands, includes, camera)
