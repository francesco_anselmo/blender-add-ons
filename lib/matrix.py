#!/usr/bin/env python

# CVS
# $Author: jan $
# $Date: 2005/06/10 09:46:52 $
# $RCSfile: matrix.py,v $
# $Revision: 1.3 $

#######################
# (c) Jan Walter 1997 #
#######################

import pntvec
import math

class Matrix:
    def __init__(self, rows, columns):
        self.rows    = rows
        self.columns = columns
        self.elements = []
        row = [None] * columns
        for i in range(rows):
            self.elements.append(row[:])

    def __getitem__(self, index):
        return self.elements[index]

    def __setitem__(self, index, value):
        self.elements[index] = value

    def __add__(self, matrix):
        if self.__class__ != matrix.__class__:
            raise TypeError("%s + %s" % (self, matrix))
        elif (self.rows != matrix.rows) or (self.columns != matrix.columns):
            raise TypeError("Matrix(%s,%s) + Matrix(%s,%s)" %
                            (self.rows, self.columns, 
                             matrix.rows, matrix.columns))
        else:
            result = Matrix(self.rows, self.columns)
            for row in range(self.rows):
                for column in range(self.columns):
                    result[row][column] = (self[row][column] + 
                                           matrix[row][column])
            return result

    def __sub__(self, matrix):
        if self.__class__ != matrix.__class__:
            raise TypeError("%s - %s" % (self, matrix))
        elif (self.rows != matrix.rows) or (self.columns != matrix.columns):
            raise TypeError("Matrix(%s,%s) - Matrix(%s,%s)" %
                            (self.rows, self.columns, 
                             matrix.rows, matrix.columns))
        else:
            result = Matrix(self.rows, self.columns)
            for row in range(self.rows):
                for column in range(self.columns):
                    result[row][column] = (self[row][column] - 
                                           matrix[row][column])
            return result

    def __mul__(self, matrix):
        if self.__class__ != matrix.__class__:
            raise TypeError("%s + %s" % (self, matrix))
        else:
            result = Matrix(self.rows, matrix.columns)
            for row in range(self.rows):
                vector1 = pntvec.Vector(*tuple(self[row]))
                for column in range(matrix.columns):
                    coords = []
                    for i in range(matrix.rows):
                        coords.append(matrix[i][column])
                    vector2 = pntvec.Vector(*tuple(coords))
                    result[row][column] = vector1 * vector2
            return result

    def __repr__(self):
        string = "Matrix(\n"
        length = len(self.elements)
        for index in range(length):
            if index == length - 1:
                string = string + "\t%s\n)" % self.elements[index]
            else:
                string = string + "\t%s\n" % self.elements[index]
        return string

    def identity(self):
        if self.rows == self.columns:
            self.init(0.0)
            for i in range(self.rows):
                self[i][i] = 1.0
        else:
            string = "NxN matrix expected, %sx%s matrix found" % (self.rows,
                                                                  self.columns)
            raise TypeError(string)

    def init(self, element):
        for i in range(self.rows):
            for j in range(self.columns):
                self[i][j] = element

    def inverse(self):
        if self.rows == self.columns:
            tmp1 = Matrix(self.rows, 2*self.rows)
            tmp2 = Matrix(self.rows, self.rows)
            tmp2.identity()
            for i in range(self.rows):
                for j in range(self.rows):
                    tmp1[i][j] = self[i][j]
                    tmp1[i][j+self.rows] = tmp2[i][j]
            tmp1.gauss()
            for i in range(self.rows):
                factor = tmp1[i][i]
                for j in range(2*self.rows):
                    tmp1[i][j] = tmp1[i][j] / factor
            for i in range(self.rows):
                for j in range(self.rows):
                    self[i][j] = tmp1[i][j+self.rows]
        else:
            string = "NxN matrix expected, %sx%s matrix found" % (self.rows,
                                                                  self.columns)
            raise TypeError(string)

    def gauss(self):
        for row in range(self.rows):
            # find biggest
            big = self[row][row]
            index = row
            for i in range(row, self.rows):
                if math.fabs(self[i][row]) > big:
                    big = math.fabs(self[i][row])
                    index = i
            self[index], self[row] = self[row], self[index]
            factor1 = self[row][row]
            if not factor1:
                for i in range(row+1, self.rows):
                    if self[i][row]:
                        self[i], self[row] = self[row], self[i]
                        factor1 = self[row][row]
                        break;
            if not factor1:
                return row
            others = range(self.rows)
            del others[row]
            for i in others:
                factor2 = self[i][row]
                if factor2:
                    tuple1 = self[row][:]
                    tuple2 = self[i][:]
                    for j in range(self.columns):
                        self[i][j] = (tuple1[j] * factor2 -
                                      tuple2[j] * factor1)

def testMatrix():
    matrix = Matrix(4, 4)
    matrix.identity()
    matrix[3] = [2, 4, -2, 1]
    print(matrix)
    matrix.inverse()
    print(matrix)
    matrix = Matrix(4, 4)
    return
    print("=======")
    print("Matrix:")
    print("=======")
    print()
    matrix = Matrix(2, 3)
    print("matrix = %s" % matrix)
    print()
    matrix1 = Matrix(2, 3)
    matrix2 = Matrix(3, 4)
    value = 0
    for i in range(2):
        for j in range(3):
            matrix1[i][j] = value
            value = value + 1
    value = 0
    for i in range(3):
        for j in range(4):
            matrix2[i][j] = value
            value = value + 1
    print("matrix1 =", matrix1)
    print("matrix2 =", matrix2)
    print("matrix1 * matrix2 =", matrix1 * matrix2)
    matrix1 = Matrix(4, 4)
    matrix2 = Matrix(4, 4)
    matrix1[0] = [0.5, 0.0, 0.0, 0.0]
    matrix1[1] = [0.0, 0.5, 0.0, 0.0]
    matrix1[2] = [0.0, 0.0, 0.5, 0.0]
    matrix1[3] = [0.0, 0.0, 0.0, 1.0]
    matrix2[0] = [1.0, 0.0, 0.0, 0.0]
    matrix2[1] = [0.0, 1.0, 0.0, 0.0]
    matrix2[2] = [0.0, 0.0, 1.0, 0.0]
    matrix2[3] = [0.0, 0.0, 1.0, 1.0]
    print("matrix1 =", matrix1)
    print("matrix2 =", matrix2)
    print("matrix1 * matrix2 =", matrix1 * matrix2)
    print()
    row1 = [1, 1, 1]
    row2 = [2, 2, 2]
    matrix[0] = row1
    matrix[1] = row2
    print("matrix = %s" % matrix)
    print()
    matrix[0][1] = 10
    print("matrix = %s" % matrix)
    print()
    print("init(element):")
    print("--------------")
    matrix.init(0)
    print("matrix = %s" % matrix)
    print()
    print("identity():")
    print("-----------")
    matrix = Matrix(3,3)
    matrix.identity()
    print(matrix)
    print()
    print("gauss():")
    print("--------")
    matrix = Matrix(4, 5)
    matrix.init(0)
    matrix[0] = [1, 2, 3, 4, -2]
    matrix[1] = [2, 3, 4, 1,  2]
    matrix[2] = [3, 4, 1, 2,  2]
    matrix[3] = [4, 1, 2, 3, -2]
    print(matrix)
    matrix.gauss()
    print(matrix)
    matrix = Matrix(5, 4)
    matrix.init(0)
    matrix[0] = [-1, -3, -12, -5]
    matrix[1] = [-1,  2,   5,  2]
    matrix[2] = [ 0,  5,  17,  7]
    matrix[3] = [ 3, -1,   2,  1]
    matrix[4] = [ 7, -4,  -1,  0]
    print(matrix)
    matrix.gauss()
    print(matrix)

if __name__ == "__main__":
    #testMatrix()
    m = Matrix(4,4)
    m[0] = [1.0, 0.0, 0.0, 0.0]
    m[1] = [0.0, 6.1230317691118863e-17, 1.0, 0.0]
    m[2] = [0.0, -1.0, 6.1230317691118863e-17, 0.0]
    m[3] = [2.0, 1.0, 1.625, 1.0]
    print(m)
    m.inverse()
    print(m)
