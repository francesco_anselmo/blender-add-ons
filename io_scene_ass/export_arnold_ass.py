"""
This script is an exporter to the ASS file format for the Arnold renderer.
"""
# for cut&paste:
# bpy.ops.export_scene.ass(filepath = "/usr/people/jan/tmp/ass/untitled.ass")

import os
import math
import time
import ctypes
import string
import platform
import subprocess
# arnold
import arnold
# Blender
import bpy
import mathutils

DIFF_DIM_LEVEL = 1.0 # 0.7
SPEC_DIM_LEVEL = 1.0
EMISSION_FACTOR = 1.0
EMISSION_POINT_LIGHTS_FACTOR = 1.0 # 1.0 / 10.0

# custom attributes:
# ------------------
# radius on point lamps
# radius on spot lamps
# disk_radius on area lamps
# sky_intensity on sun lamps
# emission on materials (using mat.emit)
# intensity, exposure, and samples on materials (w/ mat.use_shadeless, mat.emit)
# radius on genbox
# use_point_light on rings and spheres

# supported primitives:
# ---------------------
# "Cone"
# "Cylinder"
# "Disk"
# "Ring"
# "Sphere"
# "genbox"

class UniqueNames:
    def __init__(self):
        self.names = {}
        self.counter = 0

    def uniqueName(self, proposalIn):
        proposal = fix_name(proposalIn)
        useName = proposal
        try:
            test = self.names[proposal]
        except KeyError:
            # the name wasn't used yet
            self.names[proposal] = [proposal]
        else:
            # create new name
            newName = "%s_un%04d" % (proposal, self.counter)
            # try new name
            try:
                test2 = self.names[newName]
            except KeyError:
                # the name wasn't used yet
                useName = newName
            else:
                # no test, just assume we got a unique name now !!!
                newName = "%s%04d" % ("uniqueName", self.counter)
                useName = newName
            ##print("rename '%s' -> '%s'" % (proposal, useName))
            self.counter += 1
        return useName

class Options:
    def __init__(self,
                 selected = False,
                 global_surface_shader = 'none',
                 use_global_illum = True,
                 use_instancer = True,
                 render = True):
        self.selected = selected
        self.global_surface_shader = global_surface_shader
        self.use_global_illum = use_global_illum
        self.use_instancer = use_instancer
        self.render = render

def fix_name(name):
    new_name = name
    # " " -> "_"
    new_name = "_".join(new_name.split(" "))
    # "." -> "_"
    new_name = "_".join(new_name.split("."))
    # "+" -> "_"
    new_name = "_".join(new_name.split("+"))
    return new_name

# ccp = create_char_p
def ccp(aString):
    ba = bytearray(aString, "utf8")
    sb = ctypes.create_string_buffer(len(aString)+1) # len + 1 !!!
    for i in range(len(aString)):
        sb[i] = ba[i]
    return sb

class AssExporter:
    def __init__(self, options):
        # options
        self.options = options
        # unique names
        self.uniqueNames = UniqueNames()
        # material related
        self.materialNames = {}
        self.shaders = {}
        self.light_counter = None
        self.emissive_objects = None
        # motion blur related
        self.use_motion_blur = False
        self.shutter_start = 0.0
        self.shutter_end = 0.0
        self.motion_blur_samples = 0
        self.animatedObjects = []
        self.animatedUniqueNames = []
        self.motionMatrices = {}
        # use BMesh?
        self.useBmesh = False
        version = bpy.app.version
        if version[0] >= 2 and version[1] >= 63:
            self.useBmesh = True

    def connectArnoldNodeWithShaders(self, obj, arnoldNode):
        # use global surface shader?
        if self.options.global_surface_shader != 'none':
            gShader = self.options.global_surface_shader
            print(gShader)
            shader, opaque = self.shaders[gShader]
            arnold.AiNodeSetBool(arnoldNode,
                                 ccp("opaque"),
                                 opaque)
            arnold.AiNodeSetPtr(arnoldNode,
                                ccp("shader"),
                                shader)
        else:
            # assume a material is in the first slot
            if len(obj.material_slots) >= 1:
                mats = obj.material_slots.items()
                matName = mats[0][0]
                if matName == "":
                    matName = "DefaultShader"
                shader, opaque = self.shaders[matName]
                if shader == None:
                    mat = bpy.data.materials[matName]
                    if self.light_counter or self.emissive_objects:
                        if mat.use_shadeless:
                            utility = self.getUtilityMaterial(matName, mat,
                                                              0, # color
                                                              2) # flat
                            image = self.exportUtilityShader(utility)
                            if mat.emit > 0.0:
                                meshLight = self.getMeshLight(obj.name, mat)
                                meshLightNode = self.exportMeshLight(meshLight,
                                                                     image)
                                arnold.AiNodeSetPtr(meshLightNode,
                                                    ccp("mesh"),
                                                    arnoldNode)
                        else:
                            standard = self.getStandardMaterial(matName, mat)
                            image = self.exportStandardShader(standard)
                    else:
                        utility = self.getUtilityMaterial(matName, mat)
                        self.exportUtilityShader(utility)
                    shader, opaque = self.shaders[matName]
                arnold.AiNodeSetBool(arnoldNode,
                                     ccp("opaque"),
                                     opaque)
                arnold.AiNodeSetPtr(arnoldNode,
                                    ccp("shader"),
                                    shader)
            else:
                if (self.light_counter or self.emissive_objects):
                    matName = "DefaultShader"
                    shader, opaque = self.shaders[matName]
                    arnold.AiNodeSetBool(arnoldNode,
                                         ccp("opaque"),
                                         opaque)
                    arnold.AiNodeSetPtr(arnoldNode,
                                        ccp("shader"),
                                        shader)

    def createMatrix(self, node, m):
        am = arnold.AtMatrix()
        arnold.AiM4Identity(am)
        am.a00 = m[0][0]
        am.a01 = m[1][0]
        am.a02 = m[2][0]
        am.a03 = m[3][0]
        am.a10 = m[0][1]
        am.a11 = m[1][1]
        am.a12 = m[2][1]
        am.a13 = m[3][1]
        am.a20 = m[0][2]
        am.a21 = m[1][2]
        am.a22 = m[2][2]
        am.a23 = m[3][2]
        am.a30 = m[0][3]
        am.a31 = m[1][3]
        am.a32 = m[2][3]
        am.a33 = m[3][3]
        arnold.AiNodeSetMatrix(node, ccp("matrix"), am)

    def examine(self, obj):
        ignore = False
        if obj.type == 'CAMERA':
            ignore = True
        elif obj.type == 'CURVE':
            ignore = False
        elif obj.type == 'EMPTY':
            if obj.dupli_type == 'NONE':
                ignore = True
            else:
                ignore = False
        elif obj.type == 'MESH':
            ignore = False
            self.examineMaterials(obj)
        elif obj.type == 'SURFACE':
            ignore = False
            self.examineMaterials(obj)
        elif obj.type == 'LAMP':
            ignore = False
        else:
            ignore = True
            print("WARNING: ignore obj.type == \"%s\"" % obj.type)
        return ignore

    def examineMaterials(self, obj):
        data = obj.data
        if len(data.materials):
            for index in list(range(len(data.materials))):
                material = data.materials[index]
                if material != None:
                    # material attached to mesh
                    try:
                        self.materialNames[obj].append(material.name)
                    except KeyError:
                        # create list of materials
                        self.materialNames[obj] = [material.name]
                else:
                    # material attached to object
                    material = obj.material_slots[index]
                    # material attached to mesh
                    try:
                        self.materialNames[obj].append(material.name)
                    except KeyError:
                        # create list of materials
                        self.materialNames[obj] = [material.name]

    def exportDefaultOrUtilityShader(self):
        self.emissive_objects = False
        print("self.options.global_surface_shader = '%s'" %
              self.options.global_surface_shader)
        if self.options.global_surface_shader == 'none':
            # default material
            standard = self.getStandardMaterial("DefaultShader")
            self.exportStandardShader(standard)
            # loop through all materials
            materials = bpy.data.materials.items()
            for (name, material) in materials:
                if material.emit > 0.0:
                    self.emissive_objects = True
                opaque = True
                self.shaders[name] = [None, opaque]
        else:
            name = self.options.global_surface_shader
            utility = self.getUtilityMaterial(name)
            self.exportUtilityShader(utility)

    def exportInstances(self, obj, child, scene):
        if len(obj.data.vertices) == 0:
            print("WARNING: '%s' has no vertices" % obj.name)
            return
        # bounding box of child
        minX = maxX = child.bound_box[0][0]
        minY = maxY = child.bound_box[0][1]
        minZ = maxZ = child.bound_box[0][2]
        for i in range(8):
            corner = child.bound_box[i]
            if corner[0] < minX:
                minX = corner[0]
            if corner[1] < minY:
                minY = corner[1]
            if corner[2] < minZ:
                minZ = corner[2]
            if corner[0] > maxX:
                maxX = corner[0]
            if corner[1] > maxY:
                maxY = corner[1]
            if corner[2] > maxZ:
                maxZ = corner[2]
        # get access to matrices once
        pm = obj.matrix_world.copy()
        pmi = obj.matrix_world.copy()
        pmi.invert()
        cm = child.matrix_world.copy()
        # bounding box of first instance
        v = obj.data.vertices[0]
        m = pm * pm.Translation(v.co) * pmi * cm
        vt = m * v.co
        minX2 = vt[0] + minX
        maxX2 = vt[0] + maxX
        minY2 = vt[1] + minY
        maxY2 = vt[1] + maxY
        minZ2 = vt[2] + minZ
        maxZ2 = vt[2] + maxZ
        # for each vertex ...
        for vidx in range(len(obj.data.vertices)):
            if vidx != 0:
                # expand the bounding box
                v = obj.data.vertices[vidx]
                m = pm * pm.Translation(v.co) * pmi * cm
                vt = m * v.co
                if (vt[0] + minX) < minX2:
                    minX2 = vt[0] + minX
                if (vt[1] + minY) < minY2:
                    minY2 = vt[1] + minY
                if (vt[2] + minZ) < minZ2:
                    minZ2 = vt[2] + minZ
                if (vt[0] + maxX) > maxX2:
                    maxX2 = vt[0] + maxX
                if (vt[1] + maxY) > maxY2:
                    maxY2 = vt[1] + maxY
                if (vt[2] + maxZ) > maxZ2:
                    maxZ2 = vt[2] + maxZ
        # Arnold node for procedural
        procedural = arnold.AiNode(ccp("procedural"))
        arnold.AiNodeSetStr(procedural, ccp("name"),
                            ccp(self.uniqueName("instancer")))
        arnold.AiNodeSetPnt(procedural, ccp("min"), minX2, minY2, minZ2)
        arnold.AiNodeSetPnt(procedural, ccp("max"), maxX2, maxY2, maxZ2)
        ext = "dylib"
        if platform.system() == "Linux":
            ext = "so"
        # TODO: Windows
        arnold.AiNodeSetStr(procedural, ccp("dso"),
                            # TODO: .so extension
                            ccp("%s.%s" % ("simple_instancer", ext)))
        # pathIndices
        arnold.AiNodeDeclare(procedural, ccp("pathIndices"),
                             ccp("constant ARRAY INT"))
        pathIndices = arnold.AiArrayAllocate(len(obj.data.vertices),
                                             1, arnold.AI_TYPE_INT)
        for vidx in range(len(obj.data.vertices)):
            arnold.AiArraySetInt(pathIndices, vidx, vidx)
        arnold.AiNodeSetArray(procedural, ccp("pathIndices"),
                              pathIndices)
        # pathStartIndices
        arnold.AiNodeDeclare(procedural, ccp("pathStartIndices"),
                             ccp("constant ARRAY INT"))
        pathStartIndices = arnold.AiArrayAllocate(len(obj.data.vertices),
                                                  1, arnold.AI_TYPE_INT)
        for vidx in range(len(obj.data.vertices)):
            arnold.AiArraySetInt(pathStartIndices, vidx, vidx)
        arnold.AiNodeSetArray(procedural, ccp("pathStartIndices"),
                              pathStartIndices)
        # objects
        arnold.AiNodeDeclare(procedural, ccp("objects"),
                             ccp("constant ARRAY STRING"))
        objects = arnold.AiArrayAllocate(len(obj.data.vertices),
                                         1, arnold.AI_TYPE_STRING)
        for vidx in range(len(obj.data.vertices)):
            arnold.AiArraySetStr(objects, vidx, ccp(child.name))
        arnold.AiNodeSetArray(procedural, ccp("objects"),
                              objects)
        # instanceMatrix
        arnold.AiNodeDeclare(procedural, ccp("instanceMatrix"),
                             ccp("constant ARRAY MATRIX"))
        instanceMatrix = arnold.AiArrayAllocate(len(obj.data.vertices),
                                                1, arnold.AI_TYPE_MATRIX)
        for vidx in range(len(obj.data.vertices)):
            am = arnold.AtMatrix()
            arnold.AiM4Identity(am)
            v = obj.data.vertices[vidx]
            m = pm * pm.Translation(v.co) * pmi * cm
            am.a00 = m[0][0]
            am.a01 = m[0][1]
            am.a02 = m[0][2]
            am.a03 = m[0][3]
            am.a10 = m[1][0]
            am.a11 = m[1][1]
            am.a12 = m[1][2]
            am.a13 = m[1][3]
            am.a20 = m[2][0]
            am.a21 = m[2][1]
            am.a22 = m[2][2]
            am.a23 = m[2][3]
            am.a30 = m[3][0]
            am.a31 = m[3][1]
            am.a32 = m[3][2]
            am.a33 = m[3][3]
            arnold.AiArraySetMtx(instanceMatrix, vidx, am)
        arnold.AiNodeSetArray(procedural, ccp("instanceMatrix"),
                              instanceMatrix)

    def exportMeshWithFaceNormals(self, obj, data, group_matrix = None):
        if self.useBmesh:
            # delegate to new BMesh function
            return self.exportBMeshWithFaceNormals(obj, data, group_matrix)
        # polymesh
        polymesh = arnold.AiNode(ccp("polymesh"))
        arnold.AiNodeSetStr(polymesh, ccp("name"),
                            ccp(self.uniqueName(obj.name)))
        nsides = arnold.AiArrayAllocate(len(data.faces), 1,
                                        arnold.AI_TYPE_UINT)
        numVertexIndices = 0
        for i in range(len(data.faces)):
            face = data.faces[i]
            arnold.AiArraySetUInt(nsides, i, len(face.vertices))
            numVertexIndices += len(face.vertices)
        arnold.AiNodeSetArray(polymesh, ccp("nsides"), nsides)
        vidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        numVertexIndices = 0 # reset
        for face in data.faces:
            for i in list(range(len(face.vertices))):
                arnold.AiArraySetUInt(vidxs, numVertexIndices,
                                      face.vertices[i])
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("vidxs"), vidxs)
        shidxs = arnold.AiArrayAllocate(len(data.faces), 1,
                                        arnold.AI_TYPE_BYTE)
        for i in range(len(data.faces)):
            face = data.faces[i]
            mat_idx = face.material_index
            arnold.AiArraySetByte(shidxs, i, mat_idx)
        arnold.AiNodeSetArray(polymesh, ccp("shidxs"), shidxs)
        vlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                       arnold.AI_TYPE_POINT)
        for i in range(len(data.vertices)):
            v = data.vertices[i]
            arnold.AiArraySetPnt(vlist, i,
                                 arnold.AtPoint(v.co[0], v.co[1], v.co[2]))
        arnold.AiNodeSetArray(polymesh, ccp("vlist"), vlist)
        m = obj.matrix_world.copy()
        if group_matrix != None:
            m = group_matrix * m
        self.createMatrix(polymesh, m)
        return polymesh

    def exportBMeshWithFaceNormals(self, obj, data, group_matrix = None):
        # polymesh
        polymesh = arnold.AiNode(ccp("polymesh"))
        arnold.AiNodeSetStr(polymesh, ccp("name"),
                            ccp(self.uniqueName(obj.name)))
        nsides = arnold.AiArrayAllocate(len(data.polygons), 1,
                                        arnold.AI_TYPE_UINT)
        numVertexIndices = 0
        for i in range(len(data.polygons)):
            polygon = data.polygons[i]
            arnold.AiArraySetUInt(nsides, i, len(polygon.vertices))
            numVertexIndices += len(polygon.vertices)
        arnold.AiNodeSetArray(polymesh, ccp("nsides"), nsides)
        vidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        numVertexIndices = 0 # reset
        for polygon in data.polygons:
            for i in list(range(len(polygon.vertices))):
                arnold.AiArraySetUInt(vidxs, numVertexIndices,
                                      polygon.vertices[i])
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("vidxs"), vidxs)
        shidxs = arnold.AiArrayAllocate(len(data.polygons), 1,
                                        arnold.AI_TYPE_BYTE)
        for i in range(len(data.polygons)):
            polygon = data.polygons[i]
            mat_idx = polygon.material_index
            arnold.AiArraySetByte(shidxs, i, mat_idx)
        arnold.AiNodeSetArray(polymesh, ccp("shidxs"), shidxs)
        vlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                       arnold.AI_TYPE_POINT)
        for i in range(len(data.vertices)):
            v = data.vertices[i]
            arnold.AiArraySetPnt(vlist, i,
                                 arnold.AtPoint(v.co[0], v.co[1], v.co[2]))
        arnold.AiNodeSetArray(polymesh, ccp("vlist"), vlist)
        m = obj.matrix_world.copy()
        if group_matrix != None:
            m = group_matrix * m
        self.createMatrix(polymesh, m)
        return polymesh

    def exportMeshWithFaceNormalsAndUvs(self, obj, data, uv_layer,
                                        group_matrix = None):
        if self.useBmesh:
            # delegate to new BMesh function
            return self.exportBMeshWithFaceNormalsAndUvs(obj, data, uv_layer,
                                                         group_matrix)
        # polymesh
        polymesh = arnold.AiNode(ccp("polymesh"))
        arnold.AiNodeSetStr(polymesh, ccp("name"),
                            ccp(self.uniqueName(obj.name)))
        nsides = arnold.AiArrayAllocate(len(data.faces), 1,
                                        arnold.AI_TYPE_UINT)
        numVertexIndices = 0
        for i in range(len(data.faces)):
            face = data.faces[i]
            arnold.AiArraySetUInt(nsides, i, len(face.vertices))
            numVertexIndices += len(face.vertices)
        arnold.AiNodeSetArray(polymesh, ccp("nsides"), nsides)
        vidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        numVertexIndices = 0 # reset
        for face in data.faces:
            for i in list(range(len(face.vertices))):
                arnold.AiArraySetUInt(vidxs, numVertexIndices,
                                      face.vertices[i])
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("vidxs"), vidxs)
        uvidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                        arnold.AI_TYPE_UINT)
        numVertexIndices = 0 # reset
        for face in data.faces:
            for i in list(range(len(face.vertices))):
                arnold.AiArraySetUInt(uvidxs, numVertexIndices,
                                      face.vertices[i])
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("uvidxs"), uvidxs)
        shidxs = arnold.AiArrayAllocate(len(data.faces), 1,
                                        arnold.AI_TYPE_BYTE)
        for i in range(len(data.faces)):
            face = data.faces[i]
            mat_idx = face.material_index
            arnold.AiArraySetByte(shidxs, i, mat_idx)
        arnold.AiNodeSetArray(polymesh, ccp("shidxs"), shidxs)
        vlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                       arnold.AI_TYPE_POINT)
        for i in range(len(data.vertices)):
            v = data.vertices[i]
            arnold.AiArraySetPnt(vlist, i,
                                 arnold.AtPoint(v.co[0], v.co[1], v.co[2]))
        arnold.AiNodeSetArray(polymesh, ccp("vlist"), vlist)
        uvlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                        arnold.AI_TYPE_POINT2)
        numVertexIndices = 0 # reset
        for fi in list(range(len(data.faces))): # face index
            face = data.faces[fi]
            for vi in list(range(len(face.vertices))): # vertex index
                uv = uv_layer[fi].uv[vi]
                arnold.AiArraySetPnt2(uvlist, numVertexIndices,
                                      arnold.AtPoint2(uv[0], uv[1]))
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("uvlist"), uvlist)
        m = obj.matrix_world.copy()
        if group_matrix != None:
            m = group_matrix * m
        self.createMatrix(polymesh, m)
        return polymesh

    def exportBMeshWithFaceNormalsAndUvs(self, obj, data, uv_layer,
                                         group_matrix = None):
        # polymesh
        polymesh = arnold.AiNode(ccp("polymesh"))
        arnold.AiNodeSetStr(polymesh, ccp("name"),
                            ccp(self.uniqueName(obj.name)))
        nsides = arnold.AiArrayAllocate(len(data.polygons), 1,
                                        arnold.AI_TYPE_UINT)
        numVertexIndices = 0
        for i in range(len(data.polygons)):
            polygon = data.polygons[i]
            arnold.AiArraySetUInt(nsides, i, len(polygon.vertices))
            numVertexIndices += len(polygon.vertices)
        arnold.AiNodeSetArray(polymesh, ccp("nsides"), nsides)
        vidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        numVertexIndices = 0 # reset
        for polygon in data.polygons:
            for i in list(range(len(polygon.vertices))):
                arnold.AiArraySetUInt(vidxs, numVertexIndices,
                                      polygon.vertices[i])
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("vidxs"), vidxs)
        uvidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                        arnold.AI_TYPE_UINT)
        uvlist = arnold.AiArrayAllocate(numVertexIndices, 1,
                                        arnold.AI_TYPE_POINT2)
        numVertexIndices = 0 # reset
        for polygon in data.polygons:
            for i in list(range(len(polygon.vertices))):
                arnold.AiArraySetUInt(uvidxs, numVertexIndices,
                                      numVertexIndices) # polygon.vertices[i])
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("uvidxs"), uvidxs)
        shidxs = arnold.AiArrayAllocate(len(data.polygons), 1,
                                        arnold.AI_TYPE_BYTE)
        for i in range(len(data.polygons)):
            polygon = data.polygons[i]
            mat_idx = polygon.material_index
            arnold.AiArraySetByte(shidxs, i, mat_idx)
        arnold.AiNodeSetArray(polymesh, ccp("shidxs"), shidxs)
        vlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                       arnold.AI_TYPE_POINT)
        for i in range(len(data.vertices)):
            v = data.vertices[i]
            arnold.AiArraySetPnt(vlist, i,
                                 arnold.AtPoint(v.co[0], v.co[1], v.co[2]))
        arnold.AiNodeSetArray(polymesh, ccp("vlist"), vlist)
        numVertexIndices = 0 # reset
        for fi in list(range(len(data.polygons))): # polygon index
            polygon = data.polygons[fi]
            uv_layers = data.uv_layer_clone.data.values()
            for vi in list(range(len(polygon.vertices))): # vertex index
                uv = uv_layers[numVertexIndices].uv
                arnold.AiArraySetPnt2(uvlist, numVertexIndices,
                                      arnold.AtPoint2(uv[0], uv[1]))
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("uvlist"), uvlist)
        m = obj.matrix_world.copy()
        if group_matrix != None:
            m = group_matrix * m
        self.createMatrix(polymesh, m)
        return polymesh

    def exportMeshWithFaceAndVertexNormals(self, obj, data,
                                           group_matrix = None):
        if self.useBmesh:
            # delegate to new BMesh function
            return self.exportBMeshWithFaceAndVertexNormals(obj, data,
                                                            group_matrix)
        # polymesh
        polymesh = arnold.AiNode(ccp("polymesh"))
        arnold.AiNodeSetStr(polymesh, ccp("name"),
                            ccp(self.uniqueName(obj.name)))
        nsides = arnold.AiArrayAllocate(len(data.faces), 1,
                                        arnold.AI_TYPE_UINT)
        numVertexIndices = 0
        for i in range(len(data.faces)):
            face = data.faces[i]
            arnold.AiArraySetUInt(nsides, i, len(face.vertices))
            numVertexIndices += len(face.vertices)
        arnold.AiNodeSetArray(polymesh, ccp("nsides"), nsides)
        vidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        nidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        numVertexIndices = 0 # reset
        for face in data.faces:
            for i in list(range(len(face.vertices))):
                arnold.AiArraySetUInt(vidxs, numVertexIndices,
                                      face.vertices[i])
                arnold.AiArraySetUInt(nidxs, numVertexIndices,
                                      face.vertices[i])
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("vidxs"), vidxs)
        arnold.AiNodeSetArray(polymesh, ccp("nidxs"), nidxs)
        shidxs = arnold.AiArrayAllocate(len(data.faces), 1,
                                        arnold.AI_TYPE_BYTE)
        for i in range(len(data.faces)):
            face = data.faces[i]
            mat_idx = face.material_index
            arnold.AiArraySetByte(shidxs, i, mat_idx)
        arnold.AiNodeSetArray(polymesh, ccp("shidxs"), shidxs)
        vlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                       arnold.AI_TYPE_POINT)
        nlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                       arnold.AI_TYPE_VECTOR)
        for i in range(len(data.vertices)):
            v = data.vertices[i]
            arnold.AiArraySetPnt(vlist, i,
                                 arnold.AtPoint(v.co[0], v.co[1], v.co[2]))
            arnold.AiArraySetPnt(nlist, i,
                                 arnold.AtVector(v.normal[0],
                                                 v.normal[1],
                                                 v.normal[2]))
        arnold.AiNodeSetArray(polymesh, ccp("vlist"), vlist)
        arnold.AiNodeSetArray(polymesh, ccp("nlist"), nlist)
        # use smoothing
        arnold.AiNodeSetBool(polymesh,
                             ccp("smoothing"),
                             True)
        m = obj.matrix_world.copy()
        if group_matrix != None:
            m = group_matrix * m
        self.createMatrix(polymesh, m)
        return polymesh

    def exportBMeshWithFaceAndVertexNormals(self, obj, data, group_matrix = None):
        # polymesh
        polymesh = arnold.AiNode(ccp("polymesh"))
        arnold.AiNodeSetStr(polymesh, ccp("name"),
                            ccp(self.uniqueName(obj.name)))
        nsides = arnold.AiArrayAllocate(len(data.polygons), 1,
                                        arnold.AI_TYPE_UINT)
        numVertexIndices = 0
        for i in range(len(data.polygons)):
            polygon = data.polygons[i]
            arnold.AiArraySetUInt(nsides, i, len(polygon.vertices))
            numVertexIndices += len(polygon.vertices)
        arnold.AiNodeSetArray(polymesh, ccp("nsides"), nsides)
        vidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        nidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        numVertexIndices = 0 # reset
        for polygon in data.polygons:
            for i in list(range(len(polygon.vertices))):
                arnold.AiArraySetUInt(vidxs, numVertexIndices,
                                      polygon.vertices[i])
                arnold.AiArraySetUInt(nidxs, numVertexIndices,
                                      polygon.vertices[i])
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("vidxs"), vidxs)
        arnold.AiNodeSetArray(polymesh, ccp("nidxs"), nidxs)
        shidxs = arnold.AiArrayAllocate(len(data.polygons), 1,
                                        arnold.AI_TYPE_BYTE)
        for i in range(len(data.polygons)):
            polygon = data.polygons[i]
            mat_idx = polygon.material_index
            arnold.AiArraySetByte(shidxs, i, mat_idx)
        arnold.AiNodeSetArray(polymesh, ccp("shidxs"), shidxs)
        vlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                       arnold.AI_TYPE_POINT)
        nlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                       arnold.AI_TYPE_VECTOR)
        for i in range(len(data.vertices)):
            v = data.vertices[i]
            arnold.AiArraySetPnt(vlist, i,
                                 arnold.AtPoint(v.co[0], v.co[1], v.co[2]))
            arnold.AiArraySetPnt(nlist, i,
                                 arnold.AtVector(v.normal[0],
                                                 v.normal[1],
                                                 v.normal[2]))
        arnold.AiNodeSetArray(polymesh, ccp("vlist"), vlist)
        arnold.AiNodeSetArray(polymesh, ccp("nlist"), nlist)
        # use smoothing
        arnold.AiNodeSetBool(polymesh,
                             ccp("smoothing"),
                             True)
        m = obj.matrix_world.copy()
        if group_matrix != None:
            m = group_matrix * m
        self.createMatrix(polymesh, m)
        return polymesh

    def exportMeshWithVertexNormals(self, obj, data, group_matrix = None):
        if self.useBmesh:
            # delegate to new BMesh function
            return self.exportBMeshWithVertexNormals(obj, data, group_matrix)
        # polymesh
        polymesh = arnold.AiNode(ccp("polymesh"))
        arnold.AiNodeSetStr(polymesh, ccp("name"),
                            ccp(self.uniqueName(obj.name)))
        nsides = arnold.AiArrayAllocate(len(data.faces), 1,
                                        arnold.AI_TYPE_UINT)
        numVertexIndices = 0
        for i in range(len(data.faces)):
            face = data.faces[i]
            arnold.AiArraySetUInt(nsides, i, len(face.vertices))
            numVertexIndices += len(face.vertices)
        arnold.AiNodeSetArray(polymesh, ccp("nsides"), nsides)
        vidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        nidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        numVertexIndices = 0 # reset
        for face in data.faces:
            for i in list(range(len(face.vertices))):
                arnold.AiArraySetUInt(vidxs, numVertexIndices,
                                      face.vertices[i])
                arnold.AiArraySetUInt(nidxs, numVertexIndices,
                                      face.vertices[i])
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("vidxs"), vidxs)
        arnold.AiNodeSetArray(polymesh, ccp("nidxs"), nidxs)
        shidxs = arnold.AiArrayAllocate(len(data.faces), 1,
                                        arnold.AI_TYPE_BYTE)
        for i in range(len(data.faces)):
            face = data.faces[i]
            mat_idx = face.material_index
            arnold.AiArraySetByte(shidxs, i, mat_idx)
        arnold.AiNodeSetArray(polymesh, ccp("shidxs"), shidxs)
        vlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                       arnold.AI_TYPE_POINT)
        nlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                       arnold.AI_TYPE_VECTOR)
        for i in range(len(data.vertices)):
            v = data.vertices[i]
            arnold.AiArraySetPnt(vlist, i,
                                 arnold.AtPoint(v.co[0], v.co[1], v.co[2]))
            arnold.AiArraySetVec(nlist, i,
                                 arnold.AtVector(v.normal[0],
                                                 v.normal[1],
                                                 v.normal[2]))
        arnold.AiNodeSetArray(polymesh, ccp("vlist"), vlist)
        arnold.AiNodeSetArray(polymesh, ccp("nlist"), nlist)
        # use smoothing
        arnold.AiNodeSetBool(polymesh,
                             ccp("smoothing"),
                             True)
        m = obj.matrix_world.copy()
        if group_matrix != None:
            m = group_matrix * m
        self.createMatrix(polymesh, m)
        return polymesh

    def exportBMeshWithVertexNormals(self, obj, data, group_matrix = None):
        # polymesh
        polymesh = arnold.AiNode(ccp("polymesh"))
        arnold.AiNodeSetStr(polymesh, ccp("name"),
                            ccp(self.uniqueName(obj.name)))
        nsides = arnold.AiArrayAllocate(len(data.polygons), 1,
                                        arnold.AI_TYPE_UINT)
        numVertexIndices = 0
        for i in range(len(data.polygons)):
            polygon = data.polygons[i]
            arnold.AiArraySetUInt(nsides, i, len(polygon.vertices))
            numVertexIndices += len(polygon.vertices)
        arnold.AiNodeSetArray(polymesh, ccp("nsides"), nsides)
        vidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        nidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        numVertexIndices = 0 # reset
        for polygon in data.polygons:
            for i in list(range(len(polygon.vertices))):
                arnold.AiArraySetUInt(vidxs, numVertexIndices,
                                      polygon.vertices[i])
                arnold.AiArraySetUInt(nidxs, numVertexIndices,
                                      polygon.vertices[i])
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("vidxs"), vidxs)
        arnold.AiNodeSetArray(polymesh, ccp("nidxs"), nidxs)
        shidxs = arnold.AiArrayAllocate(len(data.polygons), 1,
                                        arnold.AI_TYPE_BYTE)
        for i in range(len(data.polygons)):
            polygon = data.polygons[i]
            mat_idx = polygon.material_index
            arnold.AiArraySetByte(shidxs, i, mat_idx)
        arnold.AiNodeSetArray(polymesh, ccp("shidxs"), shidxs)
        vlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                       arnold.AI_TYPE_POINT)
        nlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                       arnold.AI_TYPE_VECTOR)
        for i in range(len(data.vertices)):
            v = data.vertices[i]
            arnold.AiArraySetPnt(vlist, i,
                                 arnold.AtPoint(v.co[0], v.co[1], v.co[2]))
            arnold.AiArraySetPnt(nlist, i,
                                 arnold.AtVector(v.normal[0],
                                                 v.normal[1],
                                                 v.normal[2]))
        arnold.AiNodeSetArray(polymesh, ccp("vlist"), vlist)
        arnold.AiNodeSetArray(polymesh, ccp("nlist"), nlist)
        # use smoothing
        arnold.AiNodeSetBool(polymesh,
                             ccp("smoothing"),
                             True)
        m = obj.matrix_world.copy()
        if group_matrix != None:
            m = group_matrix * m
        self.createMatrix(polymesh, m)
        return polymesh

    def exportMeshWithVertexNormalsAndUvs(self, obj, data, uv_layer,
                                          group_matrix = None):
        if self.useBmesh:
            # delegate to new BMesh function
            return self.exportBMeshWithVertexNormalsAndUvs(obj, data, uv_layer,
                                                           group_matrix)
        # polymesh
        polymesh = arnold.AiNode(ccp("polymesh"))
        arnold.AiNodeSetStr(polymesh, ccp("name"),
                            ccp(self.uniqueName(obj.name)))
        nsides = arnold.AiArrayAllocate(len(data.faces), 1,
                                        arnold.AI_TYPE_UINT)
        numVertexIndices = 0
        for i in range(len(data.faces)):
            face = data.faces[i]
            arnold.AiArraySetUInt(nsides, i, len(face.vertices))
            numVertexIndices += len(face.vertices)
        arnold.AiNodeSetArray(polymesh, ccp("nsides"), nsides)
        vidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        nidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        uvidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                        arnold.AI_TYPE_UINT)
        numVertexIndices = 0 # reset
        for face in data.faces:
            for i in list(range(len(face.vertices))):
                arnold.AiArraySetUInt(vidxs, numVertexIndices,
                                      face.vertices[i])
                arnold.AiArraySetUInt(nidxs, numVertexIndices,
                                      face.vertices[i])
                arnold.AiArraySetUInt(uvidxs, numVertexIndices,
                                      face.vertices[i])
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("vidxs"), vidxs)
        arnold.AiNodeSetArray(polymesh, ccp("nidxs"), nidxs)
        arnold.AiNodeSetArray(polymesh, ccp("uvidxs"), uvidxs)
        shidxs = arnold.AiArrayAllocate(len(data.faces), 1,
                                        arnold.AI_TYPE_BYTE)
        for i in range(len(data.faces)):
            face = data.faces[i]
            mat_idx = face.material_index
            arnold.AiArraySetByte(shidxs, i, mat_idx)
        arnold.AiNodeSetArray(polymesh, ccp("shidxs"), shidxs)
        vlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                       arnold.AI_TYPE_POINT)
        nlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                       arnold.AI_TYPE_VECTOR)
        for i in range(len(data.vertices)):
            v = data.vertices[i]
            arnold.AiArraySetPnt(vlist, i,
                                 arnold.AtPoint(v.co[0], v.co[1], v.co[2]))
            arnold.AiArraySetPnt(nlist, i,
                                 arnold.AtVector(v.normal[0],
                                                 v.normal[1],
                                                 v.normal[2]))
        arnold.AiNodeSetArray(polymesh, ccp("vlist"), vlist)
        arnold.AiNodeSetArray(polymesh, ccp("nlist"), nlist)
        uvlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                        arnold.AI_TYPE_POINT2)
        numVertexIndices = 0 # reset
        for fi in list(range(len(data.faces))): # face index
            face = data.faces[fi]
            for vi in list(range(len(face.vertices))): # vertex index
                uv = uv_layer[fi].uv[vi]
                arnold.AiArraySetPnt2(uvlist, numVertexIndices,
                                      arnold.AtPoint2(uv[0], uv[1]))
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("uvlist"), uvlist)
        # use smoothing
        arnold.AiNodeSetBool(polymesh,
                             ccp("smoothing"),
                             True)
        m = obj.matrix_world.copy()
        if group_matrix != None:
            m = group_matrix * m
        self.createMatrix(polymesh, m)
        return polymesh

    def exportBMeshWithVertexNormalsAndUvs(self, obj, data, uv_layer,
                                          group_matrix = None):
        # polymesh
        polymesh = arnold.AiNode(ccp("polymesh"))
        arnold.AiNodeSetStr(polymesh, ccp("name"),
                            ccp(self.uniqueName(obj.name)))
        nsides = arnold.AiArrayAllocate(len(data.polygons), 1,
                                        arnold.AI_TYPE_UINT)
        numVertexIndices = 0
        for i in range(len(data.polygons)):
            polygon = data.polygons[i]
            arnold.AiArraySetUInt(nsides, i, len(polygon.vertices))
            numVertexIndices += len(polygon.vertices)
        arnold.AiNodeSetArray(polymesh, ccp("nsides"), nsides)
        vidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        nidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        uvidxs = arnold.AiArrayAllocate(len(uv_layer), 1,
                                        arnold.AI_TYPE_UINT)
        numVertexIndices = 0 # reset
        for polygon in data.polygons:
            for i in list(range(len(polygon.vertices))):
                arnold.AiArraySetUInt(vidxs, numVertexIndices,
                                      polygon.vertices[i])
                arnold.AiArraySetUInt(nidxs, numVertexIndices,
                                      polygon.vertices[i])
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("vidxs"), vidxs)
        arnold.AiNodeSetArray(polymesh, ccp("nidxs"), nidxs)
        numVertexIndices = 0 # reset
        for uvLoop in uv_layer:
            arnold.AiArraySetUInt(uvidxs, numVertexIndices,
                                  numVertexIndices)
            numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("uvidxs"), uvidxs)
        shidxs = arnold.AiArrayAllocate(len(data.polygons), 1,
                                        arnold.AI_TYPE_BYTE)
        for i in range(len(data.polygons)):
            polygon = data.polygons[i]
            mat_idx = polygon.material_index
            arnold.AiArraySetByte(shidxs, i, mat_idx)
        arnold.AiNodeSetArray(polymesh, ccp("shidxs"), shidxs)
        vlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                       arnold.AI_TYPE_POINT)
        nlist = arnold.AiArrayAllocate(len(data.vertices), 1,
                                       arnold.AI_TYPE_VECTOR)
        for i in range(len(data.vertices)):
            v = data.vertices[i]
            arnold.AiArraySetPnt(vlist, i,
                                 arnold.AtPoint(v.co[0], v.co[1], v.co[2]))
            arnold.AiArraySetPnt(nlist, i,
                                 arnold.AtVector(v.normal[0],
                                                 v.normal[1],
                                                 v.normal[2]))
        arnold.AiNodeSetArray(polymesh, ccp("vlist"), vlist)
        arnold.AiNodeSetArray(polymesh, ccp("nlist"), nlist)
        uvlist = arnold.AiArrayAllocate(len(uv_layer), 1,
                                        arnold.AI_TYPE_POINT2)
        numVertexIndices = 0 # reset
        for uvLoop in uv_layer:
            uv = uvLoop.uv
            arnold.AiArraySetPnt2(uvlist, numVertexIndices,
                                  arnold.AtPoint2(uv[0], uv[1]))
            numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, ccp("uvlist"), uvlist)
        # use smoothing
        arnold.AiNodeSetBool(polymesh,
                             ccp("smoothing"),
                             True)
        m = obj.matrix_world.copy()
        if group_matrix != None:
            m = group_matrix * m
        self.createMatrix(polymesh, m)
        return polymesh

    def exportMeshLight(self, meshLight, image):
        meshLightNode = None
        # meshLight
        name = meshLight[0]
        color = meshLight[1]
        intensity = meshLight[2]
        exposure = meshLight[3]
        samples = meshLight[4]
        textures = meshLight[5]
        uName = name
        # mesh_light
        shader = arnold.AiNode(ccp("mesh_light"))
        arnold.AiNodeSetStr(shader,
                            ccp("name"),
                            ccp(uName + "_light"))
        # textures["diffuse"]?
        try:
            diffuse_tex = textures["diffuse"]
        except KeyError:
            arnold.AiNodeSetRGB(shader,
                                ccp("color"),
                                color[0],
                                color[1],
                                color[2])
        else:
            arnold.AiNodeLink(image, ccp("color"), shader)
        arnold.AiNodeSetFlt(shader,
                            ccp("intensity"),
                            intensity)
        arnold.AiNodeSetFlt(shader,
                            ccp("exposure"),
                            exposure)
        arnold.AiNodeSetInt(shader,
                            ccp("samples"),
                            samples)
        meshLightNode = shader
        return meshLightNode

    def exportNurbsSurface(self, obj, data, group_matrix = None):
        for spline in data.splines:
            if (spline.order_u == 4 and spline.order_v == 4):
                # nurbs
                nurbs = arnold.AiNode(ccp("nurbs"))
                uniqueName = self.uniqueName(obj.name)
                arnold.AiNodeSetStr(nurbs, ccp("name"), ccp(uniqueName))
                # knots
                knots_u = arnold.AiArrayAllocate(8, 1,
                                                 arnold.AI_TYPE_FLOAT)
                knots_v = arnold.AiArrayAllocate(8, 1,
                                                 arnold.AI_TYPE_FLOAT)
                # TODO: this is an assumption !!!
                for i in range(4):
                    arnold.AiArraySetFlt(knots_u, i, 0.0)
                    arnold.AiArraySetFlt(knots_v, i, 0.0)
                for i in range(4):
                    arnold.AiArraySetFlt(knots_u, i+4, 1.0)
                    arnold.AiArraySetFlt(knots_v, i+4, 1.0)
                arnold.AiNodeSetArray(nurbs, ccp("knots_u"), knots_u)
                arnold.AiNodeSetArray(nurbs, ccp("knots_v"), knots_v)
                # cvs
                cvs = arnold.AiArrayAllocate(4 * len(spline.points), 1,
                                             arnold.AI_TYPE_FLOAT)
                counter = 0
                for v in spline.points:
                    for i in range(4):
                        arnold.AiArraySetFlt(cvs, counter * 4 + i, v.co[i])
                    counter += 1
                arnold.AiNodeSetArray(nurbs, ccp("cvs"), cvs)
                # smoothing
                smoothing = True
                arnold.AiNodeSetBool(nurbs, ccp("smoothing"), smoothing)
                # shader
                matName = "DefaultShader"
                shader, opaque = self.shaders[matName]
                arnold.AiNodeSetPtr(nurbs, ccp("shader"), shader)
                arnold.AiNodeSetBool(nurbs,
                                     ccp("opaque"),
                                     opaque)
                # matrix
                if self.use_motion_blur:
                    # check if anything down to the root is animated
                    if self.isAnimated(obj):
                        self.animatedObjects.append(obj)
                        self.animatedUniqueNames.append(uniqueName)
                m = obj.matrix_world.copy()
                if group_matrix != None:
                    m = group_matrix * m
                self.createMatrix(nurbs, m)
                worked = nurbs
            else:
                return None
        return worked

    def exportObject(self, obj, scene, group_matrix = None):
        if (self.isVisible(obj, scene) or group_matrix != None):
            data = obj.data
            ignore = self.examine(obj) # gather useful information
            if not ignore:
                if obj.type == 'EMPTY':
                    m = obj.matrix_world.copy()
                    if group_matrix != None:
                        m = group_matrix * m
                    if obj.dupli_type == 'GROUP':
                        for child in obj.dupli_group.objects:
                            self.exportObject(child, scene, m)
                    return
                isPrimitive, primType = self.isPrimitive(obj)
                if isPrimitive:
                    # primitives
                    if primType == "Cone":
                        # cone
                        p1 = obj.data.splines[0].points[0].co
                        p2 = obj.data.splines[0].points[8].co
                        cone = arnold.AiNode(ccp("cone"))
                        arnold.AiNodeSetStr(cone,
                                            ccp("name"),
                                            ccp(self.uniqueName(obj.name)))
                        arnold.AiNodeSetPnt(cone,
                                            ccp("bottom"), 0.0, 0.0, p1.z)
                        arnold.AiNodeSetPnt(cone,
                                            ccp("top"), 0.0, 0.0, p2.z)
                        arnold.AiNodeSetFlt(cone, # default is 0.5
                                            ccp("bottom_radius"),
                                            math.fabs(p1.y))
                        arnold.AiNodeSetFlt(cone, # default is 0.0
                                            ccp("top_radius"),
                                            math.fabs(p2.y))
                        m = obj.matrix_world.copy()
                        if group_matrix != None:
                            m = group_matrix * m
                        self.createMatrix(cone, m)
                        self.connectArnoldNodeWithShaders(obj, cone)
                    elif primType == "Cylinder":
                        # cylinder
                        cylinder = arnold.AiNode(ccp("cylinder"))
                        arnold.AiNodeSetStr(cylinder,
                                            ccp("name"),
                                            ccp(self.uniqueName(obj.name)))
                        arnold.AiNodeSetPnt(cylinder,
                                            ccp("bottom"), 0.0, 0.0, 0.0)
                        arnold.AiNodeSetPnt(cylinder,
                                            ccp("top"), 0.0, 0.0, 1.0)
                        arnold.AiNodeSetFlt(cylinder, # default is 0.5
                                            ccp("radius"), 1.0)
                        m = obj.matrix_world.copy()
                        if group_matrix != None:
                            m = group_matrix * m
                        self.createMatrix(cylinder, m)
                        self.connectArnoldNodeWithShaders(obj, cylinder)
                    elif primType == "Disk":
                        # disk
                        disk = arnold.AiNode(ccp("disk"))
                        arnold.AiNodeSetStr(disk,
                                            ccp("name"),
                                            ccp(self.uniqueName(obj.name)))
                        arnold.AiNodeSetFlt(disk, # default is 0.5
                                            ccp("radius"), 1.0)
                        m = obj.matrix_world.copy()
                        if group_matrix != None:
                            m = group_matrix * m
                        self.createMatrix(disk, m)
                        self.connectArnoldNodeWithShaders(obj, disk)
                    elif primType == "Ring":
                        # check on custom attribute(s)
                        keys = obj.data.keys()
                        values = obj.data.values()
                        radius = 0
                        if 'use_point_light' in keys:
                            # get radius from disk
                            p1 = obj.data.splines[0].points[0].co
                            p2 = obj.data.splines[0].points[8].co
                            r1 = math.fabs(p1.y)
                            r2 = math.fabs(p2.y)
                            if r1 < r2:
                                minRadius = r1
                                maxRadius = r2
                            else:
                                minRadius = r2
                                maxRadius = r1
                            radius = maxRadius
                            # use a point light instead of geometry
                            index = keys.index('use_point_light')
                            intensity = values[index]
                            point_light = arnold.AiNode(ccp("point_light"))
                            arnold.AiNodeSetStr(point_light,
                                                ccp("name"),
                                                ccp(self.uniqueName(obj.name)))
                            arnold.AiNodeSetFlt(point_light, # default is 0.0
                                                ccp("radius"), radius)
                            m = obj.matrix_world.copy()
                            if group_matrix != None:
                                m = group_matrix * m
                            self.createMatrix(point_light, m)
                            # assume a material is in the first slot
                            if len(obj.material_slots) >= 1:
                                mats = obj.material_slots.items()
                                matName = mats[0][0]
                                shader, opaque = self.shaders[matName]
                                mat = bpy.data.materials[matName]
                                color = mat.diffuse_color
                                ##intensity = (mat.emit *
                                ##EMISSION_POINT_LIGHTS_FACTOR)
                            arnold.AiNodeSetRGB(point_light, ccp("color"),
                                                color.r,
                                                color.g,
                                                color.b)
                            arnold.AiNodeSetFlt(point_light,
                                                ccp("intensity"),
                                                intensity)
                        else:
                            # disk
                            p1 = obj.data.splines[0].points[0].co
                            p2 = obj.data.splines[0].points[8].co
                            disk = arnold.AiNode(ccp("disk"))
                            r1 = math.fabs(p1.y)
                            r2 = math.fabs(p2.y)
                            if r1 < r2:
                                minRadius = r1
                                maxRadius = r2
                            else:
                                minRadius = r2
                                maxRadius = r1
                            arnold.AiNodeSetStr(disk,
                                                ccp("name"),
                                                ccp(self.uniqueName(obj.name)))
                            arnold.AiNodeSetFlt(disk, # default is 0.5
                                                ccp("radius"), maxRadius)
                            arnold.AiNodeSetFlt(disk, # default is 0.0
                                                ccp("hole"), minRadius)
                            m = obj.matrix_world.copy()
                            if group_matrix != None:
                                m = group_matrix * m
                            self.createMatrix(disk, m)
                            self.connectArnoldNodeWithShaders(obj, disk)
                    elif primType == "Sphere":
                        # check on custom attribute(s)
                        keys = obj.data.keys()
                        values = obj.data.values()
                        radius = 0
                        if 'use_point_light' in keys:
                            # get radius from sphere
                            radius = (obj.scale[0] +
                                      obj.scale[1] +
                                      obj.scale[2]) / 3.0
                            # use a point light instead of geometry
                            index = keys.index('use_point_light')
                            intensity = values[index]
                            point_light = arnold.AiNode(ccp("point_light"))
                            arnold.AiNodeSetStr(point_light,
                                                ccp("name"),
                                                ccp(self.uniqueName(obj.name)))
                            arnold.AiNodeSetFlt(point_light, # default is 0.0
                                                ccp("radius"), radius)
                            m = obj.matrix_world.copy()
                            if group_matrix != None:
                                m = group_matrix * m
                            self.createMatrix(point_light, m)
                            # assume a material is in the first slot
                            if len(obj.material_slots) >= 1:
                                mats = obj.material_slots.items()
                                matName = mats[0][0]
                                shader, opaque = self.shaders[matName]
                                mat = bpy.data.materials[matName]
                                color = mat.diffuse_color
                                ##intensity = (mat.emit *
                                ##EMISSION_POINT_LIGHTS_FACTOR)
                            arnold.AiNodeSetRGB(point_light, ccp("color"),
                                                color.r,
                                                color.g,
                                                color.b)
                            arnold.AiNodeSetFlt(point_light,
                                                ccp("intensity"),
                                                intensity)
                        else:
                            # sphere
                            sphere = arnold.AiNode(ccp("sphere"))
                            arnold.AiNodeSetStr(sphere,
                                                ccp("name"),
                                                ccp(self.uniqueName(obj.name)))
                            arnold.AiNodeSetFlt(sphere, # default is 0.5
                                                ccp("radius"), 1.0)
                            m = obj.matrix_world.copy()
                            if group_matrix != None:
                                m = group_matrix * m
                            self.createMatrix(sphere, m)
                            self.connectArnoldNodeWithShaders(obj, sphere)
                    elif primType == "genbox":
                        # box
                        box = arnold.AiNode(ccp("box"))
                        arnold.AiNodeSetStr(box,
                                            ccp("name"),
                                            ccp(self.uniqueName(obj.name)))
                        arnold.AiNodeSetPnt(box,
                                            ccp("min"), 0.0, 0.0, 0.0)
                        arnold.AiNodeSetPnt(box,
                                            ccp("max"), 1.0, 1.0, 1.0)
                        m = obj.matrix_world.copy()
                        if group_matrix != None:
                            m = group_matrix * m
                        self.createMatrix(box, m)
                        self.connectArnoldNodeWithShaders(obj, box)
                elif obj.type == 'LAMP':
                    # lights
                    if data.type == 'AREA':
                        # disk_light (TODO: other area lights)
                        disk_light = arnold.AiNode(ccp("disk_light"))
                        arnold.AiNodeSetStr(disk_light,
                                            ccp("name"),
                                            ccp(self.uniqueName(obj.name)))
                        m = obj.matrix_world.copy()
                        if group_matrix != None:
                            m = group_matrix * m
                        self.createMatrix(disk_light, m)
                        color = data.color
                        arnold.AiNodeSetRGB(disk_light, ccp("color"),
                                            color.r,
                                            color.g,
                                            color.b)
                        arnold.AiNodeSetFlt(disk_light,
                                            ccp("intensity"),
                                            data.energy)
                        if data.shadow_method == 'RAY_SHADOW':
                            radius = 0.0
                            keys = data.keys()
                            values = data.values()
                            if 'disk_radius' in keys:
                                index = keys.index('disk_radius')
                                radius = values[index]
                            if radius > 0.0:
                                arnold.AiNodeSetFlt(disk_light,
                                                    ccp("radius"),
                                                    radius)
                                arnold.AiNodeSetInt(disk_light,
                                                    ccp("samples"),
                                                    data.shadow_ray_samples_x)
                        arnold.AiNodeSetBool(disk_light,
                                             ccp("affect_diffuse"),
                                             data.use_diffuse)
                        arnold.AiNodeSetBool(disk_light,
                                             ccp("affect_specular"),
                                             data.use_specular)
                    elif data.type == 'POINT':
                        # point_light
                        point_light = arnold.AiNode(ccp("point_light"))
                        arnold.AiNodeSetStr(point_light,
                                            ccp("name"),
                                            ccp(self.uniqueName(obj.name)))
                        # values: constant quadratic
                        if data.falloff_type == 'INVERSE_SQUARE':
                            arnold.AiNodeSetInt(point_light,
                                                ccp("decay_type"),
                                                1)
                        else: # 'CONSTANT'
                            arnold.AiNodeSetInt(point_light,
                                                ccp("decay_type"),
                                                0)
                        m = obj.matrix_world.copy()
                        if group_matrix != None:
                            m = group_matrix * m
                        self.createMatrix(point_light, m)
                        color = data.color
                        arnold.AiNodeSetRGB(point_light, ccp("color"),
                                            color.r,
                                            color.g,
                                            color.b)
                        arnold.AiNodeSetFlt(point_light,
                                            ccp("intensity"),
                                            data.energy)
                        if data.shadow_method == 'RAY_SHADOW':
                            radius = 0.0
                            keys = data.keys()
                            values = data.values()
                            if 'radius' in keys:
                                index = keys.index('radius')
                                radius = values[index]
                            if radius > 0.0:
                                arnold.AiNodeSetFlt(point_light,
                                                    ccp("radius"),
                                                    radius)
                                arnold.AiNodeSetInt(point_light,
                                                    ccp("samples"),
                                                    data.shadow_ray_samples)
                        arnold.AiNodeSetBool(point_light,
                                             ccp("affect_diffuse"),
                                             data.use_diffuse)
                        arnold.AiNodeSetBool(point_light,
                                             ccp("affect_specular"),
                                             data.use_specular)
                    elif data.type == 'SUN':
                        # distant_light
                        distant_light = arnold.AiNode(ccp("distant_light"))
                        arnold.AiNodeSetStr(distant_light,
                                            ccp("name"),
                                            ccp(self.uniqueName(obj.name)))
                        m = obj.matrix_world.copy()
                        if group_matrix != None:
                            m = group_matrix * m
                        self.createMatrix(distant_light, m)
                        color = data.color
                        arnold.AiNodeSetRGB(distant_light, ccp("color"),
                                            color.r,
                                            color.g,
                                            color.b)
                        arnold.AiNodeSetFlt(distant_light,
                                            ccp("intensity"),
                                            data.energy)
                        if data.shadow_method == 'RAY_SHADOW':
                            arnold.AiNodeSetBool(distant_light,
                                                 ccp("cast_shadows"),
                                                 True)
                            arnold.AiNodeSetInt(distant_light,
                                                ccp("samples"),
                                                data.shadow_ray_samples)
                        arnold.AiNodeSetBool(distant_light,
                                             ccp("affect_diffuse"),
                                             data.use_diffuse)
                        arnold.AiNodeSetBool(distant_light,
                                             ccp("affect_specular"),
                                             data.use_specular)
                    elif data.type == 'SPOT':
                        # spot_light
                        spot_light = arnold.AiNode(ccp("spot_light"))
                        arnold.AiNodeSetStr(spot_light,
                                            ccp("name"),
                                            ccp(self.uniqueName(obj.name)))
                        arnold.AiNodeSetFlt(spot_light,
                                            ccp("cone_angle"),
                                            math.degrees(data.spot_size))
                        # values: constant quadratic
                        if data.falloff_type == 'INVERSE_SQUARE':
                            arnold.AiNodeSetInt(spot_light,
                                                ccp("decay_type"),
                                                1)
                        else: # 'CONSTANT'
                            arnold.AiNodeSetInt(spot_light,
                                                ccp("decay_type"),
                                                0)
                        m = obj.matrix_world.copy()
                        if group_matrix != None:
                            m = group_matrix * m
                        self.createMatrix(spot_light, m)
                        color = data.color
                        arnold.AiNodeSetRGB(spot_light, ccp("color"),
                                            color.r,
                                            color.g,
                                            color.b)
                        arnold.AiNodeSetFlt(spot_light,
                                            ccp("intensity"),
                                            data.energy)
                        if data.shadow_method == 'RAY_SHADOW':
                            radius = 0.0
                            keys = data.keys()
                            values = data.values()
                            if 'radius' in keys:
                                index = keys.index('radius')
                                radius = values[index]
                            if radius > 0.0:
                                arnold.AiNodeSetFlt(spot_light,
                                                    ccp("radius"),
                                                    radius)
                                arnold.AiNodeSetInt(spot_light,
                                                    ccp("samples"),
                                                    data.shadow_ray_samples)
                        arnold.AiNodeSetBool(spot_light,
                                             ccp("affect_diffuse"),
                                             data.use_diffuse)
                        arnold.AiNodeSetBool(spot_light,
                                             ccp("affect_specular"),
                                             data.use_specular)
                    else:
                        print('TODO: %s(%s)' % (obj.name, obj.type))
                elif obj.type == 'MESH':
                    if self.isUsingDuplication(obj):
                        if obj.dupli_type == 'VERTS':
                            # for now we expect exactly one child
                            if len(obj.children) != 1:
                                print("TODO: dupli_type('%s') with %s " %
                                      (obj.dupli_type, len(obj.children)) +
                                      "children")
                            else:
                                child = obj.children[0]
                                if self.options.use_instancer:
                                    self.exportInstances(obj, child, scene)
                                else:
                                    # get access to matrices once
                                    pm = obj.matrix_world.copy()
                                    pmi = obj.matrix_world.copy()
                                    pmi.invert()
                                    # for each vertex ...
                                    for vidx in range(len(obj.data.vertices)):
                                        v = obj.data.vertices[vidx]
                                        m = pm * pm.Translation(v.co) * pmi
                                        self.exportObject(child, scene, m)
                        else:
                            print("TODO: dupli_type = '%s'" % obj.dupli_type)
                        return
                    # first check how many faces are smooth
                    smooth_faces = 0
                    if self.useBmesh:
                        for polygon in data.polygons:
                            if polygon.use_smooth:
                                smooth_faces += 1
                    else:
                        for face in data.faces:
                            if face.use_smooth:
                                smooth_faces += 1
                    # then check for uv-coordinates
                    if (data.uv_textures.active and
                        data.uv_textures.active.data):
                        if self.useBmesh:
                            uv_layer = data.uv_layers.active.data
                        else:
                            uv_layer = data.uv_textures.active.data
                    else:
                        uv_layer = None
                    if smooth_faces == 0:
                        # flat
                        if uv_layer:
                            gm = group_matrix
                            uvs = uv_layer
                            m = self.exportMeshWithFaceNormalsAndUvs(obj,
                                                                     data,
                                                                     uvs,
                                                                     gm)
                            polymesh = m
                        else:
                            m = self.exportMeshWithFaceNormals(obj, data,
                                                               group_matrix)
                            polymesh = m
                    elif ((self.useBmesh and
                           smooth_faces == len(data.polygons)) or
                          (not self.useBmesh and
                           smooth_faces == len(data.faces))):
                        # smooth
                        if uv_layer:
                            gm = group_matrix
                            uvs = uv_layer
                            m = self.exportMeshWithVertexNormalsAndUvs(obj,
                                                                       data,
                                                                       uvs,
                                                                       gm)
                            polymesh = m
                        else:
                            m = self.exportMeshWithVertexNormals(obj, data,
                                                                 group_matrix)
                            polymesh = m
                    else:
                        # both
                        gm = group_matrix
                        m = self.exportMeshWithFaceAndVertexNormals(obj, data,
                                                                    gm)
                        polymesh = m
                        # TODO: uv_layer
                    if self.options.global_surface_shader == 'none':
                        if len(obj.material_slots) > 0:
                            numMaterials = len(obj.material_slots.items())
                            if numMaterials == 1:
                                self.connectArnoldNodeWithShaders(obj,
                                                                  polymesh)
                            else:
                                shaders = arnold.AiArray(numMaterials,
                                                         1,
                                                         arnold.AI_TYPE_NODE)
                                allOpaque = True
                                for i in range(numMaterials):
                                    mats = obj.material_slots.items()
                                    matName = mats[i][0]
                                    if matName == "":
                                        matName = "DefaultShader"
                                    shader, opaque = self.shaders[matName]
                                    if shader == None:
                                        mat = bpy.data.materials[matName]
                                        if (self.light_counter or
                                            self.emissive_objects):
                                            standard = self.getStandardMaterial(matName, mat)
                                            self.exportStandardShader(standard)
                                        else:
                                            utility = self.getUtilityMaterial(matName, mat)
                                            self.exportUtilityShader(utility)
                                        shader, opaque = self.shaders[matName]
                                    allOpaque = allOpaque and opaque
                                    arnold.AiArraySetPtr(shaders, i, shader)
                                arnold.AiNodeSetArray(polymesh,
                                                      ccp("shader"),
                                                      shaders)
                                arnold.AiNodeSetBool(polymesh,
                                                     ccp("opaque"),
                                                     allOpaque)
                        else:
                            if (self.light_counter or self.emissive_objects):
                                matName = "DefaultShader"
                                shader, opaque = self.shaders[matName]
                                arnold.AiNodeSetPtr(polymesh,
                                                    ccp("shader"),
                                                    shader)
                                arnold.AiNodeSetBool(polymesh,
                                                     ccp("opaque"),
                                                     opaque)
                    else:
                        self.connectArnoldNodeWithShaders(obj, polymesh)
                elif obj.type == 'CURVE':
                    numCurves = len(obj.data.splines)
                    if numCurves:
                        curves = arnold.AiNode(ccp("curves"))
                        arnold.AiNodeSetStr(curves,
                                            ccp("name"),
                                            ccp(self.uniqueName(obj.name)))
                        matName = "DefaultShader"
                        shader, opaque = self.shaders[matName]
                        # calculate number of points
                        ptCounter = 0
                        cvCounter = 0
                        for curveIdx in range(numCurves):
                            spline = obj.data.splines[curveIdx]
                            if spline.type == 'BEZIER':
                                cvCounter += 1
                                numPoints = len(spline.bezier_points)
                                for ptIdx in range(numPoints):
                                    if ptIdx == 0:
                                        ptCounter += 2
                                    elif ptIdx == numPoints - 1:
                                        ptCounter += 2
                                    else:
                                        ptCounter += 3
                        # set number of points
                        num_points = arnold.AiArrayAllocate(cvCounter,
                                                            1,
                                                            arnold.AI_TYPE_UINT)
                        points = arnold.AiArrayAllocate(ptCounter,
                                                        1,
                                                        arnold.AI_TYPE_POINT)
                        # use one radius for all curves !!!
                        radius = arnold.AiArrayAllocate(1,
                                                        1,
                                                        arnold.AI_TYPE_FLOAT)
                        arnold.AiArraySetFlt(radius, 0, 0.005)
                        ptCounter = 0
                        for curveIdx in range(numCurves):
                            spline = obj.data.splines[curveIdx]
                            if spline.type == 'BEZIER':
                                numPoints = len(spline.bezier_points)
                                arnold.AiArraySetUInt(num_points, curveIdx,
                                                      numPoints * 3 - 2)
                                for ptIdx in range(numPoints):
                                    pt = spline.bezier_points[ptIdx]
                                    if ptIdx == 0:
                                        print("point[%s] = %s" %
                                              (ptCounter, pt.co))
                                        arnold.AiArraySetPnt(points,
                                                             ptCounter,
                                                             arnold.AtPoint(pt.co[0], pt.co[1], pt.co[2]))
                                        ptCounter += 1
                                        print("point[%s] = %s" %
                                              (ptCounter, pt.handle_right))
                                        arnold.AiArraySetPnt(points,
                                                             ptCounter,
                                                             arnold.AtPoint(pt.handle_right[0], pt.handle_right[1], pt.handle_right[2]))
                                        ptCounter += 1
                                    elif ptIdx == numPoints - 1:
                                        print("point[%s] = %s" %
                                              (ptCounter, pt.handle_left))
                                        arnold.AiArraySetPnt(points,
                                                             ptCounter,
                                                             arnold.AtPoint(pt.handle_left[0], pt.handle_left[1], pt.handle_left[2]))
                                        ptCounter += 1
                                        print("point[%s] = %s" %
                                              (ptCounter, pt.co))
                                        arnold.AiArraySetPnt(points,
                                                             ptCounter,
                                                             arnold.AtPoint(pt.co[0], pt.co[1], pt.co[2]))
                                        ptCounter += 1
                                    else:
                                        print("point[%s] = %s" %
                                              (ptCounter, pt.handle_left))
                                        arnold.AiArraySetPnt(points,
                                                             ptCounter,
                                                             arnold.AtPoint(pt.handle_left[0], pt.handle_left[1], pt.handle_left[2]))
                                        ptCounter += 1
                                        print("point[%s] = %s" %
                                              (ptCounter, pt.co))
                                        arnold.AiArraySetPnt(points,
                                                             ptCounter,
                                                             arnold.AtPoint(pt.co[0], pt.co[1], pt.co[2]))
                                        ptCounter += 1
                                        print("point[%s] = %s" %
                                              (ptCounter, pt.handle_right))
                                        arnold.AiArraySetPnt(points,
                                                             ptCounter,
                                                             arnold.AtPoint(pt.handle_right[0], pt.handle_right[1], pt.handle_right[2]))
                                        ptCounter += 1
                        else:
                            print("TODO: curve(\"%s\") of type %s" %
                                  (obj.name, spline.type))
                        arnold.AiNodeSetArray(curves,
                                              ccp("num_points"),
                                              num_points)
                        arnold.AiNodeSetArray(curves,
                                              ccp("points"),
                                              points)
                        arnold.AiNodeSetArray(curves,
                                              ccp("radius"),
                                              radius)
                        arnold.AiNodeSetPtr(curves,
                                            ccp("shader"),
                                            shader)
                        m = obj.matrix_world.copy()
                        if group_matrix != None:
                            m = group_matrix * m
                        self.createMatrix(curves, m)
                elif obj.type == 'SURFACE':
                    nurbs = self.exportNurbsSurface(obj, data)
                    if nurbs == None:
                        # TMP
                        print("INFO: ignore %s(%s)" % (obj.name, obj.type))
                        # TMP
                    else:
                        if self.options.global_surface_shader == 'none':
                            if len(obj.material_slots) == 1:
                                self.connectArnoldNodeWithShaders(obj, nurbs)
                else:
                    print('TODO: %s(%s)' % (obj.name, obj.type))
            else:
                if obj.type != 'EMPTY' and obj.type != 'CAMERA':
                    print("INFO: ignore %s(%s)" % (obj.name, obj.type)) # TMP

    def exportStandardShader(self, standard):
        image = None
        # standard
        name = standard[0]
        Kd = standard[1] * DIFF_DIM_LEVEL
        Kd_color = standard[2]
        diffuse_roughness = standard[3]
        Ks = standard[4] * SPEC_DIM_LEVEL
        Ks_color = standard[5]
        Kr = standard[6]
        Kr_color = standard[7]
        Kt = standard[8]
        Kt_color = standard[9]
        IOR = standard[10]
        emission = standard[11] * EMISSION_FACTOR
        emission_color = standard[12]
        textures = standard[13]
        opaque = standard[14]
        if self.options.use_global_illum:
            # make sure all factors are energy conserving
            Ksum = Kd + Ks + Kr + Kt
            if Ksum > 1.0:
                Kd = Kd / Ksum
                Ks = Ks / Ksum
                Kr = Kr / Ksum
                Kt = Kt / Ksum
        uName = self.uniqueName(name)
        # textures["normal"]?
        normal_tex_type = None
        normal_tex_name = None
        try:
            normal_tex = textures["normal"]
        except KeyError:
            pass
        else:
            normal_tex_type = normal_tex[0] # e.g. "noise"
            normal_tex_name = normal_tex[1] # slot.name
        # textures["diffuse"]?
        try:
            diffuse_tex = textures["diffuse"]
        except KeyError:
            # standard
            shader = arnold.AiNode(ccp("standard"))
            if normal_tex_type == "noise":
                # noise
                noise_shader = arnold.AiNode(ccp("noise"))
                arnold.AiNodeSetStr(noise_shader,
                                    ccp("name"),
                                    ccp(normal_tex_name))
                arnold.AiNodeSetVec(noise_shader, ccp("scale"),
                                    100.0, 100.0, 100.0) # TODO
                # bump2d
                bump2d_shader = arnold.AiNode(ccp("bump2d"))
                arnold.AiNodeSetStr(bump2d_shader,
                                    ccp("name"),
                                    ccp(uName))
                arnold.AiNodeSetFlt(bump2d_shader, ccp("bump_height"),
                                    0.01) # TODO
                # link shaders
                arnold.AiNodeLink(noise_shader, ccp("bump_map"), bump2d_shader)
                arnold.AiNodeLink(shader, ccp("shader"), bump2d_shader)
                normal_shader = bump2d_shader
                # standard (name)
                arnold.AiNodeSetStr(shader,
                                    ccp("name"),
                                    ccp(uName + "_std"))
            else:
                arnold.AiNodeSetStr(shader,
                                    ccp("name"),
                                    ccp(uName))
            arnold.AiNodeSetFlt(shader,
                                ccp("Kd"),
                                Kd)
            arnold.AiNodeSetRGB(shader,
                                ccp("Kd_color"),
                                Kd_color[0],
                                Kd_color[1],
                                Kd_color[2])
        else:
            paths = bpy.utils.blend_paths()
            if diffuse_tex in paths:
                index = paths.index(diffuse_tex)
                abs_paths = bpy.utils.blend_paths(absolute = True)
                abs_path = abs_paths[index]
                # image
                image = arnold.AiNode(ccp("image"))
                arnold.AiNodeSetStr(image, ccp("name"),
                                    ccp(uName + "_diff_tex"))
                arnold.AiNodeSetStr(image, ccp("filename"), ccp(abs_path))
                # shader
                shader = arnold.AiNode(ccp("standard"))
                arnold.AiNodeSetStr(shader,
                                    ccp("name"),
                                    ccp(uName))
                arnold.AiNodeSetFlt(shader,
                                    ccp("Kd"),
                                    Kd)
                # using image
                arnold.AiNodeLink(image, ccp("Kd_color"), shader)
            else:
                print("WARNING: couldn't find '%s' in paths" % diffuse_tex)
                # shader
                shader = arnold.AiNode(ccp("standard"))
                arnold.AiNodeSetStr(shader,
                                    ccp("name"),
                                    ccp(uName))
                arnold.AiNodeSetFlt(shader,
                                    ccp("Kd"),
                                    Kd)
                arnold.AiNodeSetRGB(shader,
                                    ccp("Kd_color"),
                                    Kd_color[0],
                                    Kd_color[1],
                                    Kd_color[2])
        arnold.AiNodeSetFlt(shader,
                            ccp("diffuse_roughness"),
                            diffuse_roughness)
        arnold.AiNodeSetFlt(shader,
                            ccp("Ks"),
                            Ks)
        arnold.AiNodeSetRGB(shader,
                            ccp("Ks_color"),
                            Ks_color[0],
                            Ks_color[1],
                            Ks_color[2])
        arnold.AiNodeSetFlt(shader,
                            ccp("specular_roughness"),
                            0.1) # TODO: use something from Blender?
        arnold.AiNodeSetFlt(shader,
                            ccp("Kr"),
                            Kr)
        arnold.AiNodeSetRGB(shader,
                            ccp("Kr_color"),
                            Kr_color[0],
                            Kr_color[1],
                            Kr_color[2])
        arnold.AiNodeSetFlt(shader,
                            ccp("Kt"),
                            Kt)
        arnold.AiNodeSetRGB(shader,
                            ccp("Kt_color"),
                            Kt_color[0],
                            Kt_color[1],
                            Kt_color[2])
        arnold.AiNodeSetFlt(shader,
                            ccp("IOR"),
                            IOR)
        arnold.AiNodeSetFlt(shader,
                            ccp("emission"),
                            emission)
        # textures["diffuse"]?
        try:
            diffuse_tex = textures["diffuse"]
        except KeyError:
            arnold.AiNodeSetRGB(shader,
                                ccp("emission_color"),
                                emission_color[0],
                                emission_color[1],
                                emission_color[2])
        else:
            # using image
            arnold.AiNodeLink(image, ccp("emission_color"), shader)
        if normal_tex_type != None:
            self.shaders[name] = [normal_shader, opaque]
        else:
            self.shaders[name] = [shader, opaque]
        return image

    def exportUtilityShader(self, utility):
        image = None
        # default values
        name = utility[0]
        color_mode = utility[1]
        shade_mode = utility[2]
        color = utility[3]
        opacity = utility[4]
        textures = utility[5]
        opaque = utility[6]
        uName = self.uniqueName(name)
        # textures["diffuse"]?
        try:
            diffuse_tex = textures["diffuse"]
        except KeyError:
            shader = arnold.AiNode(ccp("utility"))
            arnold.AiNodeSetStr(shader,
                                ccp("name"),
                                ccp(uName))
            arnold.AiNodeSetInt(shader,
                                ccp("color_mode"),
                                color_mode)
            arnold.AiNodeSetInt(shader,
                                ccp("shade_mode"),
                                shade_mode)
            arnold.AiNodeSetRGB(shader,
                                ccp("color"),
                                color[0], color[1], color[2])
        else:
            paths = bpy.utils.blend_paths()
            if diffuse_tex in paths:
                index = paths.index(diffuse_tex)
                abs_paths = bpy.utils.blend_paths(absolute = True)
                abs_path = abs_paths[index]
                # image
                image = arnold.AiNode(ccp("image"))
                arnold.AiNodeSetStr(image, ccp("name"),
                                    ccp(uName + "_diff_tex"))
                arnold.AiNodeSetStr(image, ccp("filename"), ccp(abs_path))
                # shader
                shader = arnold.AiNode(ccp("utility"))
                arnold.AiNodeSetStr(shader,
                                    ccp("name"),
                                    ccp(uName))
                arnold.AiNodeSetInt(shader,
                                    ccp("color_mode"),
                                    color_mode)
                arnold.AiNodeSetInt(shader,
                                    ccp("shade_mode"),
                                    shade_mode)
                # using image
                arnold.AiNodeLink(image, ccp("color"), shader)
            else:
                shader = arnold.AiNode(ccp("utility"))
                arnold.AiNodeSetStr(shader,
                                    ccp("name"),
                                    ccp(uName))
                arnold.AiNodeSetInt(shader,
                                    ccp("color_mode"),
                                    color_mode)
                arnold.AiNodeSetInt(shader,
                                    ccp("shade_mode"),
                                    shade_mode)
                arnold.AiNodeSetRGB(shader,
                                    ccp("color"),
                                    color[0], color[1], color[2])
        arnold.AiNodeSetFlt(shader,
                            ccp("opacity"),
                            opacity)
        self.shaders[name] = [shader, opaque]
        return image

    def getMeshLight(self, matName, mat):
        textures_found = 0
        textures = {}
        if mat:
            # check for textures
            for index in list(range(len(mat.texture_slots))):
                slot = mat.texture_slots[index]
                if slot:
                    texture = slot.texture
                    if slot.texture_coords == 'UV':
                        if texture.type == 'IMAGE':
                            image = texture.image
                            src = image.filepath
                            if slot.use_map_color_diffuse:
                                # diffuse texture
                                textures["diffuse"] = src
                                textures_found += 1
        # default values
        name = matName
        color = [1.0, 1.0, 1.0]
        intensity = 1
        exposure = 0
        samples = 1
        if mat != None:
            color = mat.diffuse_color
            if mat.emit > 0.0:
                intensity = mat.emit
                keys = mat.keys()
                values = mat.values()
                if 'intensity' in keys:
                    index = keys.index('intensity')
                    intensity = intensity * values[index]
                if 'exposure' in keys:
                    index = keys.index('exposure')
                    exposure = values[index]
                if 'samples' in keys:
                    index = keys.index('samples')
                    samples = values[index]
        # return values in a list
        meshLight = [name,
                     color,
                     intensity,
                     exposure,
                     samples,
                     textures]
        return meshLight

    def getStandardMaterial(self, matName, mat = None):
        textures_found = 0
        textures = {}
        if mat:
            # check for textures
            for index in list(range(len(mat.texture_slots))):
                slot = mat.texture_slots[index]
                if slot:
                    texture = slot.texture
                    if slot.texture_coords == 'UV':
                        if texture.type == 'IMAGE':
                            image = texture.image
                            src = image.filepath
                            if slot.use_map_color_diffuse:
                                # diffuse texture
                                textures["diffuse"] = src
                                textures_found += 1
                        elif texture.type == 'NOISE':
                            if slot.use_map_normal:
                                # noise normal map
                                textures["normal"] = ["noise", slot.name]
        # default values
        name = matName
        Kd = 0.7
        Kd_color = [1.0, 1.0, 1.0]
        diffuse_roughness = 0.0
        Ks = 0.0
        Ks_color = [1.0, 1.0, 1.0]
        Kr = 0.0
        Kr_color = [1.0, 1.0, 1.0]
        Kt = 0.0
        Kt_color = [1.0, 1.0, 1.0]
        IOR = 1.0
        emission = 0.0
        emission_color = [1.0, 1.0, 1.0]
        opaque = True
        if mat != None:
            Kd = mat.diffuse_intensity
            Kd_color = mat.diffuse_color
            Ks = mat.specular_intensity
            Ks_color = mat.specular_color
            if mat.use_transparency and mat.transparency_method == 'RAYTRACE':
                Kt = 1.0 - mat.alpha
                maxColor = Kd_color[0]
                if Kd_color[1] > maxColor:
                    maxColor = Kd_color[1]
                if Kd_color[2] > maxColor:
                    maxColor = Kd_color[2]
                Kt_color = [Kd_color[0] / maxColor,
                            Kd_color[1] / maxColor,
                            Kd_color[2] / maxColor]
                IOR = mat.raytrace_transparency.ior
                opaque = False
            if mat.raytrace_mirror.use:
                Kr = mat.raytrace_mirror.reflect_factor
                Kr_color = mat.mirror_color
            if mat.emit > 0.0:
                emission = mat.emit
                keys = mat.keys()
                values = mat.values()
                if 'emission' in keys:
                    index = keys.index('emission')
                    emission = emission * values[index]
                emission_color = Kd_color
        # return values in a list
        standard = [name,
                    Kd, Kd_color, diffuse_roughness,
                    Ks, Ks_color,
                    Kr, Kr_color, Kt, Kt_color, IOR,
                    emission, emission_color,
                    textures, opaque]
        return standard

    def getUtilityMaterial(self, matName, mat = None,
                           cm # color_mode
                           = None,
                           sm # shade_mode
                           = None):
        textures_found = 0
        textures = {}
        if mat:
            # check for textures
            for index in list(range(len(mat.texture_slots))):
                slot = mat.texture_slots[index]
                if slot:
                    texture = slot.texture
                    if slot.texture_coords == 'UV':
                        if texture.type == 'IMAGE':
                            image = texture.image
                            src = image.filepath
                            if slot.use_map_color_diffuse:
                                # diffuse texture
                                textures["diffuse"] = src
                                textures_found += 1
        # default values
        name = matName
        if cm == None:
            color_mode = 0 # color ng ns n bary uv u v dpdu dpdv p
                           # prims uniformid wire polywire obj objwire
                           # edgelength floatgrid reflectline bad_uvs
                           # nlights id bumpdiff
        else:
            color_mode = cm;
        if sm == None:
            shade_mode = 0 # ndoteye lambert flat ambocc plastic
        else:
            shade_mode = sm
        color = [1.0, 1.0, 1.0]
        opacity = 1.0
        opaque = True
        if mat != None:
            color = mat.diffuse_color
            if mat.use_transparency:
                opacity = mat.alpha
                opaque = False
        else: # use global name
            if name == 'show_ndoteye' or name == 'none':
                color_mode = 0 # color
            elif name == 'show_uv':
                color_mode = 5 # uv
            elif name == 'show_u':
                color_mode = 6 # u
            elif name == 'show_v':
                color_mode = 7 # v
            elif name == 'show_bad_uvs':
                color_mode = 18 # bad_uvs
            elif name == 'show_Ng':
                color_mode = 1 # ng
            elif name == 'show_Ns':
                color_mode = 2 # ns
            elif name == 'show_wire':
                color_mode = 13 # wire
            elif name == 'show_polywire':
                color_mode = 14 # polywire
        # return values in a list
        utility = [name, color_mode, shade_mode, color, opacity, textures,
                   opaque]
        return utility

    def isAnimated(self, obj):
        if obj.animation_data != None:
            return True
        elif obj.parent != None:
            return self.isAnimated(obj.parent)
        return False

    def isPrimitive(self, obj):
        isPrimitive = False
        primType = None
        if (self.nameStartsWith(obj.data.name, "Cone")):
            isPrimitive = True
            primType = "Cone"
        elif (self.nameStartsWith(obj.data.name, "Cylinder")):
            isPrimitive = True
            primType = "Cylinder"
        elif (self.nameStartsWith(obj.data.name, "Disk")):
            isPrimitive = True
            primType = "Disk"
        elif (self.nameStartsWith(obj.data.name, "Ring")):
            isPrimitive = True
            primType = "Ring"
        elif (self.nameStartsWith(obj.data.name, "Sphere")):
            isPrimitive = True
            primType = "Sphere"
        elif (self.nameStartsWith(obj.data.name, "genbox")):
            isPrimitive = True
            primType = "genbox"
            # check for custom "radius" property
            keys = obj.keys()
            values = obj.values()
            if 'radius' in keys:
                primType += " -r"
                print(primType)
        return isPrimitive, primType

    def isTransparent(self, material):
        transparent = False
        if material.use_transparency:
            transparent = True
        return transparent

    def isUsingDuplication(self, obj):
        if obj.dupli_type != 'NONE':
            return True
        else:
            return False

    def isVisible(self, obj, scene):
        visible = False
        for i in range(20):
            if obj.layers[i]:
                if scene.layers[i]:
                    visible = True
        return visible

    def nameStartsWith(self, name, pattern):
        retBool = False
        if len(name) >= len(pattern):
            words = name.split(pattern)
            if words[0] == "":
                retBool = True
        return retBool

    def uniqueName(self, proposal):
        return self.uniqueNames.uniqueName(proposal)

    def writer(self, filename):
        # make sure that everything is unpacked
        bpy.ops.file.unpack_all(method = 'USE_LOCAL')
        # delete .ass file (if it exists already)
        if os.path.exists(filename):
            os.remove(filename)
        # AiSetAppString
        appString = "Blender %s.%s.%s" % bpy.app.version
        sb = ccp(appString)
        arnold.AiSetAppString(sb)
        # AiBegin
        arnold.AiBegin()
        # count lights (and detect sun)
        scene = bpy.context.scene
        use_sky = False
        self.light_counter = 0
        for obj in scene.objects:
            if (self.isVisible(obj, scene)):
                data = obj.data
                if obj.type == 'LAMP':
                    if data.type == 'AREA':
                        self.light_counter += 1
                    elif data.type == 'POINT':
                        self.light_counter += 1
                    elif data.type == 'SUN':
                        self.light_counter += 1
                        if data.sky.use_sky:
                            # TMP
                            m = obj.matrix_world.copy()
                            sunPos, rot, scale = m.decompose()
                            constraint = obj.constraints[0]
                            if constraint.type == 'TRACK_TO':
                                compass = constraint.target
                                m = compass.matrix_world.copy()
                                compassPos, rot, scale = m.decompose()
                                sun_dir = [sunPos[0] - compassPos[0],
                                          sunPos[1] - compassPos[1],
                                          sunPos[2] - compassPos[2]]
                            # TMP
                            use_sky = True
                            sky_color = [0.29, 0.478, 0.871] # blueish
                            sky_intensity = 1.0 # default
                            keys = data.keys()
                            values = data.values()
                            if 'sky_intensity' in keys:
                                index = keys.index('sky_intensity')
                                sky_intensity = values[index]
                    elif data.type == 'SPOT':
                        self.light_counter += 1
                    else:
                        print("WARNING: %s(%s) didn't count as light" %
                              (obj.name, obj.type))
        # options
        options = arnold.AiUniverseGetOptions()
        # procedural_searchpath
        procedural_searchpath = os.getenv("HOME")
        procedurals = "Graphics/Rendering/Arnold/procedurals/current" # TMP
        procedural_searchpath = os.path.join(procedural_searchpath,
                                             procedurals)
        arnold.AiNodeSetStr(options, ccp("procedural_searchpath"),
                            ccp(procedural_searchpath))
        # shader_searchpath
        shader_searchpath = os.getenv("HOME")
        shaders = "Graphics/Rendering/Arnold/shaders/current" # TMP
        shader_searchpath = os.path.join(shader_searchpath,
                                         shaders)
        arnold.AiNodeSetStr(options, ccp("shader_searchpath"),
                            ccp(shader_searchpath))
        # gamma (change default of 1.0)
        arnold.AiNodeSetFlt(options, ccp("texture_gamma"), 2.2)
        # anti-aliasing
        if scene.render.use_antialiasing:
            if scene.render.antialiasing_samples == '5':
                AA_samples = 5
            elif scene.render.antialiasing_samples == '8':
                AA_samples = 8
            elif scene.render.antialiasing_samples == '11':
                AA_samples = 11
            elif scene.render.antialiasing_samples == '16':
                AA_samples = 16
            arnold.AiNodeSetInt(options,
                                ccp("AA_samples"),
                                arnold.AtInt32(AA_samples))
        # change name
        arnold.AiNodeSetStr(options,
                            ccp("name"),
                            ccp(self.uniqueName("Options")))
        # set outputs
        output = "RGBA RGBA Filter Driver" # see below
        outputs = arnold.AiArray(1, 1, arnold.AI_TYPE_STRING,
                                 ccp(output))
        arnold.AiNodeSetArray(options, ccp("outputs"), outputs)
        # set render resolution
        percent = scene.render.resolution_percentage / 100.0
        xresolution = int(scene.render.resolution_x * percent)
        yresolution = int(scene.render.resolution_y * percent)
        arnold.AiNodeSetInt(options,
                            ccp("xres"), arnold.AtInt32(xresolution))
        arnold.AiNodeSetInt(options,
                            ccp("yres"), arnold.AtInt32(yresolution))
        # border for region rendering
        if scene.render.use_border:
            region_min_x = int(scene.render.border_min_x * xresolution)
            region_max_x = int(scene.render.border_max_x * xresolution)
            region_min_y = int((1.0-scene.render.border_max_y) * yresolution)
            region_max_y = int((1.0-scene.render.border_min_y) * yresolution)
            arnold.AiNodeSetInt(options,
                                ccp("region_min_x"), region_min_x)
            arnold.AiNodeSetInt(options,
                                ccp("region_min_y"), region_min_y)
            arnold.AiNodeSetInt(options,
                                ccp("region_max_x"), region_max_x)
            arnold.AiNodeSetInt(options,
                                ccp("region_max_y"), region_max_y)
        # bucket related
        arnold.AiNodeSetInt(options,
                            ccp("bucket_size"),
                            64)
        arnold.AiNodeSetStr(options,
                            ccp("bucket_scanning"),
                            ccp("spiral"))
        # background?
        if use_sky:
            if 0:
                sky = arnold.AiNode(ccp("sky"))
                arnold.AiNodeSetStr(sky, ccp("name"),
                                    ccp(self.uniqueName("Sky")))
                arnold.AiNodeSetRGB(sky, ccp("color"),
                                    sky_color[0], sky_color[1], sky_color[2])
                arnold.AiNodeSetFlt(sky, ccp("intensity"), sky_intensity)
                arnold.AiNodeSetPtr(options, ccp("background"), sky)
            else:
                ext = "dylib"
                if platform.system() == "Linux":
                    ext = "so"
                arnold.AiLoadPlugin(ccp("%s/aaPhysicalSky.%s" %
                                        (shader_searchpath, ext)))
                sky = arnold.AiNode(ccp("aaPhysicalSky"))
                if sky != None:
                    arnold.AiNodeSetStr(sky, ccp("name"),
                                        ccp(self.uniqueName("PhysicalSky")))
                    arnold.AiNodeSetBool(sky, ccp("y_is_up"), False)
                    arnold.AiNodeSetFlt(sky, ccp("turbidity"), 2.0)
                    arnold.AiNodeSetFlt(sky, ccp("sun_dir_x"), sun_dir[0])
                    arnold.AiNodeSetFlt(sky, ccp("sun_dir_y"), sun_dir[1])
                    arnold.AiNodeSetFlt(sky, ccp("sun_dir_z"), sun_dir[2])
                    arnold.AiNodeSetRGBA(sky, ccp("ground_color"),
                                         0.28, 0.18, 0.12, 1.0)
                    arnold.AiNodeSetFlt(sky, ccp("sun_intensity"),
                                        10.0 * sky_intensity)
                    arnold.AiNodeSetPtr(options, ccp("atmosphere"), sky)
                    arnold.AiNodeSetPtr(options, ccp("background"), sky)
                else:
                    print("WARNING: %s" %
                          "'arnold.AiNode(ccp(\"aaPhysicalSky\"))' failed")
        else:
            if scene.world:
                world = scene.world
                texture = world.texture_slots[0] # try first texture found
                if texture != None and texture.texture.type == 'IMAGE':
                    blendImage = texture.texture.image
                    if blendImage.source == 'FILE':
                        hdrFilepath = blendImage.filepath
                        paths = bpy.utils.blend_paths()
                        if hdrFilepath in paths:
                            index = paths.index(hdrFilepath)
                            abs_paths = bpy.utils.blend_paths(absolute = True)
                            abs_path = abs_paths[index]
                            # image shader
                            image = arnold.AiNode(ccp("image"))
                            arnold.AiNodeSetStr(image, ccp("name"),
                                                ccp("hdr_tex"))
                            arnold.AiNodeSetStr(image, ccp("filename"),
                                                ccp(abs_path))
                            # sky node (background)
                            sky = arnold.AiNode(ccp("sky"))
                            arnold.AiNodeSetStr(sky, ccp("name"),
                                                ccp(self.uniqueName("Sky")))
                            arnold.AiNodeLink(image, ccp("color"), sky)
                            intensity = 1.0 # TODO: adjust in Blender?
                            arnold.AiNodeSetFlt(sky,
                                                ccp("intensity"),
                                                intensity)
                            visibility = (arnold.AI_RAY_ALL &
                                          ~arnold.AI_RAY_DIFFUSE &
                                          ~arnold.AI_RAY_GLOSSY &
                                          ~arnold.AI_RAY_REFLECTED &
                                          ~arnold.AI_RAY_REFRACTED &
                                          ~arnold.AI_RAY_SHADOW)
                            arnold.AiNodeSetInt(sky,
                                                ccp("visibility"),
                                                visibility)
# bpy.data.worlds['World'].node_tree.nodes['Environment Texture'].projection
                            # 'EQUIRECTANGULAR'
                            # enum values: mirrored_ball angular latlong cubic
                            latlong = 2 # TODO
                            arnold.AiNodeSetInt(sky, ccp("format"), latlong)
                            arnold.AiNodeSetFlt(sky, ccp("Z_angle"), 180.0)
                            arnold.AiNodeSetVec(sky, ccp("Y"), 0.0, 0.0, 1.0)
                            arnold.AiNodeSetVec(sky, ccp("Z"), 0.0, -1.0, 0.0)
                            # link sky (background) to options
                            arnold.AiNodeSetPtr(options, ccp("background"), sky)
                            # skydome_light (using same HDR)
                            skydome_light = arnold.AiNode(ccp("skydome_light"))
                            arnold.AiNodeSetStr(skydome_light, ccp("name"),
                                                ccp(self.uniqueName("Skydome")))
                            resolution = texture.texture.image.size[0] # x-res
                            arnold.AiNodeSetInt(skydome_light,
                                                ccp("resolution"),
                                                resolution)
                            # enum values: mirrored_ball angular latlong cubic
                            arnold.AiNodeSetInt(skydome_light, ccp("format"),
                                                latlong) # TODO
                            m = [[-1.0, 0.0, 0.0, 0.0],
                                 [0.0, 0.0, 1.0, 0.0],
                                 [0.0, 1.0, 0.0, 0.0],
                                 [0.0, 0.0, 0.0, 1.0]]
                            self.createMatrix(skydome_light, m)
                            arnold.AiNodeLink(image, ccp("color"),
                                              skydome_light)
                            self.light_counter += 1
        # GI parameters
        if self.options.use_global_illum:
            arnold.AiNodeSetInt(options,
                                ccp("GI_diffuse_depth"),
                                arnold.AtInt32(3))
            arnold.AiNodeSetInt(options,
                                ccp("GI_refraction_depth"),
                                arnold.AtInt32(6))
            arnold.AiNodeSetInt(options,
                                ccp("GI_reflection_depth"),
                                arnold.AtInt32(6))
            arnold.AiNodeSetInt(options,
                                ccp("GI_total_depth"),
                                arnold.AtInt32(12))
            arnold.AiNodeSetInt(options,
                                ccp("GI_glossy_depth"),
                                arnold.AtInt32(1))
            arnold.AiNodeSetInt(options,
                                ccp("GI_diffuse_samples"),
                                arnold.AtInt32(3))
        # add filter
        filter = arnold.AiNode(ccp("gaussian_filter"))
        arnold.AiNodeSetStr(filter, ccp("name"),
                            ccp(self.uniqueName("Filter")))
        # add driver
        driver = arnold.AiNode(ccp("driver_exr"))
        arnold.AiNodeSetStr(driver, ccp("name"),
                            ccp(self.uniqueName("Driver")))
        directory, exrFile = os.path.split(filename)
        root, ext = os.path.splitext(exrFile)
        exrFile = root + ".exr"
        arnold.AiNodeSetStr(driver, ccp("filename"),
                            ccp(exrFile))
        # camera(s)
        camera_obj = None
        for obj in scene.objects:
            data = obj.data
            if obj.type == 'CAMERA':
                camera_obj = obj
                camera_dat = camera_obj.data
                name = camera_dat.name
                if camera_dat.type == "PERSP":
                    camera = arnold.AiNode(ccp("persp_camera"))
                    arnold.AiNodeSetStr(camera,
                                        ccp("name"),
                                        ccp(self.uniqueName(name)))
                    angle = math.degrees(camera_dat.angle)
                    aspect = xresolution / float(yresolution)
                    if aspect >= 1.0:
                        arnold.AiNodeSetFlt(camera, ccp("fov"), angle)
                    else:
                        fov = 2 * math.degrees(math.atan((aspect * 16.0) /
                                                         camera_dat.lens))
                        arnold.AiNodeSetFlt(camera, ccp("fov"), fov)
                    m = camera_obj.matrix_world.copy()
                    self.createMatrix(camera, m)
                else: # camera_dat.type == "ORTHO"
                    print("ortho_camera")
                    camera = arnold.AiNode(ccp("ortho_camera"))
                    arnold.AiNodeSetStr(camera,
                                        ccp("name"),
                                        ccp(self.uniqueName(name)))
                    m = camera_obj.matrix_world.copy()
                    self.createMatrix(camera, m)
                    print(m)
                if camera_obj == scene.camera:
                    arnold.AiNodeSetPtr(options, ccp("camera"), camera)
                    render_camera = name
                    # check for motion blur
                    self.use_motion_blur = scene.render.use_motion_blur
                    if self.use_motion_blur:
                        # store for later
                        self.shutter_start = 0.0
                        self.shutter_end = scene.render.motion_blur_shutter
                        self.motion_blur_samples = scene.render.motion_blur_samples
                        # camera shutter settings
                        arnold.AiNodeSetFlt(camera, ccp("shutter_start"), 0.0)
                        arnold.AiNodeSetFlt(camera, ccp("shutter_end"), 1.0) # dont use self.shutter_end !!!
        if camera_obj == None:
            print("WARNING: %s" % "no camera found")
            # creating default camera
            camera = arnold.AiNode(ccp("persp_camera"))
            arnold.AiNodeSetStr(camera,
                                ccp("name"),
                                ccp("default_camera"))
            render_camera = "default_camera"
            arnold.AiNodeSetPtr(options, ccp("default_camera"), camera)
        # is there ambient light?
        # try:
        #     ambient_color = scene.world.ambient_color
        # except AttributeError:
        #     pass
        # else:
        #     ambient_light = arnold.AiNode(ccp("ambient_light"))
        #     arnold.AiNodeSetStr(ambient_light,
        #                         ccp("name"),
        #                         ccp(self.uniqueName("AmbientLight")))
        #     arnold.AiNodeSetRGB(ambient_light, ccp("color"),
        #                         ambient_color.r,
        #                         ambient_color.g,
        #                         ambient_color.b)
        self.exportDefaultOrUtilityShader()
        # scene objects
        for obj in scene.objects:
            self.exportObject(obj, scene)
        # motion blur
        if self.use_motion_blur:
            frame_current = scene.frame_current
            frame_step = 1.0 / (self.motion_blur_samples - 1)
            for i in range(self.motion_blur_samples):
                subframe = self.shutter_end * frame_step * i
                scene.frame_set(frame_current, subframe)
                for objIdx in range(len(self.animatedObjects)):
                    obj = self.animatedObjects[objIdx]
                    # collect matrices (per object)
                    if i == 0:
                        self.motionMatrices[obj.name] = []
                    m = obj.matrix_world.copy()
                    self.motionMatrices[obj.name].append(m)
            # reset frame
            scene.frame_set(frame_current, 0.0)
            # for all animated objects ...
            for objIdx in range(len(self.animatedObjects)):
                obj = self.animatedObjects[objIdx]
                uniqueName = self.animatedUniqueNames[objIdx]
                # ... get access to the list of matrices
                matrices = self.motionMatrices[obj.name]
                # and store them in an Arnold array
                m_array = arnold.AiArrayAllocate(1,
                                                 self.motion_blur_samples,
                                                 arnold.AI_TYPE_MATRIX)
                for midx in range(len(matrices)):
                    m = matrices[midx]
                    am = arnold.AtMatrix()
                    arnold.AiM4Identity(am)
                    am.a00 = m[0][0]
                    am.a01 = m[1][0]
                    am.a02 = m[2][0]
                    am.a03 = m[3][0]
                    am.a10 = m[0][1]
                    am.a11 = m[1][1]
                    am.a12 = m[2][1]
                    am.a13 = m[3][1]
                    am.a20 = m[0][2]
                    am.a21 = m[1][2]
                    am.a22 = m[2][2]
                    am.a23 = m[3][2]
                    am.a30 = m[0][3]
                    am.a31 = m[1][3]
                    am.a32 = m[2][3]
                    am.a33 = m[3][3]
                    arnold.AiArraySetMtx(m_array, midx, am)
                # now find the Arnold node
                node = arnold.AiNodeLookUpByName(ccp(uniqueName))
                if node != None:
                    arnold.AiNodeSetArray(node, ccp("matrix"), m_array)
                else:
                    print("WARNING: couln't look up Arnold node by name " +
                          "\"%s\"" % uniqueName)
        # AiASSWrite
        sb = ccp(filename)
        node_mask = arnold.AI_NODE_ALL
        open_procs = False
        binary = False
        intRet = arnold.AiASSWrite(sb, node_mask, open_procs, binary)
        print('INFO: exported "%s"' % filename)
        # AiEnd
        arnold.AiEnd()
        # render
        if self.options.render:
            outFile = os.path.join(directory, exrFile)
            kick = "kick"
            arnold_root = os.getenv("ARNOLD_ROOT")
            if arnold_root:
                kick = os.path.join(arnold_root, "bin", kick)
            else:
                print("WARNING: no ARNOLD_ROOT set")
            if (self.options.global_surface_shader == 'none' and
                (self.light_counter or self.emissive_objects)):
                gamma = 2.2 # scene.view_settings.gamma
                exposure = scene.view_settings.exposure
                renderer_args = [kick, '-sl',
                                 '-g', '%s' % gamma,
                                 '-e', '%s' % exposure,
                                 '-o', outFile,
                                 '-c', render_camera,
                                 filename]
            else:
                renderer_args = [kick, '-sl', '-g', '2.2',
                                 '-o', outFile,
                                 filename]
            print("INFO: starting Arnold: %s" % renderer_args)
            renderer_proc = subprocess.Popen(renderer_args)
            renderer_pid = renderer_proc.pid

def save(operator, context, filepath = "",
         # user interface
         opt_selection = False,
         opt_global_surface_shader = 'none',
         opt_use_global_illum = True,
         opt_use_instancer = True,
         opt_render = True):
    if bpy.ops.object.mode_set.poll():
        bpy.ops.object.mode_set(mode = 'OBJECT')
    # print a usage message
    print("=" * 79)
    print("bpy.ops.export_scene.ass(filepath = \"%s\", " %
          "\\\\".join(filepath.split('\\')) +
          "opt_selection = %s, " % opt_selection +
          "opt_global_surface_shader = '%s', " % opt_global_surface_shader +
          "opt_use_global_illum = %s, " % opt_use_global_illum +
          "opt_use_instancer = %s, " % opt_use_instancer +
          "opt_render = %s)" % opt_render)
    # delegate work to classes
    exporter = AssExporter(Options(selected = opt_selection,
                                   global_surface_shader =
                                   opt_global_surface_shader,
                                   use_global_illum = opt_use_global_illum,
                                   use_instancer = opt_use_instancer,
                                   render = opt_render))
    exporter.writer(filepath)
    return {'FINISHED'}
