"""
This script is an exporter to the MI file format for mental ray (neuray/iray).
"""
# for cut&paste:
# bpy.ops.export_scene.mi(filepath = "/usr/people/jan/tmp/mi/untitled.mi")

# supported primitives:
# ---------------------
# "Cone" (base_x [0.5, -0.5, 0.0], tip [0.0, 0.5, 0.0], radius 0.5, height 1)
# "Cylinder"
# "Disk"
# "Ring"
# "Sphere" (radius 1.0 in origin)
# "Square" ([-0.5, -0.5, 0.0] to [0.5, 0.5, 0.0])

# supported global surface shaders:
# ---------------------------------
# 'ambient_occlusion'
# 'depth_fade'
# 'front_bright'
# 'normals_as_colors'
# 'one_color'

import os
import math
import time
import shutil
import subprocess
# Blender
import bpy

VERSION = 1 # will be divided by 100, e.g. 100 means 1.00

# TODO: Update this revision number before distributing a new version
# of this add-on !!!
REVISION = 0 # TODO

# default values (should be overwritten by environment variables)
MI_ROOT = "/mill3d/server/apps/MENTAL/MI2/mr_3.9.6.5"
OSSWITCH = "linux-x86-64"
WMRS_ROOT = "/usr/people/jan/git/mental_ray_shaders"

# the following parameters can be overwritten by a rme_config file
# located in the export directory !!!
DIFF_DIM_LEVEL = 1.0
SPEC_DIM_LEVEL = 1.0
IN_DOOR = True
SUN_LIGHT_INTENSITY_IN = 7.17e+06
SUN_INTENSITY_IN = 7.17e+06
SUN_CM2_FACTOR_IN = 0.00003
SUN_LIGHT_INTENSITY_OUT = 1.0
SUN_INTENSITY_OUT = 1.0
SUN_CM2_FACTOR_OUT = 0.25
SUN_MULTIPLIER = 1.0
AC_FACTOR = 1e4 # additional color factor
DIRECTIONAL_LIGHT_FACTOR = 5e3
POINT_LIGHT_FACTOR = 1e5
SPOT_LIGHT_FACTOR = 1e5
AREA_LIGHT_FACTOR = 1e6

class Options:
    def __init__(self,
                 selected = False,
                 renderer = "mental_ray",
                 global_surface_shader = "none",
                 geometry = True,
                 binary = True,
                 copy_textures = True,
                 copy_lightprofiles = True,
                 render = True,
                 ambient_fb = True,
                 occlusion_fb = False,
                 diffuse_fb = True,
                 specular_fb = True,
                 reflect_fb = True,
                 depth_fb = True,
                 normals_fb = True,
                 shadows_fb = True):
        self.selected = selected
        self.renderer = renderer
        self.global_surface_shader = global_surface_shader
        self.geometry = geometry
        self.binary = binary
        self.copy_textures = copy_textures
        self.copy_lightprofiles = copy_lightprofiles
        self.render = render
        self.ambient_fb = ambient_fb
        self.occlusion_fb = occlusion_fb
        self.diffuse_fb = diffuse_fb
        self.specular_fb = specular_fb
        self.reflect_fb = reflect_fb
        self.depth_fb = depth_fb
        self.normals_fb = normals_fb
        self.shadows_fb = shadows_fb

def time_stamp():
    return time.strftime('%a %b %d %H:%M:%S %Y', time.localtime())

def fix_name(name):
    new_name = name
    # " " -> "_"
    new_name = "_".join(new_name.split(" "))
    # "." -> "_"
    new_name = "_".join(new_name.split("."))
    return new_name

def read_environment_variables():
    global MI_ROOT
    global OSSWITCH
    global WMRS_ROOT
    # MI_ROOT
    try:
        tmp = os.environ['MI_ROOT']
    except KeyError:
        pass
    else:
        MI_ROOT = tmp
    # OSSWITCH
    try:
        tmp = os.environ['OSSWITCH']
    except KeyError:
        pass
    else:
        OSSWITCH = tmp
    # WMRS_ROOT
    try:
        tmp = os.environ['WMRS_ROOT']
    except KeyError:
        pass
    else:
        WMRS_ROOT = tmp
    print("INFO: %s = %s" % ('MI_ROOT', MI_ROOT))
    print("INFO: %s = %s" % ('OSSWITCH', OSSWITCH))
    print("INFO: %s = %s" % ('WMRS_ROOT', WMRS_ROOT))

class MiTranslator:
    # call class similar to the Maya version of the exporter

    def __init__(self, options):
        # options
        self.options = options
        # directories
        self.directory_path = None
        self.base_name = None
        self.base_dir = None
        self.tex_dir = None
        self.ies_dir = None
        self.export_path = None
        # camera related
        self.camera_obj = None
        self.camera = None
        self.resolution = None
        # motion blur related
        self.use_motion_blur = False
        self.shutter_start = 0.0
        self.shutter_end = 0.0
        self.motion_blur_samples = 0
        # light related
        self.light_counter = 0
        # environment related
        self.hdr = None
        self.useSun = False
        self.sunLight = None
        self.sunDir = [None, None, None]
        # object related
        self.tag_counter = 0
        # hierarchy related
        self.root_instgroup = {} # will be filled later with root objects
        # material related
        self.materialNames = {}
        # log file
        self.log_file = None
        # MI file related
        self.mi_file_types = ["main", "options", "shader_classes", "textures",
                              "materials", "objects", "camera", "lights",
                              "groups", "instances_groups"]
        self.mi_files = {} # will be filled later with above types as index
        # supported shaders
        self.mslShaderNames = ["mia_material_x",
                               "mia_physicalsky",
                               "light_area",
                               "light_directional",
                               "light_photometric",
                               "light_point",
                               "Iray_illumination_clear_coat",
                               "iray_texture_lookup",
                               "normals_make_normal",
                               "normals_bump_map",
                               "mib_lookup_spherical",
                               "illumination_clear_coat"]
        # Blender specific
        self.cameras = {}
        self.curves = {}
        self.groups = {}
        self.lamps = {}
        self.materials = {}
        self.meshes = {}
        self.objects = {}
        self.useBmesh = False
        version = bpy.app.version
        if version[0] >= 2 and version[1] >= 63:
            self.useBmesh = True

    def checkForConfigFile(self, directory_path):
        config_file = os.path.join(directory_path, "rme_config")
        if os.path.exists(config_file):
            self.log_file.write("settings from:\n")
            self.log_file.write("%s\n" % config_file)
            print("INFO: config file (\"%s\") found:" % "rme_config")
            iFile = open(config_file, "r")
            lines = iFile.readlines()
            iFile.close()
            # print info for user
            for line in lines:
                print(line[:-1])
            # execute file content
            exec(compile(open(config_file).read(), config_file,
                         'exec'), globals(), globals())
        # write known heuristics to log file
        self.log_file.write("DIFF_DIM_LEVEL = %s\n" %
                            DIFF_DIM_LEVEL)
        self.log_file.write("SPEC_DIM_LEVEL = %s\n" %
                            SPEC_DIM_LEVEL)
        self.log_file.write("IN_DOOR = %s\n" %
                            IN_DOOR)
        self.log_file.write("SUN_LIGHT_INTENSITY_IN = %s\n" %
                            SUN_LIGHT_INTENSITY_IN)
        self.log_file.write("SUN_INTENSITY_IN = %s\n" %
                            SUN_INTENSITY_IN)
        self.log_file.write("SUN_CM2_FACTOR_IN = %s\n" %
                            SUN_CM2_FACTOR_IN)
        self.log_file.write("SUN_LIGHT_INTENSITY_OUT = %s\n" %
                            SUN_LIGHT_INTENSITY_OUT)
        self.log_file.write("SUN_INTENSITY_OUT = %s\n" %
                            SUN_INTENSITY_OUT)
        self.log_file.write("SUN_CM2_FACTOR_OUT = %s\n" %
                            SUN_CM2_FACTOR_OUT)
        self.log_file.write("SUN_MULTIPLIER = %s\n" %
                            SUN_MULTIPLIER)
        self.log_file.write("AC_FACTOR = %s\n" %
                            AC_FACTOR)
        self.log_file.write("DIRECTIONAL_LIGHT_FACTOR = %s\n" %
                            DIRECTIONAL_LIGHT_FACTOR)
        self.log_file.write("POINT_LIGHT_FACTOR = %s\n" %
                            POINT_LIGHT_FACTOR)
        self.log_file.write("SPOT_LIGHT_FACTOR = %s\n" %
                            SPOT_LIGHT_FACTOR)
        self.log_file.write("AREA_LIGHT_FACTOR = %s\n" %
                            AREA_LIGHT_FACTOR)

    def openMiFiles(self, filename):
        head, tail = os.path.split(filename)
        main_mi_file = tail
        directory_path = head
        self.directory_path = head
        # split main mi file name
        root, ext = os.path.splitext(main_mi_file)
        self.base_name = root
        time_str = time_stamp()
        for type in self.mi_file_types:
            if type == "main":
                new_filename = os.path.join(directory_path, main_mi_file)
                # all other files will go into a subdirectory
                print("INFO: export path:\n\"%s\"" % directory_path)
                # create a '.rayrc' file
                rayrc_name = ".rayrc"
                rayrc_filename = os.path.join(directory_path, rayrc_name)
                print("INFO: writing to \"%s\" ..." % rayrc_name)
                rayrc_file = open(rayrc_filename, "w")
                rayrc_file.write("registry \"{%s}\"\n" % "_MI_REG_INCLUDE")
                rayrc_file.write("\tvalue \".;%s/%s;%s/%s\"\n" %
                                 (MI_ROOT, "common/include", 
                                  WMRS_ROOT, "include"))
                rayrc_file.write("end registry\n")
                rayrc_file.write("\n")
                rayrc_file.write("registry \"{%s}\"\n" % "_MI_REG_LIBRARY")
                rayrc_file.write("\tvalue \".;%s/%s/%s;%s/%s\"\n" %
                                 (MI_ROOT, OSSWITCH, "shaders", 
                                  WMRS_ROOT, "library"))
                rayrc_file.write("end registry\n")
                if self.options.renderer == "mental_ray":
                    rayrc_file.write("\n")
                    rayrc_file.write("# convenience include/link lines\n")
                    rayrc_file.write("\n")
                    # include mental images' shaders (coming with mental ray)
                    rayrc_file.write("# include mental images' shaders " +
                                     "(coming with mental ray)\n")
                    rayrc_file.write('link \"base.so\"\n')
                    rayrc_file.write('$include \"base.mi\"\n')
                    rayrc_file.write('link \"contour.so\"\n')
                    rayrc_file.write('$include \"contour.mi\"\n')
                    rayrc_file.write('link \"physics.so\"\n')
                    rayrc_file.write('$include \"physics.mi\"\n')
                    rayrc_file.write('link \"subsurface.so\"\n')
                    rayrc_file.write('$include \"subsurface.mi\"\n')
                    # include shaders from Andy Kopra's book
                    rayrc_file.write("# include shaders from " +
                                     "Andy Kopra's book\n")
                    rayrc_file.write('link \"wmrs.so\"\n')
                    rayrc_file.write('$include \"wmrs.mi\"\n')
                    rayrc_file.write('link \"newblocks.so\"\n')
                    rayrc_file.write('$include \"newblocks.mi\"\n')
                    # include shaders for Massive
                    rayrc_file.write("# include shaders for Massive\n")
                    rayrc_file.write('link \"mill_massive.so\"\n')
                    rayrc_file.write('$include \"mill_massive.mi\"\n')
                rayrc_file.close()
                # open log file
                words = main_mi_file.split('.')
                log_name_and_ext = words[0] + ".log"
                log_filename = os.path.join(directory_path, log_name_and_ext)
                print("INFO: writing to \"%s\" ..." % log_name_and_ext)
                self.log_file = open(log_filename, "w")
                self.log_file.write("bpy.ops.export_scene.mi(" +
                                    "filepath = \"%s\", " %
                                    "\\\\".join(filename.split('\\')) +
                                    "opt_selection = %s, " %
                                    self.options.selected +
                                    "opt_renderer = '%s', " %
                                    self.options.renderer +
                                    "opt_global_surface_shader = '%s', " %
                                    self.options.global_surface_shader +
                                    "opt_geometry = %s, " %
                                    self.options.geometry +
                                    "opt_binary = %s, " %
                                    self.options.binary +
                                    "opt_textures = %s, " %
                                    self.options.copy_textures +
                                    "opt_lightprofiles = %s, " %
                                    self.options.copy_lightprofiles +
                                    "opt_render = %s)\n" %
                                    self.options.render)
                self.base_dir = directory_path
                directory_path = os.path.join(directory_path,
                                              self.base_name)
                if not os.path.exists(directory_path):
                    print("INFO: creating directory:")
                    print("INFO:    \"%s\"" % directory_path)
                    os.mkdir(directory_path)
                self.export_path = directory_path
                # textures subdirectory?
                if self.options.copy_textures:
                    # check for "textures" subdirectory
                    self.tex_dir = os.path.join(self.export_path,
                                                "textures")
                    if not os.path.exists(self.tex_dir):
                        print("INFO: creating directory:")
                        print("INFO:    \"%s\"" % self.tex_dir)
                        os.mkdir(self.tex_dir)
                # lightprofiles subdirectory?
                if self.options.copy_lightprofiles:
                    # check for "lightprofiles" subdirectory
                    self.ies_dir = os.path.join(self.export_path,
                                                "lightprofiles")
                    if not os.path.exists(self.ies_dir):
                        print("INFO: creating directory:")
                        print("INFO:    \"%s\"" % self.ies_dir)
                        os.mkdir(self.ies_dir)
                # check for config file
                self.checkForConfigFile(directory_path)
                # message about current file
                print("INFO: writing to \"%s\" ..." % main_mi_file)
                oFile = open(filename, "w")
                self.mi_files[type] = oFile
                self.mi_files[type].write("# export_mental_ray " +
                                          "Version %.2f - %s\n" %
                                          (VERSION / 100.0, time_str))
                appString = "Blender %s.%s.%s" % bpy.app.version
                self.mi_files[type].write("# host app: %s\n" % appString)
                # major_version = VERSION / 100
                # minor_version = VERSION % 100
                # self.mi_files[type].write("#@prepared_by/re/%s/%s/0/%s\n" %
                # (major_version, minor_version,
                # REVISION))
                # self.mi_files[type].write("#@compatible_with/rs/3/1/1/0\n")
                # self.mi_files[type].write("#@compatible_with/neuray/3/1/1/0\n")
                self.mi_files[type].write("#@compatible_with/mental ray/3/9/6/6\n")
                self.mi_files[type].write("# %s\n\n" % main_mi_file)
                # include other MI files
                for include_mi_file in self.mi_file_types[1:]:
                    self.mi_files[type].write('$include "%s/%s%s"\n' %
                                              (self.base_name,
                                               include_mi_file, ext))
                self.mi_files[type].write("\n")
                # message for all other MI files
                print("INFO: export path:\n\"%s\"" % directory_path)
            else:
                mi_file = type + ext
                new_filename = os.path.join(directory_path, mi_file)
                print("INFO: writing to \"%s\" ..." % mi_file)
                oFile = open(new_filename, "w")
                self.mi_files[type] = oFile
                self.mi_files[type].write("# %s\n\n" % mi_file)

    def closeMiFiles(self):
        # close log file
        self.log_file.close()
        # close MI files
        mi_file_types = list(self.mi_files.keys())
        mi_file_types.sort()
        for mi_file_type in mi_file_types:
            self.mi_files[mi_file_type].close()

    def isAnimated(self, obj):
        if obj.animation_data != None:
            return True
        # elif obj.parent != None:
        #     return self.isAnimated(obj.parent)
        return False

    def isPrimitive(self, obj):
        isPrimitive = False
        primType = None
        if (self.nameStartsWith(obj.data.name, "Cone")):
            isPrimitive = True
            primType = "Cone"
        elif (self.nameStartsWith(obj.data.name, "Cylinder")):
            isPrimitive = True
            primType = "Cylinder"
        elif (self.nameStartsWith(obj.data.name, "Disk")):
            isPrimitive = True
            primType = "Disk"
        elif (self.nameStartsWith(obj.data.name, "Ring")):
            isPrimitive = True
            primType = "Ring"
        elif (self.nameStartsWith(obj.data.name, "Sphere")):
            isPrimitive = True
            primType = "Sphere"
        elif (self.nameStartsWith(obj.data.name, "Square")):
            isPrimitive = True
            primType = "Square"
        return isPrimitive, primType

    def isVisible(self, obj, scene):
        visible = False
        for i in range(20):
            if obj.layers[i]:
                if scene.layers[i]:
                    visible = True
        return visible

    def exportMslIncludes(self):
        type = "shader_classes"
        ext  = ".mi"
        if self.options.renderer == "iray":
            # write to "shader_classes" MI file
            for mslShaderName in self.mslShaderNames:
                self.mi_files[type].write('$include "%s.msl"\n' % mslShaderName)
        elif self.options.renderer == "mental_ray":
            # just the base shader for now
            self.mi_files[type].write('link "base.so"\n')
            self.mi_files[type].write('$include <base.mi>\n')

    def exportOptions(self):
        type = "options"
        ext  = ".mi"
        # write to "options" MI file
        self.mi_files[type].write('options "opt"\n')
        self.mi_files[type].write('\tsamples 1\n')
        self.mi_files[type].write('\tfilter gauss 3.0\n')
        self.mi_files[type].write('\ttrace depth 2 2 2\n')
        # motion blur related
        if self.use_motion_blur:
            inv = 1.0 / float(self.motion_blur_samples)
            self.mi_files[type].write('\ttime contrast %s %s %s %s\n' % (inv, inv, inv, inv))
            self.mi_files[type].write('\t# diagnostic samples on\n')
            self.mi_files[type].write('\tshutter %s\n' % self.shutter_end)
            self.mi_files[type].write('\t# motion steps are not used for motion transformations\n')
            self.mi_files[type].write('\tmotion steps %s\n' % self.motion_blur_samples)
        if self.options.renderer == "iray":
            self.mi_files[type].write('\tattribute %s "%s" %s\n' %
                                      ("scalar",
                                       "iray_progressive_error_threshold", 0.0))
        self.mi_files[type].write('end options\n')

    def exportDefaultShader(self):
        # include "textures" MI file
        type = "textures"
        ext  = ".mi"
        # include "materials" MI file
        type = "materials"
        ext  = ".mi"
        if self.options.renderer == "iray":
            # write to "materials" MI file
            self.mi_files[type].write('shader \"%s_shd\" \"%s\" (\n' %
                                      ("default", "metasl_mia_material_x"))
            self.mi_files[type].write('\t\"diffuse_weight\" 1,\n')
            self.mi_files[type].write('\t\"diffuse\" 0.5 0.5 0.5 1.0,\n')
            self.mi_files[type].write('\t\"diffuse_roughness\" 0.2,\n')
            self.mi_files[type].write('\t\"reflectivity\" 0,\n')
            self.mi_files[type].write('\t\"refl_color\" 1 1 1 1.0,\n')
            self.mi_files[type].write('\t\"refl_gloss\" 1,\n')
            self.mi_files[type].write('\t\"refl_gloss_samples\" 8,\n')
            self.mi_files[type].write('\t\"refl_interpolate\" off,\n')
            self.mi_files[type].write('\t\"refl_hl_only\" off,\n')
            self.mi_files[type].write('\t\"refl_is_metal\" off,\n')
            self.mi_files[type].write('\t\"transparency\" 0,\n')
            self.mi_files[type].write('\t\"refr_color\" 1 1 1 1.0,\n')
            self.mi_files[type].write('\t\"refr_gloss\" 1,\n')
            self.mi_files[type].write('\t\"refr_ior\" 1,\n')
            self.mi_files[type].write('\t\"refr_gloss_samples\" 8,\n')
            self.mi_files[type].write('\t\"refr_interpolate\" off,\n')
            self.mi_files[type].write('\t\"refr_translucency\" off,\n')
            self.mi_files[type].write('\t\"refr_trans_color\" 0.7 0.5 0.2 1.0,\n')
            self.mi_files[type].write('\t\"refr_trans_weight\" 0.5,\n')
            self.mi_files[type].write('\t\"anisotropy\" 1,\n')
            self.mi_files[type].write('\t\"anisotropy_rotation\" 0,\n')
            self.mi_files[type].write('\t\"anisotropy_channel\" -1,\n')
            self.mi_files[type].write('\t\"brdf_fresnel\" off,\n')
            self.mi_files[type].write('\t\"brdf_0_degree_refl\" 0.2,\n')
            self.mi_files[type].write('\t\"brdf_90_degree_refl\" 1,\n')
            self.mi_files[type].write('\t\"brdf_curve\" 5,\n')
            self.mi_files[type].write('\t\"brdf_conserve_energy\" on,\n')
            self.mi_files[type].write('\t\"intr_grid_density\" 2,\n')
            self.mi_files[type].write('\t\"intr_refl_samples\" 2,\n')
            self.mi_files[type].write('\t\"intr_refl_ddist_on\" off,\n')
            self.mi_files[type].write('\t\"intr_refr_samples\" 2,\n')
            self.mi_files[type].write('\t\"single_env_sample\" off,\n')
            self.mi_files[type].write('\t\"refl_falloff_on\" off,\n')
            self.mi_files[type].write('\t\"refl_falloff_dist\" 0,\n')
            self.mi_files[type].write('\t\"refl_falloff_color_on\" off,\n')
            self.mi_files[type].write('\t\"refl_falloff_color\" %s,\n' %
                                       "0.2 0.2 0.2 1.0")
            self.mi_files[type].write('\t\"refl_depth\" 4,\n')
            self.mi_files[type].write('\t\"refl_cutoff\" 0.01,\n')
            self.mi_files[type].write('\t\"refr_falloff_on\" off,\n')
            self.mi_files[type].write('\t\"refr_falloff_dist\" 0,\n')
            self.mi_files[type].write('\t\"refr_falloff_color_on\" off,\n')
            self.mi_files[type].write('\t\"refr_falloff_color\" %s,\n' %
                                       "0.2 0.2 0.2 1.0")
            self.mi_files[type].write('\t\"refr_depth\" 6,\n')
            self.mi_files[type].write('\t\"refr_cutoff\" 0.01,\n')
            self.mi_files[type].write('\t\"indirect_multiplier\" 1,\n')
            self.mi_files[type].write('\t\"fg_quality\" 1,\n')
            self.mi_files[type].write('\t\"fg_quality_w\" 1,\n')
            self.mi_files[type].write('\t\"ao_on\" off,\n')
            self.mi_files[type].write('\t\"ao_samples\" 16,\n')
            self.mi_files[type].write('\t\"ao_distance\" 4,\n')
            self.mi_files[type].write('\t\"ao_dark\" 0.2 0.2 0.2 1.0,\n')
            self.mi_files[type].write('\t\"ao_ambient\" 0 0 0 1.0,\n')
            self.mi_files[type].write('\t\"thin_walled\" off,\n')
            self.mi_files[type].write('\t\"no_visible_area_hl\" on,\n')
            self.mi_files[type].write('\t\"skip_inside_refl\" on,\n')
            self.mi_files[type].write('\t\"do_refractive_caustics\" off,\n')
            self.mi_files[type].write('\t\"backface_cull\" off,\n')
            self.mi_files[type].write('\t\"propagate_alpha\" on,\n')
            self.mi_files[type].write('\t\"hl_vs_refl_balance\" 1,\n')
            self.mi_files[type].write('\t\"additional_color\" 0 0 0 1.0,\n')
            self.mi_files[type].write('\t\"no_diffuse_bump\" off,\n')
            self.mi_files[type].write('\t\"mode\" 0)\n\n')
            # after the "shader" we export the "material"
            self.mi_files[type].write('material "%s_mat"\n' % "default")
            self.mi_files[type].write('\t= "%s_shd"\n' % "default")
            self.mi_files[type].write('end material\n\n')
        elif self.options.renderer == "mental_ray":
            diffuse = [1.0, 1.0, 1.0]
            # define used shader directly within material
            self.mi_files[type].write('material "%s_mat"\n' % "default")
            if self.options.global_surface_shader == 'ambient_occlusion':
                self.mi_files[type].write('\t"ambient_occlusion" (\n')
                # use default sample settings for now (TODO: expose)
                samples = 256
                self.mi_files[type].write('\t\t"samples" %s\n' % samples)
                self.mi_files[type].write('\t)\n')
            elif self.options.global_surface_shader == 'depth_fade':
                self.mi_files[type].write('\t"depth_fade" (\n')
                # get near and far from camera
                near = self.camera.clip_start
                far = self.camera.clip_end
                self.mi_files[type].write('\t\t"near" %s,\n' % -near)
                self.mi_files[type].write('\t\t"far" %s\n' % -far)
                self.mi_files[type].write('\t)\n')
            elif self.options.global_surface_shader == 'front_bright':
                self.mi_files[type].write('\t"front_bright" (\n')
                # use diffuse color
                self.mi_files[type].write('\t\t"tint" %s %s %s\n' %
                                          (diffuse[0],
                                           diffuse[1],
                                           diffuse[2]))
                self.mi_files[type].write('\t)\n')
            elif self.options.global_surface_shader == 'normals_as_colors':
                self.mi_files[type].write('\t"normals_as_colors" ()\n')
            elif self.options.global_surface_shader == 'one_color':
                self.mi_files[type].write('\t"one_color" (\n')
                # use diffuse color
                self.mi_files[type].write('\t\t"color" %s %s %s\n' %
                                          (diffuse[0],
                                           diffuse[1],
                                           diffuse[2]))
                self.mi_files[type].write('\t)\n')
            elif self.options.global_surface_shader == 'show_u':
                self.mi_files[type].write('\t"show_uv" (\n')
                self.mi_files[type].write('\t\t"u" %s,\n' % "on")
                self.mi_files[type].write('\t\t"v" %s\n' % "off")
                self.mi_files[type].write('\t)\n')
            elif self.options.global_surface_shader == 'show_uv':
                self.mi_files[type].write('\t"show_uv" (\n')
                self.mi_files[type].write('\t\t"u" %s,\n' % "on")
                self.mi_files[type].write('\t\t"v" %s\n' % "on")
                self.mi_files[type].write('\t)\n')
            elif self.options.global_surface_shader == 'show_v':
                self.mi_files[type].write('\t"show_uv" (\n')
                self.mi_files[type].write('\t\t"u" %s,\n' % "off")
                self.mi_files[type].write('\t\t"v" %s\n' % "on")
                self.mi_files[type].write('\t)\n')
            else:
                self.mi_files[type].write('\t"mib_illum_phong" (\n')
                self.mi_files[type].write('\t\t"ambient" 0.5 0.5 0.5,\n')
                self.mi_files[type].write('\t\t"diffuse" 0.7 0.7 0.7,\n')
                self.mi_files[type].write('\t\t"ambience" 0.3 0.3 0.3,\n')
                self.mi_files[type].write('\t\t"specular" 1.0 1.0 1.0,\n')
                self.mi_files[type].write('\t\t"exponent" 50,\n')
                self.mi_files[type].write('\t\t"mode" 2)\n') # 2=excl
                self.mi_files[type].write('\tshadow "shadow_default" ()\n')
            self.mi_files[type].write('end material\n\n')

    def exportLight(self, obj):
        type = "lights"
        ext  = ".mi"
        # write to "lights" MI file
        data = obj.data
        energy = data.energy
        color = data.color
        fixed_name = fix_name(obj.name)
        if data.type == 'POINT':
            if self.options.renderer == "iray":
                # shader
                self.mi_files[type].write('shader "%s_shd" "%s" (\n' %
                                          (fixed_name, "Light_point"))
                self.mi_files[type].write('\t"color" %s %s %s 1.0,\n' %
                                          (color[0], color[1], color[2]))
                self.mi_files[type].write('\t"intensity" %s,\n' %
                                          (POINT_LIGHT_FACTOR * energy))
                self.mi_files[type].write('\t"distance_falloff_exponent" ' +
                                          '2.0)\n')
                self.mi_files[type].write('\n')
                # light
                self.mi_files[type].write('light "%s_lgt"\n' % fixed_name)
                self.mi_files[type].write('\t= "%s_shd"\n' % fixed_name)
                self.mi_files[type].write('\torigin 0.0 0.0 0.0\n')
                self.mi_files[type].write('end light\n')
                self.mi_files[type].write('\n')
            elif self.options.renderer == "mental_ray":
                # light
                self.mi_files[type].write('light "%s_lgt"\n' % fixed_name)
                ##self.mi_files[type].write('\t"mib_light_point" (\n')
                ##self.mi_files[type].write('\t\t"color" %s %s %s,\n' %
                ##(color[0], color[1], color[2]))
                ##self.mi_files[type].write('\t\t"shadow" on,\n')
                ##self.mi_files[type].write('\t\t"factor" 0.0)\n')
                self.mi_files[type].write('\t"point_light_falloff" (\n')
                self.mi_files[type].write('\t\t"light_color" %s %s %s)\n' %
                                          (color[0] * energy, 
                                           color[1] * energy, 
                                           color[2] * energy))
                self.mi_files[type].write('\torigin 0.0 0.0 0.0\n')
                if data.falloff_type == 'INVERSE_SQUARE':
                    self.mi_files[type].write('\texponent 2\n')
                else:
                    self.mi_files[type].write('\texponent 0\n')
                self.mi_files[type].write('end light\n')
                self.mi_files[type].write('\n')
        elif data.type == 'SUN':
            if self.options.renderer == "iray":
                # shader
                self.mi_files[type].write('shader "%s_shd" "%s" (\n' %
                                          (fixed_name, "Light_directional"))
                self.mi_files[type].write('\t"color" %s %s %s 1.0,\n' %
                                          (color[0], color[1], color[2]))
                self.mi_files[type].write('\t"intensity" %s)\n' %
                                          (DIRECTIONAL_LIGHT_FACTOR * energy))
                self.mi_files[type].write('\n')
                # light
                self.mi_files[type].write('light "%s_lgt"\n' % fixed_name)
                self.mi_files[type].write('\t= "%s_shd"\n' % fixed_name)
                self.mi_files[type].write('\tdirection 0.0 0.0 -1.0\n')
                self.mi_files[type].write('end light\n')
                self.mi_files[type].write('\n')
            elif self.options.renderer == "mental_ray":
                # light
                self.mi_files[type].write('light "%s_lgt"\n' % fixed_name)
                self.mi_files[type].write('\t"mib_light_infinite" (\n')
                self.mi_files[type].write('\t\t"color" %s %s %s,\n' %
                                          (color[0], color[1], color[2]))
                self.mi_files[type].write('\t\t"shadow" on,\n')
                self.mi_files[type].write('\t\t"factor" 0.0)\n')
                self.mi_files[type].write('\tdirection 0.0 0.0 -1.0\n')
                self.mi_files[type].write('end light\n')
                self.mi_files[type].write('\n')
        elif data.type == 'SPOT':
            spot_size = data.spot_size
            spread = math.cos(spot_size/2.0)
            inner_cone_angle = spot_size/2.0
            if self.options.renderer == "iray":
                # shader
                self.mi_files[type].write('shader "%s_shd" "%s" (\n' %
                                          (fixed_name, "Light_point"))
                self.mi_files[type].write('\t"color" %s %s %s 1.0,\n' %
                                          (color[0], color[1], color[2]))
                self.mi_files[type].write('\t"intensity" %s,\n' %
                                          (SPOT_LIGHT_FACTOR * energy))
                self.mi_files[type].write('\t"distance_falloff_exponent" ' +
                                          '2.0)\n')
                self.mi_files[type].write('\n')
                # light
                self.mi_files[type].write('light "%s_lgt"\n' % fixed_name)
                self.mi_files[type].write('\t= "%s_shd"\n' % fixed_name)
                self.mi_files[type].write('\torigin 0.0 0.0 0.0\n')
                self.mi_files[type].write('\tdirection 0.0 0.0 -1.0\n')
                self.mi_files[type].write('\tspread %s\n' % spread)
                self.mi_files[type].write('end light\n')
                self.mi_files[type].write('\n')
            elif self.options.renderer == "mental_ray":
                # light
                self.mi_files[type].write('light "%s_lgt"\n' % fixed_name)
                self.mi_files[type].write('\t"soft_spotlight_falloff" (\n')
                self.mi_files[type].write('\t\t"light_color" %s %s %s)\n' %
                                          (color[0] * energy, 
                                           color[1] * energy, 
                                           color[2] * energy))
                self.mi_files[type].write('\torigin 0.0 0.0 0.0\n')
                self.mi_files[type].write('\tdirection 0.0 0.0 -1.0\n')
                self.mi_files[type].write('\tspread %s\n' % spread)
                if data.falloff_type == 'INVERSE_SQUARE':
                    self.mi_files[type].write('\texponent 2\n')
                else:
                    self.mi_files[type].write('\texponent 0\n')
                self.mi_files[type].write('end light\n')
                self.mi_files[type].write('\n')
        elif data.type == 'AREA':
            areaSizeX = data.areaSizeX
            areaSizeY = data.areaSizeY
            raySamplesX = data.raySamplesX
            raySamplesY = data.raySamplesY
            x0 = 0.0
            y0 = areaSizeY / 2.0 # first y
            z0 = 0.0
            x1 = areaSizeX / 2.0 # then x
            y1 = 0.0
            z1 = 0.0
            if self.options.renderer == "iray":
                # shader
                self.mi_files[type].write('shader "%s_shd" "%s" (\n' %
                                          (fixed_name, "Light_area"))
                self.mi_files[type].write('\t"color" %s %s %s 1.0,\n' %
                                          (color[0], color[1], color[2]))
                self.mi_files[type].write('\t"intensity" %s,\n' %
                                          (AREA_LIGHT_FACTOR * energy))
                self.mi_files[type].write('\t"input_is_power" on)\n')
                self.mi_files[type].write('\n')
                # light
                self.mi_files[type].write('light "%s_lgt"\n' % fixed_name)
                self.mi_files[type].write('\t= "%s_shd"\n' % fixed_name)
                self.mi_files[type].write('\torigin 0.0 0.0 0.0\n')
                self.mi_files[type].write('\trectangle %s %s %s %s %s %s\n' %
                                          (x0, y0, z0, x1, y1, z1))
                self.mi_files[type].write('\t%s %s 1 1 1\n' % (raySamplesX,
                                                               raySamplesY))
                self.mi_files[type].write('\tvisible\n')
                self.mi_files[type].write('end light\n')
                self.mi_files[type].write('\n')

    def exportSun(self):
        type = "lights"
        # write to "lights" MI file
        data = self.sunLight.data
        energy = data.energy # use energy to "dim" sun light
        color = data.color
        # shader
        if self.options.renderer == "iray":
            self.mi_files[type].write('shader "sun_shd" ' +
                                      '"Light_directional" (\n')
            self.mi_files[type].write('\t"color" 1.0 1.0 1.0 1.0,\n') # white
            if IN_DOOR:
                self.mi_files[type].write('\t"intensity" %s)\n' %
                                          (SUN_LIGHT_INTENSITY_IN * energy))
            else:
                self.mi_files[type].write('\t"intensity" %s)\n' %
                                          (SUN_INTENSITY_OUT * energy))
            self.mi_files[type].write('\n')
            # light
            self.mi_files[type].write('light "sun_lgt"\n')
            self.mi_files[type].write('\t= "sun_shd"\n')
            self.mi_files[type].write('\tdirection 0.0 0.0 -1.0\n')
            self.mi_files[type].write('end light\n')
            self.mi_files[type].write('\n')
        else:
            self.mi_files[type].write('light "sun_lgt" ' +
                                      '"mib_light_infinite" (\n')
            self.mi_files[type].write('\t\t"color" %s %s %s 1.0,\n' %
                                      (color[0] * energy,
                                       color[1] * energy,
                                       color[2] * energy))
            self.mi_files[type].write('\t\t"shadow" %s,\n' % "on")
            self.mi_files[type].write('\t\t"factor" %s)\n' % 0.0)
            self.mi_files[type].write('\tdirection 0.0 0.0 -1.0\n')
            self.mi_files[type].write('end light\n')
            self.mi_files[type].write('\n')

    def exportSelected(self):
        print("TODO: MiTranslator.exportSelected(self)")

    def exportAll(self):
        self.exportMslIncludes()
        self.exportOptions()
        # get access to scene
        scene = bpy.context.scene
        # get access to active camera
        self.camera_obj = scene.camera
        try:
            self.camera = self.camera_obj.data
        except AttributeError:
            print("ERROR: %s" % "no camera found")
            return
        # we need a camera BEFORE we export the default shader
        self.exportDefaultShader()
        # scene specific data
        percent = scene.render.resolution_percentage / 100.0
        xresolution = int(scene.render.resolution_x * percent)
        yresolution = int(scene.render.resolution_y * percent)
        self.resolution = [xresolution, yresolution]
        # TODO: progress bar
        # initialize groups (only export if used)
        self.groups = {}
        for group in bpy.data.groups:
            self.groups[group.name] = 0
        # root level
        todo = []
        for obj in scene.objects:
            fixed_name = fix_name(obj.name)
            if (self.isVisible(obj, scene)):
                ignore = self.examine(obj) # gather useful information
                if not ignore:
                    parent = obj.parent
                    if parent:
                        todo.append(obj)
                    else:
                        if obj.dupli_type == 'GROUP':
                            self.exportGroup(obj.dupli_group.name)
                            children = []
                            for child in obj.dupli_group.objects:
                                children.append(child)
                            self.root_instgroup[fixed_name] = [obj, children]
                        else:
                            self.root_instgroup[fixed_name] = [obj, []]
                else:
                    print("INFO: ignore %s(%s)" % (fixed_name, obj.type)) # TMP
        # if we found a "sun" let's export it now
        if self.useSun:
            self.exportSun()
        # create hierarchies
        levels = [self.root_instgroup]
        while todo:
            level = {} # new level of hierarchy
            previous_level = levels[-1]
            not_found = [] # will replace the todo list
            for obj in todo:
                fixed_name = fix_name(obj.name)
                parent = obj.parent
                try:
                    entry = previous_level[parent.name]
                except KeyError:
                    not_found.append(obj)
                else:
                    entry[1].append(obj)        # add to child list
                    level[fixed_name] = [obj, []] # new level entry
            levels.append(level)
            todo = not_found # replace todo list
        self.exportTextures()
        self.exportShaders()
        self.exportCamera(self.camera_obj)
        self.exportHierarchy(levels)
        self.mi_files["main"].write('render "%s" "%s_inst" "%s"\n' %
                                    ("root_grp", self.camera_obj.name, "opt"))

    def exportInstGroupAndInstance(self, obj, children):
        type = "instances_groups"
        # instgroup
        self.mi_files[type].write('instgroup "%s_grp"\n' %
                                  obj.name)
        for child in children:
            if child.dupli_type == 'GROUP' or child.children:
                self.mi_files[type].write('\t"%s_grp_inst"\n' %
                                          child.name)
            else:
                self.mi_files[type].write('\t"%s_inst"\n' %
                                          child.name)
        self.mi_files[type].write('end instgroup\n\n')
        # instance
        self.mi_files[type].write('instance "%s_grp_inst" ' %
                                  obj.name +
                                  '"%s_grp" \n' % obj.name)
        # don't use the original matrix !!!
        inverse = obj.matrix_local.copy()
        inverse.invert()
        self.mi_files[type].write('\ttransform\n')
        self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                  (inverse[0][0],
                                   inverse[1][0],
                                   inverse[2][0],
                                   inverse[3][0]))
        self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                  (inverse[0][1],
                                   inverse[1][1],
                                   inverse[2][1],
                                   inverse[3][1]))
        self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                  (inverse[0][2],
                                   inverse[1][2],
                                   inverse[2][2],
                                   inverse[3][2]))
        self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                  (inverse[0][3],
                                   inverse[1][3],
                                   inverse[2][3],
                                   inverse[3][3]))
        # motion blur (using motion transformations)
        if self.use_motion_blur and self.isAnimated(obj):
            # get access to scene
            scene = bpy.context.scene
            # save current frame
            frame_current = scene.frame_current
            # don't use self.shutter_end for motion transform
            scene.frame_set(frame_current, 1.0) # self.shutter_end
            # do the same as above ...
            inverse = obj.matrix_local.copy()
            inverse.invert()
            # ... but use motion transform
            self.mi_files[type].write('\tmotion transform\n')
            self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                      (inverse[0][0],
                                       inverse[1][0],
                                       inverse[2][0],
                                       inverse[3][0]))
            self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                      (inverse[0][1],
                                       inverse[1][1],
                                       inverse[2][1],
                                       inverse[3][1]))
            self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                      (inverse[0][2],
                                       inverse[1][2],
                                       inverse[2][2],
                                       inverse[3][2]))
            self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                      (inverse[0][3],
                                       inverse[1][3],
                                       inverse[2][3],
                                       inverse[3][3]))
            # reset frame
            scene.frame_set(frame_current, 0.0)
        self.mi_files[type].write('end instance\n\n')

    def exportAllNew(self):
        self.exportMslIncludes()
        # get access to scene
        scene = bpy.context.scene
        # get access to active camera
        self.camera_obj = scene.camera
        try:
            self.camera = self.camera_obj.data
        except AttributeError:
            print("ERROR: %s" % "no camera found")
            return
        # check for motion blur
        self.use_motion_blur = scene.render.use_motion_blur
        if self.use_motion_blur:
            # store for later
            self.shutter_start = 0.0
            self.shutter_end = scene.render.motion_blur_shutter
            self.motion_blur_samples = scene.render.motion_blur_samples
        # we need to know about motion blur BEFORE we export the options
        self.exportOptions()
        # we need a camera BEFORE we export the default shader
        self.exportDefaultShader()
        # scene specific data
        percent = scene.render.resolution_percentage / 100.0
        xresolution = int(scene.render.resolution_x * percent)
        yresolution = int(scene.render.resolution_y * percent)
        self.resolution = [xresolution, yresolution]
        # TODO: progress bar
        # initialize
        self.cameras = {}
        for camera in bpy.data.cameras:
            self.cameras[camera.name] = 0
        self.curves = {}
        for curve in bpy.data.curves:
            self.curves[curve.name] = 0
        self.groups = {}
        for group in bpy.data.groups:
            self.groups[group.name] = 0
        self.lamps = {}
        for lamp in bpy.data.lamps:
            self.lamps[lamp.name] = 0
        self.materials = {}
        for material in bpy.data.materials:
            self.materials[material.name] = 0
        self.meshes = {}
        for mesh in bpy.data.meshes:
            self.meshes[mesh.name] = 0
        self.objects = {}
        for obj in bpy.data.objects: # don't use object
            self.objects[obj.name] = 0
        # scene objects (root level)
        todo = []
        for obj in scene.objects:
            fixed_name = fix_name(obj.name)
            if (self.isVisible(obj, scene)):
                parent = obj.parent
                ignore = self.examine(obj) # gather useful information
                if not ignore:
                    if parent:
                        todo.append(obj)
                    else:
                        self.root_instgroup[fixed_name] = [obj, []]
                else:
                    print("INFO: ignore %s(%s)" % (fixed_name, obj.type)) # TMP
        # create hierarchies
        levels = [self.root_instgroup]
        while todo:
            level = {} # new level of hierarchy
            previous_level = levels[-1]
            not_found = [] # will replace the todo list
            for obj in todo:
                fixed_name = fix_name(obj.name)
                parent = obj.parent
                try:
                    entry = previous_level[parent.name]
                except KeyError:
                    not_found.append(obj)
                else:
                    entry[1].append(obj)          # add to child list
                    level[fixed_name] = [obj, []] # new level entry
            levels.append(level)
            todo = not_found # replace todo list
        self.exportTextures()
        self.exportShaders()
        self.exportCamera(self.camera_obj)
        # export hierarchy
        type = "instances_groups"
        indices = list(range(len(levels)))
        indices.reverse()
        todo = []
        for index in indices:
            self.mi_files[type].write('%s\n' % ("#" * 79))
            if index == 0:
                self.mi_files[type].write('# root level\n')
                self.mi_files[type].write('%s\n\n' % ("#" * 79))
            else:
                self.mi_files[type].write('# level %s\n' % index)
                self.mi_files[type].write('%s\n\n' % ("#" * 79))
            level = levels[index]
            keys = list(level.keys())
            keys.sort()
            # objects and groups on this level
            for name in keys:
                obj = level[name][0]
                if (obj.type == 'EMPTY' and obj.dupli_type == 'GROUP'):
                    self.mi_files[type].write('# object %s(%s)\n' %
                                              (obj.name, obj.dupli_type))
                else:
                    self.mi_files[type].write('# object %s(%s)\n' %
                                              (obj.name, obj.type))
                    self.exportObject(obj)
                if obj.type == 'EMPTY':
                    if obj.dupli_type == 'GROUP':
                        group = obj.dupli_group
                        self.mi_files[type].write('# \tgroup name %s\n' %
                                                  group.name)
                        children = self.exportGroupNew(group, [])
                    else:
                        children = obj.children
                    self.exportInstGroupAndInstance(obj, children)
            if index == 0:
                # instgroup "rotated_root_grp"
                self.mi_files[type].write('instgroup "rotated_root_grp"\n')
                for name in keys:
                    obj = level[name][0]
                    fixed_name = fix_name(obj.name)
                    children = level[name][1]
                    if children or obj.dupli_type == 'GROUP':
                        self.mi_files[type].write('\t"%s_grp_inst"\n' %
                                                  fixed_name)
                    else:
                        if obj.type != 'EMPTY' and obj.type != 'CAMERA':
                            self.mi_files[type].write('\t"%s_inst"\n' %
                                                      fixed_name)
                self.mi_files[type].write('end instgroup\n\n')
                # instance "rotated_root_grp_inst"
                self.mi_files[type].write('instance "rotated_root_grp_inst" ' +
                                          '"rotated_root_grp"\n')
                self.mi_files[type].write('\ttransform\n')
                self.mi_files[type].write('\t\t 1.0  0.0  0.0  0.0\n')
                # rotate around x-axis by -90 degrees (swap y- and
                # z-coords and negate)
                self.mi_files[type].write('\t\t 0.0  0.0  1.0  0.0 #  z\n') #  z
                self.mi_files[type].write('\t\t 0.0 -1.0  0.0  0.0 # -y\n') # -y
                self.mi_files[type].write('\t\t 0.0  0.0  0.0  1.0\n')
                self.mi_files[type].write('end instance\n\n')
                # instgroup "root_grp"
                self.mi_files[type].write('instgroup "root_grp"\n')
                self.mi_files[type].write('\t"%s_inst"\n' %
                                          self.camera_obj.name)
                self.mi_files[type].write('\t"rotated_root_grp_inst"\n')
                self.mi_files[type].write('end instgroup\n\n')
        self.mi_files["main"].write('render "%s" "%s_inst" "%s"\n' %
                                    ("root_grp", self.camera_obj.name, "opt"))

    def exportObject(self, obj):
        if obj.type == 'SURFACE':
            self.exportSurface(obj)
            self.exportInstance(obj)
        elif obj.type == 'MESH':
            if self.useBmesh:
                self.exportBMesh(obj)
            else:
                self.exportMesh(obj)
            self.exportInstance(obj)
        elif obj.type == 'LAMP':
            light = obj.data
            if (self.options.renderer == "iray" and
                len(obj.name) == len("sun") and
                obj.name[:len("sun")] == "sun"):
                self.exportInstance(obj)
            elif light.type == 'POINT':
                self.exportLight(obj)
                self.exportInstance(obj)
            elif light.type == 'SUN':
                self.exportLight(obj)
                self.exportInstance(obj)
            elif light.type == 'SPOT':
                self.exportLight(obj)
                self.exportInstance(obj)
            elif light.type == 'AREA':
                self.exportLight(obj)
                self.exportInstance(obj)

    def exportGroupNew(self, group, done):
        children = []
        if not self.groups[group.name]:
            try:
                objects = group.objects
            except AttributeError:
                objects = group.children # probably an EMPTY (see below)
            for child in objects:
                if child.type == 'MESH':
                    children.append(child)
                    self.examineMaterials(child)
                    if self.useBmesh:
                        self.exportBMesh(child)
                    else:
                        self.exportMesh(child)
                    self.exportInstance(child)
                elif child.type == 'SURFACE':
                    children.append(child)
                    self.examineMaterials(child)
                    self.exportSurface(child)
                    self.exportInstance(child)
                elif child.type == 'EMPTY':
                    if child.parent:
                        pass
                    else:
                        children.append(child)
                    if child.dupli_type == 'GROUP':
                        child_group = child.dupli_group
                        kids = self.exportGroupNew(child_group, done)
                    else:
                        kids = child.children
                        self.groups[child.name] = 0
                    if not child.name in done:
                        self.exportInstGroupAndInstance(child, kids)
                        done.append(child.name)
            self.groups[group.name] = self.groups[group.name] + 1
        else:
            try:
                objects = group.objects
            except AttributeError:
                objects = group.children # probably an EMPTY (see below)
            for child in objects:
                if child.type == 'MESH':
                    children.append(child)
                elif child.type == 'SURFACE':
                    children.append(child)
                elif child.type == 'EMPTY':
                    if child.parent:
                        pass
                    else:
                        children.append(child)
        return children

    def examine(self, obj):
        ignore = False
        if obj.type == 'CAMERA':
            ignore = True
        elif obj.type == 'MESH':
            ignore = False
            self.examineMaterials(obj)
        elif obj.type == 'SURFACE':
            ignore = False
            self.examineMaterials(obj)
        elif obj.type == 'LAMP':
            ignore = False
            if (len(obj.name) == len("sun") and
                (obj.name[:len("sun")] == "sun" or
                 obj.name[:len("Sun")] == "Sun")):
                m = obj.matrix_world.copy()
                sunPos, rot, scale = m.decompose()
                constraint = obj.constraints[0] # TODO: deal with constraints
                if constraint.type != 'TRACK_TO':
                    print('TODO: examine(%s)' % constraint.type)
                    return ignore
                compass = constraint.target
                m = compass.matrix_world.copy()
                compassPos, rot, scale = m.decompose()
                self.sunDir = [sunPos[0] - compassPos[0],
                          sunPos[1] - compassPos[1],
                          sunPos[2] - compassPos[2]]
                vecLength = math.sqrt((self.sunDir[0] * self.sunDir[0] +
                                       self.sunDir[1] * self.sunDir[1] +
                                       self.sunDir[2] * self.sunDir[2]))
                self.sunDir[0] /= vecLength
                self.sunDir[1] /= vecLength
                self.sunDir[2] /= vecLength
                # adjust sun direction !!!
                tmp = self.sunDir[1]
                self.sunDir[1] = self.sunDir[2]
                self.sunDir[2] = -tmp
                self.useSun = True
                self.sunLight = obj
                self.light_counter += 1
            else:
                self.light_counter += 1
        return ignore

    def examineMaterials(self, obj):
        data = obj.data
        if len(data.materials):
            for index in list(range(len(data.materials))):
                material = data.materials[index]
                if material != None:
                    # material attached to mesh
                    try:
                        self.materialNames[obj].append(material.name)
                    except KeyError:
                        # create list of materials
                        self.materialNames[obj] = [material.name]
                else:
                    # material attached to object
                    material = obj.material_slots[index]
                    # material attached to mesh
                    if material.name:
                        try:
                            self.materialNames[obj].append(material.name)
                        except KeyError:
                            # create list of materials
                            self.materialNames[obj] = [material.name]

    def exportHierarchy(self, levels):
        type = "instances_groups"
        # go through all levels (deepest first)
        indices = list(range(len(levels)))
        indices.reverse()
        for index in indices:
            self.mi_files[type].write('%s\n' % ("#" * 79))
            if index == 0:
                self.mi_files[type].write('# root level\n')
                self.mi_files[type].write('%s\n\n' % ("#" * 79))
            else:
                self.mi_files[type].write('# level %s\n' % index)
                self.mi_files[type].write('%s\n\n' % ("#" * 79))
            level = levels[index]
            keys = list(level.keys())
            keys.sort()
            # objects and groups on this level
            for name in keys:
                obj = level[name][0]
                children = level[name][1]
                if children:
                    self.exportGroupInstance(obj, level,
                                             children, levels[index+1])
                else:
                    if obj.type != 'EMPTY':
                        if obj.type == 'CAMERA':
                            # cameras are exported outside of the hierarchy
                            pass
                        elif obj.type == 'MESH':
                            if self.useBmesh:
                                self.exportBMesh(obj)
                            else:
                                self.exportMesh(obj)
                            self.exportInstance(obj)
                        elif obj.type == 'SURFACE':
                            self.exportSurface(obj)
                            self.exportInstance(obj)
                        elif obj.type == 'LAMP':
                            light = obj.data
                            if (len(obj.name) == len("sun") and
                                obj.name[:len("sun")] == "sun"):
                                self.exportInstance(obj)
                            elif light.type == 'POINT':
                                self.exportLight(obj)
                                self.exportInstance(obj)
                            elif light.type == 'SUN':
                                self.exportLight(obj)
                                self.exportInstance(obj)
                            elif light.type == 'SPOT':
                                self.exportLight(obj)
                                self.exportInstance(obj)
                            elif light.type == 'AREA':
                                self.exportLight(obj)
                                self.exportInstance(obj)
                        else:
                            print('TODO: exportHierarchy(%s)' % obj.type)
                    else:
                        if obj.dupli_type == 'GROUP':
                            self.exportGroup(obj.dupli_group.name)
            if index == 0:
                # instgroup "rotated_root_grp"
                self.mi_files[type].write('instgroup "rotated_root_grp"\n')
                for name in keys:
                    obj = level[name][0]
                    fixed_name = fix_name(obj.name)
                    children = level[name][1]
                    if children:
                        self.mi_files[type].write('\t"%s_grp_inst"\n' %
                                                  fixed_name)
                    else:
                        if obj.type != 'EMPTY' and obj.type != 'CAMERA':
                            self.mi_files[type].write('\t"%s_inst"\n' %
                                                      fixed_name)
                self.mi_files[type].write('end instgroup\n\n')
                # instance "rotated_root_grp_inst"
                self.mi_files[type].write('instance "rotated_root_grp_inst" ' +
                                          '"rotated_root_grp"\n')
                self.mi_files[type].write('\ttransform\n')
                self.mi_files[type].write('\t\t 1.0  0.0  0.0  0.0\n')
                # rotate around x-axis by -90 degrees (swap y- and
                # z-coords and negate)
                self.mi_files[type].write('\t\t 0.0  0.0  1.0  0.0 #  z\n') #  z
                self.mi_files[type].write('\t\t 0.0 -1.0  0.0  0.0 # -y\n') # -y
                self.mi_files[type].write('\t\t 0.0  0.0  0.0  1.0\n')
                self.mi_files[type].write('end instance\n\n')
                # instgroup "root_grp"
                self.mi_files[type].write('instgroup "root_grp"\n')
                self.mi_files[type].write('\t"%s_inst"\n' %
                                          self.camera_obj.name)
                self.mi_files[type].write('\t"rotated_root_grp_inst"\n')
                self.mi_files[type].write('end instgroup\n\n')

    def exportGroup(self, name):
        type = "groups"
        # group
        if not self.groups[name]:
            group = bpy.data.groups[name]
            self.mi_files[type].write('# %s\n' % group.name)
            for child in group.objects:
                if child.type == 'MESH':
                    self.mi_files[type].write('# \t%s\n' % child.name)
                    self.examineMaterials(child)
                    if self.useBmesh:
                        self.exportBMesh(child)
                    else:
                        self.exportMesh(child)
                    self.exportInstance(child, use_transform = True,
                                        type = "groups")
                elif child.type == 'EMPTY' and not child.parent:
                    self.mi_files[type].write('# \t%s\n' % child.name)
                    print("TODO: %s" % child.name)
                elif child.type == 'SURFACE':
                    self.mi_files[type].write('# \t%s\n' % child.name)
                    self.examineMaterials(child)
                    self.exportSurface(child)
                    self.exportInstance(child, use_transform = True,
                                        type = "groups")
        self.groups[name] = self.groups[name] + 1

    def exportGroupInstgroup(self, name, group_name):
        type = "groups"
        fixed_name = fix_name(name)
        # instgroup
        self.mi_files[type].write('instgroup "%s_grp"\n' % fixed_name)
        group = bpy.data.groups[group_name]
        for child in group.objects:
            self.mi_files[type].write('\t"%s_inst"\n' % child.name)
        self.mi_files[type].write('end instgroup\n\n')
        # instance
        self.mi_files[type].write('instance "%s_grp_inst" ' %
                                  fixed_name +
                                  '"%s_grp" \n' % fixed_name)
        # don't use the original matrix !!!
        obj = bpy.data.objects[name]
        inverse = obj.matrix_local.copy()
        inverse.invert()
        self.mi_files[type].write('\ttransform\n')
        self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                  (inverse[0][0],
                                   inverse[1][0],
                                   inverse[2][0],
                                   inverse[3][0]))
        self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                  (inverse[0][1],
                                   inverse[1][1],
                                   inverse[2][1],
                                   inverse[3][1]))
        self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                  (inverse[0][2],
                                   inverse[1][2],
                                   inverse[2][2],
                                   inverse[3][2]))
        self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                  (inverse[0][3],
                                   inverse[1][3],
                                   inverse[2][3],
                                   inverse[3][3]))
        self.mi_files[type].write('end instance\n\n')

    def exportGroupInstance(self, obj, level, children, next_level):
        type = "instances_groups"
        fixed_name = fix_name(obj.name)
        # if the group is a mesh let's export the mesh first !!!
        if obj.type == 'MESH':
            if self.useBmesh:
                self.exportBMesh(obj)
            else:
                self.exportMesh(obj)
            self.exportInstance(obj, use_transform = False)
        # instgroup
        self.mi_files[type].write('instgroup "%s_grp"\n' % fixed_name)
        for child in children:
            try:
                own_children = next_level[child.name][1]
            except KeyError:
                if child.type != 'EMPTY':
                    fixed_child_name = fix_name(child.name)
                    self.mi_files[type].write('\t"%s_inst"\n' % 
                                              fixed_child_name)
            else:
                fixed_child_name = fix_name(child.name)
                if own_children:
                    self.mi_files[type].write('\t"%s_grp_inst"\n' % 
                                              fixed_child_name)
                else:
                    if child.type != 'EMPTY':
                        self.mi_files[type].write('\t"%s_inst"\n' % 
                                                  fixed_child_name)
                    else:
                        if child.dupli_type == 'GROUP':
                            self.exportGroupInstgroup(child.name,
                                                      child.dupli_group.name)
                        self.mi_files[type].write('\t"%s_grp_inst"\n' % 
                                                  fixed_child_name)
        # if the group is a mesh let's add the mesh itself to the group !!!
        if obj.type == 'MESH':
            self.mi_files[type].write('\t"%s_inst"\n' % fixed_name)
        self.mi_files[type].write('end instgroup\n\n')
        # instance
        self.mi_files[type].write('instance "%s_grp_inst" ' %
                                  fixed_name +
                                  '"%s_grp" \n' % fixed_name)
        # don't use the original matrix !!!
        inverse = obj.matrix_local.copy()
        inverse.invert()
        self.mi_files[type].write('\ttransform\n')
        self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                  (inverse[0][0],
                                   inverse[1][0],
                                   inverse[2][0],
                                   inverse[3][0]))
        self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                  (inverse[0][1],
                                   inverse[1][1],
                                   inverse[2][1],
                                   inverse[3][1]))
        self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                  (inverse[0][2],
                                   inverse[1][2],
                                   inverse[2][2],
                                   inverse[3][2]))
        self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                  (inverse[0][3],
                                   inverse[1][3],
                                   inverse[2][3],
                                   inverse[3][3]))
        self.mi_files[type].write('end instance\n\n')

    def exportSurface(self, obj):
        # check for primitives
        isPrimitive, primType = self.isPrimitive(obj)
        # return early for (known) primitives
        if isPrimitive:
            if primType in ["Cone", "Cylinder", "Disk", "Ring", "Sphere",
                            "Square"]:
                return
        type = "objects"
        # write to "objects" MI file
        fixed_name = fix_name(obj.name)
        self.mi_files[type].write('object "%s_obj"\n' % fixed_name)
        self.mi_files[type].write('\tvisible\t%s\n' % "on")
        self.mi_files[type].write('\tshadow\t%s\n' % "on")
        self.mi_files[type].write('\ttrace\t\t%s\n' % "on")
        self.mi_files[type].write('\tface\t\t%s\n' % "both")
        self.mi_files[type].write('\ttagged\t%s\n' % "on")
        # "BSpline1" is need for textures
        self.mi_files[type].write('\tbasis \"BSpline1\" rational bspline 1\n') # textures
        data = obj.data
        for spline in data.splines:
            if (spline.order_u == 4 and spline.order_v == 4): # bicubic
                self.mi_files[type].write('\tbasis \"BSpline3\" rational bspline 3\n')
                self.mi_files[type].write('\tgroup\n')
                self.mi_files[type].write('\t\t# control vertices\n')
                for v in spline.points:
                    w = v.co[3] # not used yet
                    self.mi_files[type].write('\t\t%s %s %s\n' % (v.co[0], v.co[1], v.co[2]))
                self.mi_files[type].write('\t\t# texture vectors\n')
                self.mi_files[type].write('\t\t%s %s 0.0\n' % (0.0, 0.0))
                self.mi_files[type].write('\t\t%s %s 0.0\n' % (1.0, 0.0))
                self.mi_files[type].write('\t\t%s %s 0.0\n' % (0.0, 1.0))
                self.mi_files[type].write('\t\t%s %s 0.0\n' % (1.0, 1.0))
                self.mi_files[type].write('\t\t# vertices\n')
                for i in range(len(spline.points)):
                    self.mi_files[type].write('\t\tv %s\n' % i)
                self.mi_files[type].write('\t\t# texture vertices\n') 
                for i in range(4):
                   self.mi_files[type].write('\t\tv %s\n' % (len(spline.points) + i))
                self.mi_files[type].write('\t\t# surface\n')
                self.mi_files[type].write('\t\tsurface \"Surface\" 0\n') # 0 = mat index
                # TODO: this is an assumption !!!
                self.mi_files[type].write('\t\t\t# u-knots\n')
                self.mi_files[type].write('\t\t\t\"BSpline%d\" %s %s\n' % (3, 0.0, 1.0))
                for i in range(4):
                    self.mi_files[type].write('\t\t\t%s\n' % 0.0)
                for i in range(4):
                    self.mi_files[type].write('\t\t\t%s\n' % 1.0)
                self.mi_files[type].write('\t\t\t# v-knots\n')
                self.mi_files[type].write('\t\t\t\"BSpline%d\" %s %s\n' % (3, 0.0, 1.0))
                for i in range(4):
                    self.mi_files[type].write('\t\t\t%s\n' % 0.0)
                for i in range(4):
                    self.mi_files[type].write('\t\t\t%s\n' % 1.0)
                self.mi_files[type].write('\t\t\t# CV indices\n')
                for i in range(len(spline.points)):
                  self.mi_files[type].write('\t\t\t%s\n' % i)
                self.mi_files[type].write('\t\t\t# texture surface\n')
                self.mi_files[type].write('\t\t\tvolume vector texture\n')
                self.mi_files[type].write('\t\t\t\t\"BSpline1\"\n')
                self.mi_files[type].write('\t\t\t\t%s\n' % 0.0)
                self.mi_files[type].write('\t\t\t\t%s\n' % 0.0)
                self.mi_files[type].write('\t\t\t\t%s\n' % 1.0)
                self.mi_files[type].write('\t\t\t\t%s\n' % 1.0)
                self.mi_files[type].write('\t\t\t\t\"BSpline1\"\n')
                self.mi_files[type].write('\t\t\t\t%s\n' % 0.0)
                self.mi_files[type].write('\t\t\t\t%s\n' % 0.0)
                self.mi_files[type].write('\t\t\t\t%s\n' % 1.0)
                self.mi_files[type].write('\t\t\t\t%s\n' % 1.0)
                self.mi_files[type].write('\t\t\t\t# texture CV indices\n')
                for i in range(4):
                  self.mi_files[type].write('\t\t\t\t%s\n' % (len(spline.points) + i))
                self.mi_files[type].write('\t\tapproximate surface parametric 6.0 6.0 \"Surface\"\n')
                self.mi_files[type].write('\tend group\n')
                self.mi_files[type].write('end object\n\n')

    def exportMesh(self, obj):
        # check for primitives
        isPrimitive, primType = self.isPrimitive(obj)
        # return early for (known) primitives
        if isPrimitive:
            if primType in ["Cone", "Cylinder", "Disk", "Ring", "Sphere",
                            "Square"]:
                return
        type = "objects"
        # write to "objects" MI file
        fixed_name = fix_name(obj.name)
        self.mi_files[type].write('object "%s_obj"\n' % fixed_name)
        self.mi_files[type].write('\tvisible\t%s\n' % "on")
        self.mi_files[type].write('\tshadow\t%s\n' % "on")
        self.mi_files[type].write('\ttrace\t\t%s\n' % "on")
        self.mi_files[type].write('\ttagged\t%s\n' % "on")
        # bounding box
        minX = maxX = obj.bound_box[0][0]
        minY = maxY = obj.bound_box[0][1]
        minZ = maxZ = obj.bound_box[0][2]
        bb = obj.bound_box
        for i in list(range(8)):
            corner = bb[i]
            if corner[0] < minX:
                minX = corner[0]
            if corner[1] < minY:
                minY = corner[1]
            if corner[2] < minZ:
                minZ = corner[2]
            if corner[0] > maxX:
                maxX = corner[0]
            if corner[1] > maxY:
                maxY = corner[1]
            if corner[2] > maxZ:
                maxZ = corner[2]
        self.mi_files[type].write('\tbox %s %s %s\n\t    %s %s %s\n' %
                                  (minX, minY, minZ, maxX, maxY, maxZ))
        self.mi_files[type].write('\tgroup\n')
        # vertices (coordinates)
        data = obj.data
        if data.uv_textures.active and data.uv_textures.active.data:
            uv_layer = data.uv_textures.active.data
        else:
            uv_layer = None
        self.mi_files[type].write('\t\t# %s vertices\n' % len(data.vertices))
        for i in list(range(len(data.vertices))):
            vertex = data.vertices[i]
            x = vertex.co[0]
            y = vertex.co[1]
            z = vertex.co[2]
            self.mi_files[type].write('\t\t%s %s %s\n' % (x, y, z))
        self.mi_files[type].write('\t\t# %s vertex normals\n' %
                                  len(data.vertices))
        for i in list(range(len(data.vertices))):
            vertex = data.vertices[i]
            x = vertex.normal[0]
            y = vertex.normal[1]
            z = vertex.normal[2]
            self.mi_files[type].write('\t\t%s %s %s\n' % (x, y, z))
        self.mi_files[type].write('\t\t# %s face normals\n' %
                                  len(data.faces))
        for face in data.faces:
            x = face.normal[0]
            y = face.normal[1]
            z = face.normal[2]
            self.mi_files[type].write('\t\t%s %s %s\n' % (x, y, z))
        # uv-coordinates
        if uv_layer:
            self.mi_files[type].write('\t\t# uv coordinates\n')
            for fi in list(range(len(data.faces))): # face index
                face = data.faces[fi]
                for vi in list(range(len(face.vertices))):
                    uv = uv_layer[fi].uv[vi]
                    self.mi_files[type].write('\t\t%s %s 0.0\n' %
                                              (uv[0], uv[1]))
        # vertex information (relation between vertices, normals, and uv-coords)
        self.mi_files[type].write('\t\t# vertex information\n')
        counter = 0
        for fi in list(range(len(data.faces))): # face index
            face = data.faces[fi]
            numVerts = len(face.vertices)
            if numVerts in [3, 4]:
                for i, vi in enumerate(face.vertices): # vertex index
                    if face.use_smooth:
                        # vertex normal
                        if uv_layer:
                            self.mi_files[type].write('\t\tv %s n %s t %s ' %
                                                      (vi,
                                                       vi + len(data.vertices),
                                                       counter + i +
                                                       2 * len(data.vertices) +
                                                       len(data.faces)))
                            self.mi_files[type].write('# vn\n')
                        else:
                            self.mi_files[type].write('\t\tv %s n %s # vn\n' %
                                                      (vi,
                                                       vi + len(data.vertices)))
                    else:
                        # face normal
                        if uv_layer:
                            self.mi_files[type].write('\t\tv %s n %s t %s ' %
                                                      (vi,
                                                       fi +
                                                       2 * len(data.vertices),
                                                       counter + i +
                                                       2 * len(data.vertices) +
                                                       len(data.faces)))
                            self.mi_files[type].write('# fn\n')
                        else:
                            self.mi_files[type].write('\t\tv %s n %s # fn\n' %
                                                      (vi,
                                                       fi +
                                                       2 * len(data.vertices)))
                counter += numVerts
        # triangles/quads
        self.mi_files[type].write('\t\t# triangles/quads\n')
        counter = 0
        for face in data.faces:
            mat_idx = face.material_index
            numVerts = len(face.vertices)
            if not (numVerts in [3, 4]):
                self.mi_files[type].write('# no triangle/quad found\n')
            else:
                if numVerts == 3:
                    self.mi_files[type].write('\t\tc %s  %s %s %s\n' % # c
                                              (mat_idx,
                                               counter,
                                               counter + 1,
                                               counter + 2))
                    counter += 3
                else: # numVerts == 4
                    self.mi_files[type].write('\t\tp %s  %s %s %s %s\n' % # p
                                              (mat_idx,
                                               counter,
                                               counter + 1,
                                               counter + 2,
                                               counter + 3))
                    counter += 4
        self.mi_files[type].write('\tend group\n')
        self.mi_files[type].write('end object\n\n')

    def exportBMesh(self, obj):
        # check for primitives
        isPrimitive, primType = self.isPrimitive(obj)
        # return early for (known) primitives
        if isPrimitive:
            if primType in ["Cone", "Cylinder", "Disk", "Ring", "Sphere",
                            "Square"]:
                return
        type = "objects"
        # write to "objects" MI file
        fixed_name = fix_name(obj.name)
        self.mi_files[type].write('object "%s_obj"\n' % fixed_name)
        self.mi_files[type].write('\tvisible\t%s\n' % "on")
        self.mi_files[type].write('\tshadow\t%s\n' % "on")
        self.mi_files[type].write('\ttrace\t\t%s\n' % "on")
        self.mi_files[type].write('\ttagged\t%s\n' % "on")
        # bounding box
        minX = maxX = obj.bound_box[0][0]
        minY = maxY = obj.bound_box[0][1]
        minZ = maxZ = obj.bound_box[0][2]
        bb = obj.bound_box
        for i in list(range(8)):
            corner = bb[i]
            if corner[0] < minX:
                minX = corner[0]
            if corner[1] < minY:
                minY = corner[1]
            if corner[2] < minZ:
                minZ = corner[2]
            if corner[0] > maxX:
                maxX = corner[0]
            if corner[1] > maxY:
                maxY = corner[1]
            if corner[2] > maxZ:
                maxZ = corner[2]
        self.mi_files[type].write('\tbox %s %s %s\n\t    %s %s %s\n' %
                                  (minX, minY, minZ, maxX, maxY, maxZ))
        self.mi_files[type].write('\tgroup\n')
        # vertices (coordinates)
        data = obj.data
        if data.uv_textures.active and data.uv_textures.active.data:
            uv_layer = data.uv_textures.active.data
        else:
            uv_layer = None
        self.mi_files[type].write('\t\t# %s vertices\n' % len(data.vertices))
        for i in list(range(len(data.vertices))):
            vertex = data.vertices[i]
            x = vertex.co[0]
            y = vertex.co[1]
            z = vertex.co[2]
            self.mi_files[type].write('\t\t%s %s %s\n' % (x, y, z))
        self.mi_files[type].write('\t\t# %s vertex normals\n' %
                                  len(data.vertices))
        for i in list(range(len(data.vertices))):
            vertex = data.vertices[i]
            x = vertex.normal[0]
            y = vertex.normal[1]
            z = vertex.normal[2]
            self.mi_files[type].write('\t\t%s %s %s\n' % (x, y, z))
        self.mi_files[type].write('\t\t# %s face normals\n' %
                                  len(data.polygons))
        for face in data.polygons:
            x = face.normal[0]
            y = face.normal[1]
            z = face.normal[2]
            self.mi_files[type].write('\t\t%s %s %s\n' % (x, y, z))
        # uv-coordinates
        if uv_layer:
            self.mi_files[type].write('\t\t# uv coordinates\n')
            for fi in list(range(len(data.polygons))): # face index
                face = data.polygons[fi]
                uv_layers = data.uv_layer_clone.data.values()
                for vi in list(range(len(face.vertices))):
                    uv = uv_layers[vi].uv
                    self.mi_files[type].write('\t\t%s %s 0.0\n' %
                                              (uv[0], uv[1]))
        # vertex information (relation between vertices, normals, and uv-coords)
        self.mi_files[type].write('\t\t# vertex information\n')
        counter = 0
        for fi in list(range(len(data.polygons))): # face index
            face = data.polygons[fi]
            numVerts = len(face.vertices)
            if numVerts in [3, 4]:
                for i, vi in enumerate(face.vertices): # vertex index
                    if face.use_smooth:
                        # vertex normal
                        if uv_layer:
                            self.mi_files[type].write('\t\tv %s n %s t %s ' %
                                                      (vi,
                                                       vi + len(data.vertices),
                                                       counter + i +
                                                       2 * len(data.vertices) +
                                                       len(data.polygons)))
                            self.mi_files[type].write('# vn\n')
                        else:
                            self.mi_files[type].write('\t\tv %s n %s # vn\n' %
                                                      (vi,
                                                       vi + len(data.vertices)))
                    else:
                        # face normal
                        if uv_layer:
                            self.mi_files[type].write('\t\tv %s n %s t %s ' %
                                                      (vi,
                                                       fi +
                                                       2 * len(data.vertices),
                                                       counter + i +
                                                       2 * len(data.vertices) +
                                                       len(data.polygons)))
                            self.mi_files[type].write('# fn\n')
                        else:
                            self.mi_files[type].write('\t\tv %s n %s # fn\n' %
                                                      (vi,
                                                       fi +
                                                       2 * len(data.vertices)))
                counter += numVerts
        # triangles/quads
        self.mi_files[type].write('\t\t# triangles/quads\n')
        counter = 0
        for face in data.polygons:
            mat_idx = face.material_index
            numVerts = len(face.vertices)
            if not (numVerts in [3, 4]):
                self.mi_files[type].write('# no triangle/quad found\n')
            else:
                if numVerts == 3:
                    self.mi_files[type].write('\t\tc %s  %s %s %s\n' % # c
                                              (mat_idx,
                                               counter,
                                               counter + 1,
                                               counter + 2))
                    counter += 3
                else: # numVerts == 4
                    self.mi_files[type].write('\t\tp %s  %s %s %s %s\n' % # p
                                              (mat_idx,
                                               counter,
                                               counter + 1,
                                               counter + 2,
                                               counter + 3))
                    counter += 4
        self.mi_files[type].write('\tend group\n')
        self.mi_files[type].write('end object\n\n')

    def exportCamera(self, obj):
        type = "camera"
        # camera
        camera = obj.data
        # frame
        frame = 1
        # output
        filetype = "exr"
        if self.options.renderer == "iray":
            filename = "iray.exr"
        elif self.options.renderer == "mental_ray":
            filename = "mental_ray.exr"
        # focal
        focal = camera.lens
        # aperture
        aperture = 2 * 16.0
        # aspect
        aspect = self.resolution[0] / float(self.resolution[1])
        # check for HDR lighting
        scene = bpy.context.scene
        world = scene.world
        if world:
            texture = world.active_texture
            if texture:
                image = texture.image
                if image:
                    src = image.filepath
                    if ((len(src) > len("//..") and
                         src[:len("//..")] == "//..")):
                        head, tail = os.path.split(bpy.data.filepath)
                        src = os.path.join(head, src[2:]) # get rid of "//"
                    basename = os.path.basename(src)
                    shortname = fix_name(basename)
                    self.hdr = shortname
        # write camera
        fixed_name = fix_name(obj.name)
        if self.useSun:
            if self.options.renderer == "iray":
                # now write the environment shader for the sky (contains
                # sun direction)
                self.mi_files[type].write('shader "sun_sky_shd" "%s" (\n' %
                                          "mia_physicalsky")
                self.mi_files[type].write('\t"on" on,\n')
                self.mi_files[type].write('\t"multiplier" %s,\n' %
                                          SUN_MULTIPLIER)
                self.mi_files[type].write('\t"rgb_unit_conversion" ' +
                                          '1.0 1.0 1.0 1,\n')
                self.mi_files[type].write('\t"haze" 0,\n')
                self.mi_files[type].write('\t"redblueshift" 0,\n')
                self.mi_files[type].write('\t"saturation" 1,\n')
                self.mi_files[type].write('\t"horizon_height" 0,\n')
                self.mi_files[type].write('\t"horizon_blur" 0.1,\n')
                self.mi_files[type].write('\t"ground_color" 0.2 0.2 0.2 1,\n')
                self.mi_files[type].write('\t"night_color" 0 0 0 1,\n')
                self.mi_files[type].write('\t"sun_direction" %s %s %s,\n' %
                                          (self.sunDir[0],
                                           self.sunDir[1],
                                           self.sunDir[2]))
                data = self.sunLight.data
                energy = data.energy # use energy to "dim" sun light
                if IN_DOOR:
                    self.mi_files[type].write('\t"sun_disk_intensity" %s,\n' %
                                              (energy * SUN_INTENSITY_IN))
                else:
                    self.mi_files[type].write('\t"sun_disk_intensity" %s,\n' %
                                              (energy * SUN_INTENSITY_OUT))
                self.mi_files[type].write('\t"sun_disk_scale" 1,\n')
                self.mi_files[type].write('\t"sun_glow_intensity" 1,\n')
                self.mi_files[type].write('\t"use_background" off,\n')
                self.mi_files[type].write('\t"visibility_distance" 0,\n')
                self.mi_files[type].write('\t"y_is_up" on,\n')
                self.mi_files[type].write('\t"flags" 0,\n')
                self.mi_files[type].write('\t"sky_luminance_mode" 0,\n')
                self.mi_files[type].write('\t"zenith_luminance" 0,\n')
                self.mi_files[type].write('\t"diffuse_horizontal_illuminance"' +
                                          ' 0,\n')
                self.mi_files[type].write('\t"a" 0,\n')
                self.mi_files[type].write('\t"b" 0,\n')
                self.mi_files[type].write('\t"c" 0,\n')
                self.mi_files[type].write('\t"d" 0,\n')
                self.mi_files[type].write('\t"e" 0)\n\n')
                self.mi_files[type].write('camera "%s_cam"\n' % fixed_name)
                self.mi_files[type].write('\tenvironment\t= ' +
                                          '"sun_sky_shd"\n')
            else:
                self.mi_files[type].write('camera "%s_cam"\n' % fixed_name)
        elif self.hdr:
            self.mi_files[type].write('shader "mib_lookup_spherical_shd" ' +
                                      '"mib_lookup_spherical" (\n')
            self.mi_files[type].write('\t"rotate" 0,\n')
            self.mi_files[type].write('\t"tex" "%s")\n\n' % self.hdr)
            self.mi_files[type].write('camera "%s_cam"\n' % fixed_name)
            self.mi_files[type].write('\tenvironment\t= ' +
                                      '"mib_lookup_spherical_shd"\n')
        else:
            self.mi_files[type].write('camera "%s_cam"\n' % fixed_name)
        self.mi_files[type].write('\tframe\t\t%s\n' % frame)
        # primary framebuffer
        self.mi_files[type].write('\tframebuffer\t\t"%s"\n' % "primary")
        self.mi_files[type].write('\t\tdatatype\t\"%s\"\n' % "rgba_h")
        self.mi_files[type].write('\t\tfiltering\ton\n')
        self.mi_files[type].write('\t\tprimary\t\ton\n')
        self.mi_files[type].write('\t\tfilename\t"%s"\n' % filename)
        # ambient framebuffer
        if self.options.ambient_fb:
            self.mi_files[type].write('\tframebuffer\t\t"%s"\n' % "AMBIENT")
            self.mi_files[type].write('\t\tdatatype\t\"%s\"\n' % "rgb_h")
            self.mi_files[type].write('\t\tfiltering\ton\n')
            self.mi_files[type].write('\t\tuser\t\t\ton\n')
            self.mi_files[type].write('\t\tfilename\t"%s"\n' % filename)
        # occlusion framebuffer
        if self.options.occlusion_fb:
            self.mi_files[type].write('\tframebuffer\t\t"%s"\n' % "OCCLUSION")
            self.mi_files[type].write('\t\tdatatype\t\"%s\"\n' % "rgb_h")
            self.mi_files[type].write('\t\tfiltering\ton\n')
            self.mi_files[type].write('\t\tuser\t\t\ton\n')
            self.mi_files[type].write('\t\tfilename\t"%s"\n' % filename)
        for i in range(self.light_counter):
            # diffuse framebuffer
            if self.options.diffuse_fb:
                self.mi_files[type].write('\tframebuffer\t\t"%s%s"\n' %
                                          ("DIFFUSE", (i + 1)))
                self.mi_files[type].write('\t\tdatatype\t\"%s\"\n' % "rgb_h")
                self.mi_files[type].write('\t\tfiltering\ton\n')
                self.mi_files[type].write('\t\tuser\t\t\ton\n')
                self.mi_files[type].write('\t\tfilename\t"%s"\n' % filename)
            # specular framebuffer
            if self.options.specular_fb:
                self.mi_files[type].write('\tframebuffer\t\t"%s%s"\n' %
                                          ("SPECULAR", (i + 1)))
                self.mi_files[type].write('\t\tdatatype\t\"%s\"\n' % "rgb_h")
                self.mi_files[type].write('\t\tfiltering\ton\n')
                self.mi_files[type].write('\t\tuser\t\t\ton\n')
                self.mi_files[type].write('\t\tfilename\t"%s"\n' % filename)
            # shadows framebuffer
            if self.options.shadows_fb:
                self.mi_files[type].write('\tframebuffer\t\t"%s%s"\n' %
                                          ("SHADOWS", (i + 1)))
                self.mi_files[type].write('\t\tdatatype\t\"%s\"\n' % "rgb_h")
                self.mi_files[type].write('\t\tfiltering\ton\n')
                self.mi_files[type].write('\t\tuser\t\t\ton\n')
                self.mi_files[type].write('\t\tfilename\t"%s"\n' % filename)
        # reflect framebuffer
        if self.options.reflect_fb:
            self.mi_files[type].write('\tframebuffer\t\t"%s"\n' % "REFLECT")
            self.mi_files[type].write('\t\tdatatype\t\"%s\"\n' % "rgb_h")
            self.mi_files[type].write('\t\tfiltering\ton\n')
            self.mi_files[type].write('\t\tuser\t\t\ton\n')
            self.mi_files[type].write('\t\tfilename\t"%s"\n' % filename)
        # depth framebuffer
        if self.options.depth_fb:
            self.mi_files[type].write('\tframebuffer\t\t"%s"\n' % "Z")
            self.mi_files[type].write('\t\tdatatype\t\"%s\"\n' % "z")
            self.mi_files[type].write('\t\tfiltering\toff\n')
            self.mi_files[type].write('\t\tuser\t\t\toff\n')
            self.mi_files[type].write('\t\tfilename\t"%s"\n' % filename)
        # normals framebuffer
        if self.options.normals_fb:
            self.mi_files[type].write('\tframebuffer\t\t"%s"\n' % "NORMALS")
            self.mi_files[type].write('\t\tdatatype\t\"%s\"\n' % "n")
            self.mi_files[type].write('\t\tfiltering\toff\n')
            self.mi_files[type].write('\t\tuser\t\t\ton\n')
            self.mi_files[type].write('\t\tfilename\t"%s"\n' % filename)
        # materials framebuffer
        if self.options.shadows_fb:
            self.mi_files[type].write('\tframebuffer\t\t"%s"\n' % "MATERIALS")
            self.mi_files[type].write('\t\tdatatype\t\"%s\"\n' % "tag")
            self.mi_files[type].write('\t\tfiltering\toff\n')
            self.mi_files[type].write('\t\tuser\t\t\ton\n')
            self.mi_files[type].write('\t\tfilename\t"%s"\n' % "materials.tt")
        # instances framebuffer
        if self.options.shadows_fb:
            self.mi_files[type].write('\tframebuffer\t\t"%s"\n' % "INSTANCES")
            self.mi_files[type].write('\t\tdatatype\t\"%s\"\n' % "tag")
            self.mi_files[type].write('\t\tfiltering\toff\n')
            self.mi_files[type].write('\t\tuser\t\t\ton\n')
            self.mi_files[type].write('\t\tfilename\t"%s"\n' % "instances.tt")
        # remaining camera options
        self.mi_files[type].write('\tfocal\t\t%s\n' % focal)
        self.mi_files[type].write('\taperture\t%s\n' % aperture)
        self.mi_files[type].write('\taspect\t\t%s\n' % aspect)
        self.mi_files[type].write('\tresolution\t%s %s\n' %
                                  (self.resolution[0], self.resolution[1]))
        if not (self.hdr and not self.useSun): # no tonemappper for HDR
            if self.options.renderer == "iray":
                self.mi_files[type].write('\tattribute string ' +
                                          '"tm_tonemapper" ' +
                                          '"mia_exposure_photographic"\n')
                if self.useSun:
                    if IN_DOOR:
                        self.mi_files[type].write('\tattribute scalar ' +
                                                  '"mip_cm2_factor" %s\n' %
                                                  SUN_CM2_FACTOR_IN)
                    else:
                        self.mi_files[type].write('\tattribute scalar ' +
                                                  '"mip_cm2_factor" %s\n' %
                                                  SUN_CM2_FACTOR_OUT)
                else:
                    self.mi_files[type].write('\tattribute scalar ' +
                                              '"mip_cm2_factor" %s\n' %
                                              1.0) # do not dim
                self.mi_files[type].write('\tattribute color ' +
                                          '"mip_whitepoint" ' +
                                          '%s %s %s %s\n' %
                                          (1.04287, 0.983863, 1.03358, 1.0))
                if self.useSun:
                    self.mi_files[type].write('\tattribute scalar ' +
                                              '"mip_film_iso" ' +
                                              '100.0\n')
                else:
                    self.mi_files[type].write('\tattribute scalar '
                                              '"mip_film_iso" ' +
                                              '800.0\n')
                self.mi_files[type].write('\tattribute scalar ' +
                                          '"mip_camera_shutter" 250.0\n')
                self.mi_files[type].write('\tattribute scalar ' +
                                          '"mip_f_number" 8.0\n')
                self.mi_files[type].write('\tattribute scalar ' +
                                          '"mip_vignetting" ' +
                                          '0.0\n')
                self.mi_files[type].write('\tattribute scalar ' +
                                          '"mip_crush_blacks" 0.2\n')
                self.mi_files[type].write('\tattribute scalar ' +
                                          '"mip_burn_highlights" 0.25\n')
                self.mi_files[type].write('\tattribute scalar ' +
                                          '"mip_saturation" ' +
                                          '1.0\n')
                self.mi_files[type].write('\tattribute scalar ' +
                                          '"mip_gamma" 2.2\n')
        self.mi_files[type].write('end camera\n\n')
        self.exportInstance(obj)

    def exportInstance(self, obj, use_transform = True,
                       type = "instances_groups"):
        # check for primitives
        isPrimitive, primType = self.isPrimitive(obj)
        fixed_name = fix_name(obj.name)
        if obj.type == 'CAMERA':
            self.mi_files[type].write('instance "%s_inst" ' %
                                      fixed_name +
                                      '"%s_cam" \n' % fixed_name)
            # don't use the original matrix !!!
            inverse = obj.matrix_world.copy()
            inverse.invert()
        elif obj.type == 'LAMP':
            self.mi_files[type].write('instance "%s_inst" ' %
                                      fixed_name +
                                      '"%s_lgt" \n' % fixed_name)
            # don't use the original matrix !!!
            inverse = obj.matrix_local.copy()
            inverse.invert()
        elif obj.type == 'MESH':
            if isPrimitive:
                if primType == "Cone":
                    self.mi_files[type].write('instance "%s_inst"\n' %
                                              fixed_name)
                    self.mi_files[type].write('\tgeometry "bw_cone" ' +
                                              '("name" "%s_obj")\n' % 
                                              fixed_name)
                elif primType == "Cylinder":
                    self.mi_files[type].write('instance "%s_inst"\n' %
                                              fixed_name)
                    self.mi_files[type].write('\tgeometry "bw_cylinder" ' +
                                              '("name" "%s_obj")\n' % 
                                              fixed_name)
                elif primType == "Disk":
                    self.mi_files[type].write('instance "%s_inst"\n' %
                                              fixed_name)
                    self.mi_files[type].write('\tgeometry "bw_disk" ' +
                                              '("name" "%s_obj")\n' % 
                                              fixed_name)
                elif primType == "Ring":
                    self.mi_files[type].write('instance "%s_inst"\n' %
                                              fixed_name)
                    self.mi_files[type].write('\tgeometry "bw_disk" ' +
                                              '("name" "%s_obj")\n' % 
                                              fixed_name)
                elif primType == "Sphere":
                    self.mi_files[type].write('instance "%s_inst"\n' %
                                              fixed_name)
                    self.mi_files[type].write('\tgeometry "bw_ball" ' +
                                              '("name" "%s_obj")\n' % 
                                              fixed_name)
                elif primType == "Square":
                    self.mi_files[type].write('instance "%s_inst"\n' %
                                              fixed_name)
                    self.mi_files[type].write('\tgeometry "bw_square" ' +
                                              '("name" "%s_obj")\n' % 
                                              fixed_name)
                else:
                    # fall back to behaviour below
                    self.mi_files[type].write('instance "%s_inst" ' %
                                              fixed_name +
                                              '"%s_obj" \n' % fixed_name)
            else:
                self.mi_files[type].write('instance "%s_inst" ' %
                                          fixed_name +
                                          '"%s_obj" \n' % fixed_name)
            # don't use the original matrix !!!
            inverse = obj.matrix_local.copy()
            inverse.invert()
        elif obj.type == 'SURFACE':
            if isPrimitive:
                if primType == "Cone":
                    p1 = obj.data.splines[0].points[0].co
                    p2 = obj.data.splines[0].points[8].co
                    self.mi_files[type].write('instance "%s_inst"\n' %
                                              fixed_name)
                    self.mi_files[type].write('\tgeometry "bw_hyperboloid" (\n'
                                              + '\t\t"name" "%s_obj",\n' % 
                                              fixed_name +
                                              '\t\t"p1x" %s,\n' %
                                              math.fabs(p1.y) + 
                                              '\t\t"p1y" %s,\n' % 0.0 +
                                              '\t\t"p1z" %s,\n' % p1.z +
                                              '\t\t"p2x" %s,\n' %
                                              math.fabs(p2.y) + 
                                              '\t\t"p2y" %s,\n' % 0.0 +
                                              '\t\t"p2z" %s,\n' % p2.z +
                                              '\t\t"thetamax" %s\n\t)\n' % 
                                              360.0)
                elif primType == "Cylinder":
                    self.mi_files[type].write('instance "%s_inst"\n' %
                                              fixed_name)
                    self.mi_files[type].write('\tgeometry "bw_cylinder" ' +
                                              '("name" "%s_obj", ' % 
                                              fixed_name +
                                              '"radius" 1.0)\n')
                elif primType == "Disk":
                    self.mi_files[type].write('instance "%s_inst"\n' %
                                              fixed_name)
                    self.mi_files[type].write('\tgeometry "bw_disk" ' +
                                              '("name" "%s_obj")\n' % 
                                              fixed_name)
                elif primType == "Ring":
                    p1 = obj.data.splines[0].points[0].co
                    p2 = obj.data.splines[0].points[8].co
                    self.mi_files[type].write('instance "%s_inst"\n' %
                                              fixed_name)
                    self.mi_files[type].write('\tgeometry "bw_hyperboloid" (\n'
                                              + '\t\t"name" "%s_obj",\n' % 
                                              fixed_name +
                                              '\t\t"p1x" %s,\n' %
                                              math.fabs(p1.y) + 
                                              '\t\t"p1y" %s,\n' % 0.0 +
                                              '\t\t"p1z" %s,\n' % p1.z +
                                              '\t\t"p2x" %s,\n' %
                                              math.fabs(p2.y) + 
                                              '\t\t"p2y" %s,\n' % 0.0 +
                                              '\t\t"p2z" %s,\n' % p2.z +
                                              '\t\t"thetamax" %s\n\t)\n' % 
                                              360.0)
                elif primType == "Sphere":
                    self.mi_files[type].write('instance "%s_inst"\n' %
                                              fixed_name)
                    self.mi_files[type].write('\tgeometry "bw_ball" ' +
                                              '("name" "%s_obj")\n' % 
                                              fixed_name)
                elif primType == "Square":
                    self.mi_files[type].write('instance "%s_inst"\n' %
                                              fixed_name)
                    self.mi_files[type].write('\tgeometry "bw_square" ' +
                                              '("name" "%s_obj")\n' % 
                                              fixed_name)
                else:
                    # fall back to behaviour below
                    self.mi_files[type].write('instance "%s_inst" ' %
                                              fixed_name +
                                              '"%s_obj" \n' % fixed_name)
            else:
                self.mi_files[type].write('instance "%s_inst" ' %
                                          fixed_name +
                                          '"%s_obj" \n' % fixed_name)
            # don't use the original matrix !!!
            inverse = obj.matrix_local.copy()
            if isPrimitive:
                if primType == "Cylinder":
                    org_mat = obj.matrix_local.copy()
                    inverse = (org_mat *
                               org_mat.Translation([0.0, 0.0, 0.5]) * 
                               org_mat.Rotation(math.radians(-90.0), 4, 'X'))
            inverse.invert()
        else:
            # this should NOT happen !!!
            self.log_file.write("unhandled type(%s) in exportInstance(...)\n" %
                                obj.type)
            self.mi_files[type].write('instance "%s_inst" ' %
                                      fixed_name +
                                      '"%s_cam" \n' % fixed_name)
        if obj.type in ['MESH', 'SURFACE']:
            self.mi_files[type].write('\tmaterial [ ')
            try:
                matNames = self.materialNames[obj]
            except KeyError:
                line = '"%s_mat"' % "default"
                self.mi_files[type].write(line)
            else:
                for matIndex in list(range(len(matNames))):
                    matName = matNames[matIndex]
                    if matIndex == 0:
                        line = '"%s_mat"' % matName
                        self.mi_files[type].write(line)
                    else:
                        line = ', "%s_mat"' % matName
                        self.mi_files[type].write(line)
            self.mi_files[type].write(' ]\n')
        if use_transform:
            self.mi_files[type].write('\ttransform\n')
            self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                      (inverse[0][0],
                                       inverse[1][0],
                                       inverse[2][0],
                                       inverse[3][0]))
            if obj.type == 'CAMERA':
                self.mi_files[type].write('\t\t%s %s %s %s #  z\n' % #  z
                                          (inverse[0][2],
                                           inverse[1][2],
                                           inverse[2][2],
                                           inverse[3][2]))
                self.mi_files[type].write('\t\t%s %s %s %s # -y\n' % # -y
                                          (-inverse[0][1],
                                           -inverse[1][1],
                                           -inverse[2][1],
                                           -inverse[3][1]))
            else:
                self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                          (inverse[0][1],
                                           inverse[1][1],
                                           inverse[2][1],
                                           inverse[3][1]))
                self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                          (inverse[0][2],
                                           inverse[1][2],
                                           inverse[2][2],
                                           inverse[3][2]))
            self.mi_files[type].write('\t\t%s %s %s %s\n' %
                                      (inverse[0][3],
                                       inverse[1][3],
                                       inverse[2][3],
                                       inverse[3][3]))
        # use an integer tag label
        self.tag_counter += 1
        self.mi_files[type].write('\ttag\t%s\n' % self.tag_counter)
        self.mi_files[type].write('end instance\n\n')

    def exportTextures(self):
        type = "textures"
        # loop through all textures
        textures = bpy.data.textures.items()
        for (name, texture) in textures:
            # we are only interested in images
            if texture.type == 'IMAGE':
                image = texture.image
                src = image.filepath
                if (len(src) > len("//..") and src[:len("//..")] == "//.."):
                    head, tail = os.path.split(bpy.data.filepath)
                    src = os.path.join(head, src[2:]) # get rid of "//"
                if os.path.exists(src):
                    # copy texture
                    dst = self.tex_dir
                    if self.options.copy_textures:
                        print("INFO: copy:")
                        print("\"%s\"" % src)
                        print("INFO: to:")
                        print("\"%s\"" % dst)
                        shutil.copy(src, dst)
                    basename = os.path.basename(src)
                    shortname = fix_name(basename)
                    if self.options.renderer == "iray":
                        self.mi_files[type].write('color texture ' +
                                                  '"%s" "%s/%s"\n\n' %
                                                  (shortname,
                                                   "textures",
                                                   basename))
                    elif self.options.renderer == "mental_ray":
                        self.mi_files[type].write('color texture ' +
                                                  '"%s" "%s/%s/%s"\n\n' %
                                                  (shortname,
                                                   self.base_name,
                                                   "textures",
                                                   basename))

    def exportShaders(self):
        type = "materials"
        # loop through all materials
        materials = bpy.data.materials.items()
        for (name, material) in materials:
            # check for textures
            textures_found = 0
            textures = {}
            for index in list(range(len(material.texture_slots))):
                slot = material.texture_slots[index]
                if slot:
                    texture = slot.texture
                    scale = slot.scale
                    offset = slot.offset
                    if slot.texture_coords == 'UV':
                        if texture.type == 'IMAGE':
                            image = texture.image
                            src = image.filepath
                            if ((len(src) > len("//..") and
                                 src[:len("//..")] == "//..")):
                                head, tail = os.path.split(bpy.data.filepath)
                                # get rid of "//"
                                src = os.path.join(head, src[2:])
                            basename = os.path.basename(src)
                            shortname = fix_name(basename)
                            if slot.use_map_color_diffuse:
                                # diffuse texture
                                textures["diffuse"] = [shortname, scale, offset]
                                textures_found += 1
            # ambient
            scene = bpy.context.scene
            world = scene.world
            ambient = [0.0, 0.0, 0.0]
            if world:
                ambient = world.ambient_color
            ambient_weight = material.ambient
            # diffuse_weight
            diffuse_weight = material.diffuse_intensity * DIFF_DIM_LEVEL
            # diffuse
            diffuse = material.diffuse_color
            # diffuse_roughness
            diffuse_roughness = 0.0
            # use roughness only if Oren-Nayar shader is activated
            if material.diffuse_shader == "OREN_NAYAR":
                diffuse_roughness = material.roughness
            # reflectivity
            reflectivity = material.specular_intensity * SPEC_DIM_LEVEL
            specular_intensity = material.specular_intensity # mill_massive
            # refl_color
            refl_color = material.specular_color
            specular_color = material.specular_color # mill_massive
            # refl_is_metal
            refl_is_metal = False
            if (material.raytrace_mirror.use):
                # metal
                refl_is_metal = True
            # refr_ior, refr_color, and transparency
            refr_ior = 1.0
            refr_color = [1.0, 1.0, 1.0]
            transparency = 0.0
            if (material.use_transparency):
                # glass
                refr_ior = material.raytrace_transparency.ior
                alpha = material.alpha
                maxC = diffuse[0]
                for c in diffuse:
                    if c > maxC:
                        maxC = c
                for i in range(len(refr_color)):
                    refr_color[i] = (1.0 - alpha) * (diffuse[i] / maxC)
                transparency = 1.0
                refl_is_metal = False
            # additional_color
            additional_color = [0.0, 0.0, 0.0]
            emit = material.emit
            if emit > 0.0:
                additional_color = [diffuse[0] * AC_FACTOR * emit,
                                    diffuse[1] * AC_FACTOR * emit,
                                    diffuse[2] * AC_FACTOR * emit]
            # write to "materials" MI file
            fixed_name = fix_name(name)
            if self.options.renderer == "iray":
                self.mi_files[type].write("shader \"%s_shd\" " % fixed_name +
                                          "\"metasl_mia_material_x\" (\n")
                self.mi_files[type].write("\t\"diffuse_weight\" %s,\n" %
                                          diffuse_weight)
                try:
                    diffuse_tex, scale, offset = textures["diffuse"]
                except KeyError:
                    # use color
                    self.mi_files[type].write("\t\"diffuse\" %s %s %s 1.0,\n" %
                                              (diffuse[0],
                                               diffuse[1],
                                               diffuse[2]))
                else:
                    type = "textures"
                    # texture lookup shader
                    repeatU = scale[0]
                    repeatV = scale[1]
                    offsetU = offset[0]
                    offsetV = offset[1]
                    self.mi_files[type].write('shader "%s_diff_tex" "%s" (\n' %
                                              (diffuse_tex,
                                               "Iray_texture_lookup"))
                    self.mi_files[type].write('\t"texture" "%s",\n' %
                                              shortname)
                    self.mi_files[type].write('\t"uv_transform" %s 0 0 %s,\n' %
                                              (repeatU, repeatV))
                    self.mi_files[type].write('\t"uv_offset" %s %s)\n\n' %
                                              (offsetU, offsetV))
                    # use texture
                    type = "materials"
                    self.mi_files[type].write("\t\"diffuse\" = " +
                                              "\"%s_diff_tex\",\n" %
                                              diffuse_tex)
                self.mi_files[type].write("\t\"diffuse_roughness\" %s,\n" %
                                          diffuse_roughness)
                self.mi_files[type].write("\t\"reflectivity\" %s,\n" %
                                          reflectivity)
                self.mi_files[type].write("\t\"refl_color\" %s %s %s 1.0,\n" %
                                          (refl_color[0],
                                           refl_color[1],
                                           refl_color[2]))
                self.mi_files[type].write("\t\"refl_gloss\" 1.0,\n")
                self.mi_files[type].write("\t\"refl_gloss_samples\" 8,\n")
                self.mi_files[type].write("\t\"refl_interpolate\" off,\n")
                self.mi_files[type].write("\t\"refl_hl_only\" off,\n")
                if refl_is_metal:
                    self.mi_files[type].write("\t\"refl_is_metal\" on,\n")
                else:
                    self.mi_files[type].write("\t\"refl_is_metal\" off,\n")
                self.mi_files[type].write("\t\"transparency\" %s,\n" %
                                          transparency)
                self.mi_files[type].write("\t\"refr_color\" %s %s %s 1.0,\n" %
                                          (refr_color[0],
                                           refr_color[1],
                                           refr_color[2]))
                self.mi_files[type].write("\t\"refr_gloss\" 1.0,\n")
                self.mi_files[type].write("\t\"refr_ior\" %s,\n" % refr_ior)
                self.mi_files[type].write("\t\"refr_gloss_samples\" 8,\n")
                self.mi_files[type].write("\t\"refr_interpolate\" off,\n")
                self.mi_files[type].write("\t\"refr_translucency\" off,\n")
                self.mi_files[type].write("\t\"refr_trans_color\" " +
                                          "0.7 0.6 0.5 1.0,\n")
                self.mi_files[type].write("\t\"refr_trans_weight\" 0.5,\n")
                self.mi_files[type].write("\t\"anisotropy\" 1.0,\n")
                self.mi_files[type].write("\t\"anisotropy_rotation\" 0.0,\n")
                self.mi_files[type].write("\t\"anisotropy_channel\" -1,\n")
                self.mi_files[type].write("\t\"brdf_fresnel\" off,\n")
                self.mi_files[type].write("\t\"brdf_0_degree_refl\" 0.82,\n")
                self.mi_files[type].write("\t\"brdf_90_degree_refl\" 1.0,\n")
                self.mi_files[type].write("\t\"brdf_curve\" 5.0,\n")
                self.mi_files[type].write("\t\"brdf_conserve_energy\" on,\n")
                self.mi_files[type].write("\t\"intr_grid_density\" 2,\n")
                self.mi_files[type].write("\t\"intr_refl_samples\" 2,\n")
                self.mi_files[type].write("\t\"intr_refl_ddist_on\" off,\n")
                self.mi_files[type].write("\t\"intr_refl_ddist\" 0,\n")
                self.mi_files[type].write("\t\"intr_refr_samples\" 2,\n")
                self.mi_files[type].write("\t\"single_env_sample\" off,\n")
                self.mi_files[type].write("\t\"refl_falloff_on\" off,\n")
                self.mi_files[type].write("\t\"refl_falloff_dist\" 0.0,\n")
                self.mi_files[type].write("\t\"refl_falloff_color_on\" off,\n")
                self.mi_files[type].write("\t\"refl_falloff_color\" " +
                                          "0.0 0.0 0.0 1.0,\n")
                self.mi_files[type].write("\t\"refl_depth\" 5,\n")
                self.mi_files[type].write("\t\"refl_cutoff\" 0.01,\n")
                self.mi_files[type].write("\t\"refr_falloff_on\" off,\n")
                self.mi_files[type].write("\t\"refr_falloff_dist\" 0.0,\n")
                self.mi_files[type].write("\t\"refr_falloff_color_on\" off,\n")
                self.mi_files[type].write("\t\"refr_falloff_color\" " +
                                          "0.0 0.0 0.0 1.0,\n")
                self.mi_files[type].write("\t\"refr_depth\" 5,\n")
                self.mi_files[type].write("\t\"refr_cutoff\" 0.01,\n")
                self.mi_files[type].write("\t\"indirect_multiplier\" 1.0,\n")
                self.mi_files[type].write("\t\"fg_quality\" 1.0,\n")
                self.mi_files[type].write("\t\"fg_quality_w\" 1.0,\n")
                self.mi_files[type].write("\t\"ao_on\" off,\n")
                self.mi_files[type].write("\t\"ao_samples\" 16,\n")
                self.mi_files[type].write("\t\"ao_distance\" 10.0,\n")
                self.mi_files[type].write("\t\"ao_dark\" 0.2 0.2 0.2 1.0,\n")
                self.mi_files[type].write("\t\"ao_ambient\" 0.0 0.0 0.0 1.0,\n")
                self.mi_files[type].write("\t\"ao_do_details\" on,\n")
                self.mi_files[type].write("\t\"thin_walled\" off,\n")
                self.mi_files[type].write("\t\"no_visible_area_hl\" on,\n")
                self.mi_files[type].write("\t\"skip_inside_refl\" on,\n")
                self.mi_files[type].write("\t\"do_refractive_caustics\" off,\n")
                self.mi_files[type].write("\t\"backface_cull\" on,\n")
                self.mi_files[type].write("\t\"propagate_alpha\" on,\n")
                self.mi_files[type].write("\t\"hl_vs_refl_balance\" 1.0,\n")
                self.mi_files[type].write("\t\"additional_color\" " +
                                          "%s %s %s 1.0,\n" %
                                          (additional_color[0],
                                           additional_color[1],
                                           additional_color[2]))
                ##bump
                self.mi_files[type].write("\t\"no_diffuse_bump\" off,\n")
                self.mi_files[type].write("\t\"mode\" 0)\n\n")
                # after the "shader" we export the "material"
                self.mi_files[type].write("material \"%s_mat\"\n" % fixed_name)
                self.mi_files[type].write("\t= \"%s_shd\"\n" % fixed_name)
                self.mi_files[type].write("end material\n\n")
            elif self.options.renderer == "mental_ray":
                # define used shader directly within material
                if self.options.global_surface_shader == 'ambient_occlusion':
                    self.mi_files[type].write("material \"%s_mat\"\n" % 
                                              fixed_name)
                    self.mi_files[type].write('\t"ambient_occlusion" (\n')
                    # use default sample settings for now (TODO: expose)
                    samples = 256
                    self.mi_files[type].write('\t\t"samples" %s\n' % samples)
                    self.mi_files[type].write('\t)\n')
                    self.mi_files[type].write('end material\n\n')
                elif self.options.global_surface_shader == 'depth_fade':
                    self.mi_files[type].write("material \"%s_mat\"\n" % 
                                              fixed_name)
                    self.mi_files[type].write('\t"depth_fade" (\n')
                    # get near and far from camera
                    near = self.camera.clip_start
                    far = self.camera.clip_end
                    self.mi_files[type].write('\t\t"near" %s,\n' % -near)
                    self.mi_files[type].write('\t\t"far" %s\n' % -far)
                    self.mi_files[type].write('\t)\n')
                    self.mi_files[type].write('end material\n\n')
                elif self.options.global_surface_shader == 'front_bright':
                    self.mi_files[type].write("material \"%s_mat\"\n" % 
                                              fixed_name)
                    self.mi_files[type].write('\t"front_bright" (\n')
                    # use diffuse color
                    self.mi_files[type].write('\t\t"tint" %s %s %s\n' %
                                              (diffuse[0],
                                               diffuse[1],
                                               diffuse[2]))
                    self.mi_files[type].write('\t)\n')
                    self.mi_files[type].write('end material\n\n')
                elif self.options.global_surface_shader == 'normals_as_colors':
                    self.mi_files[type].write("material \"%s_mat\"\n" % 
                                              fixed_name)
                    self.mi_files[type].write('\t"normals_as_colors" ()\n')
                    self.mi_files[type].write('end material\n\n')
                elif self.options.global_surface_shader == 'one_color':
                    self.mi_files[type].write("material \"%s_mat\"\n" % 
                                              fixed_name)
                    self.mi_files[type].write('\t"one_color" (\n')
                    # use diffuse color
                    self.mi_files[type].write('\t\t"color" %s %s %s\n' %
                                              (diffuse[0],
                                               diffuse[1],
                                               diffuse[2]))
                    self.mi_files[type].write('\t)\n')
                    self.mi_files[type].write('end material\n\n')
                elif self.options.global_surface_shader == 'show_u':
                    self.mi_files[type].write("material \"%s_mat\"\n" % 
                                              fixed_name)
                    self.mi_files[type].write('\t"show_uv" (\n')
                    self.mi_files[type].write('\t\t"u" %s,\n' % "on")
                    self.mi_files[type].write('\t\t"v" %s\n' % "off")
                    self.mi_files[type].write('\t)\n')
                    self.mi_files[type].write('end material\n\n')
                elif self.options.global_surface_shader == 'show_uv':
                    self.mi_files[type].write("material \"%s_mat\"\n" % 
                                              fixed_name)
                    self.mi_files[type].write('\t"show_uv" (\n')
                    self.mi_files[type].write('\t\t"u" %s,\n' % "on")
                    self.mi_files[type].write('\t\t"v" %s\n' % "on")
                    self.mi_files[type].write('\t)\n')
                    self.mi_files[type].write('end material\n\n')
                elif self.options.global_surface_shader == 'show_v':
                    self.mi_files[type].write("material \"%s_mat\"\n" % 
                                              fixed_name)
                    self.mi_files[type].write('\t"show_uv" (\n')
                    self.mi_files[type].write('\t\t"u" %s,\n' % "off")
                    self.mi_files[type].write('\t\t"v" %s\n' % "on")
                    self.mi_files[type].write('\t)\n')
                    self.mi_files[type].write('end material\n\n')
                else:
                    # use default sample settings for now (TODO: expose)
                    ao_samples = 8
                    ao_cutoff_distance = 1.0
                    self.mi_files[type].write("material \"%s_mat\"\n" % 
                                              fixed_name)
                    self.mi_files[type].write('\t"mill_massive" (\n')
                    self.mi_files[type].write('\t\t"ambient" %s %s %s,\n' % 
                                              (ambient[0] * ambient_weight,
                                               ambient[1] * ambient_weight,
                                               ambient[2] * ambient_weight))
                    try:
                        diffuse_tex, scale, offset = textures["diffuse"]
                    except KeyError:
                        # use color
                        self.mi_files[type].write('\t\t"diffuse" %s %s %s,\n' % 
                                                  (diffuse[0] * diffuse_weight,
                                                   diffuse[1] * diffuse_weight,
                                                   diffuse[2] * diffuse_weight))
                    else:
                        type = "textures"
                        # texture lookup shader
                        repeatU = scale[0]
                        repeatV = scale[1]
                        offsetU = offset[0]
                        offsetV = offset[1]
                        self.mi_files[type].write('shader ' +
                                                  '"%s_diff_tex" "%s" (\n' %
                                                  (diffuse_tex,
                                                   "texture_uv"))
                        self.mi_files[type].write('\t"tex" "%s",\n' %
                                                  shortname)
                        self.mi_files[type].write('\t"u_scale" %s,\n' %
                                                  (repeatU,))
                        self.mi_files[type].write('\t"v_scale" %s,\n' %
                                                  (repeatV,))
                        self.mi_files[type].write('\t"u_offset" %s,\n' %
                                                  (offsetU,))
                        self.mi_files[type].write('\t"v_offset" %s)\n\n' %
                                                  (offsetV,))
                        # use texture
                        type = "materials"
                        self.mi_files[type].write("\t\"diffuse\" = " +
                                                  "\"%s_diff_tex\",\n" %
                                                  diffuse_tex)
                    self.mi_files[type].write('\t\t"specular" %s %s %s,\n' % 
                                              (specular_color[0] *
                                               specular_intensity,
                                               specular_color[1] *
                                               specular_intensity,
                                               specular_color[2] *
                                               specular_intensity))
                    refl_factor = 0.0
                    refl_color = [0.0, 0.0, 0.0]
                    if material.raytrace_mirror.use:
                        refl_factor = material.raytrace_mirror.reflect_factor
                        refl_color = material.mirror_color
                    self.mi_files[type].write('\t\t"refl_color" %s %s %s,\n' % 
                                              (refl_color[0] * refl_factor,
                                               refl_color[1] * refl_factor,
                                               refl_color[2] * refl_factor))
                    scene = bpy.context.scene
                    world = scene.world
                    if world:
                        ao_samples = world.light_settings.samples
                        ao_cutoff_distance = world.light_settings.ao_factor
                    self.mi_files[type].write('\t\t"ao_samples" %s,\n' % 
                                              ao_samples)
                    self.mi_files[type].write('\t\t"ao_cutoff_distance" %s\n' % 
                                              ao_cutoff_distance)
                    self.mi_files[type].write('\t)\n')
                    self.mi_files[type].write('\tshadow "shadow_default" ()\n')
                    self.mi_files[type].write('end material\n\n')

    def nameStartsWith(self, name, pattern):
        retBool = False
        if len(name) >= len(pattern):
            words = name.split(pattern)
            if words[0] == "":
                retBool = True
        return retBool

    def writer(self, filename):
        # call methods similar to the Maya version of the exporter
        self.openMiFiles(filename)
        if self.options.selected:
            self.exportSelected()
        else:
            self.exportAllNew()
            ##self.exportAll()
        self.closeMiFiles()
        if self.options.render:
            renderer_args = ["ray", '-xcolor', 'mrc..gb', '-verbose', '4',
                             filename]
            print("INFO: starting mental ray: %s" % renderer_args)
            renderer_proc = subprocess.Popen(renderer_args,
                                             cwd = self.directory_path)
            renderer_pid = renderer_proc.pid

def save(operator, context, filepath = "",
         # user interface
         opt_selection = False,
         opt_renderer = 'mental_ray',
         opt_global_surface_shader = 'none',
         opt_geometry = True,
         opt_binary = True,
         opt_textures = True,
         opt_lightprofiles = True,
         opt_render = False,
         opt_ambient_fb = True,
         opt_occlusion_fb = False,
         opt_diffuse_fb = True,
         opt_specular_fb = True,
         opt_reflect_fb = True,
         opt_depth_fb = True,
         opt_normals_fb = True,
         opt_shadows_fb = True):
    if bpy.ops.object.mode_set.poll():
        bpy.ops.object.mode_set(mode = 'OBJECT')
    # print a usage message
    print("=" * 79)
    print("bpy.ops.export_scene.mi(filepath = \"%s\", " %
          "\\\\".join(filepath.split('\\')) +
          "opt_selection = %s, " % opt_selection +
          "opt_renderer = '%s', " % opt_renderer +
          "opt_global_surface_shader = '%s', " % opt_global_surface_shader +
          "opt_geometry = %s, " % opt_geometry +
          "opt_binary = %s, " % opt_binary +
          "opt_textures = %s, " % opt_textures +
          "opt_lightprofiles = %s, " % opt_lightprofiles +
          "opt_render = %s, " % opt_render +
          "opt_ambient_fb = %s, " % opt_ambient_fb +
          "opt_occlusion_fb = %s, " % opt_occlusion_fb +
          "opt_diffuse_fb = %s, " % opt_diffuse_fb +
          "opt_specular_fb = %s, " % opt_specular_fb +
          "opt_reflect_fb = %s, " % opt_reflect_fb +
          "opt_depth_fb = %s, " % opt_depth_fb +
          "opt_normals_fb = %s, " % opt_normals_fb +
          "opt_shadows_fb = %s)" % opt_shadows_fb)
    # read environment variables
    read_environment_variables()
    # delegate work to classes
    exporter = MiTranslator(Options(selected = opt_selection,
                                    renderer = opt_renderer,
                                    global_surface_shader =
                                    opt_global_surface_shader,
                                    geometry = opt_geometry,
                                    binary = opt_binary,
                                    copy_textures  = opt_textures,
                                    copy_lightprofiles = opt_lightprofiles,
                                    render = opt_render,
                                    ambient_fb = opt_ambient_fb,
                                    occlusion_fb = opt_occlusion_fb,
                                    diffuse_fb = opt_diffuse_fb,
                                    specular_fb = opt_specular_fb,
                                    reflect_fb = opt_reflect_fb,
                                    depth_fb = opt_depth_fb,
                                    normals_fb = opt_normals_fb,
                                    shadows_fb = opt_shadows_fb))
    exporter.writer(filepath)
    return {'FINISHED'}
