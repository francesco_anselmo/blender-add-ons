import os
import math
import ctypes
import platform
# common exporter interface
from ..common.interface import CommonExporterInterface
from ..common.unique import UniqueNames
# Arnold
import arnold
# Blender
import bpy

class AssExporter(CommonExporterInterface):
    # use Blender's ID prefix (see DNA_ID.h) for names
    def __init__(self, options):
        CommonExporterInterface.__init__(self, "AssExporter", options)
        self.filename = None
        self.shader_searchpath = None
        self.usedMaterials = {}
        # unique names
        self.uniqueNames = UniqueNames()

    def uniqueName(self, proposal):
        return self.uniqueNames.uniqueName(proposal)

    def ccp(self, aString): # ccp = create_char_p
        ba = bytearray(aString, "utf8")
        sb = ctypes.create_string_buffer(len(aString)+1) # len + 1 !!!
        for i in range(len(aString)):
            sb[i] = ba[i]
        return sb

    def createMatrix(self, node, m, noScale = False):
        am = arnold.AtMatrix()
        arnold.AiM4Identity(am)
        if noScale:
            am.a00 = 1.0
        else:
            am.a00 = m[0][0]
        am.a01 = m[1][0]
        am.a02 = m[2][0]
        am.a03 = m[3][0]
        am.a10 = m[0][1]
        if noScale:
            am.a11 = 1.0
        else:
            am.a11 = m[1][1]
        am.a12 = m[2][1]
        am.a13 = m[3][1]
        am.a20 = m[0][2]
        am.a21 = m[1][2]
        if noScale:
            am.a22 = 1.0
        else:
            am.a22 = m[2][2]
        am.a23 = m[3][2]
        am.a30 = self.scale_length * m[0][3]
        am.a31 = self.scale_length * m[1][3]
        am.a32 = self.scale_length * m[2][3]
        am.a33 = m[3][3]
        arnold.AiNodeSetMatrix(node, self.ccp("matrix"), am)

    def createUserMatrix(self, node, m):
        # declare Pref_matrix as constant MATRIX
        worked = arnold.AiNodeDeclare(node, self.ccp("Pref_matrix"),
                                      self.ccp("constant MATRIX"))
        if (worked):
            am = arnold.AtMatrix()
            arnold.AiM4Identity(am)
            am.a00 = m[0][0]
            am.a01 = m[1][0]
            am.a02 = m[2][0]
            am.a03 = m[3][0]
            am.a10 = m[0][1]
            am.a11 = m[1][1]
            am.a12 = m[2][1]
            am.a13 = m[3][1]
            am.a20 = m[0][2]
            am.a21 = m[1][2]
            am.a22 = m[2][2]
            am.a23 = m[3][2]
            am.a30 = self.scale_length * m[0][3]
            am.a31 = self.scale_length * m[1][3]
            am.a32 = self.scale_length * m[2][3]
            am.a33 = m[3][3]
            arnold.AiNodeSetMatrix(node, self.ccp("Pref_matrix"), am)
        else:
            print("WARNING: createUserMatrix(%s, %s) failed" % (node, m))

    def prepareExport(self, scene, bbox, directory, name, mblur, light_counter,
                      sun = None):
        self.filename = os.path.join(directory, name + ".ass")
        # scene related
        self.scene = scene
        self.bbox = bbox
        if scene.unit_settings.system != 'METRIC':
            print("WARNING: unit_settings == '%s'" % scene.unit_settings.system)
        self.scale_length = scene.unit_settings.scale_length
        # light related
        self.light_counter = light_counter
        # motion blur related
        self.use_motion_blur = mblur[0]
        self.shutter_start = mblur[1]
        self.shutter_end = mblur[2]
        self.motion_blur_samples = mblur[3]
        self.animatedObjects = mblur[4]
        self.motionMatrices = mblur[5]
        # delete .ass file (if it exists already)
        if os.path.exists(self.filename):
            os.remove(self.filename)
        # AiSetAppString
        appString = "Blender %s.%s.%s" % bpy.app.version
        sb = self.ccp(appString)
        arnold.AiSetAppString(sb)
        # AiBegin
        arnold.AiBegin()

    def writeAreaLight(self, name, transform, info):
        # light_counter
        self.light_counter += 1
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        # quad_light
        quad_light = arnold.AiNode(self.ccp("quad_light"))
        # name
        arnold.AiNodeSetStr(quad_light,
                            self.ccp("name"),
                            self.ccp("LA" + # ID_LA
                                     name))
        # decay_type
        # values: constant quadratic
        arnold.AiNodeSetInt(quad_light,
                            self.ccp("decay_type"),
                            1)
        # one matrix
        m = transform
        self.createMatrix(quad_light, m)
        # color
        arnold.AiNodeSetRGB(quad_light, self.ccp("color"),
                            color[0],
                            color[1],
                            color[2])
        # intensity
        arnold.AiNodeSetFlt(quad_light,
                            self.ccp("intensity"),
                            energy * math.pi)
        # diffuse?
        arnold.AiNodeSetBool(quad_light,
                             self.ccp("affect_diffuse"),
                             use_diffuse)
        # specular?
        arnold.AiNodeSetBool(quad_light,
                             self.ccp("affect_specular"),
                             use_specular)
        # portal?
        obj = bpy.data.objects[name]
        try:
            is_portal = obj.data.cycles.is_portal
        except AttributeError:
            pass
        else:
            arnold.AiNodeSetBool(quad_light,
                                 self.ccp("portal"),
                                 is_portal)
        # vertices
        if obj.data.shape == 'RECTANGLE':
            sizeX = obj.data.size   / 2.0
            sizeY = obj.data.size_y / 2.0
        else:
            sizeX = obj.data.size / 2.0
            sizeY = obj.data.size / 2.0
        verts = [[-sizeX,  sizeY, 0.0],
                 [ sizeX,  sizeY, 0.0],
                 [ sizeX, -sizeY, 0.0],
                 [-sizeX, -sizeY, 0.0]]
        vertices = arnold.AiArrayAllocate(len(verts), 1,
                                          arnold.AI_TYPE_POINT)
        numVertexIndices = 0
        for v in verts:
            arnold.AiArraySetPnt(vertices, numVertexIndices,
                                 arnold.AtPoint(self.scale_length * v[0],
                                                self.scale_length * v[1],
                                                self.scale_length * v[2]))
            numVertexIndices += 1
        arnold.AiNodeSetArray(quad_light, self.ccp("vertices"), vertices)

    def writeAssConstantShader(self, mat, name):
        # return values
        shader = None
        opaque = True
        # ass_constant
        ##############
        ext = ".dylib"
        if platform.system() == "Linux":
            ext = ".so"
        arnold.AiLoadPlugin(self.ccp("%s" %
                                     os.path.join(self.shader_searchpath,
                                                  "ass_constant" +
                                                  ext)))
        shader = arnold.AiNode(self.ccp("ass_constant"))
        if shader == None:
            print("WARNING: %s" %
                  "'arnold.AiNode(ccp(\"ass_constant\"))' failed")
        else:
            # name
            arnold.AiNodeSetStr(shader,
                                self.ccp("name"),
                                self.ccp("MA" + # ID_MA
                                         mat.name))
            # Color
            color = mat.diffuse_color
            arnold.AiNodeSetRGB(shader,
                                self.ccp("Color"),
                                color[0], color[1], color[2])
        return shader, opaque

    def writeMeshLight(self, polymesh, mat, name):
        mesh_light = arnold.AiNode(self.ccp("mesh_light"))
        # name
        arnold.AiNodeSetStr(mesh_light,
                            self.ccp("name"),
                            self.ccp("MA" + # ID_MA
                                     name + "_light"))
        # mesh
        arnold.AiNodeSetPtr(mesh_light,
                            self.ccp("mesh"),
                            polymesh)
        # color
        color = mat.diffuse_color
        arnold.AiNodeSetRGB(mesh_light,
                            self.ccp("color"),
                            color[0], color[1], color[2])
        # intensity
        arnold.AiNodeSetFlt(mesh_light,
                            self.ccp("intensity"),
                            mat.emit)
        # samples
        arnold.AiNodeSetInt(mesh_light,
                            self.ccp("samples"),
                            arnold.AtInt32(2))

    def writeBlenderMaterial(self, mat, name):
        # return values
        shader = None
        opaque = True
        # check for proper diffuse and specular model
        if mat.diffuse_shader != 'LAMBERT':
            print("WARNING: switching %s to 'LAMBERT'" % mat.name)
            mat.diffuse_shader = 'LAMBERT'
        if mat.specular_shader != 'PHONG':
            print("WARNING: switching %s to 'PHONG'" % mat.name)
            mat.specular_shader = 'PHONG'
        # textures?
        textures_found = 0
        textures = {}
        if mat:
            # check for textures
            for index in list(range(len(mat.texture_slots))):
                slot = mat.texture_slots[index]
                if slot:
                    texture = slot.texture
                    if slot.texture_coords == 'UV':
                        if texture.type == 'IMAGE':
                            image = texture.image
                            src = image.filepath
                            if slot.use_map_color_diffuse:
                                # diffuse texture
                                textures["diffuse"] = src
                                textures_found += 1
        # shader
        if mat.emit != 0.0:
            # ass_constant
            ##############
            ext = ".dylib"
            if platform.system() == "Linux":
                ext = ".so"
            arnold.AiLoadPlugin(self.ccp("%s" %
                                         os.path.join(self.shader_searchpath,
                                                      "ass_constant" +
                                                      ext)))
            shader = arnold.AiNode(self.ccp("ass_constant"))
            if shader == None:
                print("WARNING: %s" %
                      "'arnold.AiNode(ccp(\"ass_constant\"))' failed")
            else:
                # name
                arnold.AiNodeSetStr(shader,
                                    self.ccp("name"),
                                    self.ccp("MA" + # ID_MA
                                             mat.name))
                # Color
                color = mat.diffuse_color
                arnold.AiNodeSetRGB(shader,
                                    self.ccp("Color"),
                                    color[0], color[1], color[2])
                # return
                opaque = True
                return shader, opaque
        ext = ".dylib"
        if platform.system() == "Linux":
            ext = ".so"
        arnold.AiLoadPlugin(self.ccp("%s" %
                                     os.path.join(self.shader_searchpath,
                                                  "ass_blender_material" +
                                                  ext)))
        shader = arnold.AiNode(self.ccp("ass_blender_material"))
        if shader == None:
            print("WARNING: couldn't load plugin %s" %
                  os.path.join(self.shader_searchpath,
                               "ass_blender_material" +
                               ext))
            return shader, opaque
        # name
        arnold.AiNodeSetStr(shader,
                            self.ccp("name"),
                            self.ccp("MA" + # ID_MA
                                     mat.name))
        # Opacity
        if mat.use_transparency:
            alpha = mat.alpha
            if alpha == 1.0:
                opaque = True
            else:
                opaque = False
        else:
            alpha = 1.0
            opaque = True
        arnold.AiNodeSetRGB(shader,
                            self.ccp("Opacity"),
                            alpha,
                            alpha,
                            alpha)
        # Color
        diffuse_color = mat.diffuse_color
        arnold.AiNodeSetRGB(shader,
                            self.ccp("Color"),
                            diffuse_color[0],
                            diffuse_color[1],
                            diffuse_color[2])
        # Ka
        Ka = 0.0
        arnold.AiNodeSetFlt(shader,
                            self.ccp("Ka"),
                            Ka)
        # Kd
        Kd = mat.diffuse_intensity
        arnold.AiNodeSetFlt(shader,
                            self.ccp("Kd"),
                            Kd)
        # Ks
        Ks = mat.specular_intensity
        arnold.AiNodeSetFlt(shader,
                            self.ccp("Ks"),
                            Ks)
        # Ks_color
        Ks_color = mat.specular_color
        arnold.AiNodeSetRGB(shader,
                            self.ccp("Ks_color"),
                            Ks_color[0],
                            Ks_color[1],
                            Ks_color[2])
        # specular_hardness
        specular_hardness = mat.specular_hardness
        arnold.AiNodeSetFlt(shader,
                            self.ccp("specular_hardness"),
                            specular_hardness)
        if mat.raytrace_mirror.use:
            # Kr
            Kr = mat.raytrace_mirror.reflect_factor
            arnold.AiNodeSetFlt(shader,
                                self.ccp("Kr"),
                                Kr)
            # Kr_color
            Kr_color = mat.mirror_color
            arnold.AiNodeSetRGB(shader,
                                self.ccp("Kr_color"),
                                Kr_color[0],
                                Kr_color[1],
                                Kr_color[2])
        # texture
        try:
            diffuse_tex = textures["diffuse"]
        except KeyError:
            arnold.AiNodeSetFlt(shader,
                                self.ccp("Kd"),
                                Kd)
        else:
            paths = bpy.utils.blend_paths()
            if diffuse_tex in paths:
                index = paths.index(diffuse_tex)
                abs_paths = bpy.utils.blend_paths(absolute = True)
                abs_path = abs_paths[index]
                # image
                image = arnold.AiNode(self.ccp("image"))
                arnold.AiNodeSetStr(image, self.ccp("name"),
                                    self.ccp("IM" + # ID_IM
                                             name + "_diff_tex"))
                arnold.AiNodeSetStr(image, self.ccp("filename"),
                                    self.ccp(abs_path))
                # using image
                arnold.AiNodeLink(image, self.ccp("texture"), shader)
            else:
                print("WARNING: couldn't find '%s' in paths" % diffuse_tex)
        if textures_found:
            # using image
            arnold.AiNodeLink(image, self.ccp("texture"), shader)
        return shader, opaque

    def writeCamera(self, name, lens, angle, resolution, border, AA_samples,
                    tile_size, transform, isRenderCamera, sun = None):
        # persp_camera
        camera = arnold.AiNode(self.ccp("persp_camera"))
        if isRenderCamera:
            options = arnold.AiUniverseGetOptions()
            arnold.AiNodeSetPtr(options, self.ccp("camera"), camera)
        # name
        arnold.AiNodeSetStr(camera,
                            self.ccp("name"),
                            self.ccp("CA" + # ID_CA
                                     name))
        aspect = resolution[0] / float(resolution[1])
        if aspect >= 1.0:
            fov = math.degrees(angle)
        else:
            fov = 2 * math.degrees(math.atan((aspect * 16.0) / lens))
        arnold.AiNodeSetFlt(camera, self.ccp("fov"), fov)
        m = transform
        self.createMatrix(camera, m)
        if self.use_motion_blur:
            arnold.AiNodeSetFlt(camera, self.ccp("shutter_start"), 0.0)
            # dont use self.shutter_end !!!
            arnold.AiNodeSetFlt(camera, self.ccp("shutter_end"), 1.0)
        if isRenderCamera:
            # count AOVs
            numOutputs = 1 # beauty
            if self.options.aov_Z:
                numOutputs += 1
            if self.options.aov_N:
                numOutputs += 1
            if self.options.aov_P:
                numOutputs += 1
            if self.options.aov_emission:
                numOutputs += 1
            if self.options.aov_direct_diffuse:
                numOutputs += 1
            if self.options.aov_direct_specular:
                numOutputs += 1
            if self.options.aov_reflection:
                numOutputs += 1
            if self.options.aov_refraction:
                numOutputs += 1
            if self.options.aov_indirect_diffuse:
                numOutputs += 1
            if self.options.aov_indirect_specular:
                numOutputs += 1
            # physicalsky
            if self.options.lighting == 'no_lights' and sun:
                useSky = False
                try:
                    sun_data = bpy.data.lamps['sun']
                except KeyError:
                    try:
                        sun_data = bpy.data.lamps['Sun']
                    except KeyError:
                        useSun = False
                        useSky = False
                    else:
                        useSun = True
                        useSky = sun_data.sky.use_sky
                else:
                    useSun = True
                    useSky = sun_data.sky.use_sky
                if useSun:
                    try:
                        sun = bpy.data.objects[sun_data.name]
                    except KeyError:
                        print("WARNING: No object called \"%s\" found" %
                              sun_data.name)
                    else:
                        m = sun.matrix_world.copy()
                        # UGLY: works only because getInfoLamp(...)
                        # uses no self !!!
                        info = MultiExporter.getInfoLamp(MultiExporter, sun)
                        if sun and sun.data.type == 'SUN':
                            self.writeSunLight("sun", m, info)
            # options
            options = arnold.AiUniverseGetOptions()
            # anti-aliasing
            arnold.AiNodeSetInt(options,
                                self.ccp("AA_samples"),
                                arnold.AtInt32(AA_samples))
            # outputs
            outputs = arnold.AiArrayAllocate(numOutputs, 1,
                                             arnold.AI_TYPE_STRING)
            outIndex = 0
            # beauty
            arnold.AiArraySetStr(outputs, outIndex,
                                 self.ccp("RGBA RGBA filter driver"))
            outIndex += 1
            # other (standard) AOVs
            if self.options.aov_Z:
                arnold.AiArraySetStr(outputs, outIndex,
                                     self.ccp("Z FLOAT closest_filter driver"))
                outIndex += 1
            if self.options.aov_N:
                arnold.AiArraySetStr(outputs, outIndex,
                                     self.ccp("N VECTOR filter driver_N"))
                outIndex += 1
            if self.options.aov_P:
                arnold.AiArraySetStr(outputs, outIndex,
                                     self.ccp("P POINT filter driver_P"))
                outIndex += 1
            if self.options.aov_emission:
                arnold.AiArraySetStr(outputs, outIndex,
                                     self.ccp("emission RGB filter %s" %
                                              "driver_emission"))
                outIndex += 1
            if self.options.aov_direct_diffuse:
                arnold.AiArraySetStr(outputs, outIndex,
                                     self.ccp("direct_diffuse RGB filter %s" %
                                              "driver_direct_diffuse"))
                outIndex += 1
            if self.options.aov_direct_specular:
                arnold.AiArraySetStr(outputs, outIndex,
                                     self.ccp("direct_specular RGB filter %s" %
                                              "driver_direct_specular"))
                outIndex += 1
            if self.options.aov_reflection:
                arnold.AiArraySetStr(outputs, outIndex,
                                     self.ccp("reflection RGB filter %s" %
                                              "driver_reflection"))
                outIndex += 1
            if self.options.aov_refraction:
                arnold.AiArraySetStr(outputs, outIndex,
                                     self.ccp("refraction RGB filter %s" %
                                              "driver_refraction"))
                outIndex += 1
            if self.options.aov_indirect_diffuse:
                arnold.AiArraySetStr(outputs, outIndex,
                                     self.ccp("indirect_diffuse RGB filter %s" %
                                              "driver_indirect_diffuse"))
                outIndex += 1
            if self.options.aov_indirect_specular:
                arnold.AiArraySetStr(outputs, outIndex,
                                     self.ccp("indirect_specular " +
                                              "RGB filter %s" %
                                              "driver_indirect_specular"))
                outIndex += 1
            arnold.AiNodeSetArray(options, self.ccp("outputs"), outputs)
            # xres
            arnold.AiNodeSetInt(options,
                                self.ccp("xres"), arnold.AtInt32(resolution[0]))
            # yres
            arnold.AiNodeSetInt(options,
                                self.ccp("yres"), arnold.AtInt32(resolution[1]))
            # border?
            if border:
                border_min_x = border[0]
                border_max_x = border[1]
                border_min_y = border[2]
                border_max_y = border[3]
                region_min_x = int(border_min_x * resolution[0])
                region_max_x = int(border_max_x * resolution[0])
                region_min_y = int((1.0-border_max_y) * resolution[1])
                region_max_y = int((1.0-border_min_y) * resolution[1])
                arnold.AiNodeSetInt(options,
                                    self.ccp("region_min_x"),
                                    arnold.AtInt32(region_min_x))
                arnold.AiNodeSetInt(options,
                                    self.ccp("region_min_y"),
                                    arnold.AtInt32(region_min_y))
                arnold.AiNodeSetInt(options,
                                    self.ccp("region_max_x"),
                                    arnold.AtInt32(region_max_x))
                arnold.AiNodeSetInt(options,
                                    self.ccp("region_max_y"),
                                    arnold.AtInt32(region_max_y))
            # bucket_size
            arnold.AiNodeSetInt(options,
                                self.ccp("bucket_size"),
                                arnold.AtInt32(tile_size[0]))
            # bucket_scanning
            arnold.AiNodeSetStr(options,
                                self.ccp("bucket_scanning"),
                                self.ccp("hilbert"))
            # shader_searchpath
            ##self.shader_searchpath = os.getenv("HOME")
            self.shader_searchpath = "/Users/jan" # use OS X path
            shaders = "Graphics/Rendering/Arnold/shaders/current" # TMP
            self.shader_searchpath = os.path.join(self.shader_searchpath,
                                                  shaders)
            arnold.AiNodeSetStr(options, self.ccp("shader_searchpath"),
                                self.ccp(self.shader_searchpath))
            # gamma (change default of 1.0)
            arnold.AiNodeSetFlt(options, self.ccp("texture_gamma"), 2.2)
            # GI parameters
            if self.options.lighting == 'global_illumination':
                arnold.AiNodeSetInt(options,
                                    self.ccp("GI_diffuse_depth"),
                                    arnold.AtInt32(3))
                arnold.AiNodeSetInt(options,
                                    self.ccp("GI_glossy_depth"),
                                    arnold.AtInt32(1))
                arnold.AiNodeSetInt(options,
                                    self.ccp("GI_refraction_depth"),
                                    arnold.AtInt32(6))
                arnold.AiNodeSetInt(options,
                                    self.ccp("GI_reflection_depth"),
                                    arnold.AtInt32(6))
                arnold.AiNodeSetInt(options,
                                    self.ccp("GI_total_depth"),
                                    arnold.AtInt32(12))
                arnold.AiNodeSetInt(options,
                                    self.ccp("GI_diffuse_samples"),
                                    arnold.AtInt32(3))
                arnold.AiNodeSetInt(options,
                                    self.ccp("GI_single_scatter_samples"),
                                    arnold.AtInt32(4))
                arnold.AiNodeSetInt(options,
                                    self.ccp("GI_glossy_samples"),
                                    arnold.AtInt32(3))
                arnold.AiNodeSetInt(options,
                                    self.ccp("GI_refraction_samples"),
                                    arnold.AtInt32(3))
            # filter(s)
            filter = arnold.AiNode(self.ccp("gaussian_filter"))
            arnold.AiNodeSetStr(filter, self.ccp("name"), self.ccp("filter"))
            if self.options.aov_Z:
                closest_filter = arnold.AiNode(self.ccp("closest_filter"))
                arnold.AiNodeSetStr(closest_filter, self.ccp("name"),
                                    self.ccp("closest_filter"))
            # driver(s)
            driver = arnold.AiNode(self.ccp("driver_exr"))
            arnold.AiNodeSetStr(driver, self.ccp("name"), self.ccp("driver"))
            arnold.AiNodeSetStr(driver, self.ccp("filename"),
                                self.ccp("arnold.exr"))
            if self.options.aov_N:
                driver_N = arnold.AiNode(self.ccp("driver_exr"))
                arnold.AiNodeSetStr(driver_N, self.ccp("name"),
                                    self.ccp("driver_N"))
                arnold.AiNodeSetStr(driver_N, self.ccp("filename"),
                                    self.ccp("arnold_aov_N.exr"))
            if self.options.aov_P:
                driver_P = arnold.AiNode(self.ccp("driver_exr"))
                arnold.AiNodeSetStr(driver_P, self.ccp("name"),
                                    self.ccp("driver_P"))
                arnold.AiNodeSetStr(driver_P, self.ccp("filename"),
                                    self.ccp("arnold_aov_P.exr"))
            if self.options.aov_emission:
                driver_emission = arnold.AiNode(self.ccp("driver_exr"))
                arnold.AiNodeSetStr(driver_emission, self.ccp("name"),
                                    self.ccp("driver_emission"))
                arnold.AiNodeSetStr(driver_emission, self.ccp("filename"),
                                    self.ccp("arnold_aov_emission.exr"))
            if self.options.aov_direct_diffuse:
                driver_direct_diffuse = arnold.AiNode(self.ccp("driver_exr"))
                arnold.AiNodeSetStr(driver_direct_diffuse, self.ccp("name"),
                                    self.ccp("driver_direct_diffuse"))
                arnold.AiNodeSetStr(driver_direct_diffuse, self.ccp("filename"),
                                    self.ccp("arnold_aov_direct_diffuse.exr"))
            if self.options.aov_direct_specular:
                driver_direct_specular = arnold.AiNode(self.ccp("driver_exr"))
                arnold.AiNodeSetStr(driver_direct_specular, self.ccp("name"),
                                    self.ccp("driver_direct_specular"))
                arnold.AiNodeSetStr(driver_direct_specular,
                                    self.ccp("filename"),
                                    self.ccp("arnold_aov_direct_specular.exr"))
            if self.options.aov_reflection:
                driver_reflection = arnold.AiNode(self.ccp("driver_exr"))
                arnold.AiNodeSetStr(driver_reflection, self.ccp("name"),
                                    self.ccp("driver_reflection"))
                arnold.AiNodeSetStr(driver_reflection, self.ccp("filename"),
                                    self.ccp("arnold_aov_reflection.exr"))
            if self.options.aov_refraction:
                driver_refraction = arnold.AiNode(self.ccp("driver_exr"))
                arnold.AiNodeSetStr(driver_refraction, self.ccp("name"),
                                    self.ccp("driver_refraction"))
                arnold.AiNodeSetStr(driver_refraction, self.ccp("filename"),
                                    self.ccp("arnold_aov_refraction.exr"))
            if self.options.aov_indirect_diffuse:
                driver_indirect_diffuse = arnold.AiNode(self.ccp("driver_exr"))
                arnold.AiNodeSetStr(driver_indirect_diffuse, self.ccp("name"),
                                    self.ccp("driver_indirect_diffuse"))
                arnold.AiNodeSetStr(driver_indirect_diffuse,
                                    self.ccp("filename"),
                                    self.ccp("arnold_aov_indirect_diffuse.exr"))
            if self.options.aov_indirect_specular:
                driver_indirect_specular = arnold.AiNode(self.ccp("driver_exr"))
                arnold.AiNodeSetStr(driver_indirect_specular, self.ccp("name"),
                                    self.ccp("driver_indirect_specular"))
                arnold.AiNodeSetStr(driver_indirect_specular,
                                    self.ccp("filename"),
                                    self.ccp("arnold_aov_" +
                                             "indirect_specular.exr"))

    def writeCommonPrimitive(self, name, transform, mat, primitive):
        # material
        mat = bpy.data.materials[mat.name]
        shader = arnold.AiNodeLookUpByName(self.ccp("MA" + # ID_MA
                                                    mat.name))
        if shader == None:
            if self.options.lighting == 'no_lights' or self.light_counter == 0:
                shader, opaque = self.writeDefaultSurface(mat, name)
            else:
                shader, opaque = self.writeStandardShader(mat, name)
                # shader, opaque = self.writeBlenderMaterial(mat, name)
            self.usedMaterials[mat.name] = opaque
        else:
            opaque = self.usedMaterials[mat.name]
        if shader != None:
            arnold.AiNodeSetPtr(primitive, self.ccp("shader"), shader)
        arnold.AiNodeSetBool(primitive,
                             self.ccp("opaque"),
                             opaque)
        # one matrix
        m = transform
        self.createMatrix(primitive, m)

    def writeCone(self, name, transform, mat):
        obj = bpy.data.objects[name]
        p1 = obj.data.splines[0].points[0].co
        p2 = obj.data.splines[0].points[8].co
        # cone
        cone = arnold.AiNode(self.ccp("cone"))
        # name
        arnold.AiNodeSetStr(cone,
                            self.ccp("name"),
                            self.ccp("CU" + # ID_CU
                                     self.uniqueName(name)))
        arnold.AiNodeSetPnt(cone,
                            self.ccp("bottom"), 0.0, 0.0,
                            self.scale_length * p1.z)
        arnold.AiNodeSetPnt(cone,
                            self.ccp("top"), 0.0, 0.0,
                            self.scale_length * p2.z)
        arnold.AiNodeSetFlt(cone, # default is 0.5
                            self.ccp("bottom_radius"),
                            math.fabs(self.scale_length * p1.y))
        arnold.AiNodeSetFlt(cone, # default is 0.0
                            self.ccp("top_radius"),
                            math.fabs(self.scale_length * p2.y))
        primitive = cone
        self.writeCommonPrimitive(name, transform, mat, primitive)

    def writeCylinder(self, name, transform, mat):
        # cylinder
        cylinder = arnold.AiNode(self.ccp("cylinder"))
        # name
        arnold.AiNodeSetStr(cylinder,
                            self.ccp("name"),
                            self.ccp("CU" + # ID_CU
                                     self.uniqueName(name)))
        arnold.AiNodeSetPnt(cylinder,
                            self.ccp("bottom"), 0.0, 0.0, 0.0)
        arnold.AiNodeSetPnt(cylinder,
                            self.ccp("top"), 0.0, 0.0,
                            self.scale_length * 1.0)
        arnold.AiNodeSetFlt(cylinder, # default is 0.5
                            self.ccp("radius"),
                            self.scale_length * 1.0)
        primitive = cylinder
        self.writeCommonPrimitive(name, transform, mat, primitive)

    def writeDefaultSurface(self, mat, name):
        # return values
        shader = None
        opaque = True
        # defaultsurface
        diffuse_intensity = mat.diffuse_intensity
        Kd = diffuse_intensity
        Ka = 1.0 - Kd
        textures_found = 0
        textures = {}
        if mat:
            # check for textures
            for index in list(range(len(mat.texture_slots))):
                slot = mat.texture_slots[index]
                if slot:
                    texture = slot.texture
                    if slot.texture_coords == 'UV':
                        if texture.type == 'IMAGE':
                            image = texture.image
                            src = image.filepath
                            if slot.use_map_color_diffuse:
                                # diffuse texture
                                textures["diffuse"] = src
                                textures_found += 1
        texturename = ""
        if textures_found:
            diffuse_tex = textures["diffuse"]
            paths = bpy.utils.blend_paths()
            if diffuse_tex in paths:
                index = paths.index(diffuse_tex)
                abs_paths = bpy.utils.blend_paths(absolute = True)
                abs_path = abs_paths[index]
                basename = os.path.basename(abs_path)
                # assume the texture was copied to the export directory
                texturename = basename
        if mat:
            if textures_found:
                # image
                image = arnold.AiNode(self.ccp("image"))
                # name
                arnold.AiNodeSetStr(image, self.ccp("name"),
                                    self.ccp("IM" + # ID_IM
                                             mat.name + "_diff_tex"))
                arnold.AiNodeSetStr(image, self.ccp("filename"),
                                    self.ccp(texturename))
        # shader
        ext = ".dylib"
        if platform.system() == "Linux":
            ext = ".so"
        arnold.AiLoadPlugin(self.ccp("%s" %
                                     os.path.join(self.shader_searchpath,
                                                  "ass_defaultsurface" +
                                                  ext)))
        shader = arnold.AiNode(self.ccp("ass_defaultsurface"))
        if shader == None:
            print("WARNING: couldn't load plugin %s" %
                  os.path.join(self.shader_searchpath,
                               "ass_defaultsurface" +
                               ext))
            return shader, opaque
        # name
        arnold.AiNodeSetStr(shader,
                            self.ccp("name"),
                            self.ccp("MA" + # ID_MA
                                     mat.name))
        if mat.use_transparency:
            alpha = mat.alpha
            if alpha == 1.0:
                opaque = True
            else:
                opaque = False
        else:
            alpha = 1.0
            opaque = True
        arnold.AiNodeSetRGB(shader,
                            self.ccp("Opacity"),
                            alpha,
                            alpha,
                            alpha)
        diffuse_color = mat.diffuse_color
        arnold.AiNodeSetRGB(shader,
                            self.ccp("Color"),
                            diffuse_color[0],
                            diffuse_color[1],
                            diffuse_color[2])
        arnold.AiNodeSetFlt(shader,
                            self.ccp("Ka"),
                            Ka)
        arnold.AiNodeSetFlt(shader,
                            self.ccp("Kd"),
                            Kd)
        if textures_found:
            # using image
            arnold.AiNodeLink(image, self.ccp("texture"), shader)
        return shader, opaque

    def writeHdrLighting(self, hdrImagePath):
        # light_counter
        self.light_counter += 1
        # image
        image = arnold.AiNode(self.ccp("image"))
        arnold.AiNodeSetStr(image, self.ccp("name"),
                            self.ccp("hdr_tex"))
        arnold.AiNodeSetStr(image, self.ccp("filename"),
                            self.ccp(hdrImagePath))
        # sky
        sky = arnold.AiNode(self.ccp("sky"))
        arnold.AiNodeSetStr(sky,
                            self.ccp("name"),
                            self.ccp("hdr_sky"))
        intensity = 1.0 # TODO: adjust in Blender?
        arnold.AiNodeSetFlt(sky,
                            self.ccp("intensity"),
                            intensity)
        visibility = (arnold.AI_RAY_ALL &
                      ~arnold.AI_RAY_DIFFUSE &
                      ~arnold.AI_RAY_GLOSSY &
                      ~arnold.AI_RAY_REFLECTED &
                      ~arnold.AI_RAY_REFRACTED &
                      ~arnold.AI_RAY_SHADOW)
        arnold.AiNodeSetInt(sky,
                            self.ccp("visibility"),
                            visibility)
        latlong = 2 # TODO
        arnold.AiNodeSetInt(sky, self.ccp("format"), latlong)
        arnold.AiNodeSetFlt(sky, self.ccp("Z_angle"), 270.0)
        arnold.AiNodeSetVec(sky, self.ccp("Y"), 0.0, 0.0, 1.0)
        arnold.AiNodeSetVec(sky, self.ccp("Z"), 0.0, -1.0, 0.0)
        # skydome_light
        skydome_light = arnold.AiNode(self.ccp("skydome_light"))
        arnold.AiNodeSetStr(skydome_light,
                            self.ccp("name"),
                            self.ccp("hdr_skydome"))
        arnold.AiNodeSetInt(skydome_light,
                            self.ccp("resolution"),
                            arnold.AtInt32(4096))
        arnold.AiNodeSetInt(skydome_light, self.ccp("format"), latlong)
        intensity = 1.0
        arnold.AiNodeSetFlt(skydome_light,
                            self.ccp("intensity"),
                            intensity)
        arnold.AiNodeSetInt(skydome_light,
                            self.ccp("samples"),
                            arnold.AtInt32(6))
        # m = [[0.0, 1.0, 0.0, 0.0],
        #      [0.0, 0.0, 1.0, 0.0],
        #      [1.0, 0.0, 0.0, 0.0],
        #      [0.0, 0.0, 0.0, 1.0]]
        am = arnold.AtMatrix()
        arnold.AiM4Identity(am)
        am.a00 = 0.0
        am.a01 = 1.0
        am.a11 = 0.0
        am.a12 = 1.0
        am.a20 = 1.0
        am.a22 = 0.0
        arnold.AiNodeSetMatrix(skydome_light, self.ccp("matrix"), am)
        # link image to sky and skydome_light
        arnold.AiNodeLink(image, self.ccp("color"), sky)
        arnold.AiNodeLink(image, self.ccp("color"), skydome_light)
        # link sky to options background
        options = arnold.AiUniverseGetOptions()
        arnold.AiNodeSetPtr(options, self.ccp("background"), sky)

    def writeMesh(self, name, transform, info, user_matrix = None):
        # info
        materials = info[0]
        vertices = info[1]
        vertexNormals = info[2]
        polygons = info[3]
        polygonNormals = info[4]
        smoothPolygons = info[5]
        polygonMaterialIndices = info[6]
        hasUVs, uvs = info[7]
        # if there's a single smooth polygon, we export normals
        useVertexNormals = False
        for smoothPolygon in smoothPolygons:
            if smoothPolygon:
                useVertexNormals = True
                break
        # polymesh
        polymesh = arnold.AiNode(self.ccp("polymesh"))
        # name
        arnold.AiNodeSetStr(polymesh,
                            self.ccp("name"),
                            self.ccp("ME" + # ID_ME
                                     self.uniqueName(name)))
        # nsides
        nsides = arnold.AiArrayAllocate(len(polygons), 1,
                                        arnold.AI_TYPE_UINT)
        numVertexIndices = 0
        for i in range(len(polygons)):
            polygon = polygons[i]
            arnold.AiArraySetUInt(nsides, i, len(polygon))
            numVertexIndices += len(polygon)
        arnold.AiNodeSetArray(polymesh, self.ccp("nsides"), nsides)
        numUVs = numVertexIndices # keep for later
        # vidxs
        vidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                       arnold.AI_TYPE_UINT)
        numVertexIndices = 0 # reset
        for polygon in polygons:
            for vi in polygon:
                arnold.AiArraySetUInt(vidxs, numVertexIndices,
                                      vi)
                numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, self.ccp("vidxs"), vidxs)
        # nidxs
        if useVertexNormals:
            if len(vertices) != len(vertexNormals):
                print("WARNING: len(vertices)[%s] != len(vertexNormals)[%s]" %
                      (len(vertices), len(vertexNormals)))
            nidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                           arnold.AI_TYPE_UINT)
            numVertexIndices = 0 # reset
            for polygon in polygons:
                for vi in polygon:
                    arnold.AiArraySetUInt(nidxs, numVertexIndices,
                                          vi)
                    numVertexIndices += 1
            arnold.AiNodeSetArray(polymesh, self.ccp("nidxs"), nidxs)
        # uvidxs
        if hasUVs:
            uvidxs = arnold.AiArrayAllocate(numVertexIndices, 1,
                                           arnold.AI_TYPE_UINT)
            numVertexIndices = 0 # reset
            for polygon in polygons:
                for vi in polygon:
                    arnold.AiArraySetUInt(uvidxs, numVertexIndices,
                                          numVertexIndices)
                    numVertexIndices += 1
            arnold.AiNodeSetArray(polymesh, self.ccp("uvidxs"), uvidxs)
        # shidxs
        shidxs = arnold.AiArrayAllocate(len(polygonMaterialIndices), 1,
                                        arnold.AI_TYPE_BYTE)
        for i in range(len(polygonMaterialIndices)):
            mat_idx = polygonMaterialIndices[i]
            arnold.AiArraySetByte(shidxs, i, mat_idx)
        arnold.AiNodeSetArray(polymesh, self.ccp("shidxs"), shidxs)
        # vlist
        vlist = arnold.AiArrayAllocate(len(vertices), 1,
                                       arnold.AI_TYPE_POINT)
        numVertexIndices = 0 # reset
        for v in vertices:
            arnold.AiArraySetPnt(vlist, numVertexIndices,
                                 arnold.AtPoint(self.scale_length * v[0],
                                                self.scale_length * v[1],
                                                self.scale_length * v[2]))
            numVertexIndices += 1
        arnold.AiNodeSetArray(polymesh, self.ccp("vlist"), vlist)
        # nlist
        if useVertexNormals:
            nlist = arnold.AiArrayAllocate(len(vertices), 1,
                                           arnold.AI_TYPE_VECTOR)
            numVertexIndices = 0 # reset
            for n in vertexNormals: # use vertex normal
                arnold.AiArraySetVec(nlist, numVertexIndices,
                                     arnold.AtVector(n[0], n[1], n[2]))
                numVertexIndices += 1
            arnold.AiNodeSetArray(polymesh, self.ccp("nlist"), nlist)
            # use smoothing
            arnold.AiNodeSetBool(polymesh,
                                 self.ccp("smoothing"),
                                 True)
        # uvlist
        if hasUVs:
            uvlist = arnold.AiArrayAllocate(numUVs, 1,
                                            arnold.AI_TYPE_POINT2)
            numVertexIndices = 0 # reset
            for polygon in uvs:
                for uv in polygon:
                    arnold.AiArraySetPnt2(uvlist, numVertexIndices,
                                          arnold.AtPoint2(uv[0], uv[1]))
                    numVertexIndices += 1
            arnold.AiNodeSetArray(polymesh, self.ccp("uvlist"), uvlist)
            # double check if that worked
            dummy = arnold.AiNodeGetArray(polymesh, self.ccp("uvlist"))
            if dummy.contents.data == None:
                print('WARNING: uvlist allocation for polymesh "%s" ' % name +
                      'failed for unknown reason')
                # reset uvidxs
                uvidxs = arnold.AiArrayAllocate(0, 1, arnold.AI_TYPE_UINT)
                arnold.AiNodeSetArray(polymesh, self.ccp("uvidxs"), uvidxs)
        # material(s)
        isOpaque = True
        shaders = arnold.AiArray(len(materials),
                                 1,
                                 arnold.AI_TYPE_NODE)
        if len(materials) > 1:
            for mi in range(len(materials)):
                matName = materials[mi]
                mat = bpy.data.materials[matName]
                shader = arnold.AiNodeLookUpByName(self.ccp("MA" + # ID_MA
                                                            matName))
                if shader == None:
                    if (self.options.lighting == 'no_lights' or
                        self.light_counter == 0):
                        shader, opaque = self.writeDefaultSurface(mat, name)
                    else:
                        shader, opaque = self.writeStandardShader(mat, name, mi)
                        # shader, opaque = self.writeBlenderMaterial(mat, name)
                    self.usedMaterials[mat.name] = opaque
                else:
                    opaque = self.usedMaterials[mat.name]
                isOpaque = isOpaque and opaque
                if shader != None:
                    arnold.AiArraySetPtr(shaders, mi, shader)
            arnold.AiNodeSetArray(polymesh,
                                  self.ccp("shader"),
                                  shaders)
            arnold.AiNodeSetBool(polymesh,
                                 self.ccp("opaque"),
                                 isOpaque)
        else:
            if len(materials):
                mat = bpy.data.materials[materials[0]]
            else:
                mat = None
            # check if material is light emitter
            if mat != None and mat.emit != 0.0:
                # emitter
                shader = arnold.AiNodeLookUpByName(self.ccp("MA" + # ID_MA
                                                            mat.name))
                if shader == None:
                    if (self.options.lighting == 'no_lights' or
                        self.light_counter == 0):
                        shader, opaque = self.writeAssConstantShader(mat, name)
                    else:
                        shader, opaque = self.writeAssConstantShader(mat, name)
                        # mesh_light
                        self.writeMeshLight(polymesh, mat, name)
                    self.usedMaterials[mat.name] = opaque
                else:
                    opaque = self.usedMaterials[mat.name]
                    if mat.emit != 0.0:
                        # mesh_light
                        self.writeMeshLight(polymesh, mat, name)
                if shader != None:
                    arnold.AiNodeSetPtr(polymesh, self.ccp("shader"), shader)
                    arnold.AiNodeSetBool(polymesh,
                                         self.ccp("opaque"),
                                         opaque)
            else:
                # no emitter
                shader = arnold.AiNodeLookUpByName(self.ccp("MA" + # ID_MA
                                                            mat.name))
                if shader == None:
                    if (self.options.lighting == 'no_lights' or
                        self.light_counter == 0):
                        shader, opaque = self.writeDefaultSurface(mat, name)
                    else:
                        shader, opaque = self.writeStandardShader(mat, name)
                        # shader, opaque = self.writeBlenderMaterial(mat, name)
                    self.usedMaterials[mat.name] = opaque
                else:
                    opaque = self.usedMaterials[mat.name]
                if shader != None:
                    arnold.AiNodeSetPtr(polymesh, self.ccp("shader"), shader)
                    arnold.AiNodeSetBool(polymesh,
                                         self.ccp("opaque"),
                                         opaque)
        # one matrix
        m = transform
        self.createMatrix(polymesh, m)
        # user matrix?
        if (user_matrix != None):
            m = user_matrix
            self.createUserMatrix(polymesh, m)

    def writeNurbsSurface(self, name, transform, info):
        # info
        materials = info[0]
        splines = info[1]
        # for each spline ...
        for spline in splines:
            order_u = spline[0][0]
            order_v = spline[0][1]
            # ... check if bicubic
            if (order_u == 4 and order_v == 4):
                # nurbs
                nurbs = arnold.AiNode(self.ccp("nurbs"))
                # name
                uniqueName = name # TODO
                arnold.AiNodeSetStr(nurbs, self.ccp("name"),
                                    self.ccp("CU" + # ID_CU
                                             self.uniqueName(uniqueName)))
                # knots
                knots_u = arnold.AiArrayAllocate(2 * 4, 1,
                                                 arnold.AI_TYPE_FLOAT)
                knots_v = arnold.AiArrayAllocate(2 * 4, 1,
                                                 arnold.AI_TYPE_FLOAT)
                # TODO: this is an assumption !!!
                for i in range(4):
                    arnold.AiArraySetFlt(knots_u, i, 0.0)
                    arnold.AiArraySetFlt(knots_v, i, 0.0)
                for i in range(4):
                    arnold.AiArraySetFlt(knots_u, i+4, 1.0)
                    arnold.AiArraySetFlt(knots_v, i+4, 1.0)
                arnold.AiNodeSetArray(nurbs, self.ccp("knots_u"), knots_u)
                arnold.AiNodeSetArray(nurbs, self.ccp("knots_v"), knots_v)
                # cvs
                points = spline[1]
                cvs = arnold.AiArrayAllocate(4 * len(points), 1,
                                             arnold.AI_TYPE_FLOAT)
                counter = 0
                for v in points:
                    for i in range(4):
                        arnold.AiArraySetFlt(cvs, counter * 4 + i,
                                             self.scale_length * v[i])
                    counter += 1
                arnold.AiNodeSetArray(nurbs, self.ccp("cvs"), cvs)
                # smoothing
                smoothing = True
                arnold.AiNodeSetBool(nurbs, self.ccp("smoothing"), smoothing)
                # assume ONE material
                matName = materials[0]
                mat = bpy.data.materials[matName]
                shader = arnold.AiNodeLookUpByName(self.ccp("MA" + # ID_MA
                                                            mat.name))
                if shader == None:
                    if (self.options.lighting == 'no_lights' or
                        self.light_counter == 0):
                        shader, opaque = self.writeDefaultSurface(mat, name)
                    else:
                        shader, opaque = self.writeStandardShader(mat, name)
                        # shader, opaque = self.writeBlenderMaterial(mat, name)
                    self.usedMaterials[mat.name] = opaque
                else:
                    opaque = self.usedMaterials[mat.name]
                if shader != None:
                    arnold.AiNodeSetPtr(nurbs, self.ccp("shader"), shader)
                    arnold.AiNodeSetBool(nurbs,
                                         self.ccp("opaque"),
                                         opaque)
                # one matrix
                m = transform
                self.createMatrix(nurbs, m)
            else:
                print("WARNING: TODO: nurbs [%s, %s]" %
                      (order_u, order_v))

    def writePointLight(self, name, transform, info):
        # light_counter
        self.light_counter += 1
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        # falloff
        falloff_type = falloff[0]
        # point_light
        point_light = arnold.AiNode(self.ccp("point_light"))
        # name
        arnold.AiNodeSetStr(point_light,
                            self.ccp("name"),
                            self.ccp("LA" + # ID_LA
                                     name))
        # decay_type
        # values: constant quadratic
        if falloff_type == 'INVERSE_SQUARE':
            arnold.AiNodeSetInt(point_light,
                                self.ccp("decay_type"),
                                1)
        elif (falloff_type == 'INVERSE_LINEAR' or
              falloff_type == 'CUSTOM_CURVE' or
              falloff_type == 'LINEAR_QUADRATIC_WEIGHTED'):
            print("WARNING: falling back to 'CONSTANT' falloff type")
            arnold.AiNodeSetInt(point_light,
                                self.ccp("decay_type"),
                                0)
        else: # 'CONSTANT'
            arnold.AiNodeSetInt(point_light,
                                self.ccp("decay_type"),
                                0)
        # one matrix
        m = transform
        self.createMatrix(point_light, m)
        # color
        arnold.AiNodeSetRGB(point_light, self.ccp("color"),
                            color[0],
                            color[1],
                            color[2])
        # intensity
        arnold.AiNodeSetFlt(point_light,
                            self.ccp("intensity"),
                            energy)
        # diffuse?
        arnold.AiNodeSetBool(point_light,
                             self.ccp("affect_diffuse"),
                             use_diffuse)
        # specular?
        arnold.AiNodeSetBool(point_light,
                             self.ccp("affect_specular"),
                             use_specular)

    def writeRing(self, name, transform, mat):
        # disk
        obj = bpy.data.objects[name]
        p1 = obj.data.splines[0].points[0].co
        p2 = obj.data.splines[0].points[8].co
        disk = arnold.AiNode(self.ccp("disk"))
        r1 = self.scale_length * math.fabs(p1.y)
        r2 = self.scale_length * math.fabs(p2.y)
        if r1 < r2:
            minRadius = r1
            maxRadius = r2
        else:
            minRadius = r2
            maxRadius = r1
        # name
        arnold.AiNodeSetStr(disk,
                            self.ccp("name"),
                            self.ccp("CU" + # ID_CU
                                     self.uniqueName(name)))
        arnold.AiNodeSetFlt(disk, # default is 0.5
                            self.ccp("radius"),
                            self.scale_length * 1.0)
        arnold.AiNodeSetFlt(disk, # default is 0.5
                            self.ccp("radius"), maxRadius)
        arnold.AiNodeSetFlt(disk, # default is 0.0
                            self.ccp("hole"), minRadius)
        primitive = disk
        self.writeCommonPrimitive(name, transform, mat, primitive)

    def writeSphere(self, name, transform, mat):
        # sphere
        sphere = arnold.AiNode(self.ccp("sphere"))
        # name
        arnold.AiNodeSetStr(sphere,
                            self.ccp("name"),
                            self.ccp("CU" + # ID_CU
                                     self.uniqueName(name)))
        # radius
        arnold.AiNodeSetFlt(sphere, # default is 0.5
                            self.ccp("radius"),
                            self.scale_length * 1.0)
        if mat.emit == 0.0:
            primitive = sphere
            self.writeCommonPrimitive(name, transform, mat, primitive)
        else:
            # material
            mat = bpy.data.materials[mat.name]
            shader = arnold.AiNodeLookUpByName(self.ccp("MA" + # ID_MA
                                                        mat.name))
            if shader == None:
                opaque = True
                # ass_constant
                ##############
                ext = ".dylib"
                if platform.system() == "Linux":
                    ext = ".so"
                shader_searchpath = self.shader_searchpath
                arnold.AiLoadPlugin(self.ccp("%s" %
                                             os.path.join(shader_searchpath,
                                                          "ass_constant" +
                                                          ext)))
                ass_constant = arnold.AiNode(self.ccp("ass_constant"))
                if ass_constant == None:
                    print("WARNING: %s" %
                          "'arnold.AiNode(ccp(\"ass_constant\"))' failed")
                else:
                    # name
                    arnold.AiNodeSetStr(ass_constant,
                                        self.ccp("name"),
                                        self.ccp("MA" + # ID_MA
                                                 mat.name))
                    # Color
                    color = mat.diffuse_color
                    arnold.AiNodeSetRGB(ass_constant,
                                        self.ccp("Color"),
                                        color[0],
                                        color[1],
                                        color[2])
                    shader = ass_constant
                self.usedMaterials[mat.name] = opaque
            else:
                opaque = self.usedMaterials[mat.name]
            # sphere
            ########
            # visibility
            obj = bpy.data.objects[name]
            if obj.hide:
                # not visible to AI_RAY_ALL
                arnold.AiNodeSetByte(sphere, self.ccp("visibility"), 0)
            else:
                # not visible to AI_RAY_SHADOW
                arnold.AiNodeSetByte(sphere, self.ccp("visibility"), 253)
            if shader != None:
                # shader
                arnold.AiNodeSetPtr(sphere, self.ccp("shader"), shader)
            # opaque
            arnold.AiNodeSetBool(sphere,
                                 self.ccp("opaque"),
                                 opaque)
            # one matrix
            m = transform
            self.createMatrix(sphere, m)
            # point_light
            #############
            point_light = arnold.AiNode(self.ccp("point_light"))
            # name
            arnold.AiNodeSetStr(point_light,
                                self.ccp("name"),
                                self.ccp("LA" + # ID_LA
                                         name))
            # radius
            m = transform
            center, rot, scale = m.decompose()
            if (scale[0] == scale[1] == scale[2]):
                radius = self.scale_length * scale[0]
            else:
                # print('WARNING: AssExporter.writeSphere' +
                #       '("%s") scale (%s, %s, %s)' %
                #       (mat.name, scale[0], scale[1], scale[2]))
                radius = self.scale_length * scale[0]
            arnold.AiNodeSetFlt(point_light,
                                self.ccp("radius"),
                                radius)
            # one matrix
            m = transform
            # no scaling (it's in the radius)
            self.createMatrix(point_light, m, True)
            # color
            color = mat.diffuse_color
            arnold.AiNodeSetRGB(point_light,
                                self.ccp("color"),
                                color[0],
                                color[1],
                                color[2])
            # intensity
            arnold.AiNodeSetFlt(point_light,
                                self.ccp("intensity"),
                                mat.emit)
            # samples
            arnold.AiNodeSetInt(point_light,
                                self.ccp("samples"),
                                1)

    def writeSpotLight(self, name, transform, info):
        # light_counter
        self.light_counter += 1
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        spot_size = info[4][0]
        spot_blend = info[4][1]
        # falloff
        falloff_type = falloff[0]
        # spot_light
        spot_light = arnold.AiNode(self.ccp("spot_light"))
        # name
        arnold.AiNodeSetStr(spot_light,
                            self.ccp("name"),
                            self.ccp("LA" + # ID_LA
                                     name))
        arnold.AiNodeSetFlt(spot_light,
                            self.ccp("cone_angle"),
                            math.degrees(spot_size))
        # decay_type
        # values: constant quadratic
        if falloff_type == 'INVERSE_SQUARE':
            arnold.AiNodeSetInt(spot_light,
                                self.ccp("decay_type"),
                                1)
        elif (falloff_type == 'INVERSE_LINEAR' or
              falloff_type == 'CUSTOM_CURVE' or
              falloff_type == 'LINEAR_QUADRATIC_WEIGHTED'):
            print("WARNING: falling back to 'CONSTANT' falloff type")
            arnold.AiNodeSetInt(spot_light,
                                self.ccp("decay_type"),
                                0)
        else: # 'CONSTANT'
            arnold.AiNodeSetInt(spot_light,
                                self.ccp("decay_type"),
                                0)
        # one matrix
        m = transform
        self.createMatrix(spot_light, m)
        # color
        arnold.AiNodeSetRGB(spot_light, self.ccp("color"),
                            color[0],
                            color[1],
                            color[2])
        # intensity
        if (math.fabs(math.degrees(spot_size) - 180.0) < 0.0001):
            arnold.AiNodeSetFlt(spot_light,
                                self.ccp("intensity"),
                                energy * math.pi / 2.0)
        else:
            arnold.AiNodeSetFlt(spot_light,
                                self.ccp("intensity"),
                                energy * math.pi)
        # diffuse?
        arnold.AiNodeSetBool(spot_light,
                             self.ccp("affect_diffuse"),
                             use_diffuse)
        # specular?
        arnold.AiNodeSetBool(spot_light,
                             self.ccp("affect_specular"),
                             use_specular)

    def writeSunLight(self, name, transform, info):
        # light_counter
        self.light_counter += 1
        # info
        color = info[0]
        energy = info[1]
        falloff = info[2]
        use_diffuse = info[3][0]
        use_specular = info[3][1]
        # lamp data
        lamp = bpy.data.lamps[name]
        if (name == 'sun' or name == 'Sun') and lamp.sky.use_sky:
            # physical_sky
            physical_sky = arnold.AiNode(self.ccp("physical_sky"))
            # name
            arnold.AiNodeSetStr(physical_sky,
                                self.ccp("name"),
                                self.ccp(name + "_sky"))
            # turbidity
            turbidity = lamp.sky.atmosphere_turbidity
            arnold.AiNodeSetFlt(physical_sky,
                                self.ccp("turbidity"),
                                turbidity)
            # sun_direction
            arnold.AiNodeSetVec(physical_sky,
                                self.ccp("sun_direction"),
                                0.0, 1.0, 0.0)
            # calculate elevation and azimuth
            m = transform
            pos, rot, scale = m.decompose()
            euler = rot.to_euler()
            # elevation
            elevation = 90.0 - math.degrees(euler.x)
            arnold.AiNodeSetFlt(physical_sky,
                                self.ccp("elevation"),
                                elevation)
            # azimuth
            azimuth = math.degrees(euler.z) - 90.0
            arnold.AiNodeSetFlt(physical_sky,
                                self.ccp("azimuth"),
                                azimuth)
            # intensity
            intensity = energy
            arnold.AiNodeSetFlt(physical_sky,
                                self.ccp("intensity"),
                                intensity)
            # X
            arnold.AiNodeSetVec(physical_sky,
                                self.ccp("X"),
                                1.0, 0.0, 0.0)
            # Y (swap)
            arnold.AiNodeSetVec(physical_sky,
                                self.ccp("Y"),
                                0.0, 0.0, 1.0)
            # Z (swap)
            arnold.AiNodeSetVec(physical_sky,
                                self.ccp("Z"),
                                0.0, 1.0, 0.0)
            # options
            options = arnold.AiNodeLookUpByName(self.ccp("options"))
            arnold.AiNodeSetPtr(options, self.ccp("background"), physical_sky)
            if self.options.lighting == 'global_illumination':
                # skydome_light
                skydome_light = arnold.AiNode(self.ccp("skydome_light"))
                # name
                arnold.AiNodeSetStr(skydome_light,
                                    self.ccp("name"),
                                    self.ccp(name + "_skydome"))
                # resolution
                arnold.AiNodeSetInt(skydome_light,
                                    self.ccp("resolution"),
                                    arnold.AtInt32(4096))
                # intensity
                intensity = energy
                arnold.AiNodeSetFlt(skydome_light,
                                    self.ccp("intensity"),
                                    intensity)
                # samples
                arnold.AiNodeSetInt(skydome_light,
                                    self.ccp("samples"),
                                    arnold.AtInt32(6))
                # link to skydome_light
                arnold.AiNodeLink(physical_sky, self.ccp("color"),
                                  skydome_light)
                return
        if self.options.lighting != 'no_lights':
            # distant_light
            distant_light = arnold.AiNode(self.ccp("distant_light"))
            # name
            arnold.AiNodeSetStr(distant_light,
                                self.ccp("name"),
                                self.ccp("LA" + # ID_LA
                                         name))
            # one matrix
            m = transform
            self.createMatrix(distant_light, m)
            # color
            arnold.AiNodeSetRGB(distant_light, self.ccp("color"),
                                color[0],
                                color[1],
                                color[2])
            # intensity
            arnold.AiNodeSetFlt(distant_light,
                                self.ccp("intensity"),
                                energy * math.pi)
            # diffuse?
            arnold.AiNodeSetBool(distant_light,
                                 self.ccp("affect_diffuse"),
                                 use_diffuse)
            # specular?
            arnold.AiNodeSetBool(distant_light,
                                 self.ccp("affect_specular"),
                                 use_specular)

    def writeStandardShader(self, mat, name, mi = None):
        # return values
        shader = None
        opaque = True
        # try first to use Cycles nodes
        if mat.use_nodes:
            surface = mat.node_tree.nodes['Material Output'].inputs['Surface']
            if len(surface.links) >= 1:
                try:
                    color = surface.links[0].from_node.inputs['Color']
                except KeyError:
                    # TODO: handle mix of diffuse and glossy
                    pass
                else:
                    if len(color.links) >= 1:
                        inputs = color.links[0].from_node.inputs
                        try:
                            filepath = color.links[0].from_node.script.filepath
                        except AttributeError:
                            pass
                        else:
                            print("INFO: filepath = %s" % filepath)
                            # shader
                            basename = os.path.basename(filepath)
                            root, ext = os.path.splitext(basename)
                            ext = ".oso" # compiled OSL shader
                            osoFilepath = os.path.join(self.shader_searchpath,
                                                       root + ext)
                            arnold.AiLoadPlugin(self.ccp("%s" % osoFilepath))
                            oslShader = arnold.AiNode(self.ccp(root))
                            if oslShader == None:
                                print("WARNING: can't load %s" % osoFilepath)
                                # return shader, opaque
                            else:
                                # name
                                arnold.AiNodeSetStr(oslShader,
                                                    self.ccp("name"),
                                                    self.ccp("MAOSL" + # ID_MA
                                                             mat.name))
                                for p in inputs:
                                    print("%s %s" % (p.type, p.name))
                                    if p.type == 'VECTOR':
                                        x = p.default_value[0]
                                        y = p.default_value[1]
                                        z = p.default_value[2]
                                        arnold.AiNodeSetVec(oslShader,
                                                            self.ccp(p.name),
                                                            x, y, z)
                                    elif p.type == 'VALUE':
                                        arnold.AiNodeSetFlt(oslShader,
                                                            self.ccp(p.name),
                                                            p.default_value)
                                    elif p.type == 'RGBA':
                                        r = p.default_value[0]
                                        g = p.default_value[1]
                                        b = p.default_value[2]
                                        a = p.default_value[3]
                                        # ignore alpha
                                        arnold.AiNodeSetRGB(oslShader,
                                                            self.ccp(p.name),
                                                            r, g, b)
                                    else:
                                        print("TODO: %s %s" % (p.type, p.name))
                                # create standard shader
                                standard = arnold.AiNode(self.ccp("standard"))
                                arnold.AiNodeSetStr(standard,
                                                    self.ccp("name"),
                                                    self.ccp("MA" + # ID_MA
                                                             mat.name))
                                # link standard shader and OSL pattern
                                arnold.AiNodeLink(oslShader, self.ccp("Kd_color"),
                                                  standard)
                                return standard, opaque
        # check for proper diffuse and specular model
        if mat.diffuse_shader != 'LAMBERT':
            print("WARNING: switching %s to 'LAMBERT'" % mat.name)
            mat.diffuse_shader = 'LAMBERT'
        if mat.specular_shader != 'PHONG':
            print("WARNING: switching %s to 'PHONG'" % mat.name)
            mat.specular_shader = 'PHONG'
        # textures?
        textures_found = 0
        textures = {}
        if mat:
            # check for textures
            for index in list(range(len(mat.texture_slots))):
                slot = mat.texture_slots[index]
                if slot:
                    texture = slot.texture
                    if slot.texture_coords == 'UV':
                        if texture.type == 'IMAGE':
                            image = texture.image
                            src = image.filepath
                            if slot.use_map_color_diffuse:
                                # diffuse texture
                                textures["diffuse"] = src
                                textures_found += 1
        # shader
        shader = arnold.AiNode(self.ccp("standard"))
        # name
        arnold.AiNodeSetStr(shader,
                            self.ccp("name"),
                            self.ccp("MA" + # ID_MA
                                     mat.name))
        # Kd
        Kd = mat.diffuse_intensity
        try:
            diffuse_tex = textures["diffuse"]
        except KeyError:
            arnold.AiNodeSetFlt(shader,
                                self.ccp("Kd"),
                                Kd)
        else:
            paths = bpy.utils.blend_paths()
            if diffuse_tex in paths:
                index = paths.index(diffuse_tex)
                abs_paths = bpy.utils.blend_paths(absolute = True)
                abs_path = abs_paths[index]
                # image
                image = arnold.AiNode(self.ccp("image"))
                if mi == None:
                    arnold.AiNodeSetStr(image, self.ccp("name"),
                                        self.ccp("IM" + # ID_IM
                                                 name + "_diff_tex"))
                else:
                    arnold.AiNodeSetStr(image, self.ccp("name"),
                                        self.ccp("IM" + # ID_IM
                                                 name + "_%02d_diff_tex" %
                                                 mi))
                arnold.AiNodeSetStr(image, self.ccp("filename"),
                                    self.ccp(abs_path))
                # Kd
                arnold.AiNodeSetFlt(shader,
                                    self.ccp("Kd"),
                                    Kd)
                # using image
                arnold.AiNodeLink(image, self.ccp("Kd_color"), shader)
            else:
                print("WARNING: couldn't find '%s' in paths" % diffuse_tex)
                arnold.AiNodeSetFlt(shader,
                                    self.ccp("Kd"),
                                    Kd)
        # Kd_color
        Kd_color = mat.diffuse_color
        arnold.AiNodeSetRGB(shader,
                            self.ccp("Kd_color"),
                            Kd_color[0],
                            Kd_color[1],
                            Kd_color[2])
        # Ks
        Ks = mat.specular_intensity
        arnold.AiNodeSetFlt(shader,
                            self.ccp("Ks"),
                            Ks)
        # Ks_color
        Ks_color = mat.specular_color
        arnold.AiNodeSetRGB(shader,
                            self.ccp("Ks_color"),
                            Ks_color[0],
                            Ks_color[1],
                            Ks_color[2])
        if mat.use_transparency and mat.transparency_method == 'RAYTRACE':
            # Kd
            Kd = 0.0
            arnold.AiNodeSetFlt(shader,
                                self.ccp("Kd"),
                                Kd)
            # Ks
            Ks = 0.0
            arnold.AiNodeSetFlt(shader,
                                self.ccp("Ks"),
                                Ks)
            # Kr
            Kr = mat.alpha
            arnold.AiNodeSetFlt(shader,
                                self.ccp("Kr"),
                                Kr)
            # Kt
            Kt = 1.0 - mat.alpha
            arnold.AiNodeSetFlt(shader,
                                self.ccp("Kt"),
                                Kt)
            # Kt_color
            maxColor = Kd_color[0]
            if Kd_color[1] > maxColor:
                maxColor = Kd_color[1]
            if Kd_color[2] > maxColor:
                maxColor = Kd_color[2]
            Kt_color = [Kd_color[0] / maxColor,
                        Kd_color[1] / maxColor,
                        Kd_color[2] / maxColor]
            arnold.AiNodeSetRGB(shader,
                                self.ccp("Kt_color"),
                                Kt_color[0],
                                Kt_color[1],
                                Kt_color[2])
            # IOR
            IOR = mat.raytrace_transparency.ior
            arnold.AiNodeSetFlt(shader,
                                self.ccp("IOR"),
                                IOR)
            # opaque
            opaque = False
        if mat.raytrace_mirror.use:
            # specular_roughness
            specular_roughness = math.sqrt(mat.roughness)
            arnold.AiNodeSetFlt(shader,
                                self.ccp("specular_roughness"),
                                specular_roughness)
            if specular_roughness > 0.0:
                # Ks
                Ks = mat.raytrace_mirror.reflect_factor
                arnold.AiNodeSetFlt(shader,
                                    self.ccp("Ks"),
                                    Ks)
                # Ks_color
                Ks_color = mat.mirror_color
                arnold.AiNodeSetRGB(shader,
                                    self.ccp("Ks_color"),
                                    Ks_color[0],
                                    Ks_color[1],
                                    Ks_color[2])
            # Kr
            Kr = mat.raytrace_mirror.reflect_factor
            arnold.AiNodeSetFlt(shader,
                                self.ccp("Kr"),
                                Kr)
            # Kr_color
            Kr_color = mat.mirror_color
            arnold.AiNodeSetRGB(shader,
                                self.ccp("Kr_color"),
                                Kr_color[0],
                                Kr_color[1],
                                Kr_color[2])
        elif Ks > 0.0:
            # specular_roughness
            specular_roughness = math.sqrt(mat.roughness)
            arnold.AiNodeSetFlt(shader,
                                self.ccp("specular_roughness"),
                                specular_roughness)
            if specular_roughness == 0.0:
                # Kr
                Kr = Ks
                arnold.AiNodeSetFlt(shader,
                                    self.ccp("Kr"),
                                    Kr)
                # Kr_color
                Kr_color = Ks_color
                arnold.AiNodeSetRGB(shader,
                                    self.ccp("Kr_color"),
                                    Kr_color[0],
                                    Kr_color[1],
                                    Kr_color[2])
        if mat.emit != 0.0:
            # emission
            emission = mat.emit
            arnold.AiNodeSetFlt(shader,
                                self.ccp("emission"),
                                emission)
            # emission_color
            emission_color = mat.diffuse_color
            arnold.AiNodeSetRGB(shader,
                                self.ccp("emission_color"),
                                emission_color[0],
                                emission_color[1],
                                emission_color[2])
            # opacity
            opacity = [1.0, 1.0, 1.0]
            arnold.AiNodeSetRGB(shader,
                                self.ccp("opacity"),
                                opacity[0],
                                opacity[1],
                                opacity[2])
            # return
            opaque = True
        return shader, opaque

    def finishExport(self, sun = None):
        # motion blur
        if self.use_motion_blur:
            # for all animated objects ...
            for objIdx in range(len(self.animatedObjects)):
                objName = self.animatedObjects[objIdx]
                matrices = self.motionMatrices[objName]
                # and store them in an Arnold array
                m_array = arnold.AiArrayAllocate(1,
                                                 self.motion_blur_samples,
                                                 arnold.AI_TYPE_MATRIX)
                for midx in range(len(matrices)):
                    m = matrices[midx][0]
                    am = arnold.AtMatrix()
                    arnold.AiM4Identity(am)
                    am.a00 = m[0][0]
                    am.a01 = m[1][0]
                    am.a02 = m[2][0]
                    am.a03 = m[3][0]
                    am.a10 = m[0][1]
                    am.a11 = m[1][1]
                    am.a12 = m[2][1]
                    am.a13 = m[3][1]
                    am.a20 = m[0][2]
                    am.a21 = m[1][2]
                    am.a22 = m[2][2]
                    am.a23 = m[3][2]
                    am.a30 = self.scale_length * m[0][3]
                    am.a31 = self.scale_length * m[1][3]
                    am.a32 = self.scale_length * m[2][3]
                    am.a33 = m[3][3]
                    arnold.AiArraySetMtx(m_array, midx, am)
                # now find the Arnold node
                obj = bpy.data.objects[objName]
                if obj.type == 'MESH':
                    objName = "ME" + objName # ID_ME
                elif obj.type == 'SURFACE':
                    objName = "CU" + objName # ID_CU
                elif obj.type == 'CAMERA':
                    objName = "CA" + objName # ID_CA
                node = arnold.AiNodeLookUpByName(self.ccp(objName))
                if node != None:
                    arnold.AiNodeSetArray(node, self.ccp("matrix"), m_array)
                else:
                    if obj.type != 'EMPTY':
                        print("WARNING: couln't look up Arnold node by name " +
                              "\"%s\"" % objName)
        # AiASSWrite
        sb = self.ccp(self.filename)
        node_mask = arnold.AI_NODE_ALL
        open_procs = False
        binary = False
        intRet = arnold.AiASSWrite(sb, node_mask, open_procs, binary)
        print("INFO: %s" % self.filename)
        # AiEnd
        arnold.AiEnd()
