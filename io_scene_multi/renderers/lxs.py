import os
import math
# common exporter interface
from ..common.interface import CommonExporterInterface
# Blender
import bpy

class LxsExporter(CommonExporterInterface):
    def __init__(self, options):
        CommonExporterInterface.__init__(self, "LxsExporter", options)
        self.lxsFile = None

    def prepareExport(self, scene, bbox, directory, name, mblur, light_counter,
                      sun = None):
        # scene related
        self.scene = scene
        self.bbox = bbox
        if scene.unit_settings.system != 'METRIC':
            print("WARNING: unit_settings == '%s'" % scene.unit_settings.system)
        self.scale_length = scene.unit_settings.scale_length
        # light related
        self.light_counter = light_counter
        # open
        filename = os.path.join(directory, name + ".lxs")
        print('INFO: %s' % filename)
        self.lxsFile = open(filename, "w")

    def writeCamera(self, name, lens, angle, resolution, border, AA_samples,
                    tile_size, transform, isRenderCamera, sun = None):
        if not isRenderCamera:
            # name
            self.lxsFile.write("# %s\n" % name)
            # LookAt (see luxblend/properties/camera.py)
            m = transform
            matrix = m.transposed()
            pos = matrix[3]
            forwards = -matrix[2]
            target = (pos + forwards)
            up = matrix[1]
            self.lxsFile.write("# LookAt %s %s %s # position\n" %
                               (self.scale_length * pos[0],
                                self.scale_length * pos[1],
                                self.scale_length * pos[2]))
            self.lxsFile.write("#        %s %s %s # target\n" %
                               (self.scale_length * target[0],
                                self.scale_length * target[1],
                                self.scale_length * target[2]))
            self.lxsFile.write("#        %s %s %s # up\n" %
                               (up[0], up[1], up[2]))
            # Camera
            self.lxsFile.write('# Camera "perspective"\n')
            aspect = resolution[0] / float(resolution[1])
            if aspect >= 1.0:
                fov = 2 * math.degrees(math.atan(16.0 / (aspect * lens)))
            else:
                fov = 2 * math.degrees(math.atan((aspect * 16.0) / lens))
            self.lxsFile.write('#   "float fov" [ %s ]\n' % fov)
        else:
            # name
            self.lxsFile.write("# %s\n" % name)
            # LookAt (see luxblend/properties/camera.py)
            m = transform
            matrix = m.transposed()
            pos = matrix[3]
            forwards = -matrix[2]
            target = (pos + forwards)
            up = matrix[1]
            self.lxsFile.write("LookAt %s %s %s # position\n" %
                               (self.scale_length * pos[0],
                                self.scale_length * pos[1],
                                self.scale_length * pos[2]))
            self.lxsFile.write("       %s %s %s # target\n" %
                               (self.scale_length * target[0],
                                self.scale_length * target[1],
                                self.scale_length * target[2]))
            self.lxsFile.write("       %s %s %s # up\n" %
                               (up[0], up[1], up[2]))
            # Camera
            self.lxsFile.write('Camera "perspective"\n')
            aspect = resolution[0] / float(resolution[1])
            if aspect >= 1.0:
                fov = 2 * math.degrees(math.atan(16.0 / (aspect * lens)))
            else:
                fov = 2 * math.degrees(math.atan((aspect * 16.0) / lens))
            self.lxsFile.write('  "float fov" [ %s ]\n' % fov)
            # Film
            self.lxsFile.write('Film "fleximage"\n')
            self.lxsFile.write('  "integer xresolution" [ %s ]\n' %
                               resolution[0])
            self.lxsFile.write('  "integer yresolution" [ %s ]\n' %
                               resolution[1])
            # fireflies 0 = off [3-10]
            self.lxsFile.write('  "integer outlierrejection_k" [ %s ]\n' % 10)
            # Sampler
            self.lxsFile.write('#Sampler "metropolis"\n')
            self.lxsFile.write('Sampler "sobol"\n') # based on Cycles
            # PixelFilter
            self.lxsFile.write('PixelFilter "blackmanharris"\n')
            # SurfaceIntegrator
            #
            # bidirectional, path, exphotonmap, directlighting, igi,
            # distributedpath, sppm
            if self.options.lighting == 'direct_lighting':
                self.lxsFile.write('SurfaceIntegrator "directlighting"\n')
            else:
                self.lxsFile.write('SurfaceIntegrator "bidirectional"\n')
            # WorldBegin
            self.lxsFile.write("WorldBegin\n")
            # export all materials
            for mat in bpy.data.materials:
                # check for textures
                textures_found = 0
                textures = {}
                for index in list(range(len(mat.texture_slots))):
                    slot = mat.texture_slots[index]
                    if slot:
                        texture = slot.texture
                        if slot.texture_coords == 'UV':
                            if texture.type == 'IMAGE':
                                image = texture.image
                                src = image.filepath
                                if slot.use_map_color_diffuse:
                                    # diffuse texture
                                    textures["diffuse"] = src
                                    textures_found += 1
                                    # Texture
                                    paths = bpy.utils.blend_paths()
                                    if src in paths:
                                        index = paths.index(src)
                                        abs_paths = bpy.utils.blend_paths(absolute =
                                                                          True)
                                        abs_path = abs_paths[index]
                                        basename = os.path.basename(abs_path)
                                        self.lxsFile.write('  Texture "%s::Kd" ' %
                                                           mat.name +
                                                           '"color" "imagemap"\n')
                                        self.lxsFile.write('    "string wrap" ' +
                                                           '["repeat"]\n')
                                        self.lxsFile.write('    "string ' +
                                                           'filename" ' +
                                                           '["%s"]\n' % basename)
                                        self.lxsFile.write('    "float gamma" ' +
                                                           '[2.2]\n')
                                        self.lxsFile.write('    "float gain" ' +
                                                           '[1.000000]\n')
                                        self.lxsFile.write('    "string ' +
                                                           'filtertype" ' +
                                                           '["bilinear"]\n')
                                        self.lxsFile.write('    "string mapping" ' +
                                                           '["uv"]\n')
                                        self.lxsFile.write('    "float uscale" ' +
                                                           '[1.000000]\n')
                                        self.lxsFile.write('    "float vscale" ' +
                                                           '[-1.000000]\n')
                                        self.lxsFile.write('    "float vscale" ' +
                                                           '[-1.000000]\n')
                                        self.lxsFile.write('    "float vdelta" ' +
                                                           '[1.000000]\n')
                self.lxsFile.write("  # %s\n" % mat.name)
                self.lxsFile.write('  MakeNamedMaterial "%s"\n' % mat.name)
                if mat.use_transparency:
                    # glass
                    self.lxsFile.write('    "string type" [ "%s" ]\n' % "glass")
                    index = mat.raytrace_transparency.ior
                    self.lxsFile.write('    "float index" [ %s ]\n' % index)
                    if mat.raytrace_mirror.use:
                        Kr = mat.mirror_color
                    else:
                        Kr = [1.0, 1.0, 1.0]
                    self.lxsFile.write('    "color Kr" [ %s %s %s ]\n' %
                                       (Kr[0], Kr[1], Kr[2]))
                    Kd = mat.diffuse_color
                    maxColor = Kd[0]
                    if Kd[1] > maxColor:
                        maxColor = Kd[1]
                    if Kd[2] > maxColor:
                        maxColor = Kd[2]
                    w = maxColor
                    if maxColor > 0.7:
                        Kt = [Kd[0], Kd[1], Kd[2]]
                    else:
                        # works with tapestry in gallery scene
                        Kt = [0.96*Kd[0]/w, 0.96*Kd[1]/w, 0.96*Kd[2]/w]
                    self.lxsFile.write('    "color Kt" [ %s %s %s ]\n' %
                                       (Kt[0], Kt[1], Kt[2]))
                elif mat.raytrace_mirror.use:
                    # mirror
                    self.lxsFile.write('    "string type" ' +
                                       '[ "%s" ]\n' % "mirror")
                    Kr = mat.mirror_color
                    try:
                        diffuse_tex = textures["diffuse"]
                    except KeyError:
                        self.lxsFile.write('    "color Kr" [ %s %s %s ]\n' %
                                           (Kr[0], Kr[1], Kr[2]))
                    else:
                        self.lxsFile.write('    "texture Kr" [ "%s::Kd" ]\n' %
                                           mat.name)
                elif mat.specular_intensity > 0.0:
                    # glossy
                    self.lxsFile.write('    "string type" ' +
                                       '[ "%s" ]\n' % "glossy")
                    Kd = mat.diffuse_color
                    try:
                        diffuse_tex = textures["diffuse"]
                    except KeyError:
                        self.lxsFile.write('    "color Kd" [ %s %s %s ]\n' %
                                           (Kd[0], Kd[1], Kd[2]))
                    else:
                        self.lxsFile.write('    "texture Kd" [ "%s::Kd" ]\n' %
                                           mat.name)
                    Ks = mat.specular_intensity
                    Ks_color = mat.specular_color
                    self.lxsFile.write('    "color Ks" [ %s %s %s ]\n' %
                                       (Ks * Ks_color[0],
                                        Ks * Ks_color[1],
                                        Ks * Ks_color[2]))
                    roughness = mat.roughness
                    self.lxsFile.write('    "float uroughness" [ %s ]\n' %
                                       roughness)
                    self.lxsFile.write('    "float vroughness" [ %s ]\n' %
                                       roughness)
                else:
                    # matte
                    self.lxsFile.write('    "string type" [ "%s" ]\n' % "matte")
                    Kd = mat.diffuse_color
                    try:
                        diffuse_tex = textures["diffuse"]
                    except KeyError:
                        self.lxsFile.write('    "color Kd" [ %s %s %s ]\n' %
                                           (Kd[0], Kd[1], Kd[2]))
                    else:
                        self.lxsFile.write('    "texture Kd" [ "%s::Kd" ]\n' %
                                           mat.name)

    def writeCommonPrimitive(self, name, transform, mat):
        self.lxsFile.write("  # %s\n" % name)
        # AttributeBegin
        self.lxsFile.write("  AttributeBegin\n")
        # Transform
        m = transform
        self.lxsFile.write('    Transform [\n')
        self.lxsFile.write('      %s %s %s %s\n' %
                           (m[0][0],
                            m[1][0],
                            m[2][0],
                            m[3][0]))
        self.lxsFile.write('      %s %s %s %s\n' %
                           (m[0][1],
                            m[1][1],
                            m[2][1],
                            m[3][1]))
        self.lxsFile.write('      %s %s %s %s\n' %
                           (m[0][2],
                            m[1][2],
                            m[2][2],
                            m[3][2]))
        self.lxsFile.write('      %s %s %s %s\n' %
                           (self.scale_length * m[0][3],
                            self.scale_length * m[1][3],
                            self.scale_length * m[2][3],
                            m[3][3]))
        self.lxsFile.write('    ]\n')

    def writeCone(self, name, transform, mat):
        self.writeCommonPrimitive(name, transform, mat)
        # NamedMaterial
        matName = mat.name
        self.lxsFile.write('    NamedMaterial "%s"\n' % matName)
        if mat.emit != 0.0:
            # LightGroup
            self.lxsFile.write('    LightGroup "%s"\n' % matName)
            # AreaLightSource
            self.lxsFile.write('    AreaLightSource "%s"\n' % "area")
            power = mat.emit
            self.lxsFile.write('      "float power" [ %s ]\n' % power)
            L = mat.diffuse_color
            self.lxsFile.write('      "color L" [ %s %s %s ]\n' %
                               (L[0], L[1], L[2]))
        # Cone
        obj = bpy.data.objects[name]
        p1 = obj.data.splines[0].points[0].co
        p2 = obj.data.splines[0].points[8].co
        radius1 = math.fabs(p1.y)
        radius2 = math.fabs(p2.y)
        if radius1 < radius2:
            height = math.fabs(p2.z - p1.z)
            self.lxsFile.write('    Translate 0 0 %s\n' %
                               (self.scale_length * height))
            self.lxsFile.write('    Rotate 180 1 0 0\n')
            self.lxsFile.write('    Shape "cone"\n')
            self.lxsFile.write('      "float radius" [ %s ]\n' %
                               (self.scale_length * math.fabs(p2.y)))
            self.lxsFile.write('      "float radius2" [ %s ]\n' %
                               (self.scale_length * math.fabs(p1.y)))
        else:
            self.lxsFile.write('    Shape "cone"\n')
            self.lxsFile.write('      "float radius" [ %s ]\n' %
                               (self.scale_length * math.fabs(p1.y)))
            self.lxsFile.write('      "float radius2" [ %s ]\n' %
                               (self.scale_length * math.fabs(p2.y)))

        self.lxsFile.write('      "float height" [ %s ]\n' %
                           (self.scale_length * math.fabs(p2.z - p1.z)))
        self.lxsFile.write('      "float phimax" [ 360 ]\n')
        # AttributeEnd
        self.lxsFile.write("  AttributeEnd\n")

    def writeCylinder(self, name, transform, mat):
        self.writeCommonPrimitive(name, transform, mat)
        # NamedMaterial
        matName = mat.name
        self.lxsFile.write('    NamedMaterial "%s"\n' % matName)
        if mat.emit != 0.0:
            # LightGroup
            self.lxsFile.write('    LightGroup "%s"\n' % matName)
            # AreaLightSource
            self.lxsFile.write('    AreaLightSource "%s"\n' % "area")
            power = mat.emit
            self.lxsFile.write('      "float power" [ %s ]\n' % power)
            L = mat.diffuse_color
            self.lxsFile.write('      "color L" [ %s %s %s ]\n' %
                               (L[0], L[1], L[2]))
        # Cylinder
        self.lxsFile.write('    Shape "cylinder"\n')
        self.lxsFile.write('      "float radius" [ %s ]\n' % self.scale_length)
        self.lxsFile.write('      "float zmin" [ 0 ]\n')
        self.lxsFile.write('      "float zmax" [ %s ]\n' % self.scale_length)
        self.lxsFile.write('      "float phimax" [ 360 ]\n')
        # AttributeEnd
        self.lxsFile.write("  AttributeEnd\n")

    def writeHdrLighting(self, hdrImagePath):
        self.lxsFile.write('  # HDR lighting\n')
        self.lxsFile.write('  LightSource "infinite"\n')
        self.lxsFile.write('    "float gain" [1.0]\n')
        self.lxsFile.write('    "float importance" [1.0]\n')
        self.lxsFile.write('    "string mapname" ["%s"]\n' % hdrImagePath)
        self.lxsFile.write('    "string mapping" ["latlong"]\n')
        self.lxsFile.write('    "float gamma" [1.0]\n')
        self.lxsFile.write('    "integer nsamples" [1]\n')

    def writeMesh(self, name, transform, info, user_matrix = None):
        # info
        materials = info[0]
        vertices = info[1]
        vertexNormals = info[2]
        polygons = info[3]
        polygonNormals = info[4]
        smoothPolygons = info[5]
        polygonMaterialIndices = info[6]
        hasUVs, uvs = info[7]
        # name
        self.lxsFile.write("  # %s\n" % name)
        # AttributeBegin
        self.lxsFile.write("  AttributeBegin\n")
        # Transform
        m = transform
        self.lxsFile.write('    Transform [\n')
        self.lxsFile.write('      %s %s %s %s\n' %
                           (m[0][0],
                            m[1][0],
                            m[2][0],
                            m[3][0]))
        self.lxsFile.write('      %s %s %s %s\n' %
                           (m[0][1],
                            m[1][1],
                            m[2][1],
                            m[3][1]))
        self.lxsFile.write('      %s %s %s %s\n' %
                           (m[0][2],
                            m[1][2],
                            m[2][2],
                            m[3][2]))
        self.lxsFile.write('      %s %s %s %s\n' %
                           (self.scale_length * m[0][3],
                            self.scale_length * m[1][3],
                            self.scale_length * m[2][3],
                            m[3][3]))
        self.lxsFile.write('    ]\n')
        # sort by materials
        infoMaterial = {}
        for matIndex in range(len(materials)):
            matName = materials[matIndex]
            # store for each material a list with len(vertices) 0 entries
            matIndixes = [0] * len(vertices)
            infoMaterial[matIndex] = matIndixes
        for pi in range(len(polygonMaterialIndices)):
            pmi = polygonMaterialIndices[pi]
            polygon = polygons[pi]
            for vi in polygon:
                # count how often a vertex is used per material
                infoMaterial[pmi][vi] += 1
        for matIndex in range(len(materials)):
            counter = 0
            for index in range(len(infoMaterial[matIndex])):
                vi = infoMaterial[matIndex][index]
                # replace the counter by ...
                if vi == 0:
                    # ... -1 if the material doesn't use this vertex
                    infoMaterial[matIndex][index] = -1
                else:
                    # ... or by an increasing index
                    infoMaterial[matIndex][index] = counter
                    counter += 1
        # for each material ...
        for matIndex in range(len(materials)):
            matName = materials[matIndex]
            # NamedMaterial
            self.lxsFile.write('    NamedMaterial "%s"\n' % matName)
            mat = bpy.data.materials[matName]
            if mat.emit != 0.0:
                # LightGroup
                self.lxsFile.write('    LightGroup "%s"\n' % matName)
                # AreaLightSource
                self.lxsFile.write('    AreaLightSource "%s"\n' % "area")
                power = mat.emit
                self.lxsFile.write('      "float power" [ %s ]\n' % power)
                L = mat.diffuse_color
                self.lxsFile.write('      "color L" [ %s %s %s ]\n' %
                                   (L[0], L[1], L[2]))
            # if there's a single smooth polygon, we export normals
            useVertexNormals = False
            for pi in range(len(polygonMaterialIndices)):
                pmi = polygonMaterialIndices[pi]
                if pmi == matIndex:
                    smoothPolygon = smoothPolygons[pi]
                    if smoothPolygon:
                        useVertexNormals = True
                        break
            # mesh
            self.lxsFile.write('    Shape "mesh"\n')
            self.lxsFile.write('      "point P" [\n')
            for pi in range(len(polygonMaterialIndices)):
                pmi = polygonMaterialIndices[pi]
                if pmi == matIndex:
                    polygon = polygons[pi]
                    for vi in polygon:
                        v = vertices[vi] # use original index
                        self.lxsFile.write('        %s %s %s\n' %
                                           (self.scale_length * v[0],
                                            self.scale_length * v[1],
                                            self.scale_length * v[2]))
            self.lxsFile.write('      ]\n')
            # normals (based on smoothPolygons)
            if useVertexNormals:
                # normals
                self.lxsFile.write('      "normal N" [\n')
                for pi in range(len(polygonMaterialIndices)):
                    pmi = polygonMaterialIndices[pi]
                    smoothPolygon = smoothPolygons[pi]
                    if pmi == matIndex:
                        if not smoothPolygon:
                            # use face normal
                            normal = polygonNormals[pi]
                        polygon = polygons[pi]
                        for vi in polygon:
                            if smoothPolygon:
                                # use vertex normal
                                n = vertexNormals[vi]
                            else:
                                n = normal # face normal
                            self.lxsFile.write('        %s %s %s\n' %
                                               (n[0], n[1], n[2]))

                self.lxsFile.write('      ]\n')
            # quads or triangles?
            triangles = []
            quads = []
            for pi in range(len(polygonMaterialIndices)):
                pmi = polygonMaterialIndices[pi]
                if pmi == matIndex:
                    polygon = polygons[pi]
                    if (len(polygon) == 3):
                        triangles.append(polygon)
                    elif (len(polygon) == 4):
                        quads.append(polygon)
            # quads
            if len(quads):
                counter = 0
                self.lxsFile.write('      "integer quadindices" [\n')
                for pi in range(len(polygonMaterialIndices)):
                    pmi = polygonMaterialIndices[pi]
                    if pmi == matIndex:
                        polygon = polygons[pi]
                        if (len(polygon) == 3):
                            counter += 3
                        elif (len(polygon) == 4):
                            self.lxsFile.write('        ')
                            for dummy in range(4):
                                self.lxsFile.write("%s " % counter)
                                counter += 1
                            self.lxsFile.write('\n')
                self.lxsFile.write('      ]\n')
            # triangles
            if len(triangles):
                counter = 0
                self.lxsFile.write('      "integer triindices" [\n')
                for pi in range(len(polygonMaterialIndices)):
                    pmi = polygonMaterialIndices[pi]
                    if pmi == matIndex:
                        polygon = polygons[pi]
                        if (len(polygon) == 4):
                            counter += 4
                        elif (len(polygon) == 3):
                            self.lxsFile.write('        ')
                            for dummy in range(3):
                                self.lxsFile.write("%s " % counter)
                                counter += 1
                            self.lxsFile.write('\n')
                self.lxsFile.write('      ]\n')
            # uv-coordinates
            if len(uvs):
                self.lxsFile.write('      "float uv" [\n')
                for pi in range(len(polygonMaterialIndices)):
                    pmi = polygonMaterialIndices[pi]
                    if pmi == matIndex:
                        uvs_poly = uvs[pi]
                        for uv in uvs_poly:
                            self.lxsFile.write('        %s %s\n' %
                                               (uv[0], uv[1]))
                self.lxsFile.write('      ]\n')
        # AttributeEnd
        self.lxsFile.write("  AttributeEnd\n")

    def writePointLight(self, name, transform, info):
        self.lxsFile.write('  # %s\n' % name)
        # lamp data
        lamp = bpy.data.lamps[name]
        # AttributeBegin
        self.lxsFile.write("  AttributeBegin\n")
        # LightGroup
        self.lxsFile.write('    LightGroup "%s"\n' % name)
        # LightSource
        loc, rot, scale = transform.decompose()
        self.lxsFile.write('    LightSource "point"\n')
        self.lxsFile.write('      "color L" [ %s %s %s ]\n' %
                           (lamp.color[0], lamp.color[1], lamp.color[2]))
        self.lxsFile.write('      "point from" [ %s %s %s ]\n' %
                           (loc[0], loc[1], loc[2]))
        # AttributeEnd
        self.lxsFile.write("  AttributeEnd\n")

    def writeRing(self, name, transform, mat):
        self.writeCommonPrimitive(name, transform, mat)
        # NamedMaterial
        matName = mat.name
        self.lxsFile.write('    NamedMaterial "%s"\n' % matName)
        if mat.emit != 0.0:
            # LightGroup
            self.lxsFile.write('    LightGroup "%s"\n' % matName)
            # AreaLightSource
            self.lxsFile.write('    AreaLightSource "%s"\n' % "area")
            power = mat.emit
            self.lxsFile.write('      "float power" [ %s ]\n' % power)
            L = mat.diffuse_color
            self.lxsFile.write('      "color L" [ %s %s %s ]\n' %
                               (L[0], L[1], L[2]))
        # disk
        obj = bpy.data.objects[name]
        p1 = obj.data.splines[0].points[0].co
        p2 = obj.data.splines[0].points[8].co
        radius = math.fabs(p2.y)
        innerradius = math.fabs(p1.y)
        if innerradius > radius:
            tmp = innerradius
            innerradius = radius
            radius = tmp
        self.lxsFile.write('    Shape "disk"\n')
        self.lxsFile.write('      "float height" [ 0 ]\n')
        self.lxsFile.write('      "float innerradius" [ %s ]\n' %
                           (self.scale_length * innerradius))
        self.lxsFile.write('      "float radius" [ %s ]\n' %
                           (self.scale_length * radius))
        self.lxsFile.write('      "float phimax" [ 360 ]\n')
        # AttributeEnd
        self.lxsFile.write("  AttributeEnd\n")

    def writeSphere(self, name, transform, mat):
        self.writeCommonPrimitive(name, transform, mat)
        # NamedMaterial
        matName = mat.name
        self.lxsFile.write('    NamedMaterial "%s"\n' % matName)
        if mat.emit != 0.0:
            # LightGroup
            self.lxsFile.write('    LightGroup "%s"\n' % matName)
            # AreaLightSource
            self.lxsFile.write('    AreaLightSource "%s"\n' % "area")
            power = mat.emit
            self.lxsFile.write('      "float power" [ %s ]\n' % power)
            L = mat.diffuse_color
            self.lxsFile.write('      "color L" [ %s %s %s ]\n' %
                               (L[0], L[1], L[2]))
        # sphere
        self.lxsFile.write('    Shape "sphere"\n')
        self.lxsFile.write('      "float radius" [ %s ]\n' % self.scale_length)
        self.lxsFile.write('      "float zmin" [ -%s ]\n' % self.scale_length)
        self.lxsFile.write('      "float zmax" [ %s ]\n' % self.scale_length)
        self.lxsFile.write('      "float phimax" [ 360 ]\n')
        # AttributeEnd
        self.lxsFile.write("  AttributeEnd\n")

    def writeSunLight(self, name, transform, info):
        self.lxsFile.write('  # %s\n' % name)
        # lamp data
        lamp = bpy.data.lamps[name]
        # AttributeBegin
        self.lxsFile.write("  AttributeBegin\n")
        # LightGroup
        self.lxsFile.write('    LightGroup "%s"\n' % name)
        # LightSource
        gain = 1.0
        importance = 1.0
        nsamples = 1
        turbidity = lamp.sky.atmosphere_turbidity
        # see export_arnold_ass.py for sundir calculation
        m = transform
        sunPos, rot, scale = m.decompose()
        obj = bpy.data.objects[name]
        constraint = obj.constraints[0]
        if constraint.type == 'TRACK_TO':
            compass = constraint.target
            m = compass.matrix_world.copy()
            compassPos, rot, scale = m.decompose()
            sundir = [sunPos[0] - compassPos[0],
                      sunPos[1] - compassPos[1],
                      sunPos[2] - compassPos[2]]
            sundirLen = math.sqrt((sundir[0] * sundir[0] +
                                   sundir[1] * sundir[1] +
                                   sundir[2] * sundir[2]))
            sundir[0] = sundir[0] / sundirLen
            sundir[1] = sundir[1] / sundirLen
            sundir[2] = sundir[2] / sundirLen
        self.lxsFile.write('    LightSource "sunsky2"\n')
        self.lxsFile.write('      "float gain" [ %s ]\n' % gain)
        self.lxsFile.write('      "float importance" [ %s ]\n' % importance)
        self.lxsFile.write('      "integer nsamples" [ %s ]\n' % nsamples)
        self.lxsFile.write('      "float turbidity" [ %s ]\n' % turbidity)
        self.lxsFile.write('      "vector sundir" [ %s %s %s ]\n' %
                           (sundir[0], sundir[1], sundir[2]))
        # AttributeEnd
        self.lxsFile.write("  AttributeEnd\n")

    def finishExport(self, sun = None):
        # WorldEnd
        self.lxsFile.write("WorldEnd\n")
        # close
        self.lxsFile.close()
