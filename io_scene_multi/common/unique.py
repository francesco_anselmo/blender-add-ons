def fix_name(name):
    new_name = name
    # " " -> "_"
    new_name = "_".join(new_name.split(" "))
    # "." -> "_"
    new_name = "_".join(new_name.split("."))
    # "+" -> "_"
    new_name = "_".join(new_name.split("+"))
    # "-" -> "_"
    new_name = "_".join(new_name.split("-"))
    return new_name

class UniqueNames:
    def __init__(self):
        self.names = {}
        self.counter = 0

    def uniqueName(self, proposalIn):
        proposal = fix_name(proposalIn)
        useName = proposal
        try:
            test = self.names[proposal]
        except KeyError:
            # the name wasn't used yet
            self.names[proposal] = [proposal]
        else:
            # create new name
            newName = "%s_un%04d" % (proposal, self.counter)
            # try new name
            try:
                test2 = self.names[newName]
            except KeyError:
                # the name wasn't used yet
                useName = newName
            else:
                # no test, just assume we got a unique name now !!!
                newName = "%s%04d" % ("uniqueName", self.counter)
                useName = newName
            ##print("rename '%s' -> '%s'" % (proposal, useName))
            self.counter += 1
        return useName
