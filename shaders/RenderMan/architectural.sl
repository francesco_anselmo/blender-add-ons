/*
	Copyright (c) 2007 TAARNA Studios International.
*/

//
// Architectural shader for 3delight.
// Authors: Aghiles Kheffache & the /*jupiter jazz*/ group
//

#define bool float
#define true 1
#define false 0

/*
 * Oren and Nayar's generalization of Lambert's reflection model.
 * The roughness parameter gives the standard deviation of angle
 * orientations of the presumed surface grooves.  When roughness=0,
 * the model is identical to Lambertian reflection.
 * Taken from Arman so credits go to Larry Gritz.
 */
color
LocIllumOrenNayar (normal N;  vector V;  float roughness;)
{
    /* Surface roughness coefficients for Oren/Nayar's formula */
    float sigma2 = roughness * roughness;
    float A = 1 - 0.5 * sigma2 / (sigma2 + 0.33);
    float B = 0.45 * sigma2 / (sigma2 + 0.09);
    /* Useful precomputed quantities */
    float  theta_r = acos (V . N);        /* Angle between V and N */
    vector V_perp_N = normalize(V-N*(V.N)); /* Part of V perpendicular to N */

    /* Accumulate incoming radiance from lights in C */
    color  C = 0;
    extern point P;
    illuminance (P, N, PI/2) {
	/* Must declare extern L & Cl because we're in a function */
	extern vector L;  extern color Cl;
	float nondiff = 0;
	lightsource ("__nondiffuse", nondiff);
	if (nondiff < 1) {
	    vector LN = normalize(L);
	    float cos_theta_i = LN . N;
	    float cos_phi_diff = V_perp_N . normalize(LN - N*cos_theta_i);
	    float theta_i = acos (cos_theta_i);
	    float alpha = max (theta_i, theta_r);
	    float beta = min (theta_i, theta_r);
	    C += (1-nondiff) * Cl * cos_theta_i * 
		(A + B * max(0,cos_phi_diff) * sin(alpha) * tan(beta));
	}
    }
    return C;

}


// Compute the square of a given value.
float sq(float x)
{
	return x * x;
}

// Compute the CIE luminance (Rec. 709) of a given color.
float luminance(color c)
{
	return
	      comp(c, 0) * 0.212671
	    + comp(c, 1) * 0.715160
	    + comp(c, 2) * 0.072169;
}

// Compute the roughness corresponding to a given glossiness value.
float roughness(float gloss)
{
	return pow(2.0, 8.0 * gloss);
}

// Compute sampling parameters.
float compute_sampling_angle( float glossiness; )
{
	if( glossiness >= 1.0 )
		return 0.0;
	else

	    return (1.0 - pow(clamp(glossiness, 0.0, 1.0), 0.2)) * PI / 2.0;
}

/*
   traces a ray into the scene and lookup the environment
   map if nothing is hit.
*/
color trace_with_environment(
	point i_P;
	vector i_direction;
	uniform string i_environment_map;
	uniform float i_angle, i_samples;
   	uniform float i_single_env_sample;
	uniform float i_ray_type; )
{
	color env_color;
	color reflection = 0;
	if( i_single_env_sample == 1 || i_angle == 0 )
	{
		color T;
		reflection = trace( i_P, i_direction,
			"samplecone", i_angle,
			"samples", i_samples, "transmission", T );

		if( i_environment_map!="" && (T[0]>0 || T[1]>0 || T[2]>0) )
		{
			reflection +=
				T * environment( i_environment_map, vtransform("world", i_direction) );
		}
	}
	else
	{
		float num_samples = 0;
		color hit_ci = 0;
		vector ray_dir = 0;
		gather( "illuminance",
			i_P, i_direction, i_angle, i_samples,
			"distribution", "cosine",
			"surface:Ci", hit_ci, "ray:dir", ray_dir )
		{
			reflection += hit_ci;
			num_samples += 1;
		}
		else
		{
			if( i_environment_map != "" )
				reflection +=
					environment( i_environment_map, vtransform("world", ray_dir) );
			num_samples += 1;
		}

		reflection /= num_samples;
	}

	return reflection;
}

//
// Compute reflection component.
//
color trace_reflection(
	vector In;
	normal Nf;
	vector V;
	uniform string i_environment_map;
	float i_refl_gloss;
	uniform float i_refl_gloss_samples;
	uniform bool i_single_env_sample; )
{
	extern point P;

	// Compute the sampling parameters.
	uniform float sample_angle = compute_sampling_angle( i_refl_gloss );
	uniform float sample_count = (i_refl_gloss == 0) ?
		min(4, i_refl_gloss_samples) : i_refl_gloss_samples;

	// Compute reflected direction.
	vector refl_dir = reflect(In, Nf);

	return trace_with_environment(
			P, refl_dir, i_environment_map,
			sample_angle, sample_count, i_single_env_sample, 1 );
}

//
// Compute refraction component.
//
color trace_refraction(
	normal Nn;
	vector In;
	normal Nf;
	vector V;
	uniform string i_environment_map;
	float i_refr_ior, i_refr_gloss;
	uniform float i_refr_gloss_samples;
	uniform bool i_single_env_sample; )
{
	extern point P;

	// Compute the sampling parameters.
	uniform float sample_angle = compute_sampling_angle( i_refr_gloss );
	uniform float sample_count = (i_refr_gloss == 0) ?
		min(4, i_refr_gloss_samples) : i_refr_gloss_samples;

	// Compute refracted direction.
	float eta = (In . Nn < 0.0) ? 1.0 / i_refr_ior : i_refr_ior;
	vector refr_dir = refract(In, Nf, eta);

	return trace_with_environment(
		P, refr_dir, i_environment_map, sample_angle, sample_count, 
		i_single_env_sample, 2 );
}

/*
	Compute specular highlights.

	NOTES
	- We use the ward anisotropic model. But we sum 3 highlights with decreasing
	roughness and increasing contribution.
*/
color specular_highlight(
	vector In;
	normal Nf;
	vector V;
	float  refl_roughness_u, refl_roughness_v;)
{
	extern color Cl;
	extern vector dPdu, dPdv;
	extern point P;

	vector xdir = normalize( dPdu );
	vector ydir = normalize( dPdv );

	color highlights = 0;

	/* We have three specular higlihgts of diminushing roughness but increasing
	   brightness */
	uniform float component_coefs[3] = {0.5, 1.0, 1.5}; 

	illuminance( P, Nf, PI * 0.5 )
	{
	    vector Ln = normalize(L);

	    float dot_ln = Ln . Nf;
	    float dot_vn = V . Nf;

	    if( dot_vn*dot_ln>0.0 )
	    {
		    vector Hn = normalize(V + Ln);
		    float dot_hn2 = min(sq(Hn . Nf), 1.0);

		    if( dot_hn2>0.0 )
		    {
			    /* precompute this to get it out of the loop below */
			    float k1_devider = 1 / (sqrt(dot_vn * dot_ln) * 4.0 * PI);  
			    float smooth_step_ln = smoothstep( 0, 0.25, dot_ln );

			    uniform float i=0;
			    uniform float roughness_coef = 1;

			    for( i=0; i<3; i+=1.0 )
			    {
				    // Compute the highlight due to this light source.
				    float k1 =
					    (refl_roughness_u * refl_roughness_v * roughness_coef * roughness_coef )
					    * k1_devider;
				    float k2 =
					    sq(Hn . xdir * refl_roughness_u * roughness_coef)
					    + sq(Hn . ydir * refl_roughness_v * roughness_coef);
				    color c =
					    k1 * exp(-k2 / dot_hn2)
					    * dot_ln
					    * smooth_step_ln;

				    // Accumulate highlights.
				    highlights += Cl * c * component_coefs[i];

					roughness_coef *= 0.5;
				}
		    }
	    }
	}

	return highlights;
}

//
// Main line of the architectural shader.
//
#define RGBA_COLOR( x ) color x; float x##_a;

surface architectural(
	color i_ambient = 0;

	// Diffuse reflection parameters.
	uniform float diffuse_scale = 0.8;
	color i_diffuse_color = 1;
	float i_diffuse_roughness = 0.5;

	// Reflection parameters.
	float reflectivity = 0.5;
	color reflection_color = 1;
	float reflection_gloss = 1; /* perfectly glossy */
	float reflection_gloss_samples = 2;
	bool highlights_only = false;
	bool is_metallic = false;
	string environment_map = "";

	// Transparency/refraction parameters.
	float transparency = 0;
	color refraction_color = 1;
	float refraction_gloss = 1;
	float refraction_ior = 1.3;
	float refraction_gloss_samples = 2; 
#if 0
	bool refraction_translucency = false;
	color refraction_translucency_color = 0;
	float refraction_translucency_weight = 0;
#endif
	float anisotropy = 1;
	float anisotropy_rotation = 0;

	// BRDF parameters.
	bool brdf_fresnel = false;
	float brdf_0_degree_refl = 0.8;
	float brdf_90_degree_refl = 1;
	float brdf_curve = 2;
	bool brdf_conserve_energy = false;

	bool single_env_sample = true;
#if 0
	// Reflection
	bool i_refl_falloff_on = false;
	float i_refl_falloff_dist = 0;
	bool i_refl_falloff_color_on = false;
	color i_refl_falloff_color = 0;
	float i_refl_cutoff = 0;

	// Refraction
	bool i_refr_falloff_on = false;
	float i_refr_falloff_dist = 0;
	bool i_refr_falloff_color_on = false;
	color i_refr_falloff_color = 1;
	float i_refr_cutoff = 0;
#endif

	/* Indirect illumination */
	float indirect_multiplier = 0;

	/* Ambient occlusion. */
	bool enable_ao = false;
	float ao_samples = 32;
	float ao_distance = 0;
	color ao_dark = 0;
	color ao_ambient = 1;

	// Additional flags
	bool thin_walled = false;
	float hl_vs_refl_balance = 1; )
{
	normal Nn = normalize(N);
	vector In = normalize(I);
	normal Nf = faceforward(Nn, In);
	vector V = -In;

	uniform string rtype;
	uniform float rdepth;
	rayinfo( "type", rtype ); rayinfo( "depth", rdepth );

	bool shadow_ray = (rtype == "transmission") ? 1 : 0;
	bool photon_ray = (rtype == "light") ? 1 : 0;

	uniform color refl_color = reflection_color;
	uniform color refr_color = refraction_color;
	uniform bool ao_on = enable_ao;

	if( shadow_ray == true )
	{
		/* Disable further refraction and ambient occlusion */
		refr_color = 0;
		ao_on = false;
	}

	if( rdepth >= 2 )
		refl_color = 0;

	float reflectivity_scale = reflectivity;
	float _diffuse_scale = diffuse_scale;
	float transparency_scale = transparency;

	// Compute the reflection curve.
	if (brdf_fresnel == true)
	{
		// Automatic mode.
		float eta = (In . Nn < 0.0) ? 1.0 / refraction_ior : refraction_ior;
		float kr, kt;
		fresnel( In, Nf, eta, kr, kt);
		reflectivity_scale *= kr;
	}
	else
	{
		// Manual mode.
		reflectivity_scale *=
			mix(
				brdf_0_degree_refl,
				brdf_90_degree_refl,
				pow(1-abs(V . Nf), brdf_curve));
	}

	// Compute component weights.

	color scaled_refl_color = refl_color * reflectivity_scale;
	float scaled_refl_color_a = luminance(scaled_refl_color);

	if (is_metallic == true)
	{
		_diffuse_scale *= 1.0 - luminance(refl_color) * reflectivity_scale;
		_diffuse_scale = max(_diffuse_scale, 0.0);
		scaled_refl_color *= i_diffuse_color;
	}

	RGBA_COLOR(scaled_refr_color);
	scaled_refr_color = refr_color;
	scaled_refr_color_a = luminance(scaled_refr_color);

	if (brdf_conserve_energy == true)
	{
		_diffuse_scale *= 1.0 - min(scaled_refr_color_a * transparency, 1.0);
		_diffuse_scale *= 1.0 - min(scaled_refl_color_a, 1.0);
		transparency_scale *= 1.0 - min(scaled_refl_color_a, 1.0);
	}

	scaled_refr_color *= transparency_scale;

	RGBA_COLOR(scaled_diffuse_color);
	scaled_diffuse_color = i_diffuse_color * _diffuse_scale;

	// Ambient occlusion.
	color ambient_occlusion = 0;

	if (ao_on == true)   // no AO for secondary reflections and refractions
	{
		// Add in the ambient occlusion term. FIXME: transform distance into
		// world units -- todo: aren't shader inputs (and thus ao_distance)
		// already expressed in world space?

		uniform float adaptive = ao_samples >= 256 ? 1 : 0;
		float occ =
			occlusion(
					P, Nf, ao_samples,
					"adaptive", adaptive,
					"maxdist", ao_distance);

		ambient_occlusion = mix(ao_ambient, ao_dark, occ);
	}

	// Diffuse component.
	color diffuse_component =
		scaled_diffuse_color
		* (ambient_occlusion + LocIllumOrenNayar(Nf, V, i_diffuse_roughness));

	// Reflected component.
	color reflected_component = 0;
	if( refl_color != 0 && highlights_only==false )
	{
		reflected_component = scaled_refl_color *
			trace_reflection(
				In, Nf, V,
				environment_map,
				reflection_gloss, reflection_gloss_samples, single_env_sample );
	}

	// Compute reflection roughness values.
	float refl_roughness_u = roughness(reflection_gloss);
	float refl_roughness_v = refl_roughness_u * max(anisotropy, 0.01);

	if (refl_roughness_u >= 80.0)
		refl_roughness_u = 80.0 + sqrt(refl_roughness_u - 80.0);
	if (refl_roughness_v >= 80.0)
		refl_roughness_v = 80.0 + sqrt(refl_roughness_v - 80.0);

	/* Compute specular highlights. In "metal mode" the higlight color is
	   inherited from the diffuse color. */

	color highlights_component = 0;
	color hcolor = (is_metallic == true) ?
		i_diffuse_color*reflectivity : scaled_refl_color; 
	hcolor *= hl_vs_refl_balance;
	if( hcolor != 0 )
	{
		highlights_component = hcolor * 
			specular_highlight(
					In, Nf, V, refl_roughness_u, refl_roughness_v );
	}

	// Refracted component.
	color refracted_component = 0;
	if( refr_color != 0 )
	{
		refracted_component =  scaled_refr_color *
			trace_refraction(
					Nn, In, Nf, V,
					environment_map,
					thin_walled == true ? 1.0 : refraction_ior,
					refraction_gloss,
					refraction_gloss_samples,
					single_env_sample );
	}

	Oi = Os;
	if( shadow_ray == true )
		Oi *= (1-transparency_scale);
	else if( photon_ray == true )
		Oi = scaled_refr_color;

	Ci = diffuse_component
		+ reflected_component
		+ highlights_component
		+ refracted_component
		+ i_ambient;
	Ci*=Oi;
}


#pragma annotation "grouping" "diffuse/diffuse_scale;"
#pragma annotation "grouping" "diffuse/i_diffuse_color;"
#pragma annotation "grouping" "diffuse/i_diffuse_roughness;"

#pragma annotation "grouping" "reflections/reflectivity;"
#pragma annotation "grouping" "reflections/reflection_color;"
#pragma annotation "grouping" "reflections/reflection_gloss;"
#pragma annotation "grouping" "reflections/reflection_gloss_samples;"
#pragma annotation "grouping" "reflections/highlights_only;"
#pragma annotation "grouping" "reflections/is_metallic;"
#pragma annotation "grouping" "reflections/hl_vs_refl_balance;"
#pragma annotation "grouping" "reflections/environment_map;"

#pragma annotation "grouping" "refractions/transparency;"
#pragma annotation "grouping" "refractions/refraction_color;"
#pragma annotation "grouping" "refractions/refraction_gloss;"
#pragma annotation "grouping" "refractions/refraction_gloss_samples;"
#pragma annotation "grouping" "refractions/refraction_ior;"
#pragma annotation "grouping" "refractions/refraction_translucency;"
#pragma annotation "grouping" "refractions/refraction_translucency_color;"
#pragma annotation "grouping" "refractions/refraction_translucency_weight;"
#pragma annotation "grouping" "refractions/thin_walled;"

#pragma annotation "grouping" "ambientOcclusion/enable_ao;"
#pragma annotation "grouping" "ambientOcclusion/ao_samples;"
#pragma annotation "grouping" "ambientOcclusion/ao_dark;"
#pragma annotation "grouping" "ambientOcclusion/ao_ambient;"
#pragma annotation "grouping" "ambientOcclusion/ao_distance;"
#pragma annotation "grouping" "ambientOcclusion/indirect_multiplier;"

#pragma annotation "grouping" "anisotropy/anisotropy;"
#pragma annotation "grouping" "anisotropy/anisotropy_rotation;"

#pragma annotation "grouping" "brdf/brdf_fresnel;"
#pragma annotation "grouping" "brdf/brdf_0_degree_refl;"
#pragma annotation "grouping" "brdf/brdf_90_degree_refl;"
#pragma annotation "grouping" "brdf/brdf_curve;"
#pragma annotation "grouping" "brdf/brdf_conserve_energy;"

#pragma annotation "grouping" "optimization/single_env_sample;"

