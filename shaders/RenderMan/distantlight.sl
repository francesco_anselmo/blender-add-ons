light
distantlight( 
	float intensity = 1;
	color lightcolor = 1; 
	point from = point "shader" (0,0,0);
	point to = point "shader" (0,0,1);
	float __nondiffuse=0, __nonspecular=0;
	string __category = "" )
{
	solar(to - from, 0.0)
		Cl = intensity * lightcolor;
}
