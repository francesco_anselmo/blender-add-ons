surface rib_constant(output varying normal aov_N = 0;
                     output varying point aov_P = 0;)
{
  /* AOVs */
  normal Nn = normalize(N);
	aov_N = transform("current", "world", Nn);
	aov_P = transform("current", "world", P);

	Oi = Os;
	Ci = Cs;

	Ci *= Oi;
}
