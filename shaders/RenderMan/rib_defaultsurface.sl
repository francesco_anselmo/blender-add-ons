surface rib_defaultsurface(float Kd=.8, Ka=.2;
                           string texturename = "";
                           output varying normal aov_N = 0;
                           output varying point aov_P = 0;)
{
  normal Nn = normalize(N);
  aov_N = transform("current", "world", Nn);
  aov_P = transform("current", "world", P);
  float diffuse = I.N;
  diffuse = (diffuse*diffuse) / (I.I * N.N);

  color texturecolor = ( texturename != "" ) ? texture( texturename, mod(s, 1.0), mod(t, 1.0) ) : 1;

  Ci = Cs * ( Ka + Kd*diffuse ) * texturecolor;
  Oi = Os;

  Ci *= Oi;
}
