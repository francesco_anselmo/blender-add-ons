/* about __category, __nondiffuse, and __nonspecular see link below */
/* file:///usr/people/jan/bin/3delight-10.0.50/Linux-x86_64/doc/html/3delight_34.html */

light
rib_blender_pointlight(float intensity = 1;
                       color lightcolor = 1;
                       point from = point "shader" (0,0,0);
                       float decay = 2.0;
                       float __nondiffuse = 0;
                       float __nonspecular = 0;
                       string __category = "")
{
	illuminate( from )
	{
		float distance_squared = L.L;

		if( decay == 2.0 )
      {
        Cl = intensity * lightcolor / distance_squared;
      }
		else
      {
        Cl = intensity * lightcolor / pow(distance_squared, 0.5 * decay ); 
      }
	}
}
