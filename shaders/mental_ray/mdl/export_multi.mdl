
mdl 1.0;
import df::*;
import math::*;

export material matte(
	color tint = color(0.8),
	uniform float roughness = 0.1
) = material (
	surface: material_surface(
		scattering: df::diffuse_reflection_bsdf(
			roughness: roughness,
			tint: tint)
	)
);

export material mirror(
	color tint = color(0.9, 0.9, 0.9),
	uniform float ior = 2.4
) = material (
	surface: material_surface(
		scattering: df::fresnel_layer(
			layer: df::specular_bsdf(
				mode: df::scatter_reflect,
				tint: tint),
			ior: ior
		)
	)
);

export material glass(
	color tint = color(0.9),
	uniform float ior = 1.5,
	uniform float distance_scale = 1
) = material(
	ior: color(ior),
    surface: material_surface(
        scattering: df::specular_bsdf (
            tint: color (1.0),
			mode: df::scatter_reflect_transmit
        )
    ),
	volume:  material_volume(
		absorption_coefficient: (distance_scale <= 0) ? color(0) : math::log(tint) / -distance_scale
	)
);

export material metal(
	color tint = color(0.9),
	uniform float roughness = 0.3,
	uniform float ior = 2.4
) = material(
	surface: material_surface(
		scattering: df::fresnel_layer(
			layer: df::simple_glossy_bsdf(
				mode: df::scatter_reflect,
				roughness_u: roughness,
				roughness_v: roughness,
				tint: tint),
			ior: ior
		)
	)
);

export material plastic(
	color tint = color(0.8),
	uniform float roughness_glossy = 0.2,
	uniform float roughness_diffuse = 0.1,
	uniform float ior = 1.46
) = material(
	surface: material_surface(
		scattering: df::fresnel_layer(
			layer: df::specular_bsdf(
				mode: df::scatter_reflect,
				tint: color(0.9)),
			base: df::fresnel_layer(
				layer: df::simple_glossy_bsdf(
					mode: df::scatter_reflect,
					roughness_u: roughness_glossy,
					roughness_v: roughness_glossy,
					tint: color(0.9)),
				base: df::diffuse_reflection_bsdf(
					roughness: roughness_diffuse,
					tint: tint),
				ior: ior
			),
			ior: ior
		)
	)
);

export material diffuse_light(
	color tint = color(1.0),
	float intensity = 1.0,
	float exposure = 0.0
) = material(
	surface : material_surface(
		emission : material_emission(
			emission : df::diffuse_edf(),
			intensity : tint * intensity * math::pow(2.0, exposure)
		)
	)
);

