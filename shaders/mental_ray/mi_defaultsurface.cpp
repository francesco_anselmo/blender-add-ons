#include "shader.h"

#define WHITE(C) ((C).r==1 && (C).g==1 && (C).b==1)

typedef struct {
  miColor		Color;
  miColor		Opacity;
  miScalar	Kd;
  miScalar	Ka;
  miColor		texture;
} mi_defaultsurface_t;

extern "C" DLLEXPORT
int
mi_defaultsurface_version(void) {
  return 1;
}

extern "C" DLLEXPORT
miBoolean
mi_defaultsurface_init(miState* state,
                       void* params,
                       miBoolean* instance_init_required) {
  // see named_framebuffer_put.cpp
  size_t count = 0;

  mi::shader::Access_fb framebuffers(state->camera->buffertag);
  framebuffers->get_buffercount(count);
  for (unsigned int i = 0; i < count; i++) {
    const char *name = NULL;
    if (framebuffers->get_buffername(i, name)) {
      size_t index;
      if (framebuffers->get_index(name, index)) {
        mi_info("  framebuffer '%s' has index %i", name, (int) index);
      }
      else  {
        mi_info("  framebuffer '%s' has no index", name);
      }
    }
  }

  return miTRUE;
}

extern "C" DLLEXPORT
miBoolean
mi_defaultsurface(miColor* result,
                  miState* state,
                  mi_defaultsurface_t* param) {
  char normals_fb[] = "aov_N";
  char points_fb[] = "aov_P";
  mi::shader::Access_fb framebuffers(state->camera->buffertag);
  size_t buffer_index;
  miVector normal;
  miVector point;

  // normals
  if (framebuffers->get_index(normals_fb, buffer_index)) {
    if (state->type == miRAY_EYE) {
      mi_normal_to_world(state, &normal, &state->normal);
      mi_vector_normalize(&normal);
      // compensate for the way the .mi files rotate the scene
      // (in rotated_root_grp_inst)
      miScalar tmp = normal.z;
      normal.z = normal.y;
      normal.y = -tmp;
      mi_fb_put(state, buffer_index, &normal);
    }
  }
  // points
  if (framebuffers->get_index(points_fb, buffer_index)) {
    if (state->type == miRAY_EYE) {
      mi_point_to_world(state, &point, &state->point);
      // compensate for the way the .mi files rotate the scene
      // (in rotated_root_grp_inst)
      miScalar tmp = point.z;
      point.z = point.y;
      point.y = -tmp;
      mi_fb_put(state, buffer_index, &point);
    }
  }
  if (state->type == miRAY_EYE || state->type == miRAY_TRANSPARENT) {
    // I
    miVector I;
    mi_vector_sub(&I, &state->point, &state->org);
    // I.N
    miScalar diffuse = mi_vector_dot(&I, &state->normal);
    // diffuse = (diffuse*diffuse) / (I.I * N.N)
    diffuse = ((diffuse * diffuse) /
               (mi_vector_dot(&I, &I) *
                mi_vector_dot(&state->normal, &state->normal)));

    miScalar Ka = *mi_eval_scalar(&param->Ka);
    miScalar Kd = *mi_eval_scalar(&param->Kd);
    miColor Color = *mi_eval_color(&param->Color);
    miColor texture = *mi_eval_color(&param->texture);
    // Ci = Cs * ( Ka + Kd*diffuse ) * texturecolor
    result->r = (Ka + Kd * diffuse) * Color.r * texture.r;
    result->g = (Ka + Kd * diffuse) * Color.g * texture.g;
    result->b = (Ka + Kd * diffuse) * Color.b * texture.b;
    // Oi = Os
    miColor Opacity = *mi_eval_color(&param->Opacity);
    if (WHITE(Opacity)) {
      result->a = 1.0f;
    } else {
      result->a = (Opacity.r + Opacity.g + Opacity.b) / 3.0f;
      miColor trans;
      if (mi_trace_transparent(&trans, state)) {
        result->r = (Opacity.r * result->r +
                     (1.0f - Opacity.r) * trans.r);
        result->g = (Opacity.g * result->g +
                     (1.0f - Opacity.g) * trans.g);
        result->b = (Opacity.b * result->b +
                     (1.0f - Opacity.b) * trans.b);
        result->a = result->a + trans.a * (1.0f - result->a);
      }
    }
  }
  return miTRUE;
}
